package com.smarket.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * Created by MI-062 on 31/7/18.
 */

@Dao
interface CMSDao {

    @Insert()
    fun setData(data: List<CMS>)

    @Query("SELECT * FROM CMS WHERE id = :id")
    fun getData(id: Long): CMS?

    @Query("SELECT *FROM CMS WHERE seoUrl = :seoUrl")
    fun getDataBySeoUrl(seoUrl:String):CMS?

    @Query("DELETE FROM CMS")
    fun deleteData()
}