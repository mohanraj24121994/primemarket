package com.smarket.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * Created by MI-062 on 31/7/18.
 */

@Dao
interface FAQDao {

    @Insert()
    fun setData(data: List<FAQ>)

    @Query("SELECT * FROM FAQ")
    fun getData(): List<FAQ>

    @Query("DELETE FROM FAQ")
    fun deleteData()
}