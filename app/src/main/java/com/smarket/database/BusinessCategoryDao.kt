package com.smarket.database

import androidx.room.*
import com.smarket.webservices.ApiParam

/**
 * Created by MI-062 on 31/7/18.
 */

@Dao
interface BusinessCategoryDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(data: List<BusinessCategory>)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(data: List<BusinessCategory>)

    @Query("DELETE FROM BusinessCategory WHERE status != ${ApiParam.STATUS_ACTIVE}")
    fun deleteInActiveData()

    @Query("SELECT * FROM BusinessCategory")
    fun getData(): List<BusinessCategory>

    @Query("DELETE FROM BusinessCategory")
    fun deleteData()

    @Query("SELECT name FROM BusinessCategory ORDER BY name")
    fun getNames(): List<String>

    @Query("SELECT id FROM BusinessCategory WHERE name = :name")
    fun getIdByName(name: String): Long

    @Query("SELECT name FROM BusinessCategory WHERE id = :id")
    fun getNameById(id: Long): String
}