package com.smarket.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 31/7/18.
 */

@Entity
class CMS {

    @PrimaryKey
    var id = 0L

    @SerializedName("cms_desc")
    var content = ""

    @SerializedName("seo_url")
    var seoUrl = ""

    @SerializedName("meta_keyword")
    var metaKeyword = ""

    var title = ""
}