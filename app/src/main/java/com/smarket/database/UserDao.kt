package com.smarket.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.smarket.database.User

/**
 * Created by MI-062 on 31/7/18.
 */

@Dao
interface UserDao {

    @Insert()
    fun setData(data: User)

    @Query("SELECT * FROM User")
    fun getData(): User?

    @Query("UPDATE User SET refcash = :refcash")
    fun updateRefCash(refcash: Double)

    @Update
    fun updateData(data: User)

    @Query("DELETE FROM User")
    fun deleteData()
}