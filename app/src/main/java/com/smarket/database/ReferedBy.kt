package com.smarket.database

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReferedBy() : Parcelable {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("google_id")
    @Expose
    var googleId: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null

    @SerializedName("mobile_number")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("profile_pic")
    @Expose
    var profilePic: String? = null

    @SerializedName("otp")
    @Expose
    var otp: Int? = null

    @SerializedName("verify_code")
    @Expose
    var verifyCode: String? = null

    @SerializedName("reset_token")
    @Expose
    var resetToken: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("push_notification")
    @Expose
    var pushNotification: String? = null

    @SerializedName("refcash")
    @Expose
    var refcash: Int? = null

    @SerializedName("parter_id")
    @Expose
    var parterId: Int? = null

    @SerializedName("parter_percent")
    @Expose
    var parterPercent: Int? = null

    @SerializedName("status_id")
    @Expose
    var statusId: Int? = null

    @SerializedName("stripe_cus_id")
    @Expose
    var stripeCusId: String? = null

    @SerializedName("referenced_by")
    @Expose
    var referencedBy: Int? = null

    @SerializedName("referral_code")
    @Expose
    var referralCode: String? = null

    @SerializedName("deleted_at")
    @Expose
    var deletedAt: Any? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

    override fun writeToParcel(dest: Parcel? , flags: Int) {
        dest?.writeInt(id !!)
    }

    override fun describeContents(): Int {
        return 0
    }

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
    }

    companion object CREATOR : Parcelable.Creator<ReferedBy?> {
        override fun createFromParcel(parcel: Parcel): ReferedBy? {
            return ReferedBy(parcel)
        }

        override fun newArray(size: Int): Array<ReferedBy?> {
            return arrayOfNulls<ReferedBy>(size)
        }
    }
}