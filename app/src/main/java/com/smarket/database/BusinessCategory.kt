package com.smarket.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.smarket.SMarket

/**
 * Created by MI-062 on 11/7/18.
 */
@Entity
class BusinessCategory {

    @PrimaryKey
    var id = 0L

    var name = ""

    var status = 0

    companion object {

        fun setData(data: List<BusinessCategory>) {
            SMarket.appDB.businessCategoryDao().insert(data)
            SMarket.appDB.businessCategoryDao().update(data)
        }
    }
}