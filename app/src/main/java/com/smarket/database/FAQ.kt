package com.smarket.database

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by MI-062 on 11/7/18.
 */
@Entity
class FAQ {

    @PrimaryKey
    var id = 0L

    var question = ""

    var answer = ""

    var isChecked = false
}