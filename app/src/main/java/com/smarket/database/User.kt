package com.smarket.database

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.smarket.SMarket


/**
 * Created by MI-062 on 31/7/18.
 */

@Entity
class User() : Parcelable {
    @Ignore
    var google_id = ""

    var token = ""

    @PrimaryKey
    var id = 0L

    @SerializedName("login_user")
    var login_user = ""

    @SerializedName("login_type")
    var loginType = ""

    @SerializedName("user_type")
    var userType = ""

    @SerializedName("business_logo")
    var businessLogo = ""

    @SerializedName("profile_pic")
    var profilePic = ""

    @SerializedName("business_name")
    var businessName = ""

    @SerializedName("tag_line")
    var tagLine = ""

    var name = ""

    var email = ""

    @SerializedName("country_code")
    var countryCode = ""

    var mobile = ""

    var website = ""

    @SerializedName("business_category_id")
    var businessCategoryId = 0L

    var address = ""

    var latitude = 0.0

    var longitude = 0.0

    @SerializedName("post_code")
    var postCode = ""

    var description = ""

    @SerializedName("product_and_services")
    var productAndServices = ""

    var refcash = 0.0

    @SerializedName("min_withdraw_amount")
    var minWithdrawAmount = 0.0

    @SerializedName("min_withdraw_amount_outside")
    var min_withdraw_amount_outside = 0.0

    @SerializedName("refferal_alert")
    var refferalAlert = 0

    @SerializedName("awaiting_rewards")
    var awaitingRewards = 0

    @SerializedName("store_credit")
    var storeCredit = 0.0

    var notification = 0

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    @SerializedName("notification_status")
    var notificationStatus = 0L

    @SerializedName("referral_code")
    var referral_code = ""

    @SerializedName("referral_message")
    var referral_message = ""

    @SerializedName("refered_person")
    var refered_person_mer = ""

    @SerializedName("refered_person_id")
    var refered_person_id = ""

    @SerializedName("refered_business_name")
    var refered_business_name = ""

    /* @SerializedName("refered_by")
     var refered_by: ReferedBy? = null

     var rrr : ReferedBy? = null*/

    /* @SerializedName("refered_by")
     var refered_by = ReferedBy()*/

    constructor(parcel: Parcel) : this() {
        google_id = parcel.readString() !!
        token = parcel.readString() !!
        id = parcel.readLong()
        login_user = parcel.readString() ?: ""
        loginType = parcel.readString() !!
        userType = parcel.readString() !!
        businessLogo = parcel.readString() !!
        profilePic = parcel.readString() !!
        businessName = parcel.readString() !!
        tagLine = parcel.readString() !!
        name = parcel.readString() !!
        email = parcel.readString() !!
        countryCode = parcel.readString() !!
        mobile = parcel.readString() !!
        website = parcel.readString() !!
        businessCategoryId = parcel.readLong()
        address = parcel.readString() !!
        latitude = parcel.readDouble()
        longitude = parcel.readDouble()
        postCode = parcel.readString() !!
        description = parcel.readString() !!
        productAndServices = parcel.readString() !!
        refcash = parcel.readDouble()
        minWithdrawAmount = parcel.readDouble()
        min_withdraw_amount_outside = parcel.readDouble()
        refferalAlert = parcel.readInt()
        awaitingRewards = parcel.readInt()
        storeCredit = parcel.readDouble()
        notification = parcel.readInt()
        averageRating = parcel.readFloat()
        noOfRating = parcel.readLong()
        notificationStatus = parcel.readLong()
        referral_code = parcel.readString() !!
        referral_message = parcel.readString() !!
        refered_person_mer = parcel.readString() !!
        // refered_person_id = parcel.readDouble()

        //parcel.unmarshall(bytes, 0, bytes.length);
        //parcel.setDataPosition(0);
        // parcel.readTypedList(refered_by !! , ReferedBy.CREATOR)

        // parcel.readParcelable(ReferedBy.CREATOR)

        // refered_by = parcel.readParcelable(ReferedBy::class.java.classLoader) !!
    }

    override fun writeToParcel(parcel: Parcel , flags: Int) {
        parcel.writeString(google_id)
        parcel.writeString(token)
        parcel.writeLong(id)
        parcel.writeString(login_user)
        parcel.writeString(loginType)
        parcel.writeString(userType)
        parcel.writeString(businessLogo)
        parcel.writeString(profilePic)
        parcel.writeString(businessName)
        parcel.writeString(tagLine)
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(countryCode)
        parcel.writeString(mobile)
        parcel.writeString(website)
        parcel.writeLong(businessCategoryId)
        parcel.writeString(address)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(postCode)
        parcel.writeString(description)
        parcel.writeString(productAndServices)
        parcel.writeDouble(refcash)
        parcel.writeDouble(minWithdrawAmount)
        parcel.writeDouble(min_withdraw_amount_outside)
        parcel.writeInt(refferalAlert)
        parcel.writeInt(awaitingRewards)
        parcel.writeDouble(storeCredit)
        parcel.writeInt(notification)
        parcel.writeFloat(averageRating)
        parcel.writeLong(noOfRating)
        parcel.writeLong(notificationStatus)
        parcel.writeString(referral_code)
        parcel.writeString(referral_message)
        parcel.writeString(refered_person_mer)
        // parcel.writeDouble(refered_person_id)

        // parcel.writeParcelable(refered_by,flags)
        //   parcel.writeString(refered_by.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }

        fun getLoginUserId(): Long {
            return SMarket.appDB.userDao().getData() !!.id
        }
    }

}