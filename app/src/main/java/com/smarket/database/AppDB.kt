package com.smarket.database

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Created by MI-062 on 31/7/18.
 */

@Database(entities = [(CMS::class), (BusinessCategory::class), (FAQ::class), (User::class)]
        , version = 1)
abstract class AppDB : RoomDatabase() {

    abstract fun cmsDao(): CMSDao

    abstract fun businessCategoryDao(): BusinessCategoryDao

    abstract fun faqDao(): FAQDao

    abstract fun userDao(): UserDao
}