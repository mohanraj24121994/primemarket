package com.smarket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.lrf.BaseLRFFragment
import com.smarket.main.BaseMainFragment
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 11/4/18.
 */
class DemoFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): DemoFragment {
            val fragment = DemoFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_lrf, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = resources.getString(R.string.app_name)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            else -> {
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }
}