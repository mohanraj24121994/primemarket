package utils

/**
 * Created by MI-062 on 22/1/18.
 */

enum class AnimationType {

    BottomInBottomOut,
    FadeInFadeOut,
    ZoomInZoomOut,
    RightInZoomOut,
    LeftInZoomOut,
    RightInRightOut,
    LeftInLeftOut,
    None
}