package com.smarket.utils

import android.app.DatePickerDialog
import android.content.Context
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*
import android.net.ParseException
import android.util.Log

/**
 * Created by MI-062 on 18/4/18.
 */

const val APP_DATE_FORMAT = "dd MMM, yyyy"
const val APP_DATE_TIME_FORMAT = "dd MMM, yyyy hh:mm aa"
const val DATE_FORMAT = "yyyy-MM-dd"
const val DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss"

fun Context.showDatePicker(tv_date: TextView, dateFormat: String = APP_DATE_FORMAT) {

    val cal = Calendar.getInstance()
    cal.timeInMillis = System.currentTimeMillis()

    if (tv_date.text.toString().isRequiredField()) {

        val dateMillis = getMillisFromDate(tv_date.text.toString(), dateFormat)

        if (dateMillis != 0L) {
            cal.timeInMillis = dateMillis
        }
    }

    val currentYear = cal.get(Calendar.YEAR)
    val currentMonth = cal.get(Calendar.MONTH)
    val currentDay = cal.get(Calendar.DAY_OF_MONTH)

    val datePickerDialog = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                cal.set(year, monthOfYear, dayOfMonth)

                val mDate = getDateFromMillis("" + cal.timeInMillis, dateFormat)

                tv_date.text = mDate

            }, currentYear, currentMonth, currentDay)

    datePickerDialog.datePicker.minDate = System.currentTimeMillis()

    datePickerDialog.show()
}

fun getMillisFromDate(str_date: String, dateFormat: String): Long {
    return try {
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        val date = formatter.parse(str_date)
        date.time
    } catch (e: Exception) {
        e.printStackTrace()
        0
    }
}

fun getDateFromMillis(mDateInMillis: String?, mDateFormat: String): String {

    var miliis: Long = 0

    if (mDateInMillis != null && !mDateInMillis.isEmpty()) {
        try {
            miliis = java.lang.Long.parseLong(mDateInMillis)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    val formatter = SimpleDateFormat(mDateFormat, Locale.getDefault())

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = miliis

    return formatter.format(calendar.time)
}

fun getChangedDateFormat(mDate: String, mOldDateFormat: String, newDateFormat: String): String {
    val inputFormat = SimpleDateFormat(mOldDateFormat, Locale.getDefault())
    val outputFormat = SimpleDateFormat(newDateFormat, Locale.getDefault())

    var date: Date? = null
    var str: String? = null

    try {
        date = inputFormat.parse(mDate)
        str = outputFormat.format(date)

        return str

    } catch (e: ParseException) {
        e.printStackTrace()
    } catch (e: java.text.ParseException) {
        e.printStackTrace()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return ""
}

fun getTimePeriod(timestamp: Long): String {
    val currentTimestamp = System.currentTimeMillis()
    val addedTimestamp = timestamp * 1000
    var difference = Math.abs(currentTimestamp - addedTimestamp)

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24
    val weeksInMilli = daysInMilli * 7
    val monthsInMilli = daysInMilli * 30
    val yearsInMilli = monthsInMilli * 12

    val elapsedYears = difference / yearsInMilli
    difference = difference % yearsInMilli

    val elapsedMonths = difference / monthsInMilli
    difference = difference % monthsInMilli

    val elapsedDays = difference / daysInMilli
    difference = difference % daysInMilli

    val elapsedHours = difference / hoursInMilli
    difference = difference % hoursInMilli

    val elapsedMinutes = difference / minutesInMilli
    difference = difference % minutesInMilli

    val elapsedSeconds = difference / secondsInMilli
    difference = difference % secondsInMilli

    if (elapsedYears != 0L) {
        var years = ""
        if (elapsedYears > 1) {
            years = elapsedYears.toString() + " years ago"
        } else {
            years = elapsedYears.toString() + " year ago"
        }
        return years
    } else if (elapsedMonths != 0L) {
        var months = ""
        if (elapsedMonths > 1) {
            months = elapsedMonths.toString() + " months ago"
        } else {
            months = elapsedMonths.toString() + " month ago"
        }
        return months
    } else if (elapsedDays != 0L) {
        var days = ""
        if (elapsedDays > 1) {
            days = elapsedDays.toString() + " days ago"
        } else {
            days = elapsedDays.toString() + " day ago"
        }
        return days
    } else if (elapsedDays == 0L) {
        return getDateCurrentTimeZone(timestamp, " hh:mm a")
    }
    return ""
}

fun getDateCurrentTimeZone(timestamp: Long, dateFormat: String): String {
    try {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp * 1000
        val df = java.util.Date(timestamp * 1000)
        return SimpleDateFormat(dateFormat).format(df)
    } catch (e: Exception) {
    }

    return ""
}

fun getLocalTimeFromUTC(oldDate: String): String {

    if (!oldDate.isRequiredField()) {
        return ""
    }

    val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    val outPutForamt = SimpleDateFormat("dd MMM, yyyy hh:mm aa")
    dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
    var date: Date? = null
    var localDate = ""
    try {
        date = dateFormatter.parse(oldDate)

        outPutForamt.timeZone = TimeZone.getDefault()
        localDate = outPutForamt.format(date)

    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return localDate
}
