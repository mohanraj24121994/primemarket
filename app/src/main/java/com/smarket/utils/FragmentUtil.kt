package com.smarket.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.appcompat.app.AppCompatActivity
import com.smarket.R
import com.smarket.main.MainActivity
import utils.AnimationType
import java.util.ArrayList

/**
 * Created by MI-062 on 20/1/18.
 */

const val fragmentContainerId = R.id.fl_container

fun androidx.fragment.app.FragmentManager.inTransaction(func: androidx.fragment.app.FragmentTransaction.() -> androidx.fragment.app.FragmentTransaction) {
    beginTransaction().func().commit()
}

fun AppCompatActivity.pushFragment(fragment: androidx.fragment.app.Fragment , addToBackStack: Boolean = false , ignoreIfCurrent: Boolean ,
                                   justAdd: Boolean = true , animationType: AnimationType?) {

    val currentFragment = supportFragmentManager.findFragmentById(fragmentContainerId)

    if (ignoreIfCurrent && currentFragment != null) {
        if (fragment.javaClass.canonicalName.equals(currentFragment.tag)) {
            return
        }
    }

    supportFragmentManager.inTransaction {

        when (animationType) {
            AnimationType.BottomInBottomOut ->
                setCustomAnimations(R.anim.bottom_in, R.anim.zoom_out, R.anim.zoom_in, R.anim.bottom_out)
            AnimationType.FadeInFadeOut ->
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            AnimationType.ZoomInZoomOut ->
                setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out, R.anim.zoom_in, R.anim.zoom_out)
            AnimationType.RightInZoomOut ->
                setCustomAnimations(R.anim.right_in, R.anim.zoom_out, R.anim.zoom_in, R.anim.right_out)
            AnimationType.LeftInZoomOut ->
                setCustomAnimations(R.anim.left_in, R.anim.zoom_out, R.anim.zoom_in, R.anim.left_out)
            AnimationType.RightInRightOut ->
                setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out)
            AnimationType.LeftInLeftOut ->
                setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out)
            AnimationType.None -> {
                // Do Nothing
            }
        }

        if (currentFragment != null) {
            hide(currentFragment)
        }

        if (addToBackStack) {
            addToBackStack(fragment.javaClass.canonicalName)
        } else {
            if (currentFragment != null) {
                remove(currentFragment)
            }
        }

        hideKeyboard()

        if (justAdd) {
            add(fragmentContainerId, fragment, fragment.javaClass.canonicalName)
        } else {
            replace(fragmentContainerId, fragment, fragment.javaClass.canonicalName)
        }
    }
}

fun AppCompatActivity.addFragment(fragment: androidx.fragment.app.Fragment , addToBackStack: Boolean = false , ignoreIfCurrent: Boolean = true ,
                                  animationType: AnimationType = AnimationType.None) {
    pushFragment(fragment, addToBackStack, ignoreIfCurrent, animationType = animationType)
}

fun AppCompatActivity.replaceFragment(fragment: androidx.fragment.app.Fragment , ignoreIfCurrent: Boolean = true ,
                                      animationType: AnimationType = AnimationType.None) {
    pushFragment(fragment, ignoreIfCurrent = ignoreIfCurrent, justAdd = false, animationType = animationType)
}

fun AppCompatActivity.clearBackStack() {
    supportFragmentManager.popBackStackImmediate(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    val fragmentList = supportFragmentManager.fragments
    if (fragmentList != null && !fragmentList.isEmpty()) {
        fragmentList.filterNotNull().forEach {
            supportFragmentManager.inTransaction {
                remove(it)
            }
        }
    }
}

fun AppCompatActivity.popBackStack(tag: String?) {
    try {
        hideKeyboard()
        supportFragmentManager.popBackStack(tag, if (tag == null) {
            androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE} else {0})
//        supportFragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun AppCompatActivity.getCurrentFragment(): androidx.fragment.app.Fragment {
    // Find current visible fragment
    return supportFragmentManager.findFragmentById(R.id.fl_container)!!
}


fun getAllFragments(mActivity: MainActivity): ArrayList<androidx.fragment.app.Fragment> {
    val lista = ArrayList<androidx.fragment.app.Fragment>()
    for (fragment in mActivity.supportFragmentManager.fragments) {
        try {
            fragment.getTag()
            lista.add(fragment)
        } catch (e: NullPointerException) {

        }
    }
    return lista
}