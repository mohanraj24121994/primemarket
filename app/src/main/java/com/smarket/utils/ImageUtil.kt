package com.smarket.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.backends.pipeline.PipelineDraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.smarket.R
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by MI-062 on 11/4/18.
 */

val userImage = "https://i.amz.mshcdn.com/flSse3TR6HJfHM-qFXbG8018Zuo=/950x534/filters:quality(90)/2013%2F09%2F17%2Fad%2FMarkyZ.e407c.jpg"
val businessLogo = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyx-nurZ3l96r_BIx2uk-MovTVdKZSjSPupN_NL8iVZDbbf5BXPQ"
val offer1 = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-htDDSEGt0IeOhXfQ3TspY54DrZnDNnRO8k0AQ_aq4jeU6DRlIg"
val offer2 = "https://media.istockphoto.com/vectors/summer-background-with-text-on-sand-happy-summer-holidays-vector-id533069794"
val offer3 = "https://cdn4.vectorstock.com/i/1000x1000/83/38/chips-ads-pack-explosion-crab-flavour-background-vector-20188338.jpg"
val offerImage = "https://5.imimg.com/data5/RP/OV/MY-24440516/shirts-500x500.jpg"

private var request: ImageRequest? = null

/**
 *  Used to load fresco image from url.
 */
fun SimpleDraweeView.loadFrescoImage(imagePath: String, aspectRatio: Int = 1) {

    val uri = Uri.parse(imagePath)
    val imageSize = context.getScreenWidth() / aspectRatio

    request = ImageRequestBuilder.newBuilderWithSource(uri)
            .setResizeOptions(ResizeOptions(imageSize, imageSize))

            .build()
    controller = Fresco.newDraweeControllerBuilder()
            .setOldController(this@loadFrescoImage.controller)
            .setImageRequest(request)
//            .setAutoPlayAnimations(true)
            .build() as PipelineDraweeController

    this@loadFrescoImage.controller = controller
}

/**
 *  Used to load fresco image from file.
 */
fun SimpleDraweeView.loadFrescoImageFromFile(file: File?, aspectRatio: Int = 1) {

    try {
        if (file != null && file.exists()) {
            val uri = Uri.fromFile(file)
            val imageSize = context.getScreenWidth() / aspectRatio

            request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setResizeOptions(ResizeOptions(imageSize, imageSize))
                    .build()
            controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(this.controller)
                    .setImageRequest(request)
                    .build() as PipelineDraweeController
            this.controller = controller
        } else {
            request = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.ic_launcher)
                    .build()


            controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(this.controller)
                    .setImageRequest(request)
                    .build() as PipelineDraweeController
            this.controller = controller
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

/**
 *  Used to load fresco image from resource.
 */
fun SimpleDraweeView.loadFrescoImageFromResource(context: Context, resourceId: Int, aspectRatio: Int = 1) {
    var viewAspectRatio: Int = 1
    if (aspectRatio == 0) {
        viewAspectRatio = 1
    }
    try {
        if (resourceId != 0) {
            val imageSize = context.getScreenWidth() / viewAspectRatio

            request = ImageRequestBuilder.newBuilderWithResourceId(resourceId)
                    //                        .setResizeOptions(new ResizeOptions(imageSize, imageSize))
                    .build()
            controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(this.controller)
                    .setImageRequest(request)
                    .build() as PipelineDraweeController
            this.controller = controller
        } else run {

            request = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.ic_launcher)
                    .build()

            controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(this.controller)
                    .setImageRequest(request)
                    .build() as PipelineDraweeController
            this.controller = controller
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun getOutputMediaFile(mActivity: AppCompatActivity): File {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.
    // File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Pennyer/" + "/Images");
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.
    // Create the storage directory if it does not exist

    val wallpaperDirectory = File(mActivity.cacheDir.toString() + "/SMarket/" + "/Images")
    if (!wallpaperDirectory.exists()) {
        wallpaperDirectory.mkdirs()
    }

    // Create a media file name
    val timeStamp = SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(Date())
    val mediaFile: File
    // UserDetailTable userDetailTable = UserDetailTable.getUserInfo();
    val mImageName = "Profile_pic$timeStamp.jpg"
    mediaFile = File(wallpaperDirectory.path + File.separator + mImageName)
    return mediaFile
}

fun storeImageToStorage(image: Bitmap, mActivity: AppCompatActivity): String {
    val pictureFile = getOutputMediaFile(mActivity)
    val filename = File(pictureFile.toString()).name
    val file = File(File(mActivity.cacheDir.toString() + "/SMarket/" + "/Images", filename).toString())
    if (file.exists()) {
        file.delete()
    }
    return try {
        val fos: FileOutputStream? = FileOutputStream(pictureFile)
        image.compress(Bitmap.CompressFormat.JPEG, 90, fos)
        if (fos != null) {
            fos.flush()
            fos.close()
        }
        pictureFile.absolutePath
    } catch (e: FileNotFoundException) {
        e.printStackTrace()
        ""
    } catch (e: IOException) {
        e.printStackTrace()
        ""
    }
}

class DownloadImage : AsyncTask<String, Void, Bitmap>() {

    override fun onPreExecute() {
        super.onPreExecute()
    }

    override fun doInBackground(vararg URL: String): Bitmap? {
        var imageBitmap: Bitmap? = null
        val imageURL = URL[0]
        try {
            // Download Image from URL
            val input = java.net.URL(imageURL).openStream()
            imageBitmap = BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return imageBitmap
    }

    override fun onPostExecute(result: Bitmap?) {
        return super.onPostExecute(result)
    }
}