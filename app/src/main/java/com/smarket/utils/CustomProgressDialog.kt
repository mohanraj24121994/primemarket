package com.smarket.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.widget.TextView
import com.smarket.R


class CustomProgressDialog {

    lateinit var dialog: CustomDialog

    fun show(context: Context): Dialog {
        val inflater = (context as Activity).layoutInflater
        val view = inflater.inflate(R.layout.progressbar_referral, null)

        val tvMessage = view .findViewById<TextView>(R.id.tv_comparing)
        val array = intArrayOf(R.string.fetching_deal , R.string.comparing_deal)
        tvMessage.post(object : Runnable {
            var i = 0
            override fun run() {
                tvMessage.setText(array[i])
                i ++
                if (i == 2) i = 0
                tvMessage.postDelayed(this , 3000)
            }
        })

        dialog = CustomDialog(context)
        dialog.setContentView(view)
        dialog.show()
        return dialog
    }

    class CustomDialog(context: Context) : Dialog(context, R.style.CustomDialogTheme) {
        init {
            window?.decorView?.setOnApplyWindowInsetsListener { _, insets ->
                insets.consumeSystemWindowInsets()
            }
        }
    }
}