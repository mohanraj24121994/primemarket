package com.smarket.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.smarket.R
import com.smarket.SMarket
import java.net.URLEncoder

/**
 * Created by MI-062 on 29/6/18.
 */

val MAP_KEY = SMarket.appContext.resources.getString(R.string.google_key)
const val CURRENT_MARKER = "https://lh5.googleusercontent.com/MeINHqClSF9NaIbw2KIaZ-YBmqPI0tiLySxkawLb_jf50cNEyf8WYiciwJ03d4t1S6xH6LjxTTaxyqNtvvQe=w1533-h807"

private val STATIC_API = "https://maps.googleapis.com/maps/api/staticmap?"
val MARKER_ANCHOR = "center"
val ZOOM = "16"  //TO SHOW STREET VIEW

val PICTURE_SIZE = "675x450"
var CENTER_LATITUDE = "23.056784"
var CENTER_LONGITUDE = "72.540579"
val LATITUDE = "23.057471"
val LONGITUDE = "72.534370"
var DEST_LATITUDE = "23.039784"
var DEST_LONGITUDE = "72.577014"
val LAT = "23.058126"
val LONG = "72.538240"
val PATH_COLOR = "0x10d6fd"
val MARKER_ANCHOR1 = "center"
val WEIGHT = "5"  //PATH THICKNESS

val MAP_CUSTOM_STYLE = "style=element:labels.icon|visibility:off" +
        "&style=element:labels.text.fill|color:0x4A4A4A" + // text color
        "&style=element:labels.text.stroke|visibility:off" +
        "&style=feature:administrative|element:geometry|visibility:off" +
        "&style=feature:administrative|element:labels.text.fill|color:0x4A4A4A" + // text color
        "&style=feature:administrative.province|visibility:off" +
        "&style=feature:landscape.man_made|element:geometry.fill|color:0x121212" + // buildings color
        "&style=feature:landscape.man_made|element:geometry.stroke|visibility:simplified" +
        "&style=feature:landscape.natural|color:0x121212" +
        "&style=feature:poi|element:geometry|color:0x121212|visibility:off" + // public properties color
        "&style=feature:poi|element:geometry.fill|color:0x121212" + // public properties color
        "&style=feature:poi|element:labels.text|visibility:off" +
        "&style=feature:poi|element:labels.text.fill|color:0x4A4A4A" + // text color
        "&style=feature:poi.attraction|visibility:off" +
        "&style=feature:poi.business|visibility:off" +
        "&style=feature:poi.government|visibility:off" +
        "&style=feature:poi.medical|visibility:off" +
        "&style=feature:poi.place_of_worship|visibility:off" +
        "&style=feature:poi.school|visibility:off" +
        "&style=feature:road|element:geometry|color:0x1A1A1A" + // road color
        "&style=feature:road|element:labels.icon|visibility:off" +
        "&style=feature:road|element:labels.text.fill|color:0x4A4A4A" + // text color
        "&style=feature:transit|visibility:off" +
        "&style=feature:transit|element:geometry|color:0x1A1A1A" + // road color
        "&style=feature:transit|element:labels.text.fill|color:0x4A4A4A" + // text color
        "&style=feature:water|element:geometry|color:0x121212" + // water color
        "&style=feature:water|element:labels.text.fill|color:0x4A4A4A" // text color

fun getStaticApi(viewWidth: Int, viewHeight: Int, latitude: String, longitude: String): String {

    val pictureSize = viewWidth.toString() + "x" + viewHeight.toString()

    return STATIC_API +
            "size=" + pictureSize +
            "&zoom=" + ZOOM +
            "&markers=anchor:" + MARKER_ANCHOR + "|icon:" + CURRENT_MARKER +
            "|" + latitude + "," + longitude +
            "&sensor=false" +
            "&key=" + MAP_KEY
}

fun navigateToGoogleMap(context: Context, latitude: Double, longitude: Double, address: String = "") {

    val uri: Uri = if (address.isRequiredField()) {
        Uri.parse("geo:$latitude,$longitude?q=${URLEncoder.encode(address, "utf-8")}")
    } else {
        Uri.parse("geo:$latitude,$longitude")
    }

    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = uri

    if (intent.resolveActivity(context.packageManager) == null) {
        intent.data = Uri.parse("https://www.google.com/maps/place/${URLEncoder.encode(address, "utf-8")}/@$latitude,$longitude")
    }

    context.startActivity(intent)
}