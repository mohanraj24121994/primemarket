package com.smarket.utils

import android.content.Context
import android.net.ConnectivityManager
import com.smarket.SMarket

/**
 * Created by MI-062 on 22/1/18.
 */

object NetworkUtil{

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = SMarket.appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }
}