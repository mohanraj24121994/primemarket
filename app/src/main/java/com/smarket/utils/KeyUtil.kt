package com.smarket.utils

/**
 * Created by MI-062 on 20/6/18.
 */

val USER_TYPE = "USER_TYPE"
val USER_TYPE_CUSTOMER = 1
val USER_TYPE_MERCHANT = 2

val REQUEST_CODE_GOOGLE_PLACE_API = 201

val IS_FROM = "IS_FROM"
val IS_FROM_DRAWER = "IS_FROM_DRAWER"
val IS_FROM_HOME = "IS_FROM_HOME"
val OFFER_TYPE = "OFFER_TYPE"

val LOAD_MORE = 501
val OTHER = 502

val OFFER_DETAILS = "OFFER_DETAILS"
val SUB_OFFER_DETAILS = "OFFER_DETAILS"
val MERCHANT_DETAILS = "MERCHANT_DETAILS"

val HISTORY_TYPE = "HISTORY_TYPE"

val MERCHANT_ID = "MERCHANT_ID"
val REFERRAL_ID = "REFERRAL_ID"
val REFERRAL_MESSAGE = "REFERRAL_MESSAGE"

val USER_SIGNUP_DETAILS = "USER_SIGNUP_DETAILS"

val GPS_STATUS = "GPS_STATUS"

val DEVICE_TOKEN = "DEVICE_TOKEN"
val COUPON_IMAGE = "COUPON_IMAGE"

