package com.smarket.utils

import android.content.Context
import com.google.gson.Gson
import com.smarket.model.CountryCode
import com.smarket.model.CountryData
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by MI-062 on 30/7/18.
 */

fun Context.loadJSONFromAsset(): String {

    var json = ""

    try {

        val `is` = assets.open("countries.json")

        val size = `is`.available()

        val buffer = ByteArray(size)

        `is`.read(buffer)

        `is`.close()

        json = String(buffer, Charset.defaultCharset())


    } catch (ex: IOException) {
        ex.printStackTrace()
        json = ""
    }

    return json
}

fun Context.getCountryCodeList(): List<String> {

    val countryData = Gson().fromJson(loadJSONFromAsset(), CountryData::class.java)

    return countryData.data.distinctBy { it.PhoneCode }.sortedBy { it.PhoneCode.toInt() }.map { "+" + it.PhoneCode }
}