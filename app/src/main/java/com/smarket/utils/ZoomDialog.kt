package com.smarket.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.utils.photoview.ImageDownloadListener
import kotlinx.android.synthetic.main.dialog_zoom.*


class ZoomDialog: Dialog {
    var  mImageUrl:String = ""
    constructor(context: Context,mImageUrl:String) : super(context) {
        this.mImageUrl = mImageUrl
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_zoom)

        val window = window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.CENTER
        window.attributes = wlp
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        this.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        photoView.setImageUri(mImageUrl)
        photoView.setZoomable(true)
        photoView.setImageDownloadListener(ImageDownloadListener {
            progressBar.visibility = View.GONE
        })
        iv_close.setOnClickListener(View.OnClickListener {
            cancel()
        })
    }

}