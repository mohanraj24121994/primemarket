package com.smarket.utils

interface OnCartTextChanged {
    fun onTextChanged(position: Int  , count: String?)
}