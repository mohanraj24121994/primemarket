package com.smarket.utils

import android.widget.Toast
import com.smarket.SMarket

/**
 * Created by MI-062 on 22/1/18.
 */

var toast: Toast? = null

fun showToast(message: String?) {
    if (toast != null) {
        toast!!.cancel()
    }
    toast = Toast.makeText(SMarket.appContext, message, Toast.LENGTH_SHORT)
    toast!!.show()
}