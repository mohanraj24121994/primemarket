package com.smarket.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.Uri
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.core.app.ShareCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.lrf.LRFActivity
import com.smarket.webservices.ApiUtil


/**
 * Created by MI-062 on 20/1/18.
 */

fun AppCompatActivity.hideKeyboard() {

    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    val view = currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun AppCompatActivity.showKeyboard(view: View) {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}

fun Context.convertDpToPixel(dp: Float): Float {
    val resources: Resources = resources
    val metrics: DisplayMetrics = resources.displayMetrics
    return dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
}

fun Context.convertSpToPixels(sp: Float): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.displayMetrics).toInt()
}

fun Context.getScreenWidth(): Int {
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val dm = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(dm)
    return dm.widthPixels
}

fun Context.getScreenHeight(): Int {
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val dm = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(dm)
    return dm.heightPixels
}

fun Context.sendMail(subject: String = "", extraText: String = "", emailId: String) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.parse("mailto:") // only email apps should handle this
    intent.putExtra(Intent.EXTRA_EMAIL, Array(1) { emailId })
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, extraText)
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(Intent.createChooser(intent, "Send mail using:"))
    }
}

fun Context.rateApp() {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse("market://details?id=${this.packageName}") // only play store app should handle this
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    } else {

    }
}

fun Context.makeCall(phoneNumber: String) {

    val testIntent = Intent(Intent.ACTION_VIEW)
    val data = Uri.parse("tel:$phoneNumber")
    testIntent.data = data
    this.startActivity(Intent.createChooser(testIntent, "Call to"))
}

fun Context.showWeb(webUrl: String) {
    var webUrl = webUrl

    if (!webUrl.startsWith("http://") && !webUrl.startsWith("https://")) {
        webUrl = "http://$webUrl"
    }

    val testIntent = Intent(Intent.ACTION_VIEW)
    val data = Uri.parse(webUrl)
    testIntent.data = data
    this.startActivity(Intent.createChooser(testIntent, "Browse"))
}

fun Context.share(activity: AppCompatActivity, message: String) {
    ShareCompat.IntentBuilder
            .from(activity)
            .setText(message)
            .setType("text/plain")
            .setChooserTitle("Share using:")
            .startChooser()
}

fun Context.getPrimaryColor(): Int {

    val typedValue = TypedValue()

    val a = this.obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorPrimary))
    val color = a.getColor(0, 0)

    a.recycle()

    return color
}

fun avoidDoubleClicks(view: View) {

    val DELAY_IN_MS: Long = 900

    if (!view.isClickable) {
        return
    }

    view.isClickable = false

    view.postDelayed({ view.isClickable = true }, DELAY_IN_MS)
}

fun appInstalledOrNot(uri: String, context: Context): Boolean {
    val pm = context.packageManager
    var app_installed = false
    try {
        pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
        app_installed = true
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }

    return app_installed
}

fun dpToPx(context: androidx.fragment.app.Fragment , i: Int): Float {
    val metrics = context.resources.displayMetrics;
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i.toFloat(), metrics);
}

fun logoutAndMoveToLrf(activity: AppCompatActivity) {
    val userDetails = SMarket.appDB.userDao().getData()
    if (PreferenceUtil.getIntPref(USER_TYPE) == USER_TYPE_CUSTOMER) {
        //removing token to stop Notifications , when he logged out as a customer
        ApiUtil.callAddRemoveDeviceTokenAPI(User.getLoginUserId(), PreferenceUtil.getStringPref(DEVICE_TOKEN), "remove")
        clearNotifications(activity)//To clear the status bar notifications when he logged out as customer.
        PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_CUSTOMER, "${userDetails?.countryCode},${userDetails?.mobile}")
    } else {
        PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_MERCHANT, userDetails?.email)
    }

    Handler().postDelayed({
        SMarket.appDB.userDao().deleteData()
    }, 300)

    val intent = Intent(activity, LRFActivity::class.java)
    activity.startActivity(intent)
    activity.overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out)
    activity.finish()
}