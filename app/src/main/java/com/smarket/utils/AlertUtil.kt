package com.smarket.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.view.Gravity
import android.widget.TextView
import com.smarket.R

/**
 * Created by MI-062 on 22/1/18.
 */

var alertDialog: AlertDialog? = null

fun Context.showDialog(message: String, title: String = "") {

    if (alertDialog != null) {
        alertDialog!!.dismiss()
    }

    alertDialog = AlertDialog.Builder(this).create()

    if (title.isRequiredField()) {
        alertDialog?.setTitle(title)
    }

    alertDialog?.setMessage(message)

    alertDialog?.setButton(AlertDialog.BUTTON_POSITIVE, resources.getString(R.string.ok)) { dialog, which -> dialog.dismiss() }

    alertDialog?.show()
}

fun Context.showDialogWithBackNavigation(mActivity: AppCompatActivity, message: String, title: String = "") {

    if (alertDialog != null) {
        alertDialog!!.dismiss()
    }

    alertDialog = AlertDialog.Builder(this).create()

    alertDialog?.setCancelable(false)

    if (title.isRequiredField()) {
        alertDialog?.setTitle(title)
    }

    alertDialog?.setMessage(message)

    alertDialog?.setButton(AlertDialog.BUTTON_POSITIVE, resources.getString(R.string.ok)) { dialog, which ->
        dialog.dismiss()
        mActivity.onBackPressed()
    }

    alertDialog?.show()
}

fun Context.showDialogWithAction(message: String, title: String = "",
                                 positiveButtonLabel: String = resources.getString(R.string.ok),
                                 negativeButtonLabel: String = resources.getString(R.string.cancel),
                                 requiredAlign: Boolean = false,
                                 showNegativeButton: Boolean = false, function: () -> Unit) {

    if (alertDialog != null) {
        alertDialog!!.dismiss()
    }

    alertDialog = AlertDialog.Builder(this).create()

    alertDialog?.setCancelable(false)

    if (title.isRequiredField()) {
        alertDialog?.setTitle(title)
    }

    alertDialog?.setMessage(message)

    alertDialog?.setButton(AlertDialog.BUTTON_POSITIVE, positiveButtonLabel) { dialog, which ->
        dialog.dismiss()
        function()
    }

    if (showNegativeButton) {
        alertDialog?.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButtonLabel) { dialog, which ->
            dialog.dismiss()
        }
    }

    alertDialog?.show()

    if (requiredAlign) {
        // Must call show() prior to fetching text view
        val messageView = alertDialog?.findViewById<TextView>(android.R.id.message) as? TextView
        messageView?.gravity = Gravity.CENTER_HORIZONTAL
    }
}

fun Context.showDialogWithActions(message: String, title: String = "", positiveButtonLabel: String = resources.getString(R.string.ok),
                                  negativeButtonLabel: String = resources.getString(R.string.ok)
                                  , showNegativeButton: Boolean = false, positiveFunction: () -> Unit,
                                  negativeFunction: () -> Unit,
                                  requiredAlign: Boolean = false) {

    if (alertDialog != null) {
        alertDialog!!.dismiss()
    }

    alertDialog = AlertDialog.Builder(this).create()

    alertDialog?.setCancelable(false)

    if (title.isRequiredField()) {
        alertDialog?.setTitle(title)
    }

    alertDialog?.setMessage(message)

    alertDialog?.setButton(AlertDialog.BUTTON_POSITIVE, positiveButtonLabel) { dialog, which ->
        dialog.dismiss()
        positiveFunction()
    }

    if (showNegativeButton) {
        alertDialog?.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButtonLabel) { dialog, which ->
            dialog.dismiss()
            negativeFunction()
        }
    }

    alertDialog?.show()

    if (requiredAlign) {
        // Must call show() prior to fetching text view
        val messageView = alertDialog?.findViewById<TextView>(android.R.id.message) as? TextView
        messageView?.gravity = Gravity.CENTER_HORIZONTAL
    }
}