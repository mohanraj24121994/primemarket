package com.smarket.utils

import androidx.constraintlayout.widget.ConstraintLayout
import android.view.animation.Animation
import android.view.animation.Transformation

internal class MyAnim(private val view: ConstraintLayout , private var targetBackGround: Int) : Animation() {
    override fun applyTransformation(interpolatedTime: Float , t: Transformation) {
        super.applyTransformation(interpolatedTime , t)
        view.setBackgroundResource(targetBackGround)
    }

    fun setBackground(drawable: Int) {
        targetBackGround = drawable
    }

}