package com.smarket.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.smarket.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 16/3/17.
 */

public class ImageChooserActivity extends AppCompatActivity implements ImagePickerCallback {

    public static final String KEY_OF_URI = "KEY_OF_URI";
    public static final int REQUEST_CODE = 486;

    public final static int MY_PERMISSIONS_REQUEST_CAMERA = 501;

    private String pickerPath = "";
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;

    String[] choose_options ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        choose_options = new String[]{getResources().getString(R.string.take_photo),getResources().getString(R.string.gallery)};
        askPermission();
    }

    /**
     * this method will show permission pop up messages to ic_user.
     */
    public void askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Permission mpermission = Permission.getInstance();
            ArrayList<String> permissionsToAsk = mpermission.checkPermission(ImageChooserActivity.this);
            String[] permissionArray = new String[permissionsToAsk.size()];
            permissionArray = permissionsToAsk.toArray(permissionArray);

            if (permissionArray.length > 0) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(ImageChooserActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale(ImageChooserActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    requestPermissions(permissionArray, MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    // No explanation needed, we can request the permission.
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                    requestPermissions(permissionArray, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            } else {
                showCameraDialog();
            }
        } else {
            showCameraDialog();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // After Activity recreate, you need to re-initialize these
        // two values to be able to re-initialize CameraImagePicker
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
                System.out.println("#####pickerPath===>" + pickerPath);
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            respondToCameraImage(requestCode, permissions, grantResults);
        }
    }

    /**
     * this method will work on the response captured from User regarding Run Time Permissions.
     *
     * @param permissions  Permissions for which we got result
     * @param grantResults Grant Results we got from User
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void respondToCameraImage(int requestCode, String permissions[], int[] grantResults) {
        PermissionPojo permissionPojo = setPermissionFlagsForImages(permissions, grantResults, ImageChooserActivity.this);

        if (permissionPojo.isRead() && permissionPojo.isCamera()) {
            showCameraDialog();
        } else {
            finish();
            Toast.makeText(ImageChooserActivity.this, "Permissions are not granted. Please allow it from setting.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * this method will find out the permission from the permission array which we have passed in parameter
     *
     * @param permissions  permission array for which we have showned pop up to ic_user as permission request
     * @param grantResults it the result of the ic_user's input.
     * @param mainActivity Activity.
     * @return it will return boolean array and order will be Camera/Read Storage/Write Storage
     */
    public PermissionPojo setPermissionFlagsForImages(String permissions[], int[] grantResults, Activity mainActivity) {
        PermissionPojo permissionPojo = new PermissionPojo();
        for (int i = 0; i < permissions.length; i++) {
            // Read Permission
            if (permissions[i].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE)
                    || permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    permissionPojo.setRead(true);
                    permissionPojo.setWrite(true);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //Show permission explanation dialog...
                        permissionPojo.setRead(false);
                        permissionPojo.setWrite(false);
                    }
                }
                permissionPojo.setCamera(!ActivityCompat.shouldShowRequestPermissionRationale(ImageChooserActivity.this, Manifest.permission.CAMERA));
            } else if (permissions[i].equalsIgnoreCase(Manifest.permission.CAMERA)) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    permissionPojo.setCamera(true);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //Show permission explanation dialog...
                        permissionPojo.setCamera(false);
                    }
                }
                permissionPojo.setRead(!ActivityCompat.shouldShowRequestPermissionRationale(ImageChooserActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE));
                permissionPojo.setWrite(!ActivityCompat.shouldShowRequestPermissionRationale(ImageChooserActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE));
            }
        }

        return permissionPojo;
    }

    /**
     * Show dialog.
     */
    public void showCameraDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ImageChooserActivity.this);
//        builder.setTitle(getResources().getString(R.string.add_a_photo))
        builder.setItems(choose_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0:
                                takePicture();
                                dialog.dismiss();
                                break;
                            case 1:
                                pickImageSingle();
                                dialog.dismiss();
                                break;
                        }
                    }
                });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });

        builder.show();
    }

    /**
     * Take image from gallery.
     */
    public void pickImageSingle() {
        imagePicker = new ImagePicker(this);

        // if need to choose multiple images
//        imagePicker.allowMultiple();

        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    /**
     *  Take multiple images from gallery.
     */
    public void pickImageMultiple() {
        imagePicker = new ImagePicker(this);
        imagePicker.setImagePickerCallback(this);
        imagePicker.allowMultiple();
        imagePicker.pickImage();
    }


    /**
     * Take picture from camera.
     */
    public void takePicture() {
        cameraPicker = new CameraImagePicker(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        cameraPicker.setImagePickerCallback(this);
        pickerPath = cameraPicker.pickImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
//                    imagePicker.setImagePickerCallback(imagePickerCallback);
                }
                cameraPicker.submit(data);
            }
        } else {
            finish();
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {

        Intent intent = new Intent();
        String uri = images.get(0).getOriginalPath();
        intent.putExtra(KEY_OF_URI, uri);
        setResult(REQUEST_CODE, intent);
        finish();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(ImageChooserActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // You have to save path in case your activity is killed.
        // In such a scenario, you will need to re-initialize the CameraImagePicker
        outState.putString("picker_path", pickerPath);
        super.onSaveInstanceState(outState);
    }

}


/**
 * Permission class for applying permissions.
 */
class Permission {

    private static Permission mPermission;
    ArrayList<String> permissionsToAsk;
    final static int MY_PERMISSIONS_REQUEST_CAMERA = 1;

    private Permission() {
    }

    public static Permission getInstance() {
        if (mPermission == null) {
            mPermission = new Permission();
        }
        return mPermission;
    }

    public ArrayList<String> checkPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissionsToAsk = new ArrayList<String>();
            if (context.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                permissionsToAsk.add(Manifest.permission.CAMERA);
            }
            if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsToAsk.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsToAsk.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        return permissionsToAsk;
    }
}

/**
 * Model class for the permission status.
 */
class PermissionPojo {

    boolean camera;
    boolean read;
    boolean write;
    boolean audio;

    public boolean isCamera() {
        return camera;
    }

    public void setCamera(boolean camera) {
        this.camera = camera;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isWrite() {
        return write;
    }

    public void setWrite(boolean write) {
        this.write = write;
    }

    public boolean isAudio() {
        return audio;
    }

    public void setAudio(boolean audio) {
        this.audio = audio;
    }
}



