package com.smarket.utils

import android.content.Context
import com.smarket.R
import com.smarket.SMarket

class PreferenceUtil {

    companion object {

        var prefID = SMarket.appContext.resources.getString(R.string.app_name)

        var PREF_KEY_BUSINESS_CATEGORY_TIMESTAMP = "PREF_KEY_BUSINESS_CATEGORY_TIMESTAMP"
        var PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION = "PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION"
        var PREF_KEY_LAST_LOGGED_IN_MERCHANT = "PREF_KEY_LAST_LOGGED_IN_MERCHANT"
        var PREF_KEY_LAST_LOGGED_IN_CUSTOMER = "PREF_KEY_LAST_LOGGED_IN_CUSTOMER"

        fun clearAllPreferences() {
            try {
                val prefs = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE)
                val editor = prefs.edit()
                editor.clear()
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun setPref(mPrefKey: String, mPrefValue: String?) {
            try {
                val editor = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE).edit()
                editor.remove(mPrefKey)
                editor.putString(mPrefKey, mPrefValue)
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun getStringPref(mPrefKey: String): String {
            return try {
                val prefs = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE)
                prefs.getString(mPrefKey, "")!!
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }
        }

        fun setPref(mPrefKey: String, mPrefValue: Int) {
            try {
                val editor = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE).edit()
                editor.remove(mPrefKey)
                editor.putInt(mPrefKey, mPrefValue)
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun getIntPref(mPrefKey: String): Int {
            return try {
                val prefs = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE)
                prefs.getInt(mPrefKey, 0)
            } catch (e: Exception) {
                e.printStackTrace()
                0
            }
        }

        fun setPref(mPrefKey: String, mPrefValue: Boolean) {
            try {
                val editor = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE).edit()
                editor.remove(mPrefKey)
                editor.putBoolean(mPrefKey, mPrefValue)
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun getBooleanPref(mPrefKey: String): Boolean {
            return try {
                val prefs = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE)
                prefs.getBoolean(mPrefKey, false)
            } catch (e: Exception) {
                e.printStackTrace()
                false
            }
        }

        fun setPref(mPrefKey: String, mPrefValue: Long) {
            try {
                val editor = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE).edit()
                editor.remove(mPrefKey)
                editor.putLong(mPrefKey, mPrefValue)
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun getLongPref(mPrefKey: String): Long {
            return try {
                val prefs = SMarket.appContext.getSharedPreferences(prefID, Context.MODE_PRIVATE)
                prefs.getLong(mPrefKey, 0L)
            } catch (e: Exception) {
                e.printStackTrace()
                0L
            }
        }

    }
}
