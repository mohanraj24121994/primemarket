package com.smarket.utils

interface OnEditTextChanged {
    fun onTextChanged(position: Int , amount: Int , count: String?)
}