package com.smarket.utils

interface GetProductBestBuy {
    fun getProductBestBuy(name: String , price: String , url: String, image: String)
}