package com.smarket.utils

import android.graphics.Shader
import android.graphics.Typeface
import androidx.core.content.res.ResourcesCompat
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.View
import android.widget.TextView
import com.tap.app.utils.CustomTypefaceSpan

/**
 * Created by MI-062 on 22/1/18.
 */

fun TextView.setCustomText(prefix: String, text: String, postfix: String = ""
                           , fontColor: Int = 0, relativeFontSize: Float = 1f
                           , isNewLineText: Boolean = false
                           , fontFamilyId: Int = 0
                           , typefaceOnPrefix: Boolean = false) {

    val sBuilder = SpannableStringBuilder()

    if (postfix.isRequiredField()) {
        sBuilder.append(prefix)
                .append(" ")
                .append(text)
                .append(" ")
                .append(postfix)
    } else {
        if (isNewLineText) {
            sBuilder.append(prefix)
                    .append("\n")
                    .append(text)
        } else {
            sBuilder.append(prefix)
                    .append(" ")
                    .append(text)
        }
    }

    if (fontColor != 0) {
        sBuilder.setSpan(ForegroundColorSpan(fontColor), prefix.length, sBuilder.length - postfix.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    if (fontFamilyId != 0) {
        val typeface: Typeface = ResourcesCompat.getFont(context, fontFamilyId)!!
        if (typefaceOnPrefix) {
            sBuilder.setSpan(CustomTypefaceSpan("", typeface), 0, prefix.length, 0)
        } else {
            sBuilder.setSpan(CustomTypefaceSpan("", typeface), prefix.length, sBuilder.length - postfix.length, 0)
        }
    }

    sBuilder.setSpan(RelativeSizeSpan(relativeFontSize), prefix.length, sBuilder.length - postfix.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    setText(sBuilder, TextView.BufferType.SPANNABLE)
}