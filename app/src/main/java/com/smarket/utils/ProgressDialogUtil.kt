package com.smarket.utils

import android.app.Activity
import android.app.ProgressDialog
import com.smarket.R


/**
 * Created by android on 15/2/17.
 */

object ProgressDialogUtil {

    private var progressDialog: ProgressDialog? = null

    fun showProgressDialog(mainActivity: Activity) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(mainActivity)
            progressDialog!!.setMessage(mainActivity.resources.getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        }
        progressDialog!!.show()
    }

    fun hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = null
        }
    }
}
