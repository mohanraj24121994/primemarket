package com.smarket.stripepayment

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.smarket.R
import com.smarket.SMarket
import com.smarket.utils.*
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.activity_stripe_payment.*
import kotlinx.android.synthetic.main.header.*

class StripePaymentActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        val TAG = "StripePaymentActivity"
        val STRIPE_PAYMENT_AMOUNT = "stripe_payment_amount"
        val STRIPE_PAYMENT_REQUEST_CODE = 101
        val STRIPE_TOKEN = "stripe_token"
        val STRIPE_TRANSACTION_RESULT = "stripe_transaction_result"
        val STRIPE_TRANSACTION_ERROR_MESSAGE = "stripe_transaction_error_message"
    }

    //    val STRIPE_PUBLISHABLE_KEY = "pk_test_ES1coDryWf1QL2VmCiOWKUtI" // Test
    val STRIPE_PUBLISHABLE_KEY = "pk_live_OlowoLDnCFJQ6TGIAP8B64wc" // Live

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppThemeMerchant)
        setContentView(R.layout.activity_stripe_payment)
        setHeader()
        btn_add_money.setOnClickListener(this)
    }

    private fun setHeader() {
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = SMarket.appContext.resources.getString(R.string.add_refcash)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_add_money -> {
                avoidDoubleClicks(btn_add_money)
                hideKeyboard()
                checkPaymentValidation()
            }
            R.id.iv_back -> {
                onBackPressed()
            }
        }
    }

    /**
     * here we will check payment input's validation
     */
    private fun checkPaymentValidation() {
        val cardToSave = card_input_widget.card

        if (cardToSave != null) {
            createPaymentToken(cardToSave)
        } else {
            this@StripePaymentActivity.showDialog("Invalid Card Data")
        }
    }

    private fun createPaymentToken(cardToSave: Card) {
        ProgressDialogUtil.showProgressDialog(this)
        val stripe = Stripe(this, STRIPE_PUBLISHABLE_KEY)
        stripe.createToken(cardToSave, object : TokenCallback {
            override fun onSuccess(token: Token) {
                ProgressDialogUtil.hideProgressDialog()
                val intent = Intent()
                val stripeToken = token.id
                println("!@#" + stripeToken)
                if (stripeToken.isRequiredField()) {
                    // Send token to your server
                    intent.putExtra(STRIPE_TOKEN, token.id)
                    intent.putExtra(STRIPE_TRANSACTION_RESULT, true)
                } else {
                    intent.putExtra(STRIPE_TRANSACTION_RESULT, false)
                    intent.putExtra(STRIPE_TRANSACTION_ERROR_MESSAGE, "Empty or null payment token found. Please retry.")
                }
                setResult(STRIPE_PAYMENT_REQUEST_CODE, intent)
                finish()//finishing activity
            }

            override fun onError(error: Exception) {
                ProgressDialogUtil.hideProgressDialog()
                showToast(error.message.toString())
                val intent = Intent()
                intent.putExtra(STRIPE_TRANSACTION_RESULT, false)
                intent.putExtra(STRIPE_TRANSACTION_ERROR_MESSAGE, error.message.toString())
                setResult(STRIPE_PAYMENT_REQUEST_CODE, intent)
                finish()//finishing activity
            }
        })
    }
}