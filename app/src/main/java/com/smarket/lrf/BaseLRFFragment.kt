package com.smarket.lrf

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.smarket.R

/**
 * Created by MI-062 on 20/6/18.
 */
open class BaseLRFFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    lateinit var mActivity: LRFActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = activity as LRFActivity
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_back -> mActivity.onBackPressed()
            else -> {
            }
        }
    }
}