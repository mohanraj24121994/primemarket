package com.smarket.lrf

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Handler
import android.view.ContextThemeWrapper
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.main.MainActivity
import com.smarket.utils.DEVICE_TOKEN
import com.smarket.utils.PreferenceUtil
import com.smarket.utils.clearNotifications
import com.smarket.webservices.ApiUtil
import org.json.JSONObject
import org.jsoup.Jsoup
import java.io.IOException


class ForceUpdateAsync(private val currentVersion: String , private val context: Activity ,
                       var sharedPreferences: SharedPreferences) : AsyncTask<String? , String? , JSONObject?>() {
    private var latestVersion: String? = null
    override fun doInBackground(vararg params: String?): JSONObject {
        try {
            latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.packageName.toString() + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return JSONObject()
    }

    override fun onPostExecute(jsonObject: JSONObject?) {
        if (latestVersion != null) {
            if (! currentVersion.equals(latestVersion , ignoreCase = true)) { // Toast.makeText(context,"update is available.",Toast.LENGTH_LONG).show();
                showForceUpdateDialog()
            } else {
                val userDetails = SMarket.appDB.userDao().getData()
                if (userDetails == null) {
                    val isVisible = sharedPreferences.getBoolean("visible" , true)
                    if (isVisible) {
                        navigateTermsConditionsActivity()
                    } else {
                        navigateToSelectUserTypeActivity()
                    }
                } else {
                    navigateToMainActivity()
                }
            }
        }
        super.onPostExecute(jsonObject)
    }

    private fun navigateTermsConditionsActivity() {
        val intent = Intent(context , TermsAndConditionsActivity::class.java)
        context.startActivity(intent)
        context.overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        context.finish()
    }

    private fun navigateToMainActivity() {
        val intent = Intent(context , MainActivity::class.java)
        context.startActivity(intent)
        context.overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        context.finish()
    }

    private fun navigateToSelectUserTypeActivity() {
        val intent = Intent(context , SelectUserTypeActivity::class.java)
        context.startActivity(intent)
        context.overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        context.finish()
    }

    private fun showForceUpdateDialog() {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(ContextThemeWrapper(context ,
                android.R.style.Theme_DeviceDefault_Light))

        val prefs = SMarket.appContext.getSharedPreferences(PreferenceUtil.prefID , Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.remove("shareURL")
        editor.apply()
        editor.commit()

        clearNotifications(context)

        Handler().postDelayed({
            SMarket.appDB.userDao().deleteData()
        } , 300)

        PreferenceUtil.clearAllPreferences()

        alertDialogBuilder.setTitle("Update Available")
        alertDialogBuilder.setMessage("A new version of SMarket app is available in PlayStore. Please update to version $latestVersion now")
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setPositiveButton("Update") { dialog , id ->
            context.startActivity(Intent(Intent.ACTION_VIEW , Uri.parse("market://details?id=" + context.packageName)))
            dialog.cancel()
        }
        alertDialogBuilder.show()
    }
}
