package com.smarket.lrf

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.LinkBuilder
import com.smarket.R
import com.smarket.database.User
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.view_mobile_number.*
import utils.AnimationType

/**
 * Created by MI-062 on 20/6/18.
 */
class LoginFragment : BaseLRFFragment() , View.OnClickListener {

    companion object {
        fun getInstance(): LoginFragment {
            val fragment = LoginFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var email = ""
    var countryCode = ""
    var mobile = ""
    var password = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        if (mActivity.isUserTypeCustomer) {

            iv_app_logo.setImageDrawable(resources.getDrawable(R.drawable.customer_logo))
            et_email.visibility = View.GONE
            //tv_or.visibility = View.GONE
            //btn_login_with_google.visibility = View.GONE

            mActivity.setCountryCodeSpinner(sp_country_code)

            sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>) {
                }

                override fun onItemSelected(p0: AdapterView<*>? , p1: View? , position: Int , p3: Long) {
                    countryCode = mActivity.countryCodeList[position]
                }
            }

        } else {

            iv_app_logo.setImageDrawable(resources.getDrawable(R.drawable.merchant_logo))
            cl_mobile_number.visibility = View.GONE
            et_password.visibility = View.GONE
            tv_forgot_password.visibility = View.INVISIBLE
        }

        setUpSignUpText()

        tv_forgot_password.setOnClickListener(this)
        btn_login.setOnClickListener(this)
        btn_login_with_google.setOnClickListener(this)
        tv_sign_up.setOnClickListener(this)
    }

    private fun setUpSignUpText() {

        val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_semi_bold) !!

        val linkSignUp = Link(mActivity.resources.getString(R.string.sign_up_here))
                .setTypeface(typeface)
                .setTextColor(if (mActivity.isUserTypeCustomer) {
                    ContextCompat.getColor(mActivity , R.color.customer_app_color)
                } else {
                    ContextCompat.getColor(mActivity , R.color.merchant_app_color)
                })

        LinkBuilder.on(tv_sign_up)
                .addLink(linkSignUp)
                .build()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val lastLoginDetails: String
        if (mActivity.isUserTypeCustomer) {
            lastLoginDetails = PreferenceUtil.getStringPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_CUSTOMER)
            if (lastLoginDetails.isRequiredField()) {
                if (lastLoginDetails.contains(",")) {
                    mActivity.setSelectedCountyCode(sp_country_code , lastLoginDetails.split(",")[0])
                    et_mobile.setText(lastLoginDetails.split(",")[1].trim())
                }
            }
        } else {
            lastLoginDetails = PreferenceUtil.getStringPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_MERCHANT)
            if (lastLoginDetails.isRequiredField()) {
                et_email.setText(lastLoginDetails)
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_forgot_password -> mActivity.addFragment(ForgotPasswordFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.btn_login -> {
                if (checkForValidations()) {
                    callLoginAPI()
                }
            }
            R.id.btn_login_with_google -> {
                MIGoogleActivity.startGoogleActivityForResult(this@LoginFragment)
            }
            R.id.tv_sign_up -> mActivity.addFragment(SignUpFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        if (mActivity.isUserTypeCustomer) {

            mobile = et_mobile.text.toString()

            if (! mobile.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                return false
            } else if (! mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                return false
            }

            password = et_password.text.toString()

            if (password.isEmpty()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.password_req))
                return false
            }

        } else {

            email = et_email.text.toString()

            if (! email.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
                return false
            } else if (! email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            }
        }

        return true
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        if (requestCode == MIGoogleActivity.GOOGLE_REQUEST_CODE) {
            if (data != null) {
                if (data.extras != null && data.extras !!.containsKey(MIGoogleActivity.KEY_GOOGLE_SIGNIN_ACCOUNT)) {
                    val userSocialDetails = User()
                    val googleUserDetail = data.getParcelableExtra<GoogleSignInAccount>(MIGoogleActivity.KEY_GOOGLE_SIGNIN_ACCOUNT)
                    if (googleUserDetail != null) {
                        userSocialDetails.google_id = googleUserDetail.id.toString()
                        userSocialDetails.loginType = ApiParam.LOGIN_TYPE_GOOGLE
                        if (googleUserDetail.photoUrl != null) {
                            userSocialDetails.businessLogo = googleUserDetail.photoUrl.toString()
                        }
                        if (googleUserDetail.displayName != null) {
                            userSocialDetails.businessName = googleUserDetail.displayName !!
                        }
                        if (googleUserDetail.email != null) {
                            userSocialDetails.email = googleUserDetail.email !!
                        }
                    }
                    // if (mActivity.isUserTypeCustomer) {
                    callSocialLoginAPI(userSocialDetails)

                    /*mActivity.addFragment(SignUpFragment.getInstance(userSocialDetails)
                            , true, animationType = utils.AnimationType.BottomInBottomOut)*/
                }
            }
        }
    }

    private fun callLoginAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_LOGIN
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
            hashMap[ApiParam.PASSWORD] = password
        } else {
            tag = ApiParam.Merchant.TAG_LOGIN_OTP
            hashMap[ApiParam.EMAIL] = email
        }

        disposable = WebApiClient.webApi().loginAPI(tag , hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        if (data != null) {

                                                            val userDetails = data !!
                                                            userDetails.token = token

                                                            mActivity.navigateToMainActivity(userDetails)

                                                        } else {
                                                            if (mActivity.isUserTypeCustomer) {
                                                                mActivity.showDialog(message)
                                                            } else {
                                                                mActivity.isMerChantLogin = true
                                                                mActivity.navigateToVerifyOTPFragment(email)
                                                            }
                                                        }
                                                    }
                                                    ApiParam.META_UNVERIFIED_USER -> {
                                                        if (mActivity.isUserTypeCustomer) {
                                                            mActivity.navigateToVerifyOTPFragment(countryCode = countryCode , mobile = mobile)
                                                        } else {
                                                            mActivity.navigateToVerifyOTPFragment(email)
                                                        }
                                                    }
                                                    ApiParam.META_UNAUTHORIZED_USER -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callSocialLoginAPI(userSocialDetails: User) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag = if (mActivity.isUserTypeCustomer) {
            ApiParam.Customer.TAG_SOCIAL_LOGIN
        } else {
            ApiParam.Merchant.TAG_SOCIAL_LOGIN
        }

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.GOOGLE_ID] = userSocialDetails.google_id
        if (userSocialDetails.email.isRequiredField()) {
            hashMap[ApiParam.EMAIL] = userSocialDetails.email
        }

        disposable = WebApiClient.webApi().socialLoginAPI(tag , hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        if (data != null) {
                                                            val userDetails = data !!
                                                            userDetails.token = token
                                                            mActivity.navigateToMainActivity(userDetails)
                                                        } else {
                                                            mActivity.showDialog(message)
                                                        }
                                                    }
                                                    ApiParam.META_UNREGISTERED_USER -> {
                                                        mActivity.addFragment(SignUpFragment.getInstance(userSocialDetails) , true , animationType = AnimationType.BottomInBottomOut)
                                                    }
                                                    ApiParam.META_UNVERIFIED_USER -> {
                                                        mActivity.navigateToVerifyOTPFragment(email)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}