package com.smarket.lrf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.smarket.utils.*
import com.smarket.R
import com.smarket.utils.showDialog
import com.smarket.utils.showDialogWithAction
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_mobile_number.*
import utils.AnimationType

/**
 * Created by MI-062 on 20/6/18.
 */
class ForgotPasswordFragment : BaseLRFFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): ForgotPasswordFragment {
            val fragment = ForgotPasswordFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var email = ""
    var countryCode = ""
    var mobile = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        if (mActivity.isUserTypeCustomer) {

            tv_instruction.text = mActivity.resources.getString(R.string.forgot_password_instruction_customer)
            et_email.visibility = View.GONE
            btn_send_code_submit.text = mActivity.resources.getString(R.string.send_code)

            mActivity.setCountryCodeSpinner(sp_country_code)

            sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    countryCode = mActivity.countryCodeList[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }

        } else {

            tv_instruction.text = mActivity.resources.getString(R.string.forgot_password_instruction_merchant)
            cl_mobile_number.visibility = View.GONE
            btn_send_code_submit.text = mActivity.resources.getString(R.string.submit)
        }

        btn_send_code_submit.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.forgot_password)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_send_code_submit -> {
                if (checkForValidations()) {
                    callForgotPasswordAPI()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        if (mActivity.isUserTypeCustomer) {

            mobile = et_mobile.text.toString()

            if (!mobile.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                return false
            } else if (!mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                return false
            }

        } else {

            email = et_email.text.toString()

            if (!email.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
                return false
            } else if (!email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            }
        }

        return true
    }

    private fun callForgotPasswordAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_FORGOT_PASSWORD
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
        } else {
            tag = ApiParam.Merchant.TAG_FORGOT_PASSWORD
            hashMap[ApiParam.EMAIL] = email
        }

        disposable = WebApiClient.webApi().forgotPasswordAPI(tag, hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    mActivity.showDialogWithAction(message) {
                                                        mActivity.addFragment(if (mActivity.isUserTypeCustomer) {
                                                            ResetPasswordFragment.getInstance(countryCode = countryCode, mobile = mobile)
                                                        } else {
                                                            ResetPasswordFragment.getInstance(email)
                                                        }, true, animationType = AnimationType.RightInZoomOut)
                                                    }

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}