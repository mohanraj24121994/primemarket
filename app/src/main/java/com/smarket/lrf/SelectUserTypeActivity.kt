package com.smarket.lrf

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.smarket.utils.PreferenceUtil
import com.smarket.R
import com.smarket.utils.USER_TYPE
import com.smarket.utils.USER_TYPE_CUSTOMER
import com.smarket.utils.USER_TYPE_MERCHANT
import kotlinx.android.synthetic.main.activity_select_user_type.*

class SelectUserTypeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_user_type)

        iv_customer_type.setOnClickListener(this)
        iv_customer_type_bg.setOnClickListener(this)
        iv_merchant_type.setOnClickListener(this)
        iv_merchant_type_bg.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val intent: Intent
        when (v?.id) {
            R.id.iv_customer_type,
            R.id.iv_customer_type_bg -> {
                PreferenceUtil.setPref(USER_TYPE, USER_TYPE_CUSTOMER)
                navigateToLRFActivity()
            }
            R.id.iv_merchant_type,
            R.id.iv_merchant_type_bg-> {
                PreferenceUtil.setPref(USER_TYPE, USER_TYPE_MERCHANT)
                navigateToLRFActivity()
            }
            else -> {
            }
        }
    }

    fun navigateToLRFActivity() {
        val intent = Intent(this, LRFActivity::class.java)
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out)
        startActivity(intent)
    }
}
