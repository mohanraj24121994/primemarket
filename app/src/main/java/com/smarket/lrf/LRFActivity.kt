package com.smarket.lrf

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.Html
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.smarket.BuildConfig
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.CountryCodeAdapter
import com.smarket.database.User
import com.smarket.main.MainActivity
import com.smarket.utils.*
import com.smarket.webservices.ApiUtil
import kotlinx.android.synthetic.main.activity_lrf.*
import kotlinx.android.synthetic.main.view_mobile_number.*
import utils.AnimationType
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by MI-062 on 20/6/18.
 */
class LRFActivity : AppCompatActivity() {

    var isUserTypeCustomer = false
    var isMerChantLogin = false

    lateinit var countryCodeList: List<String>

    var customerSignUpDetails: User? = null
    var merchantSignUpDetails: User? = null
    var merchantSocialSignUpDetails: User? = null


    var getCurrentCountryCode: String? = null
    protected var mLastLocation: Location? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isUserTypeCustomer = PreferenceUtil.getIntPref(USER_TYPE) == USER_TYPE_CUSTOMER
        if (isUserTypeCustomer) {
            setTheme(R.style.LRFThemeCustomer)
        } else {
            setTheme(R.style.LRFThemeMerchant)
        }

        setContentView(R.layout.activity_lrf)
/*

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (getCurrentCountryCode == "IN") {
            val countryList = ArrayList(this.getCountryCodeList())
            countryList.add(0 , "+91")
            countryCodeList = countryList
        } else if (getCurrentCountryCode == "US") {
            val countryList = ArrayList(this.getCountryCodeList())
            countryList.add(1 , "+91")
            countryCodeList = countryList
        } else {
*/
            val countryList = ArrayList(this.getCountryCodeList())
            countryList.add(0 , "+91")
            countryCodeList = countryList
//        }

        //countryCodeList = this.getCountryCodeList()

        var isFrom = ""
        if (intent != null) {
            if (intent.extras != null) {
                val bundle = intent.extras
                if (bundle != null) {
                    if (bundle.containsKey(IS_FROM)) {
                        isFrom = bundle.getString(IS_FROM) !!
                    }
                    if (bundle.containsKey(USER_SIGNUP_DETAILS)) {
                        customerSignUpDetails = bundle.getParcelableArrayList<User?>(USER_SIGNUP_DETAILS) !![0]
                        merchantSignUpDetails = bundle.getParcelableArrayList<User?>(USER_SIGNUP_DETAILS) !![1]
                        merchantSocialSignUpDetails = bundle.getParcelableArrayList<User?>(USER_SIGNUP_DETAILS) !![2]
                    }
                }
            }
        }

        if (isFrom == SignUpFragment::class.java.canonicalName) {
            replaceFragment(SignUpFragment.getInstance())
        } else {
            replaceFragment(LoginFragment.getInstance())
        }
    }

    public override fun onStart() {
        super.onStart()
/*`
        if (! checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }*/
    }

    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mFusedLocationClient !!.lastLocation
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful && task.result != null) {
                        mLastLocation = task.result
                        getCurrentCountryCode = getCountryName(applicationContext , mLastLocation !!.latitude , mLastLocation !!.longitude)
                        if (getCurrentCountryCode == "IN") {
                            val countryList = ArrayList(this.getCountryCodeList())
                            countryList.add(0 , "+91")
                            countryCodeList = countryList

                            setCountryCodeSpinner(sp_country_code)

                        } else if (getCurrentCountryCode == "US") {
                            val countryList = ArrayList(this.getCountryCodeList())
                            countryList.add(1 , "+91")
                            countryCodeList = countryList

                            setCountryCodeSpinner(sp_country_code)
                        } else {

                            val countryList = ArrayList(this.getCountryCodeList())
                            countryCodeList = countryList
                            setCountryCodeSpinner(sp_country_code)
                        }

                    } else {
                        Log.w("LocationProvider" , "getLastLocation:exception" , task.exception)
                    }
                }
    }

    private fun getCountryName(context: Context? , latitude: Double , longitude: Double): String? {
        val geocoder = Geocoder(context , Locale.getDefault())
        val addresses: List<Address>?
        try {
            addresses = geocoder.getFromLocation(latitude , longitude , 1)
            return if (addresses != null && ! addresses.isEmpty()) {
                addresses[0].countryCode
            } else null
        } catch (ignored: IOException) {

        }
        return null
    }

    private fun requestPermissions() {
        startLocationPermissionRequest()
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this ,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this ,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION) ,
                REQUEST_PERMISSIONS_REQUEST_CODE)
    }

    override fun onBackPressed() {
        hideKeyboard()
        if (getCurrentFragment() is SignUpFragment && supportFragmentManager.backStackEntryCount == 0) {
            replaceFragment(LoginFragment.getInstance() , animationType = AnimationType.LeftInZoomOut)
        } else {
            super.onBackPressed()
        }
    }

    fun setCountryCodeSpinner(sp_country_code: AppCompatSpinner) {

        val countryCodeAdapter = CountryCodeAdapter(this , R.layout.row_spinner_country_code_with_name , countryCodeList)
        sp_country_code.adapter = countryCodeAdapter

        setCurrentCountryCodeAsDefault(sp_country_code)
    }

    fun setCurrentCountryCodeAsDefault(sp_country_code: AppCompatSpinner) {

        val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val currentCountryCode = tm.networkCountryIso

        if (currentCountryCode.isRequiredField()) {
            val currentCountryDetails = countryCodeList.find { it.equals(currentCountryCode , true) }
            if (currentCountryDetails != null) {
                sp_country_code.setSelection(countryCodeList.indexOf(currentCountryDetails))
            }
        }
    }

    fun setSelectedCountyCode(sp_country_code: AppCompatSpinner , countryCode: String) {

        sp_country_code.setSelection(countryCodeList.indexOf(countryCodeList.find { it == countryCode }))
    }

    fun navigateToVerifyOTPFragment(email: String = "" , countryCode: String = "" , mobile: String = "") {

        clearBackStack()

        replaceFragment(if (isUserTypeCustomer) {
            VerifyOTPFragment.getInstance(countryCode = countryCode , mobile = mobile)
        } else {
            VerifyOTPFragment.getInstance(email)
        } , animationType = AnimationType.RightInZoomOut)
    }

    fun navigateToMainActivity(userDetails: User) {

        SMarket.appDB.userDao().deleteData()

        SMarket.appDB.userDao().setData(userDetails)

        if (isUserTypeCustomer) {
            //adding token for getting notifications when he login as a customer
            ApiUtil.callAddRemoveDeviceTokenAPI(User.getLoginUserId() , PreferenceUtil.getStringPref(DEVICE_TOKEN) , "add")
        }

        val intent = Intent(this , MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }
/*

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> ,
                                            grantResults: IntArray) {
        Log.i("TAG" , "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i("TAG" , "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                needLocation(this)
            }
        }
    }
*/

    private fun needLocation(activity: Activity): Snackbar? {
        val snackbar = Snackbar.make(activity.findViewById(android.R.id.content) , Html.fromHtml("<font color=#ffffff>Need Location Permission!</font>") , Snackbar.LENGTH_INDEFINITE)
        snackbar.setActionTextColor(Color.YELLOW)
        snackbar.setAction("Settings") {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package",
                    BuildConfig.APPLICATION_ID, null)
            intent.data = uri
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
        }
        snackbar.show()
        return snackbar
    }

    companion object {
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }
}