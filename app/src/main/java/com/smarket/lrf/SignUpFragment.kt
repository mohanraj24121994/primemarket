package com.smarket.lrf

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlacePicker
import com.smarket.utils.*
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.GetPostCode
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.LRFResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_business_category.*
import kotlinx.android.synthetic.main.view_mobile_number.*
import kotlinx.android.synthetic.main.view_select_user_type.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File

/**
 * Created by MI-062 on 20/6/18.
 */
class SignUpFragment : BaseLRFFragment() , View.OnClickListener {

    companion object {
        fun getInstance(userSocialDetails: User? = null): SignUpFragment {
            val signUpFragment = SignUpFragment()
            val bundle = Bundle()
            bundle.putParcelable("userSocialDetails" , userSocialDetails)
            signUpFragment.arguments = bundle
            return signUpFragment
        }

    }

    var businessCategoriesList = ArrayList<String>()

    var picture: File? = null
    var name = ""
    var businessName = ""
    var tagLine = ""
    var email = ""
    var countryCode = ""
    var mobile = ""
    var website = ""
    var businessCategory = ""
    var latitude = 0.0
    var longitide = 0.0
    var postCode = ""
    var address = ""
    var description = ""
    var productAndServices = ""
    var password = ""
    var confirmPassword = ""
    private var isNeedToVerifyEmail = ApiParam.TRUE

    private var userSocialDetails: User? = null

    lateinit var signUpObservable: Observable<Response<LRFResponse>>
    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        if (mActivity.isUserTypeCustomer) {

            ctv_customer.isChecked = true
            sdv_profile_pic.setBackgroundResource(R.drawable.upload_profile_pic_customer)
            et_business_name.visibility = View.GONE
            et_tag_line.visibility = View.GONE
            et_website.visibility = View.GONE
            cl_business_category.visibility = View.GONE
            et_address.visibility = View.GONE
            et_description.visibility = View.GONE
            et_product_and_services.visibility = View.GONE
            et_referral_code.visibility = View.VISIBLE
            tv_refer.visibility = View.VISIBLE

        } else {

            ctv_merchant.isChecked = true
            sw_user_type.isChecked = true
            sdv_profile_pic.setBackgroundResource(R.drawable.upload_profile_pic_merchant)
            et_name.visibility = View.GONE

            businessCategoriesList = SMarket.appDB.businessCategoryDao().getNames() as ArrayList<String>

            val businessCategoryAdapter = ArrayAdapter<String>(mActivity
                    , R.layout.row_spinner_business_category
                    , businessCategoriesList)
            businessCategoryAdapter.setDropDownViewResource(R.layout.row_dropdown_business_category)
            sp_business_category.adapter = businessCategoryAdapter

            sp_business_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*> , view: View , position: Int , id: Long) {
                    businessCategory = businessCategoriesList[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }

        mActivity.setCountryCodeSpinner(sp_country_code)

        sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*> , view: View , position: Int , id: Long) {
                countryCode = mActivity.countryCodeList[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        sdv_profile_pic.setOnClickListener(this)
        et_address.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("userSocialDetails")) {
                userSocialDetails = bundle.getParcelable("userSocialDetails")
            }
        }
    }

    private fun setHeader() {
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)

        if (! userSocialDetails?.google_id.isNullOrEmpty()) {
            if (mActivity.isUserTypeCustomer) {
                tv_title.setText(R.string.referral)
                lay_select_user.visibility = View.INVISIBLE
                et_mobile.hint = resources.getString(R.string.mobile_number_optional)
            } else {
                tv_title.setText(R.string.sign_up)
            }
        } else {
            tv_title.setText(R.string.sign_up)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setUserSignUpDetails()

        if (mActivity.merchantSocialSignUpDetails != null) {
            userSocialDetails = mActivity.merchantSocialSignUpDetails
        }

        setUserSocialDetails()

        sw_user_type.setOnCheckedChangeListener { buttonView , isChecked ->
            if (isChecked) {
                PreferenceUtil.setPref(USER_TYPE , USER_TYPE_MERCHANT)
                ctv_customer.isChecked = false
                ctv_merchant.isChecked = true
            } else {
                PreferenceUtil.setPref(USER_TYPE , USER_TYPE_CUSTOMER)
                ctv_merchant.isChecked = false
                ctv_customer.isChecked = true
            }
            saveUserSignUpDetails()
            Handler().postDelayed({
                val intent = Intent(mActivity , LRFActivity::class.java)
                intent.putExtra(IS_FROM , SignUpFragment::class.java.canonicalName)
                intent.putParcelableArrayListExtra(USER_SIGNUP_DETAILS , arrayListOf(mActivity.customerSignUpDetails
                        , mActivity.merchantSignUpDetails , userSocialDetails))
                mActivity.startActivity(intent)
                mActivity.overridePendingTransition(android.R.anim.fade_in , android.R.anim.fade_out)
                mActivity.finish()
            } , 200)
        }
    }

    private fun setUserSignUpDetails() {

        if (mActivity.isUserTypeCustomer) {
            if (mActivity.customerSignUpDetails == null) {
                return
            } else {
                with(mActivity.customerSignUpDetails !!) {
                    if (profilePic.isRequiredField()) {
                        try {
                            picture = File(profilePic)
                            sdv_profile_pic.loadFrescoImageFromFile(picture)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    et_name.setText(name)
                    et_email.setText(email)
                    mActivity.setSelectedCountyCode(sp_country_code , countryCode)
                    et_mobile.setText(mobile)
                }
            }
        } else {
            et_password.visibility = View.GONE
            et_confirm_password.visibility = View.GONE
            if (mActivity.merchantSignUpDetails == null) {
                return
            } else {
                with(mActivity.merchantSignUpDetails !!) {
                    if (businessLogo.isRequiredField()) {
                        try {
                            picture = File(businessLogo)
                            sdv_profile_pic.loadFrescoImageFromFile(picture)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    et_business_name.setText(businessName)
                    et_tag_line.setText(tagLine)
                    et_email.setText(email)
                    mActivity.setSelectedCountyCode(sp_country_code , countryCode)
                    et_mobile.setText(mobile)
                    et_website.setText(website)
                    sp_business_category.setSelection(businessCategoriesList.indexOf(SMarket.appDB.businessCategoryDao().getNameById(businessCategoryId)))
                    et_address.setText(address)
                    this@SignUpFragment.postCode = postCode
                    this@SignUpFragment.latitude = latitude
                    this@SignUpFragment.longitide = longitude
                    et_description.setText(description)
                    et_product_and_services.setText(productAndServices)
                }
            }
        }
    }

    private fun saveUserSignUpDetails() {

        if (mActivity.isUserTypeCustomer) {
            mActivity.customerSignUpDetails = User()
            with(mActivity.customerSignUpDetails !!) {
                if (this@SignUpFragment.picture != null) {
                    profilePic = this@SignUpFragment.picture !!.path
                }
                name = et_name.text.toString()
                email = et_email.text.toString()
                countryCode = this@SignUpFragment.countryCode
                mobile = et_mobile.text.toString()
            }

        } else {
            mActivity.merchantSignUpDetails = User()
            with(mActivity.merchantSignUpDetails !!) {
                if (this@SignUpFragment.picture != null) {
                    businessLogo = this@SignUpFragment.picture !!.path
                }
                businessName = et_business_name.text.toString()
                tagLine = et_tag_line.text.toString()
                email = et_email.text.toString()
                countryCode = this@SignUpFragment.countryCode
                mobile = et_mobile.text.toString()
                website = et_website.text.toString()
                businessCategoryId = SMarket.appDB.businessCategoryDao().getIdByName(businessCategory)
                address = et_address.text.toString()
                postCode = this@SignUpFragment.postCode
                latitude = this@SignUpFragment.latitude
                longitude = this@SignUpFragment.longitide
                description = et_description.text.toString()
                productAndServices = et_product_and_services.text.toString()
            }
            mActivity.merchantSocialSignUpDetails = userSocialDetails
        }
    }

    private fun setUserSocialDetails() {

        if (userSocialDetails != null) {

            et_password.visibility = View.GONE
            et_confirm_password.visibility = View.GONE

            with(userSocialDetails !!) {

                if (businessLogo.isRequiredField()) {
                    picture = File(storeImageToStorage(DownloadImage().execute(businessLogo).get() , mActivity))
                    sdv_profile_pic.loadFrescoImage(businessLogo)
                }
                if (! mActivity.isUserTypeCustomer) {
                    et_business_name.setText(businessName)
                } else {
                    et_name.setText(businessName)
                }
                if (email.isRequiredField()) {
                    et_tag_line.nextFocusDownId = R.id.et_mobile
                    et_email.setText(email)
                    et_email.isEnabled = false
                    et_email.setTextColor(mActivity.resources.getColor(R.color.dim_gray_txt))
                    isNeedToVerifyEmail = ApiParam.FALSE
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.sdv_profile_pic -> {
                val intent = Intent(mActivity , ImageChooserActivity::class.java)
                startActivityForResult(intent , ImageChooserActivity.REQUEST_CODE)
            }
            R.id.et_address -> {
                avoidDoubleClicks(et_address)
                val builder = PlacePicker.IntentBuilder()
                try {
                    startActivityForResult(builder.build(mActivity) , REQUEST_CODE_GOOGLE_PLACE_API)
                } catch (e: GooglePlayServicesRepairableException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesNotAvailableException) {
                    e.printStackTrace()
                }
            }
            R.id.btn_submit -> {
                if (checkForValidations()) {
                    callSignUpAPI()
                }
            }
            else -> {
            }
        }
    }


    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        email = et_email.text.toString()
        mobile = et_mobile.text.toString()

        if (mActivity.isUserTypeCustomer) {

            name = et_name.text.toString()
            password = et_password.text.toString()
            confirmPassword = et_confirm_password.text.toString()

            if (! name.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.name_req))
                return false
            } else if (email.isRequiredField() && ! email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            }

            if (userSocialDetails?.google_id.isNullOrEmpty()) {
                if (! mobile.isRequiredField()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                    return false
                } else if (! mobile.isValidMobile()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                    return false
                }
            }

        } else {

            businessName = et_business_name.text.toString()
            tagLine = et_tag_line.text.toString()
            website = et_website.text.toString()
            address = et_address.text.toString()
            description = et_description.text.toString()
            productAndServices = et_product_and_services.text.toString()
            password = "123456a"
            confirmPassword = "123456a"

            /* if (picture == null) {
                 mActivity.showDialog(mActivity.resources.getString(R.string.business_logo_req))
                 return false
             } else*/ if (! businessName.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.business_name_req))
                return false
            } else if (! email.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
                return false
            } else if (! email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            } else if (! mobile.isRequiredField() && ! mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                return false
            } /*else if (! businessCategory.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.business_category_req))
                return false
            } else if (! address.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.address_req))
                return false
            } else if (! productAndServices.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.product_and_services_req))
                return false
            }*/
        }

        if (userSocialDetails == null) {
            if (mActivity.isUserTypeCustomer) {
                if (password.isEmpty()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.password_req))
                    return false
                } else if (! password.isValidPassword()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.valid_password_req))
                    return false
                } else if (confirmPassword.isEmpty()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.confirm_password_req))
                    return false
                } else if (password != confirmPassword) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.passwords_mismatch))
                    return false
                }
            }
        }

        return true
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        if (data != null) {
            when (requestCode) {
                ImageChooserActivity.REQUEST_CODE -> {
                    val picturePath = data.getStringExtra(ImageChooserActivity.KEY_OF_URI)
                    picture = File(picturePath)
                    sdv_profile_pic.loadFrescoImageFromFile(picture)
                }
                REQUEST_CODE_GOOGLE_PLACE_API -> {
                    val place = PlacePicker.getPlace(mActivity , data)
                    val postCodeUrl = ApiParam.getPostCodeUrl(mActivity.resources.getString(R.string.google_key) , place.id)
                    postCode = GetPostCode().execute(postCodeUrl).get()
                    val latLng = place.latLng
                    latitude = latLng.latitude
                    longitide = latLng.longitude
                    Log.d("id",place.id)
                    Log.d("address",place.address.toString())
//                    Log.d("locale",place.locale.toString())
                    Log.d("id",place.name.toString())
                    Log.d("placeTypes",place.placeTypes.toString())
                    Log.d("phoneNumber",place.phoneNumber.toString())
                    showAddressDialog(place.address !!.toString())
                }
                else -> {
                }
            }
        }
    }

    private fun showAddressDialog(address: String) {
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_address , null)
        val et_selected_address = dialogView.findViewById<EditText>(R.id.et_selected_address)
        et_selected_address.setText(address)
        val alertDialog = AlertDialog.Builder(mActivity).create()
        alertDialog.setView(dialogView)
        alertDialog.setCancelable(false)
        alertDialog.setTitle("Please Enter Your  Mailing /Postal Address")
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE , mActivity.resources.getString(R.string.ok)) { dialogInterface , i ->
            et_address.setText(et_selected_address.text.toString())
            dialogInterface.dismiss()
        }
        alertDialog.show()
    }

    private fun callSignUpAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.VERIFICATION_MAIL] = isNeedToVerifyEmail
        //hashMap[ApiParam.PASSWORD] = password

        if (email.isRequiredField()) {
            hashMap[ApiParam.EMAIL] = email
        }

        /*if (mobile.isRequiredField()) {
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            *//*hashMap[ApiParam.MOBILE] = mobile*//*
        }*/

        if (mActivity.isUserTypeCustomer) {

            tag = ApiParam.Customer.TAG_SIGN_UP
            hashMap[ApiParam.NAME] = name
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = et_mobile.text.toString()
            hashMap[ApiParam.REFERRAL_CODE] = et_referral_code.text.toString()
            if (userSocialDetails != null) {
                with(userSocialDetails !!) {
                    hashMap[ApiParam.LOGIN_TYPE] = ApiParam.LOGIN_TYPE_GOOGLE
                    hashMap[ApiParam.GOOGLE_ID] = userSocialDetails !!.google_id
                }
            } else {
                hashMap[ApiParam.PASSWORD] = password
                hashMap[ApiParam.LOGIN_TYPE] = ApiParam.LOGIN_TYPE_NORMAL
            }
        } else {
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
            tag = ApiParam.Merchant.TAG_SIGN_UP
            if (userSocialDetails != null) {
                with(userSocialDetails !!) {
                    hashMap[ApiParam.LOGIN_TYPE] = ApiParam.LOGIN_TYPE_GOOGLE
                    hashMap[ApiParam.GOOGLE_ID] = userSocialDetails !!.google_id
                }
            } else {
                hashMap[ApiParam.LOGIN_TYPE] = ApiParam.LOGIN_TYPE_NORMAL
                hashMap[ApiParam.PASSWORD] = password
            }
            hashMap[ApiParam.BUSINESS_NAME] = businessName
            if (tagLine.isRequiredField()) {
                hashMap[ApiParam.TAG_LINE] = tagLine
            }
            hashMap[ApiParam.WEBSITE] = website
            hashMap[ApiParam.BUSINESS_CATEGORY_ID] = "${SMarket.appDB.businessCategoryDao().getIdByName(businessCategory)}"
            hashMap[ApiParam.ADDRESS] = address
            hashMap[ApiParam.LATITUDE] = "$latitude"
            hashMap[ApiParam.LONGITUDE] = "$longitide"
            hashMap[ApiParam.POST_CODE] = postCode
            hashMap[ApiParam.DESCRIPTION] = description
            hashMap[ApiParam.PRODUCT_AND_SERVICES] = productAndServices
        }

        signUpObservable = if (picture == null) {
            WebApiClient.webApi().signUpAPI(tag , hashMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        } else {
            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data") , picture !!)
            val body: MultipartBody.Part = MultipartBody.Part.createFormData(if (mActivity.isUserTypeCustomer) {
                ApiParam.PROFILE_PIC
            } else {
                ApiParam.BUSINESS_LOGO
            } , picture !!.name , requestFile)
            WebApiClient.webApi().signUpAPI(tag , hashMap , body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        }

        disposable = signUpObservable.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    kotlin.with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                if (data != null) {

                                                    val userDetails = data !!
                                                    userDetails.token = token

                                                    mActivity.navigateToMainActivity(userDetails)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                            ApiParam.META_UNVERIFIED_USER -> {
                                                if (mActivity.isUserTypeCustomer) {
                                                    if (userSocialDetails?.google_id.isNullOrEmpty()) {
                                                        mActivity.navigateToVerifyOTPFragment(countryCode = countryCode , mobile = mobile)
                                                    } else {
                                                        if (mobile != "") {
                                                            mActivity.navigateToVerifyOTPFragment(countryCode = countryCode , mobile = mobile)
                                                        } else {
                                                            val userDetails = data !!
                                                            userDetails.token = token
                                                            mActivity.navigateToMainActivity(userDetails)
                                                        }
                                                    }
                                                } else {
                                                    mActivity.navigateToVerifyOTPFragment(email)
                                                }
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
        )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}