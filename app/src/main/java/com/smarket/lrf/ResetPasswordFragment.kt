package com.smarket.lrf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.*
import com.smarket.R
import com.smarket.utils.showDialog
import com.smarket.utils.showDialogWithAction
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_reset_password.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 20/6/18.
 */
class ResetPasswordFragment : BaseLRFFragment(), View.OnClickListener {

    companion object {
        fun getInstance(email: String = "", countryCode: String = "", mobile: String = ""): ResetPasswordFragment {
            val fragment = ResetPasswordFragment()
            val bundle = Bundle()
            if (email.isRequiredField()) {
                bundle.putString("email", email)
            }
            if (countryCode.isRequiredField()) {
                bundle.putString("countryCode", countryCode)
            }
            if (mobile.isRequiredField()) {
                bundle.putString("mobile", mobile)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    var email = ""
    var countryCode = ""
    var mobile = ""
    var otp = ""
    var newPassword = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        btn_update_password.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("email")) {
                email = bundle.getString("email") !!
            }
            if (bundle.containsKey("countryCode")) {
                countryCode = bundle.getString("countryCode") !!
            }
            if (bundle.containsKey("mobile")) {
                mobile = bundle.getString("mobile") !!
            }
        }
    }

    private fun setHeader() {
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.setText(R.string.reset_password)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_update_password -> {
                if (checkForValidations()) {
                    callResetPasswordAPI()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        otp = et_otp.text.toString()
        newPassword = et_new_password.text.toString()
        val confirmPassword = et_confirm_password.text.toString()

        if (!otp.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.otp_req))
            return false
        } else if (newPassword.isEmpty()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.new_password_req))
            return false
        } else if (!newPassword.isValidPassword()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_new_password_req))
            return false
        } else if (confirmPassword.isEmpty()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.confirm_password_req))
            return false
        } else if (newPassword != confirmPassword) {
            mActivity.showDialog(mActivity.resources.getString(R.string.reset_passwords_mismatch))
            return false
        }

        return true
    }

    private fun callResetPasswordAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.OTP] = otp
        hashMap[ApiParam.PASSWORD] = newPassword

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_RESET_PASSWORD
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
        } else {
            tag = ApiParam.Merchant.TAG_RESET_PASSWORD
            hashMap[ApiParam.EMAIL] = email
        }

        disposable = WebApiClient.webApi().resetPasswordAPI(tag, hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    mActivity.showDialogWithAction(message) {
                                                        mActivity.popBackStack(null)
                                                    }

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}