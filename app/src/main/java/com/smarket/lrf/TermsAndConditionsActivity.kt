package com.smarket.lrf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.CheckBox
import com.smarket.R
import kotlinx.android.synthetic.main.activity_terms_conditions.*

class TermsAndConditionsActivity : AppCompatActivity() , View.OnClickListener {

    var MyPREFERANCE = "Pref"
    lateinit var sharedPreferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_conditions)

        sharedPreferences = getSharedPreferences(MyPREFERANCE, Context.MODE_PRIVATE);

        chk_terms.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v is CheckBox) {
            val checked: Boolean = v.isChecked
            when (v.id) {
                R.id.chk_terms -> {
                    if (checked) {
                        val editor = sharedPreferences.edit()
                        editor.putBoolean("visible", false)
                        editor.apply()

                        val intent = Intent(this , TutorialActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
                        finish()

                    } else {
                        val editor = sharedPreferences.edit()
                        editor.putBoolean("visible", true)
                        editor.apply()
                    }
                }
            }
        }
    }
}