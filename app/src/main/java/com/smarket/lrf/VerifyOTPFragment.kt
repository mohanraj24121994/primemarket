package com.smarket.lrf

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.LinkBuilder
import com.smarket.utils.*
import com.smarket.R
import com.smarket.utils.showDialog
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_verify_otp.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 20/6/18.
 */
class VerifyOTPFragment : BaseLRFFragment() , View.OnClickListener {

    companion object {
        fun getInstance(email: String = "" , countryCode: String = "" , mobile: String = ""): VerifyOTPFragment {
            val fragment = VerifyOTPFragment()
            val bundle = Bundle()
            if (email.isRequiredField()) {
                bundle.putString("email" , email)
            }
            if (countryCode.isRequiredField()) {
                bundle.putString("countryCode" , countryCode)
            }
            if (mobile.isRequiredField()) {
                bundle.putString("mobile" , mobile)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var et_mobile: EditText
    lateinit var et_email: EditText

    var oldEmail = ""
    var oldCountryCode = ""
    var oldMobile = ""
    var email = ""
    var countryCode = ""
    var mobile = ""
    var otp = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_verify_otp , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        if (mActivity.isUserTypeCustomer) {
            tv_instruction.setText(R.string.verify_otp_instruction_customer)
            tv_mobile_email.setCompoundDrawablesWithIntrinsicBounds(0 , 0 , R.drawable.edit_mobile_number , 0)
        } else {
            tv_instruction.setText(R.string.verify_otp_instruction_merchant)
            tv_mobile_email.setCompoundDrawablesWithIntrinsicBounds(0 , 0 , R.drawable.edit_email_address , 0)
        }

        setMobileEmailData()

        setUpResendOTPText()

        tv_mobile_email.setOnClickListener(this)
        btn_verify_now.setOnClickListener(this)
        tv_resend_otp.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("email")) {
                oldEmail = bundle.getString("email") !!
                email = oldEmail
            }
            if (bundle.containsKey("countryCode")) {
                oldCountryCode = bundle.getString("countryCode") !!
                countryCode = oldCountryCode
            }
            if (bundle.containsKey("mobile")) {
                oldMobile = bundle.getString("mobile") !!
                mobile = oldMobile
            }
        }
    }

    private fun setHeader() {
        if (mActivity.isUserTypeCustomer) {
            tv_title.setText(R.string.verify_mobile_number)
        } else {
            tv_title.setText(R.string.verify_email_address)
        }
    }

    private fun setUpResendOTPText() {

        val linkResendOTP = Link(mActivity.resources.getString(R.string.resend_otp))
                .setTextColor(if (mActivity.isUserTypeCustomer) {
                    mActivity.resources.getColor(R.color.customer_app_color)
                } else {
                    mActivity.resources.getColor(R.color.merchant_app_color)
                })

        LinkBuilder.on(tv_resend_otp)
                .addLink(linkResendOTP)
                .build()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_mobile_email -> showChangeMobileEmailDialog()
            R.id.btn_verify_now -> {
                if (checkForValidations()) {
                    callVerifyAccountAPI()
                }
            }
            R.id.tv_resend_otp -> callResendOTPAPI()
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        if (mActivity.isUserTypeCustomer) {

            if (! mobile.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                return false
            } else if (! mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                return false
            }

        } else {

            if (! email.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
                return false
            } else if (! email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            }
        }

        otp = et_otp.text.toString()

        if (! otp.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.otp_req))
            return false
        }

        return true
    }

    private fun showChangeMobileEmailDialog() {
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_change_mobile_email , null)
        val cl_mobile_number = dialogView.findViewById<ConstraintLayout>(R.id.cl_mobile_number)
        val sp_country_code = dialogView.findViewById<AppCompatSpinner>(R.id.sp_country_code)
        et_mobile = dialogView.findViewById(R.id.et_mobile)
        et_email = dialogView.findViewById(R.id.et_email)
        var newCountryCode = ""
        if (mActivity.isUserTypeCustomer) {
            et_email.visibility = View.GONE
            mActivity.setCountryCodeSpinner(sp_country_code)
            sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>? , view: View? , position: Int , id: Long) {
                    newCountryCode = mActivity.countryCodeList[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
            mActivity.setSelectedCountyCode(sp_country_code , countryCode)
            et_mobile.setText(mobile)
        } else {
            cl_mobile_number.visibility = View.GONE
            et_email.setText(email)
        }
        val alertDialog = AlertDialog.Builder(mActivity).create()
        alertDialog.setView(dialogView)
        alertDialog.setCancelable(false)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE , mActivity.resources.getString(R.string.ok)) { dialogInterface , i ->
            if (mActivity.isUserTypeCustomer) {
                val newMobile = et_mobile.text.toString().trim()
                if (! newMobile.isRequiredField()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                    return@setButton
                } else if (! newMobile.isValidMobile()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                    return@setButton
                } else {
                    if (oldCountryCode != newCountryCode || oldMobile != newMobile) {
                        callEditMobileAPI(newCountryCode , newMobile)
                    }
                }
            } else {
                val newEmail = et_email.text.toString().trim()
                if (! newEmail.isRequiredField()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
                    return@setButton
                } else if (! newEmail.isValidEmail()) {
                    mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                    return@setButton
                } else {
                    if (oldEmail != newEmail) {
                        callEditEmailAPI(newEmail)
                    }
                }
            }
            dialogInterface.dismiss()
        }
        alertDialog.show()
    }

    private fun setMobileEmailData() {
        if (mActivity.isUserTypeCustomer) {
            tv_mobile_email.text = "$countryCode $mobile"
        } else {
            tv_mobile_email.text = email
        }
    }

    private fun callResendOTPAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_RESEND_OTP
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
        } else {
            if (mActivity.isMerChantLogin) {
                tag = ApiParam.Merchant.TAG_LOGIN_OTP
                hashMap[ApiParam.EMAIL] = email
            } else {
                tag = ApiParam.Merchant.TAG_RESEND_OTP
                hashMap[ApiParam.EMAIL] = email
            }
        }

        disposable = WebApiClient.webApi().resendOTPAPI(tag , hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callEditEmailAPI(newEmail: String) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.EditEmail.OLD_EMAIL] = oldEmail
        hashMap[ApiParam.EditEmail.NEW_EMAIL] = newEmail

        disposable = WebApiClient.webApi().editEmailAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            kotlin.with(meta !!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    oldEmail = newEmail
                                                    email = oldEmail

                                                    setMobileEmailData()

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callEditMobileAPI(newCountryCode: String , newMobile: String) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.EditMobile.OLD_COUNTRY_CODE] = oldCountryCode
        hashMap[ApiParam.EditMobile.OLD_MOBILE] = oldMobile
        hashMap[ApiParam.EditMobile.NEW_COUNTRY_CODE] = newCountryCode
        hashMap[ApiParam.EditMobile.NEW_MOBILE] = newMobile

        disposable = WebApiClient.webApi().editMobileAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            kotlin.with(meta !!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    oldCountryCode = newCountryCode
                                                    oldMobile = newMobile
                                                    countryCode = oldCountryCode
                                                    mobile = oldMobile

                                                    setMobileEmailData()

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callVerifyAccountAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.OTP] = otp

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_VERIFY_ACCOUNT
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
        } else {
            if (mActivity.isMerChantLogin) {
                tag = ApiParam.Merchant.LOGIN_OTP_VERIFY
            } else {
                tag = ApiParam.Merchant.TAG_VERIFY_ACCOUNT
                hashMap[ApiParam.EMAIL] = email
            }
        }

        disposable = WebApiClient.webApi().verifyAccountAPI(tag , hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            kotlin.with(meta !!) {
                                                if (status == ApiParam.META_SUCCESS) {
                                                    if (data != null) {

                                                        val userDetails = data !!
                                                        userDetails.token = token

                                                        mActivity.navigateToMainActivity(userDetails)

                                                    } else {
                                                        mActivity.showDialog(message)
                                                    }
                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}