package com.smarket.lrf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import com.smarket.R
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.activity_tutorial.tv_next
import kotlinx.android.synthetic.main.fragment_cust_home.*


class TutorialActivity : AppCompatActivity() {

    var tutorialImages = intArrayOf(R.drawable.tutorial_one , R.drawable.tutorial_two , R.drawable.tutorial_three)
    var MyPREFERANCE = "Pref"
    lateinit var sharedPreferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        sharedPreferences = getSharedPreferences(MyPREFERANCE, Context.MODE_PRIVATE);

        val carouselView = findViewById<CarouselView>(R.id.carouselView)
        carouselView.pageCount = tutorialImages.size
        carouselView.setImageListener(imageListener)

        carouselView.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }
            override fun onPageScrolled(p0: Int , p1: Float , p2: Int) {
            }
            override fun onPageSelected(p0: Int) {

                if (p0 == 2) {
                    tv_next.text = resources!!.getString(R.string.next_under)
                    iv_right.visibility = View.GONE
                } else if (p0 == 1) {
                    tv_next.text = resources !!.getString(R.string.skip)
                    iv_left.visibility = View.VISIBLE
                    iv_right.visibility = View.VISIBLE
                } else {
                    iv_left.visibility = View.GONE
                    tv_next.text = resources !!.getString(R.string.skip)
                }
            }
        })

        tv_next.setOnClickListener {

            val editor = sharedPreferences.edit()
            editor.putBoolean("visible", false)
            editor.apply()

            val intent = Intent(this , SelectUserTypeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
            finish()
        }
    }

    var imageListener: ImageListener = ImageListener { position , imageView ->
        imageView.setImageResource(tutorialImages[position])
        imageView.setScaleType(ImageView.ScaleType.FIT_XY)
    }
}