package com.smarket.lrf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.MainActivity
import com.smarket.utils.PreferenceUtil
import com.smarket.utils.PreferenceUtil.Companion.setPref

class SplashActivity : AppCompatActivity() {

    lateinit var splashHandler: Handler
    lateinit var runnable: Runnable
    var MyPREFERANCE = "Pref"
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPreferences = getSharedPreferences(MyPREFERANCE , Context.MODE_PRIVATE)
        val splash = sharedPreferences.getString("countryCode" , "")
        val getCountry = applicationContext.resources.configuration.locale.country

        if (splash == "" && PreferenceUtil.getStringPref("shareURL") == "") {
            if (getCountry != "IN") {
                setTheme(R.style.SplashThemeALL)
            } else {
                setTheme(R.style.SplashTheme)
            }
        } else {
            if (splash != "+91") {
                setTheme(R.style.SplashThemeALL)
            } else {
                setTheme(R.style.SplashTheme)
            }
        }

        super.onCreate(savedInstanceState)

        splashHandler = Handler()

        runnable = Runnable {
            navigateToNextActivity()
        }

        splashHandler.postDelayed(runnable , 3000)
    }

    private fun navigateToNextActivity() {
        val prefs = SMarket.appContext.getSharedPreferences(PreferenceUtil.prefID , Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.remove("shareURL")
        editor.apply()
        editor.commit()

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent) // Handle text being sent
                } else if (intent.type?.startsWith("image/") == true) {
                    handleSendImage(intent) // Handle single image being sent
                }
            }
            intent?.action == Intent.ACTION_SEND_MULTIPLE
                    && intent.type?.startsWith("image/") == true -> {
                handleSendMultipleImages(intent) // Handle multiple images being sent
            }
            else -> {
                // Handle other intents, such as being started from the home screen
            }
        }

        val userDetails = SMarket.appDB.userDao().getData()

        if (userDetails == null) {
            val isVisible = sharedPreferences.getBoolean("visible" , true)
            if (isVisible) {
                navigateTermsConditionsActivity()
            } else {
                navigateToSelectUserTypeActivity()
            }
        } else {
            navigateToMainActivity()
        }

        //forceUpdate()
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            setPref("shareURL" , "" + it)
        }
    }

    private fun handleSendImage(intent: Intent) {
        (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let {
        }
    }

    private fun handleSendMultipleImages(intent: Intent) {
        intent.getParcelableArrayListExtra<Parcelable>(Intent.EXTRA_STREAM)?.let {
        }
    }

    fun navigateToSelectUserTypeActivity() {
        val intent = Intent(this , SelectUserTypeActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }

    fun navigateTutorialActivity() {
        val intent = Intent(this , TutorialActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }

    fun navigateTermsConditionsActivity() {
        val intent = Intent(this , TermsAndConditionsActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }

    fun navigateToMainActivity() {
        val intent = Intent(this , MainActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        splashHandler.removeCallbacks(runnable)
    }

    fun forceUpdate() {
        val packageManager: PackageManager = this.packageManager
        var packageInfo: PackageInfo? = null
        try {
            packageInfo = packageManager.getPackageInfo(packageName , 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val currentVersion: String = packageInfo !!.versionName
        ForceUpdateAsync(currentVersion , this , sharedPreferences).execute()
    }
}