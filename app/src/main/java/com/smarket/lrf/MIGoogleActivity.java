package com.smarket.lrf;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by mind on 15/5/17.
 */

public class MIGoogleActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

//    ========================= Detail ============================

    // If need to do put release sha 1 key the follow following command terminal.
    // For release:  keytool -exportcert -keystore ./app/mainlist.jks -list -v
    // For debug: keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android

//    ========================= Detail ============================

    // Request code for the google login.
    public static final int GOOGLE_REQUEST_CODE = 102;

    // It's Parcelable intent key.
    // Need to check for the detail and u can get detail from this class.
    public static final String KEY_GOOGLE_SIGNIN_ACCOUNT = "GoogleSignInAccount";

    // Runtime permission request code.
    final int GOOGLE_PERMISSION_CODE = 103;

    GoogleSignInClient mGoogleSignInClient = null;
    public GoogleSignInClient googleSignInClient;
    public GoogleSignInOptions gso;


    public static void startGoogleActivityForResult(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), MIGoogleActivity.class);
        fragment.startActivityForResult(intent, GOOGLE_REQUEST_CODE);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleSignInClient = getGoogleSignInClient(this);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, GOOGLE_PERMISSION_CODE);
            } else {
                googlePlusSignIn();
            }
        } else {
            googlePlusSignIn();
        }*/

        googlePlusSignIn();
    }

    /**
     * Doing intent for the google sign in.
     */
    private void googlePlusSignIn() {
        mGoogleSignInClient = getGoogleSignInClient(this);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_REQUEST_CODE);
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GOOGLE_PERMISSION_CODE:
                if (permissions.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        googlePlusSignIn();
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MIGoogleActivity.this, Manifest.permission.GET_ACCOUNTS)) {

                        } else {
                            Toast.makeText(MIGoogleActivity.this, "Need to grant Account Permission from app info for sign in with Google.", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
*/
    /**
     * @return: getting sign in option with request email.
     */
    public GoogleSignInOptions getGoogleSignInOptions() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("670027731026-ns19dgct538jckmlb06a26bln4gdset2.apps.googleusercontent.com")
                .requestEmail()
                .build();
        return gso;
    }

    /**
     * @param listener: connection failed listener.
     * @return: googleSignInClient object.
     */
    public GoogleSignInClient getGoogleSignInClient(GoogleApiClient.OnConnectionFailedListener listener) {
        if (googleSignInClient == null) {
            googleSignInClient = GoogleSignIn.getClient(this, getGoogleSignInOptions());
        }
        return googleSignInClient;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_REQUEST_CODE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            finish();
        }
    }

    /**
     * Handle results after getting intent from google.
     *
     * @param completedTask: GoogleSignInResult object.
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            if (account != null) {
                if (account.getEmail() != null && !account.getEmail().equalsIgnoreCase("")) {
                    setResultForGoogle(account);
                } else {
                    // If user not grant gmail permission from his/her gmail account.
                    Toast.makeText(MIGoogleActivity.this, "Please grant EmailID permission from gmail for login.", Toast.LENGTH_SHORT).show();
                }
            } else {
                finish();
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.d("Exception",e.getMessage());
            finish();
        }
    }

    /**
     * Sign out from the google.
     */
    public void signOutFromGoogle() {
        final GoogleSignInClient googleSignInClient = getGoogleSignInClient(this);
        googleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Log.d("MIGoogleActivity", connectionResult.getErrorMessage());
        finish();
    }

    /**
     * Set results after doing all process.
     *
     * @param acct:    GoogleSignInAccount object. Can get detail from this class.
     */
    private void setResultForGoogle(GoogleSignInAccount acct) {
        Intent intent = new Intent();
        intent.putExtra(KEY_GOOGLE_SIGNIN_ACCOUNT, acct);
        setResult(GOOGLE_REQUEST_CODE, intent);
        signOutFromGoogle();
        finish();
    }
}