package com.smarket

import android.app.Activity
import android.app.Application
import androidx.room.Room
import android.content.Context
import android.os.Bundle
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.mi.mycontacts.Contacts
import com.smarket.database.AppDB
import com.smarket.main.MainActivity
import com.smarket.webservices.ApiUtil

/**
 * Created by MI-062 on 18/6/18.
 */
class SMarket : Application() {

    companion object {

        lateinit var appContext: Context

        lateinit var appDB: AppDB
    }

    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext

        initFresco()

        initContacts()

        initRoomDB()

        registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
    }

    private fun initFresco() {
        val config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build()
        Fresco.initialize(this , config)
    }

    private fun initContacts() {
        Contacts.initialize(this)
    }

    private fun initRoomDB() {
        appDB = Room.databaseBuilder(this , AppDB::class.java , "smarket-database")
                .allowMainThreadQueries().fallbackToDestructiveMigration().build()
    }

    /*private var activityLifecycleCallbacks = object :ActivityLifecycleCallbacks{

        override fun onActivityPaused(activity: Activity?) {
        }

        override fun onActivityResumed(activity: Activity?) {

            ApiUtil.callBusinessCategoryAPI()

            if (activity is MainActivity) {
                ApiUtil.callCMSAPI()
                ApiUtil.callFAQAPI(activity)
            }
        }

        override fun onActivityStarted(activity: Activity?) {
        }

        override fun onActivityDestroyed(activity: Activity?) {
        }

        override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        }

        override fun onActivityStopped(activity: Activity?) {
        }

        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        }
    }*/
    private var activityLifecycleCallbacks = object : ActivityLifecycleCallbacks {

        override fun onActivityPaused(activity: Activity) {

        }
        override fun onActivityStarted(activity: Activity) {

        }
        override fun onActivityDestroyed(activity: Activity) {

        }
        override fun onActivitySaveInstanceState(activity: Activity , p1: Bundle) {

        }
        override fun onActivityStopped(activity: Activity) {

        }
        override fun onActivityCreated(activity: Activity , p1: Bundle?) {

        }
        override fun onActivityResumed(activity: Activity) {
            ApiUtil.callBusinessCategoryAPI()
            if (activity is MainActivity) {
                ApiUtil.callCMSAPI()
                ApiUtil.callFAQAPI(activity)
            }
        }
    }
}