package com.smarket.webservices.response

import com.smarket.model.ReferralAlertDetails
import com.smarket.model.Referrals

/**
 * Created by mind on 29/8/18.
 */
class ReferralDetailsResponse : BaseResponse() {

    var data = ArrayList<Referrals>()
}