package com.smarket.webservices.response

import com.smarket.model.Links
import com.smarket.model.Meta

/**
 * Created by MI-062 on 31/7/18.
 */
open class BaseResponse {

    var meta: Meta? = null

    var links: Links? = null
}