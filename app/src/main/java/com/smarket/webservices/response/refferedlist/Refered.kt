package com.smarket.webservices.response.refferedlist

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Refered {

    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("google_id")
    @Expose
    private var googleId: String? = null

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("email")
    @Expose
    private var email: String? = null

    @SerializedName("country_code")
    @Expose
    private var countryCode: String? = null

    @SerializedName("mobile_number")
    @Expose
    private var mobileNumber: String? = null

    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("profile_pic")
    @Expose
    private var profilePic: String? = null

    @SerializedName("otp")
    @Expose
    private var otp: Int? = null

    @SerializedName("verify_code")
    @Expose
    private var verifyCode: String? = null

    @SerializedName("reset_token")
    @Expose
    private var resetToken: String? = null

    @SerializedName("type")
    @Expose
    private var type: String? = null

    @SerializedName("push_notification")
    @Expose
    private var pushNotification: String? = null

    @SerializedName("refcash")
    @Expose
    private var refcash: Int? = null

    @SerializedName("parter_id")
    @Expose
    private var parterId: Int? = null

    @SerializedName("parter_percent")
    @Expose
    private var parterPercent: Int? = null

    @SerializedName("status_id")
    @Expose
    private var statusId: Int? = null

    @SerializedName("stripe_cus_id")
    @Expose
    private var stripeCusId: String? = null

    @SerializedName("referenced_by")
    @Expose
    private var referencedBy: Int? = null

    @SerializedName("referral_code")
    @Expose
    private var referralCode: String? = null

    @SerializedName("deleted_at")
    @Expose
    private var deletedAt: Any? = null

    @SerializedName("created_at")
    @Expose
    private var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    private var updatedAt: String? = null

    @SerializedName("refer_percentage")
    @Expose
    var refer_percentage: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getGoogleId(): String? {
        return googleId
    }

    fun setGoogleId(googleId: String?) {
        this.googleId = googleId
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getCountryCode(): String? {
        return countryCode
    }

    fun setCountryCode(countryCode: String?) {
        this.countryCode = countryCode
    }

    fun getMobileNumber(): String? {
        return mobileNumber
    }

    fun setMobileNumber(mobileNumber: String?) {
        this.mobileNumber = mobileNumber
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getProfilePic(): String? {
        return profilePic
    }

    fun setProfilePic(profilePic: String?) {
        this.profilePic = profilePic
    }

    fun getOtp(): Int? {
        return otp
    }

    fun setOtp(otp: Int?) {
        this.otp = otp
    }

    fun getVerifyCode(): String? {
        return verifyCode
    }

    fun setVerifyCode(verifyCode: String?) {
        this.verifyCode = verifyCode
    }

    fun getResetToken(): String? {
        return resetToken
    }

    fun setResetToken(resetToken: String?) {
        this.resetToken = resetToken
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }

    fun getPushNotification(): String? {
        return pushNotification
    }

    fun setPushNotification(pushNotification: String?) {
        this.pushNotification = pushNotification
    }

    fun getRefcash(): Int? {
        return refcash
    }

    fun setRefcash(refcash: Int?) {
        this.refcash = refcash
    }

    fun getParterId(): Int? {
        return parterId
    }

    fun setParterId(parterId: Int?) {
        this.parterId = parterId
    }

    fun getParterPercent(): Int? {
        return parterPercent
    }

    fun setParterPercent(parterPercent: Int?) {
        this.parterPercent = parterPercent
    }

    fun getStatusId(): Int? {
        return statusId
    }

    fun setStatusId(statusId: Int?) {
        this.statusId = statusId
    }

    fun getStripeCusId(): String? {
        return stripeCusId
    }

    fun setStripeCusId(stripeCusId: String?) {
        this.stripeCusId = stripeCusId
    }

    fun getReferencedBy(): Int? {
        return referencedBy
    }

    fun setReferencedBy(referencedBy: Int?) {
        this.referencedBy = referencedBy
    }

    fun getReferralCode(): String? {
        return referralCode
    }

    fun setReferralCode(referralCode: String?) {
        this.referralCode = referralCode
    }

    fun getDeletedAt(): Any? {
        return deletedAt
    }

    fun setDeletedAt(deletedAt: Any?) {
        this.deletedAt = deletedAt
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String?) {
        this.updatedAt = updatedAt
    }
}