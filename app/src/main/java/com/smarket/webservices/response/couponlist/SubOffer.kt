package com.smarket.webservices.response.couponlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class SubOffer(
    @SerializedName("id")
    @Expose
    var id: Int? = null ,

    @SerializedName("parent_id")
    @Expose
    var parentId: Int? = null ,

    @SerializedName("sub_offer_type")
    @Expose
    var subOfferType: String? = null ,

    @SerializedName("sub_offer_category")
    @Expose
    var subOfferCategory: String? = null ,

    @SerializedName("amount")
    @Expose
    var amount: String? = null ,

    @SerializedName("title")
    @Expose
    var title: String? = null ,

    @SerializedName("coupon_image")
    @Expose
    var couponImage: String? = null ,

    @SerializedName("conditions")
    @Expose
    var conditions: String? = null ,

    @SerializedName("is_redemption")
    @Expose
    var isRedemption: String? = null
)

