package com.smarket.webservices.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Data {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null

    @SerializedName("merchant_name")
    @Expose
    var merchantName: String? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("rewards_value")
    @Expose
    var rewardsValue: String? = null

    @SerializedName("product_url")
    @Expose
    var productUrl: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("device_type")
    @Expose
    var deviceType: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: Any? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: Any? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

}