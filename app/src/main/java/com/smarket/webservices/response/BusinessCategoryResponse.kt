package com.smarket.webservices.response

import com.smarket.database.BusinessCategory

/**
 * Created by MI-062 on 31/7/18.
 */
class BusinessCategoryResponse : BaseResponse() {

    var data = ArrayList<BusinessCategory>()
}