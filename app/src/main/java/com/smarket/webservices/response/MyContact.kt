package com.smarket.webservices.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 14/9/18.
 */
class MyContact() : Parcelable {

    @SerializedName("contact_list")
    var contactList = ArrayList<ContactDetails>()

    @SerializedName("smarket_contact_list")
    var smarketContactList = ArrayList<ContactDetails>()

    @SerializedName("other_contact_list")
    var otherContactList = ArrayList<ContactDetails>()

    constructor(parcel: Parcel) : this() {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyContact> {
        override fun createFromParcel(parcel: Parcel): MyContact {
            return MyContact(parcel)
        }

        override fun newArray(size: Int): Array<MyContact?> {
            return arrayOfNulls(size)
        }
    }
}