package com.smarket.webservices.response

import com.smarket.model.RedemptionHistory

/**
 * Created by mind on 24/8/18.
 */
class RedemptionHistoryResponse : BaseResponse() {

    var data = ArrayList<RedemptionHistory>()
}