package com.smarket.webservices.response.cashbackoffer

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Datum (
    @SerializedName("id")
    @Expose
    var id: Int? = null,
    @SerializedName("merchant_id")
    @Expose
    var merchantId: Int? = null,
    @SerializedName("status")
    @Expose
    var status: String? = null,
    @SerializedName("offer_percentage")
    @Expose
    var offerPercentage: Int? = null,
    @SerializedName("offer_condition")
    @Expose
    var offerCondition: String? = null
)