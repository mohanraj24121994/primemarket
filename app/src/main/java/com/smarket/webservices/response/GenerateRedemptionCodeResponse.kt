package com.smarket.webservices.response

import com.smarket.model.Referrals

/**
 * Created by mind on 28/8/18.
 */
class GenerateRedemptionCodeResponse : BaseResponse() {

    var data: Referrals? = null
}