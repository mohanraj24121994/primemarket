package com.smarket.webservices.response

import com.google.gson.annotations.SerializedName
import com.smarket.model.BaseMerchantDetails

/**
 * Created by mind on 25/9/18.
 */
class RedeemRefCashHistory : BaseMerchantDetails(){

    var id = 0L

    @SerializedName("transaction_time")
    var transactionTime = ""

    var amount = 0.0

    var status = 1

}