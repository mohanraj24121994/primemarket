package com.smarket.webservices.response.memberlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Meta {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
}