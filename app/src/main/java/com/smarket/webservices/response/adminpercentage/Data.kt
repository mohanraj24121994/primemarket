package com.smarket.webservices.response.adminpercentage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Data {
    @SerializedName("rewardpercent")
    @Expose
    var rewardpercent: Rewardpercent? = null
}