package com.smarket.webservices.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductResponse {

    @SerializedName("merchantName")
    @Expose
    private var merchantName: String? = null
    @SerializedName("canonicalUrl")
    @Expose
    private var canonicalUrl: String? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("upc")
    @Expose
    private var upc: Any? = null
    @SerializedName("sku")
    @Expose
    private var sku: String? = null
    @SerializedName("url")
    @Expose
    private var url: String? = null
    @SerializedName("price")
    @Expose
    private var price: String? = null
    @SerializedName("imageUrl")
    @Expose
    private var imageUrl: String? = null
    @SerializedName("brandName")
    @Expose
    private var brandName: String? = null
    @SerializedName("description")
    @Expose
    private var description: String? = null
    @SerializedName("canonicalDomain")
    @Expose
    private var canonicalDomain: String? = null

    fun getMerchantName(): String? {
        return merchantName
    }

    fun setMerchantName(merchantName: String?) {
        this.merchantName = merchantName
    }

    fun getCanonicalUrl(): String? {
        return canonicalUrl
    }

    fun setCanonicalUrl(canonicalUrl: String?) {
        this.canonicalUrl = canonicalUrl
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getUpc(): Any? {
        return upc
    }

    fun setUpc(upc: Any?) {
        this.upc = upc
    }

    fun getSku(): String? {
        return sku
    }

    fun setSku(sku: String?) {
        this.sku = sku
    }

    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url
    }

    fun getPrice(): String? {
        return price
    }

    fun setPrice(price: String?) {
        this.price = price
    }

    fun getImageUrl(): String? {
        return imageUrl
    }

    fun setImageUrl(imageUrl: String?) {
        this.imageUrl = imageUrl
    }

    fun getBrandName(): String? {
        return brandName
    }

    fun setBrandName(brandName: String?) {
        this.brandName = brandName
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }

    fun getCanonicalDomain(): String? {
        return canonicalDomain
    }

    fun setCanonicalDomain(canonicalDomain: String?) {
        this.canonicalDomain = canonicalDomain
    }
}