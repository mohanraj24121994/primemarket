package com.smarket.webservices.response

import com.smarket.model.MerchantDetails

/**
 * Created by mind on 27/8/18.
 */
class MerchantDetailsResponse : BaseResponse() {

    var data: MerchantDetails? = null
}