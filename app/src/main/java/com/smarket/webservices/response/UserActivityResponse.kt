package com.smarket.webservices.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserActivityResponse {
    @SerializedName("data")
    @Expose
    private var data: Data? = null
    @SerializedName("meta")
    @Expose
    private var meta: Meta? = null

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data?) {
        this.data = data
    }

    fun getMeta(): Meta? {
        return meta
    }

    fun setMeta(meta: Meta?) {
        this.meta = meta
    }
}