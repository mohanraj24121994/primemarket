package com.smarket.webservices.response

import com.smarket.model.Offer

/**
 * Created by MI-062 on 31/7/18.
 */
class MyOffersResponse : BaseResponse() {

    var data = ArrayList<Offer>()
}