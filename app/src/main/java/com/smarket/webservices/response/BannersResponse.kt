package com.smarket.webservices.response

import com.smarket.model.Banner

/**
 * Created by MI-062 on 31/7/18.
 */
class BannersResponse : BaseResponse() {

    var data = ArrayList<Banner>()
}