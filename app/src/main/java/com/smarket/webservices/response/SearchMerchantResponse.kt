package com.smarket.webservices.response

import com.smarket.model.MerchantDetails

/**
 * Created by mind on 27/8/18.
 */
class SearchMerchantResponse : BaseResponse(){

    var data = ArrayList<MerchantDetails>()
}