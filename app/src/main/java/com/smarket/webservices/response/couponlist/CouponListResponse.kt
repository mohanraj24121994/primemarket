package com.smarket.webservices.response.couponlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.smarket.webservices.response.BaseResponse


class CouponListResponse : BaseResponse() {
    @SerializedName("data")
    @Expose
    var data = ArrayList<CouponOffer>()
}