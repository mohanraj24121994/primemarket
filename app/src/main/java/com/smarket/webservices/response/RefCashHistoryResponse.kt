package com.smarket.webservices.response

import com.smarket.model.RefCashHistory

/**
 * Created by MI-062 on 31/7/18.
 */
class RefCashHistoryResponse : BaseResponse() {

    var data = ArrayList<RefCashHistory>()
}