package com.smarket.webservices.response.adminpercentage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AdminPercentage {
    @SerializedName("data")
    @Expose
    var data: Data? = null

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
}