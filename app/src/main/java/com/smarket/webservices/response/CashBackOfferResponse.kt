package com.smarket.webservices.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class CashBackOfferResponse (
    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null
)