package com.smarket.webservices.response

import com.smarket.model.Notifications


class NotificationListResponse : BaseResponse() {

    var data = ArrayList<Notifications>()
}