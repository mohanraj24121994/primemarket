package com.smarket.webservices.response.couponlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class CouponOffer(

    @SerializedName("id")
    @Expose
    var id: Long = 0L ,

    @SerializedName("offer_type")
    @Expose
    var offerType: String? = null ,

    @SerializedName("merchant_id")
    @Expose
    var merchantId: Int? = null ,

    @SerializedName("expiry_date")
    @Expose
    var expiryDate: String? = null ,

    @SerializedName("used_coupon")
    @Expose
    var usedCoupon: String? = null ,

    @SerializedName("sub_offer")
    @Expose
    var subOffer: ArrayList<SubOffer> = ArrayList() ,

    @SerializedName("merchant")
    @Expose
    var merchant: ArrayList<Merchant> = ArrayList()
)
