package com.smarket.webservices.response

data class VigilinkProductResponse(
    val brandName: String,
    val canonicalDomain: String,
    val canonicalUrl: String,
    val description: String,
    val imageUrl: String,
    val merchantName: String,
    val price: String,
    val sku: String,
    val title: String,
    val upc: Any,
    val url: String
)