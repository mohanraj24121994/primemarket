package com.smarket.webservices.response.cashbackoffer

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class ListCashBackOfferResponse (
    @SerializedName("status")
    @Expose
    val status: String? = null,

    @SerializedName("data")
    @Expose
    val data: List<Datum>? = null
)