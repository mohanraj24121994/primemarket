package com.smarket.webservices.response.adminpercentage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Rewardpercent {

    @SerializedName("customer_percent")
    @Expose
    var customerPercent: Int? = null

    @SerializedName("refer_percent")
    @Expose
    var referPercent: Int? = null

}