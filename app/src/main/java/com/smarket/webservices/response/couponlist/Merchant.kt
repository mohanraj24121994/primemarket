package com.smarket.webservices.response.couponlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Merchant(
    @SerializedName("id")
    @Expose
    var id: Int? = null ,

    @SerializedName("name")
    @Expose
    var name: String? = null ,

    @SerializedName("mobile_number ")
    @Expose
    var mobileNumber: String? = null ,

    @SerializedName("business_logo")
    @Expose
    var businessLogo: String? = null ,

    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null ,

    @SerializedName("email")
    @Expose
    var email: String? = null ,

    @SerializedName("address")
    @Expose
    var address: String? = null ,

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null ,

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null ,

    @SerializedName("business_name")
    @Expose
    var businessName: String? = null ,

    @SerializedName("category_name")
    @Expose
    var categoryName: String? = null ,

    @SerializedName("tagline")
    @Expose
    var tagline: String? = null ,

    @SerializedName("website")
    @Expose
    var website: String? = null ,

    @SerializedName("product_and_services")
    @Expose
    var productAndServices: String? = null
)


