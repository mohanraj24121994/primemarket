package com.smarket.webservices.response

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by mind on 14/9/18.
 */
 class ContactDetails() : Parcelable {

    var id = 0L

    var name = ""

    var number = ""

    var isSelected = false

    var isSmarketContact = false



    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        name = parcel.readString()!!
        number = parcel.readString()!!
        isSelected = parcel.readByte() != 0.toByte()
        isSmarketContact = parcel.readByte() != 0.toByte()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(number)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeByte(if (isSmarketContact) 1 else 0)


    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactDetails> {
        override fun createFromParcel(parcel: Parcel): ContactDetails {
            return ContactDetails(parcel)
        }

        override fun newArray(size: Int): Array<ContactDetails?> {
            return arrayOfNulls(size)
        }
    }

}