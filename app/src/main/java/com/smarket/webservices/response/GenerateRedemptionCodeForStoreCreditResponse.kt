package com.smarket.webservices.response

import com.smarket.model.OfferDetails

/**
 * Created by mind on 28/8/18.
 */
class GenerateRedemptionCodeForStoreCreditResponse : BaseResponse() {

    var data: OfferDetails? = null
}