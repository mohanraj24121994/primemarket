package com.smarket.webservices.response.memberlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MemberListResponse {
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
}