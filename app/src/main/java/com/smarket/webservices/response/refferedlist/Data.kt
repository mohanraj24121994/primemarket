package com.smarket.webservices.response.refferedlist

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Data {
    @SerializedName("refered")
    @Expose
    private var refered: List<Refered?>? = null

    fun getRefered(): List<Refered?>? {
        return refered
    }

    fun setRefered(refered: List<Refered?>?) {
        this.refered = refered
    }
}