package com.smarket.webservices.response

import com.smarket.model.RedemptionHistory
import com.smarket.model.StoreCredit

/**
 * Created by mind on 24/8/18.
 */
class StoreCreditResponse : BaseResponse() {

    var data = ArrayList<StoreCredit>()
}