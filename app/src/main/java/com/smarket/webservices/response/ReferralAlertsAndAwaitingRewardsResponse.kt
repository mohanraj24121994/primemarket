package com.smarket.webservices.response

import com.smarket.model.ReferralAlertAndAwaitingReward

/**
 * Created by MI-062 on 31/7/18.
 */
class ReferralAlertsAndAwaitingRewardsResponse : BaseResponse() {

    var data = ArrayList<ReferralAlertAndAwaitingReward>()
}