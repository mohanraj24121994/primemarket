package com.smarket.webservices

import com.smarket.model.Offer
import com.smarket.model.compsrisonresult.ComparisonResult
import com.smarket.model.getproductdetails.GetDetails
import com.smarket.model.giftcard.GiftList
import com.smarket.model.refcashredeemrequest.RefCashRedeemRequest
import com.smarket.model.refcashredeemrequest.response.RedeemRefcashResponse
import com.smarket.model.searchproduct.Searchproductresponse
import com.smarket.webservices.response.*
import com.smarket.webservices.response.adminpercentage.AdminPercentage
import com.smarket.webservices.response.cashbackoffer.ListCashBackOfferResponse
import com.smarket.webservices.response.couponlist.CouponListResponse
import com.smarket.webservices.response.memberlist.MemberListResponse
import com.smarket.webservices.response.refferedlist.ReferredList
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface WebApi {

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_BUSINESS_CATEGORY)
    fun businessCategoryAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BusinessCategoryResponse>>

    @GET(ApiParam.API_PATH + ApiParam.TAG_CMS)
    fun cmsAPI(): Observable<Response<CMSResponse>>

    @POST(ApiParam.API_PATH + ApiParam.TAG_FAQ)
    fun faqAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<FAQResponse>>

    /*@POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_SOCIAL_LOGIN)
    fun socialLoginAPI(@QueryMap hashMap: HashMap<String, String>): Observable<Response<LRFResponse>>*/

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun socialLoginAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun loginAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @Multipart
    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun signUpAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String> , @Part file: MultipartBody.Part): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun signUpAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_EDIT_EMAIL)
    fun editEmailAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_EDIT_MOBILE)
    fun editMobileAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun verifyAccountAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun resendOTPAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun forgotPasswordAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun resetPasswordAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.TAG_CHANGE_PASSWORD)
    fun changePasswordAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @Multipart
    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun editProfileAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String> , @Part file: MultipartBody.Part): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun editProfileAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + ApiParam.TAG_BANNERS)
    fun bannersAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BannersResponse>>

    @GET(ApiParam.API_PATH + ApiParam.Merchant.TAG_MY_OFFERS)
    fun myOffersAPI(): Observable<Response<MyOffersResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_DELETE_OFFER)
    fun deleteOfferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<MyOffersResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_ADD_EDIT_OFFER)
    fun addEditOfferAPI(@Body offer: Offer): Observable<Response<MyOffersResponse>>

    @GET(ApiParam.API_PATH + ApiParam.Merchant.TAG_CREATE_PDF_AND_SEND_EMAIL)
    fun createPDFAndSendEmailAPI(): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_ADD_REFCASH)
    fun addRefCashAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun refCashHistoryAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<RefCashHistoryResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_REFERRAL_ALERTS)
    fun referralAlertsAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_AWAITING_REWARDS)
    fun awaitingRewardsAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REFER_MERCHANT)
    fun referMerchantAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REDEMPTION_HISTORY)
    fun redemptionHistoryAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<RedemptionHistoryResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_STORE_CREDIT)
    fun storeCreditAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<StoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_MERCHANT_DETAILS)
    fun merchantDetailsAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<MerchantDetailsResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_SEARCH_MERCHANT)
    fun searchMerchantAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<SearchMerchantResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_STORE_CREDIT)
    fun searchStoreCreditAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<StoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_REFERRAL_ALERTS)
    fun searchReferalListAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_AWAITING_REWARDS)
    fun searchAwaitingRewardsAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>
    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_COUPON_LIST)
    fun searchCouponOfferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<CouponListResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_GENERATE_REDEMPTION_CODE_FOR_STORE_CREDIT)
    fun generateRedemptionCodeForStoreCreditAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REFERRAL_DETAILS)
    fun referralDetailsAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralDetailsResponse>>

    @Multipart
    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_RATE_MERCHANT)
    fun rateAndReferAPI(@QueryMap hashMap: HashMap<String , String> , @Part file: MultipartBody.Part): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_RATE_MERCHANT)
    fun rateAndReferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_SCAN_OFFER)
    fun scanOfferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ScanOfferResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_NOTIFICATION_ON_OFF)
    fun notificationOnOffAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REDEEM_OFFER)
    fun redeemOfferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_CONTACT_LIST)
    fun contactListAPI(@Body contact: MyContact): Observable<Response<ContactListResponse>>

    @Multipart
    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_RATE_REFERRAL_MERCHANT)
    fun rateReferralMerchantAPI(@QueryMap hashMap: HashMap<String , String> , @Part file: MultipartBody.Part): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_RATE_REFERRAL_MERCHANT)
    fun rateReferralMerchantAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_GENERATE_REDEMPTION_CODE)
    fun generateRedemptionCodeAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_OFFER_DETAIL)
    fun offerDetailAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + "{${ApiParam.TAG}}")
    fun redeemRefCashHistoryAPI(@Path(ApiParam.TAG) tag: String , @QueryMap hashMap: HashMap<String , String>): Observable<Response<RedeemRefCashHistoryResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REDEEM_REFCASH)
    fun redeemRefCashAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @GET(ApiParam.API_PATH + ApiParam.TAG_USER_DETAILS)
    fun userDetailAPI(): Observable<Response<LRFResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_ADD_REMOVE_TOKEN)
    fun addRemoveTokenAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<BaseResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_MERCHANT_RATE_REVIEW)
    fun merchantRateReviewAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<ReferralDetailsResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_NOTIFICATION)
    fun notificationListAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<NotificationListResponse>>

    @GET(ApiParam.VIGILINK_PATH)
    fun vigiLinkProductDetails(@Query("url") url: String): Call<VigilinkProductResponse?>?


    @GET(ApiParam.VIGILINK_PATH)
    fun productDetails(@Query("url") url: String): Call<ProductResponse?>?

    @POST(ApiParam.API_PATH + "user-activity")
    fun userActivity(@QueryMap(encoded = true) hashMap: HashMap<String , String>): Call<UserActivityResponse?>?

    @POST(ApiParam.API_PATH + "referedto-list")
    fun referList(@Query("user_id") userID: String): Call<ReferredList?>?

    @GET(ApiParam.API_PATH + "reward-percent")
    fun adminPercentage(): Call<AdminPercentage?>?

    @GET(ApiParam.API_PATH + "giftcard-list")
    fun giftList(): Call<GiftList?>?

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_REDEEM_REFCASH)
    fun redeemRefCash(@Body redeem: RefCashRedeemRequest): Observable<Response<RedeemRefcashResponse>>

    @GET(ApiParam.API_PATH + "html-parsing-test")
    fun getProductDetailAPI(@Query(value = "product_url" , encoded = true) url: String , @Query("country") country: String): Call<GetDetails?>?

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_SEARCH_RESULT)
    fun searchProductList(@Query("search_keyword") search_keyword: String): Call<Searchproductresponse?>?

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_SEND_MESSAGE)
    fun sendMessage(@Query("message") message : String): Call<UserActivityResponse?>?

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_KEY_LIST)
    fun comparisonResult(@Query("type") type : String): Call<ComparisonResult?>?

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_ADD_MEMBER)
    fun addMemberAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<UserActivityResponse>>

    @GET(ApiParam.API_PATH + ApiParam.Merchant.TAG_LIST_MEMBERS)
    fun listMembersAPI(): Observable<Response<MemberListResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_DELETE_MEMBERS)
    fun deleteMembersAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<UserActivityResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_UPDATE_MEMBER)
    fun updateMemberAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<UserActivityResponse>>

    @Multipart
    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_ADD_EDIT_OFFER)
    fun addEditOfferAPI(@PartMap offer: HashMap<String, RequestBody> ,@Part coupon_image: MultipartBody.Part ,@PartMap sub_offer: HashMap<String, RequestBody>): Observable<Response<MyOffersResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_COUPON_LIST)
    fun couponListAPI(): Observable<Response<CouponListResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Customer.TAG_COUPON_QR)
    fun generateRedemptionCodeForCouponOfferAPI(@QueryMap hashMap: HashMap<String , String>): Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_CREATE_CASHBACK)
    fun createCashBackAPI(@Body hashMap: HashMap<String , String>): Observable<Response<CashBackOfferResponse>>

    @POST(ApiParam.API_PATH + ApiParam.Merchant.TAG_LIST_CASHBACK)
    fun listCashBackAPI(): Observable<Response<ListCashBackOfferResponse>>

}