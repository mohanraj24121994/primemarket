package com.smarket.webservices

/**
 * Created by Mind3 on 30/4/18.
 */

data class ApiError(val message:String, val status_code:Int, val otp:Long,val token:String){
    constructor():this("",0,0,"")
}