package com.smarket.webservices

import android.accounts.NetworkErrorException
import android.content.Intent
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.lrf.LRFActivity
import com.smarket.utils.*
import retrofit2.Response
import java.io.IOException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

/**
 * Created by Mind3 on 30/4/18.
 */
object ErrorUtil {

    const val NO_INTERNET = "No internet connection."
    private const val SERVER_NOT_RESPONDING = "Server not responding."
    private const val SERVER_TIMEOUT = "Server timeout error."
    private const val SOMETHING_WENT_WRONG = "Something has gone wrong."

    private fun parseError(response: Response<*>?): ApiError {
        var error: ApiError? = null
        try {
            if (response?.errorBody() != null) {
                val errorString = response.errorBody()!!.string()
                if (errorString != null) {
                    val gson = GsonBuilder().create()
                    error = gson.fromJson<ApiError>(errorString, ApiError::class.java)
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (error == null) {
            error = ApiError()
            error.message
        }
        return error
    }

    fun error(response: Response<*>): String {
        val error = parseError(response)
        return error.message
    }

    fun showError(response: Response<*>, activity: AppCompatActivity? = null) {

        if (activity != null && response.code() == ApiParam.HTTP_UNAUTHORIZED_USER) {
            logoutAndMoveToLrf(activity)
        } else {
            showToast(error(response))
        }
    }

    fun setExceptionMessage(t: Throwable?) {
        if (t != null) {
            val errorMessage = if (t is IndexOutOfBoundsException) {
                SOMETHING_WENT_WRONG
            } else if (t is TimeoutException) {
                SERVER_TIMEOUT
            } else if (t is UnknownHostException || t is NetworkErrorException) {
                NO_INTERNET
            } else if (t is IOException || t is ConnectException) {
                SERVER_NOT_RESPONDING
            } else {
                SOMETHING_WENT_WRONG
            }
            showToast(errorMessage)
        }
    }
}