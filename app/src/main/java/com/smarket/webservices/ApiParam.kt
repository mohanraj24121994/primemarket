package com.smarket.webservices

import okhttp3.RequestBody
import java.util.*

/**
 * Created by MI-062 on 1/8/18.
 */
object ApiParam {
//https://www.smarketworld.net/SMarket/api/v1/
    /*Local*/
//    const val BASE_URL = "http://192.168.1.20"
    /*ITRAIN*/
//    const val BASE_URL = "https://itrainacademy.in"
    /*Live*/
//    const val BASE_URL = "http://18.219.0.157"
   // const val BASE_URL = "https://smarketworld.net"
    const val BASE_URL = "https://smarketdeals.com"
  //  const val BASE_URL = "https://smarketdeals.com"
   // const val BASE_URL = "https://smarketworld.colanapps.in"
    const val BASE_URL_VIGILINK = "https://rest.viglink.com/api/product/"

    /*Local & ITRAIN*/
//    const val API_PATH = "/SMarket/api/v1/"
    /*Live*/
    const val API_PATH = "/api/v1/"
    //const val API_PATH = "/SMarket/api/v1/"
 //   const val API_PATH = "/api/v1/"
   // const val API_PATH = "/api/v1/"
    const val VIGILINK_PATH = "metadata"

    const val HTTP_SUCCESS = 200
    const val HTTP_VALIDATIONS = 400
    const val HTTP_UNAUTHORIZED_USER = 401
    const val HTTP_UNVERIFIED_USER = 402
    const val HTTP_DEACTIVED_USER = 403
    const val HTTP_DELETED_USER = 405

    const val META_SUCCESS = 0
    const val META_UNAUTHORIZED_USER = 1
    const val META_UNVERIFIED_USER = 4
    const val META_UNREGISTERED_USER = 10

    const val STATUS_ACTIVE = 1

    const val TOKEN_PREFIX = "Bearer "

    const val PAGE = "page"
    const val PER_PAGE = "per_page"
    const val PER_PAGE_VALUE = "20"

    const val TIME_STAMP = "timestamp"

    const val FALSE = "0"
    const val TRUE = "1"

    const val USER_TYPE_CUSTOMER = "1"
    const val USER_TYPE_MERCHANT = "2"

    const val LOGIN_TYPE_NORMAL = "1"
    const val LOGIN_TYPE_GOOGLE = "2"

    const val OFFER_TYPE_REFERRAL = "1"
    const val OFFER_TYPE_RATE_AND_REVIEW = "2"
    const val OFFER_TYPE_COUPON = "3"

    const val SUB_OFFER_TYPE_REFERRAL = "1"
    const val SUB_OFFER_TYPE_BONUS = "2"
    const val SUB_OFFER_TYPE_REWARD = "3"
    const val SUB_OFFER_TYPE_RATE = "4"
    const val SUB_OFFER_TYPE_COUPON_RATE = "5"

    const val SUB_OFFER_CATEGORY_GIFT_CARD = "1"
    const val SUB_OFFER_CATEGORY_IN_STORE = "2"
    const val SUB_OFFER_CATEGORY_STORE_CREDIT = "3"

    const val HISTORY_TYPE_CREDIT = "1"
    const val HISTORY_TYPE_DEBIT = "2"

    // broadcast receiver intent filters
    val PUSH_NOTIFICATION = "pushNotification"

    fun getHashMap(): HashMap<String, String> {
        return HashMap()
    }

    fun getHashMapCoupon(): HashMap<String, RequestBody> {
        return HashMap()
    }

    fun getPostCodeUrl(googleAPIKey: String, placeId: String): String {
        return "https://maps.googleapis.com/maps/api/place/details/json?key=$googleAPIKey&placeid=$placeId"
    }

    const val USER_TYPE = "user_type"
    const val LOGIN_TYPE = "login_type"
    const val GOOGLE_ID = "google_id"
    const val BUSINESS_LOGO = "business_logo"
    const val PROFILE_PIC = "profile_pic"
    const val BUSINESS_NAME = "business_name"
    const val TAG_LINE = "tag_line"
    const val NAME = "name"
    const val EMAIL = "email"
    const val COUNTRY_CODE = "country_code"
    const val MOBILE = "mobile"
    const val WEBSITE = "website"
    const val BUSINESS_CATEGORY_ID = "business_category_id"
    const val ADDRESS = "address"
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
    const val POST_CODE = "post_code"
    const val DESCRIPTION = "description"
    const val PRODUCT_AND_SERVICES = "product_and_services"
    const val PASSWORD = "password"
    const val OTP = "otp"
    const val VERIFICATION_MAIL = "verification_mail"
    const val AMOUNT = "amount"
    const val STRIPE_TOKEN = "stripe_token"
    const val HISTORY_TYPE = "history_type"
    const val FILTER_BY = "filter_by"
    const val NOTIFICATION_STATUS = "notification_status"
    const val CODE = "code"
    const val REFERRAL_CODE  = "referral_code"
    const val COUPON_PIC  = "coupon_image"

    const val ID = "id"
    const val OFFER_ID = "offer_id"
    const val MEMBER_ID = "member_id"
    const val SUB_OFFER_ID = "sub_offer_id"
    const val MERCHANT_ID = "merchant_id"

    const val TAG = "tag"
    const val TAG_CMS = "cms"
    const val TAG_FAQ = "faq"
    const val TAG_CHANGE_PASSWORD = "change-password"
    const val TAG_BANNERS = "banner"
    const val TAG_USER_DETAILS = "user-detail"

    const val REFERRAL_STATUS_PENDING = 1
    const val REFERRAL_STATUS_ACCEPTED = 2
    const val REFERRAL_STATUS_REJECTED = 3

    const val DEVICE_TOKEN = "device_token"
    const val USER_ID = "user_id"
    const val DEVICE_TYPE = "device_type"
    const val TYPE = "type"
    const val CUSTOMER = "customer"
    const val MERCHANT = "merchant"

    const val NO_BONUS = "No Bonus"

    const val PHONE_NUMBER = "phone_number"
    const val MERCHANT_NAME = "merchant_name"
    const val PRODUCT_NAME = "product_name"
    const val PRODUCT_PRICE = "product_price"
    const val REWARD_VALUE = "rewards_value"
    const val STATUS = "status"
    const val PRODUCT_URL = "product_url"
    const val PRODUCT_IMAGE_URL = "product_image_url"
    const val CLICKED_AT = "clicked_at"

    const val OFFER_TYPE = "offer_type"
    const val EXPIRY_DATE = "expiry_date"
    const val REDEEMED_DATE = "redeemed_date"
    const val REFERRED_DATE = "referred_date"
    const val MESSAGE = "message"
    const val MAIL_PDF = "mail_pdf"
    const val REFERRAL_COUNT = "referral_count"

    object Merchant {
        const val TAG_BUSINESS_CATEGORY = "category"
        const val TAG_LOGIN = "merchant-login"
        const val TAG_LOGIN_OTP = "login-mail-otp"
        const val TAG_SOCIAL_LOGIN = "social-login"
        const val TAG_SIGN_UP = "merchant-sign-up"
        const val TAG_EDIT_EMAIL = "edit-email"
        const val TAG_VERIFY_ACCOUNT = "merchant-verified"
        const val LOGIN_OTP_VERIFY = "login-verify-otp"
        const val TAG_RESEND_OTP = "merchant-resend-verification"
        const val TAG_FORGOT_PASSWORD = "merchant-forgot-password"
        const val TAG_RESET_PASSWORD = "merchant-reset-password"
        const val TAG_EDIT_PROFILE = "merchant-edit-profile"
        const val TAG_MY_OFFERS = "merchant/offers"
        const val TAG_DELETE_OFFER = "deleted-offer"
        const val TAG_ADD_MEMBER = "add-member"
        const val TAG_UPDATE_MEMBER = "edit-member"
        const val TAG_ADD_EDIT_OFFER = "merchant/add-offer"
        const val TAG_CREATE_PDF_AND_SEND_EMAIL = "create-pdf-send-email"
        const val TAG_ADD_REFCASH = "add-refcash"
        const val TAG_REFCASH_HISTORY = "merchant-refcash-history"
        const val TAG_REFERRAL_ALERTS = "referral-list"
        const val TAG_AWAITING_REWARDS = "awaiting-reward-list"
        const val TAG_LIST_MEMBERS = "list-member"
        const val TAG_DELETE_MEMBERS = "delete-member"
        const val TAG_SEND_MESSAGE = "merchant-refmsg"
        const val TAG_CREATE_CASHBACK = "merchant/add-cashback"
        const val TAG_LIST_CASHBACK = "merchant/list-cashback"
    }

    object Customer {
        const val TAG_LOGIN = "customer-login"
        const val TAG_SIGN_UP = "customer-sign-up"
        const val TAG_SOCIAL_LOGIN = "customer-social-login"
        const val TAG_EDIT_MOBILE = "edit-mobile"
        const val TAG_VERIFY_ACCOUNT = "customer-verified"
        const val TAG_RESEND_OTP = "customer-resend-verification"
        const val TAG_FORGOT_PASSWORD = "customer-forgot-password"
        const val TAG_RESET_PASSWORD = "customer-reset-password"
        const val TAG_EDIT_PROFILE = "customer-edit-profile"
        const val TAG_REFCASH_HISTORY = "customer-refcash-history"
        const val TAG_REFER_MERCHANT = "refer-merchant"
        const val TAG_RATE_MERCHANT = "rate-merchant"
        const val TAG_SCAN_OFFER = "merchant/scan-offer"
        const val TAG_REDEMPTION_HISTORY = "redemption-history"
        const val TAG_STORE_CREDIT = "store-credit"
        const val TAG_MERCHANT_DETAILS = "scan-merchant-detail"
        const val TAG_SEARCH_MERCHANT = "search-merchant"
        const val TAG_GENERATE_REDEMPTION_CODE_FOR_STORE_CREDIT = "generate-redemption-code-store"
        const val TAG_REFERRAL_DETAILS = "referral-detail"
        const val TAG_NOTIFICATION_ON_OFF = "notification-on-off"
        const val TAG_REDEEM_OFFER = "redeem-offer"
        const val TAG_CONTACT_LIST = "contact-list"
        const val TAG_RATE_REFERRAL_MERCHANT = "rate-referral-merchant"
        const val TAG_GENERATE_REDEMPTION_CODE = "generate-redemption-code"
        const val TAG_OFFER_DETAIL = "offer-detail"
        const val TAG_REDEEM_REFCASH = "redeem-refcash"
        const val TAG_ADD_REMOVE_TOKEN = "add-remove-token"
        const val TAG_MERCHANT_RATE_REVIEW = "merchant-rate-review"
        const val TAG_NOTIFICATION="notification"
        const val TAG_KEY_LIST="merchant-key-list"
        const val TAG_SEARCH_RESULT="search_product_result"
        const val TAG_COUPON_LIST="coupon-list"
        const val TAG_COUPON_QR="couponQR"
    }

    object EditEmail {
        const val OLD_EMAIL = "old_email"
        const val NEW_EMAIL = "new_email"
    }

    object EditMobile {
        const val OLD_COUNTRY_CODE = "old_country_code"
        const val OLD_MOBILE = "old_mobile"
        const val NEW_COUNTRY_CODE = "new_country_code"
        const val NEW_MOBILE = "new_mobile"
    }

    object ChangePassword {
        const val OLD_PASSWORD = "old_password"
        const val NEW_PASSWORD = "new_password"
    }

    object SearchMerchant {
        const val SEARCH_TEXT = "search_text"
        const val DISTANCE = "distance"
        const val POST_CODE = "post_code"
        const val SHOW_TOP_MERCHANT = "show_top_merchant"
    }

    object RateAndRefer {
        const val RATING = "rating"
        const val PRODUCT_NAME = "product_name"
        const val REVIEW = "review"
        const val REFER_SMS = "refersms"
        const val REFER_MAIL = "refermail"
        const val PRODUCT_IMAGE = "product_image"
        const val SMARKET_CONTACT_LIST = "smarket_contact_list"
    }

    object CreateCashBackOffer {
        const val OFFER_PERCENTAGE = "offer_percentage"
        const val OFFER_CONDITION = "offer_condition"
        const val STATUS = "status"
    }
}