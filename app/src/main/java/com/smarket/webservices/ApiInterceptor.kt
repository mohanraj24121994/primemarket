package com.smarket.webservices


import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class ApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val originalRequest: Request = chain!!.request()
        val adminAuthToken = "0bcbd92ab7b7789fc98dca6cd281a31228d6405b"
        val newRequest = originalRequest.newBuilder()
            .header("Authorization", adminAuthToken)
            .build()

        return chain.proceed(newRequest)
    }
}