package com.smarket.webservices

import com.smarket.utils.NetworkUtil
import com.smarket.utils.ProgressDialogUtil
import com.smarket.utils.showToast
import com.smarket.utils.PreferenceUtil
import com.smarket.SMarket
import com.smarket.database.BusinessCategory
import com.smarket.main.MainActivity
import com.smarket.utils.showDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by MI-062 on 31/7/18.
 */
object ApiUtil {

    private lateinit var disposable: Disposable

    fun callBusinessCategoryAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            return
        }

        val timeStamp = PreferenceUtil.getLongPref(PreferenceUtil.PREF_KEY_BUSINESS_CATEGORY_TIMESTAMP)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.TIME_STAMP] = timeStamp.toString()

        disposable = WebApiClient.webApi().businessCategoryAPI(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { it ->
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    if (timeStamp == 0L) {
                                                        SMarket.appDB.businessCategoryDao().deleteData()
                                                    }

                                                    if (data.isNotEmpty()) {
                                                        BusinessCategory.setData(data)
                                                    }

                                                    SMarket.appDB.businessCategoryDao().deleteInActiveData()

                                                    PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_BUSINESS_CATEGORY_TIMESTAMP, timestamp)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {

                        }
                )
    }

    fun callCMSAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            return
        }

        disposable = WebApiClient.webApi().cmsAPI().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    SMarket.appDB.cmsDao().deleteData()

                                                    if (data.isNotEmpty()) {
                                                        SMarket.appDB.cmsDao().setData(data)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {

                        }
                )
    }

    fun callFAQAPI(mActivity: MainActivity) {

        if (!NetworkUtil.isNetworkAvailable()) {
            return
        }

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.USER_TYPE] = if (mActivity.isUserTypeCustomer) {
            ApiParam.USER_TYPE_CUSTOMER
        } else {
            ApiParam.USER_TYPE_MERCHANT
        }

        disposable = WebApiClient.webApi().faqAPI(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    SMarket.appDB.faqDao().deleteData()

                                                    if (data.isNotEmpty()) {
                                                        SMarket.appDB.faqDao().setData(data)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {

                        }
                )
    }

    fun callCreatePDFAndSendEmail(mActivity: MainActivity) {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        disposable = WebApiClient.webApi().createPDFAndSendEmailAPI().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    fun callNotificationOnOffAPI(mActivity: MainActivity, status_value: String) {

        if (!NetworkUtil.isNetworkAvailable()) {
            return
        }

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.NOTIFICATION_STATUS] = status_value

        disposable = WebApiClient.webApi().notificationOnOffAPI(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {
                                                    if (data != null) {

                                                        val userDetails = data!!
                                                        userDetails.token = mActivity.userToken

                                                        SMarket.appDB.userDao().updateData(userDetails)

                                                        mActivity.updateUserDetailsObject()

                                                        mActivity.setUserDetails()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {

                        }
                )
    }

    fun callAddRemoveDeviceTokenAPI(user_id: Long, device_token: String,type: String) {

        if (!NetworkUtil.isNetworkAvailable()) {
            return
        }

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.USER_ID] = "$user_id"
        hashMap[ApiParam.DEVICE_TOKEN] = device_token
        hashMap[ApiParam.TYPE] = type
        hashMap[ApiParam.DEVICE_TYPE] = "2"//"2" for android

        disposable = WebApiClient.webApi().addRemoveTokenAPI(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {

                        }
                )
    }
}