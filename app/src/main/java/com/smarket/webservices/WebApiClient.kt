package com.smarket.webservices

import com.smarket.BuildConfig
import com.smarket.SMarket
import com.smarket.webservices.ApiParam.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by MI-062 on 31/7/18.
 */
class WebApiClient {

    companion object {

        fun webApi(): WebApi {

            // preparing log level for interceptor.
            val logging = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                logging.level = HttpLoggingInterceptor.Level.BODY
            } else {
                logging.level = HttpLoggingInterceptor.Level.NONE
            }

            // here we are defining client and making configuration for read/connection time out.
            // also adding interceptor which we use for api logging.
            val client = OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .addInterceptor { chain ->
                        val request = chain.request().newBuilder()
                                .addHeader("Accept", "application/json")
                                .addHeader("Authorization", "${ApiParam.TOKEN_PREFIX}${SMarket.appDB.userDao().getData()?.token}")
                                .build()
                        chain.proceed(request)
                    }
                    .addInterceptor(logging).build()

            // preparing retrofit builder and adding
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofit.create(WebApi::class.java)
        }
    }
}