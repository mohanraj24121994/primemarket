package com.smarket.interfaces

import com.smarket.SMarket
import com.smarket.main.MainActivity
import java.util.*

/**
 * Created by MI-062 on 14/8/18.
 */


interface UpdateRefCash {

    fun onUpdateRefCash(refCash: Double)
}

object UpdateRefCashManager {

    var updateRefCashList: ArrayList<UpdateRefCash>? = null

    private fun init() {

        if (updateRefCashList == null) {
            updateRefCashList = ArrayList()
        }
    }

    fun addCallBack(updateRefCash: UpdateRefCash) {

        init()

        if (!updateRefCashList!!.contains(updateRefCash)) {
            updateRefCashList!!.add(updateRefCash)
        }
    }

    fun removeCallBack(updateRefCash: UpdateRefCash) {

        init()

        if (updateRefCashList!!.contains(updateRefCash)) {
            updateRefCashList!!.remove(updateRefCash)
        }
    }

    fun executeCallBacks(mActivity: MainActivity, refCash: Double) {

        SMarket.appDB.userDao().updateRefCash(refCash)

        mActivity.updateUserDetailsObject()

        init()

        for (updateRefCash in updateRefCashList!!) {
            updateRefCash.onUpdateRefCash(refCash)
        }
    }
}