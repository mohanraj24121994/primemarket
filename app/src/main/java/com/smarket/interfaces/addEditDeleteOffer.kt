package com.smarket.interfaces

import com.smarket.model.Offer
import java.util.*

/**
 * Created by MI-062 on 14/8/18.
 */

interface AddEditDeleteOffer {

    fun onAddEditDeleteOffer(updatedOffers: ArrayList<Offer>)
}

object AddEditDeleteOfferManager {

    var addEditDeleteOfferList: ArrayList<AddEditDeleteOffer>? = null

    private fun init() {

        if (addEditDeleteOfferList == null) {
            addEditDeleteOfferList = ArrayList()
        }
    }

    fun addCallBack(addEditDeleteOffer: AddEditDeleteOffer) {

        init()

        if (!addEditDeleteOfferList!!.contains(addEditDeleteOffer)) {
            addEditDeleteOfferList!!.add(addEditDeleteOffer)
        }
    }

    fun removeCallBack(addEditDeleteOffer: AddEditDeleteOffer) {

        init()

        if (addEditDeleteOfferList!!.contains(addEditDeleteOffer)) {
            addEditDeleteOfferList!!.remove(addEditDeleteOffer)
        }
    }

    fun executeCallBacks(updatedOffers: ArrayList<Offer>) {

        init()

        for (addEditDeleteOffer in addEditDeleteOfferList!!) {
            addEditDeleteOffer.onAddEditDeleteOffer(updatedOffers)
        }
    }
}