package com.smarket.location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mind on 27/7/17.
 */

public interface LocationUpdates {

    void onFoundLocation(boolean isLatest, LatLng latLng);

}
