package com.smarket.fcm

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.iid.FirebaseInstanceId
import com.smarket.utils.DEVICE_TOKEN
import com.smarket.utils.PreferenceUtil.Companion.setPref


class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()

        val refreshedToken = FirebaseInstanceId.getInstance().token

        // sending reg id to your server
        Log.e("registration_token", "sendRegistrationToServer: $refreshedToken")

        setPref(DEVICE_TOKEN, refreshedToken)
    }
}