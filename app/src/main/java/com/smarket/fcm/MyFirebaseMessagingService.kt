package com.smarket.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.MainActivity
import com.smarket.utils.isAppIsInBackground
import com.smarket.utils.isRequiredField
import com.smarket.webservices.ApiParam
import org.json.JSONObject
import java.util.*


class MyFirebaseMessagingService : FirebaseMessagingService() {

    var message: String? = ""
    var title: String? = ""
    var type: String? = ""
    private val TAG = "SMARKET"

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.e("NEW_TOKEN", token)
//        setPref(DEVICE_TOKEN, token)

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        createNotificationChannel()

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        if (remoteMessage.notification != null && remoteMessage.notification!!.title!!.isRequiredField()) {
            title = remoteMessage.notification!!.title
        }

        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification_ Body: " + remoteMessage.notification!!.body!!)
            if (remoteMessage.notification!!.body != null) {
                message = remoteMessage.notification!!.body
            }
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            try {
                val params = remoteMessage.data
                val `object` = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", `object`.toString())

                if (`object`.has("type")) {
                    type = `object`.getString("type")
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        handleNotification()

    }

    private fun handleNotification() {
        if (!isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val pushNotification = Intent(ApiParam.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", message)
            pushNotification.putExtra("title", title)
            pushNotification.putExtra("type", type)
            androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
        } else {
            // If the app is in background, firebase itself handles the notification
            sendNotification(title, message)
        }
    }

    private fun sendNotification(title: String?, messageBody: String?) {
        val rnd = Random()
        val i = rnd.nextInt(100)
        val intent = Intent(this, MainActivity::class.java)
        val action = "Notification"
        intent.action = action
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        val stackBuilder = TaskStackBuilder.create(this)
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity::class.java)
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(intent)
        val resultPendingIntent = stackBuilder.getPendingIntent(i, PendingIntent.FLAG_UPDATE_CURRENT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setColor(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    SMarket.appContext.resources.getColor(android.R.color.white)
                } else {
                    SMarket.appContext.resources.getColor(android.R.color.transparent)
                })
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(resultPendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(i, notificationBuilder.build())
    }


    private fun createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = SMarket.appContext.resources.getString(R.string.default_notification_channel_id)
            val notificationChannel = NotificationChannel(channelId, SMarket.appContext.resources.getString(R.string.notifications), NotificationManager.IMPORTANCE_DEFAULT)
            /*  notificationChannel.enableLights(false)
              notificationChannel.enableVibration(false)
              notificationChannel.setShowBadge(true)*/
            val notificationManager = SMarket.appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
}