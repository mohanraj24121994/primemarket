package com.app.nerd.utils.item_decoration

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * Created by MI-062 on 16/4/18.
 */
class HorizontalDividerDecoration(var spacing: Int, var includeTopBottomSpacing: Boolean = false) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect , view: View , parent: androidx.recyclerview.widget.RecyclerView , state: androidx.recyclerview.widget.RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)

        if (position == 0) {
            outRect.left = spacing
        }

        outRect.right = spacing

        if (includeTopBottomSpacing) {
            outRect.top = spacing
            outRect.bottom = spacing
        }
    }
}
