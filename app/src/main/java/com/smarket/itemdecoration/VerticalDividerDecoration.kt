package com.smarket.itemdecoration

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * Created by MI-062 on 16/4/18.
 */
class VerticalDividerDecoration(var spacing: Int, var includeTopSpacing: Boolean = false) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect , view: View , parent: androidx.recyclerview.widget.RecyclerView , state: androidx.recyclerview.widget.RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)

        if (includeTopSpacing && position ==0) {
            outRect.top = spacing
        }

        outRect.bottom = spacing
        outRect.left = spacing
        outRect.right = spacing
    }
}