package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class getProductData {
    @SerializedName("amazon.in")
    @Expose
    private var amazonIn: AmazonIn? = null

    @SerializedName("flipkart")
    @Expose
    private var flipkart: Flipkart? = null

    fun getAmazonIn(): AmazonIn? {
        return amazonIn
    }

    fun setAmazonIn(amazonIn: AmazonIn?) {
        this.amazonIn = amazonIn
    }

    fun getFlipkart(): Flipkart? {
        return flipkart
    }

    fun setFlipkart(flipkart: Flipkart?) {
        this.flipkart = flipkart
    }
}