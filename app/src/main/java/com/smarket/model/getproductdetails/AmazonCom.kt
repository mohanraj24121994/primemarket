package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class AmazonCom {
    @SerializedName("product_name")
    @Expose
    private var productName: String? = null

    @SerializedName("product_price")
    @Expose
    private var productPrice: String? = null

    @SerializedName("product_image")
    @Expose
    private var productImage: String? = null

    @SerializedName("product_url")
    @Expose
    private var productUrl: String? = null

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String?) {
        this.productName = productName
    }

    fun getProductPrice(): String? {
        return productPrice
    }

    fun setProductPrice(productPrice: String?) {
        this.productPrice = productPrice
    }

    fun getProductImage(): String? {
        return productImage
    }

    fun setProductImage(productImage: String?) {
        this.productImage = productImage
    }

    fun getProductUrl(): String? {
        return productUrl
    }

    fun setProductUrl(productUrl: String?) {
        this.productUrl = productUrl
    }

}