package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class OtherSites {
    @SerializedName("merchantName")
    @Expose
    var merchantName: String? = null

    @SerializedName("canonicalUrl")
    @Expose
    var canonicalUrl: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("upc")
    @Expose
    var upc: Any? = null

    @SerializedName("sku")
    @Expose
    var sku: String? = null

    @SerializedName("price")
    @Expose
    var price: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("imageUrl")
    @Expose
    var imageUrl: String? = null

    @SerializedName("brandName")
    @Expose
    var brandName: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("canonicalDomain")
    @Expose
    var canonicalDomain: String? = null
}