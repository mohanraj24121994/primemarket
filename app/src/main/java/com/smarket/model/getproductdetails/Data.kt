package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Data {

    @SerializedName("amazon.in")
    @Expose
    private var amazonIn: AmazonIn? = null

    @SerializedName("other_sites")
    @Expose
    private var other_sites: OtherSites? = null

    @SerializedName("amazon.com")
    @Expose
    private var amazonCom: AmazonCom? = null

    @SerializedName("flipkart")
    @Expose
    private var flipkart: Flipkart? = null

    @SerializedName("ebay")
    @Expose
    private var ebay: Ebay? = null

    @SerializedName("walmart")
    @Expose
    private var walmart: Walmart? = null

    fun getAmazonIn(): AmazonIn? {
        return amazonIn
    }

    fun setAmazonIn(amazonIn: AmazonIn?) {
        this.amazonIn = amazonIn
    }

    fun other_sites(): OtherSites? {
        return other_sites
    }

    fun setother_sites(other_sites: OtherSites?) {
        this.other_sites = other_sites
    }

    fun amazonCom(): AmazonCom? {
        return amazonCom
    }

    fun setamazonCom(amazonCom: AmazonCom?) {
        this.amazonCom = amazonCom
    }

    fun flipkart(): Flipkart? {
        return flipkart
    }

    fun setflipkart(flipkart: Flipkart?) {
        this.flipkart = flipkart
    }

    fun ebay(): Ebay? {
        return ebay
    }

    fun setebay(ebay: Ebay?) {
        this.ebay = ebay
    }

    fun walmart(): Walmart? {
        return walmart
    }

    fun setwalmart(walmart: Walmart?) {
        this.walmart = walmart
    }
}