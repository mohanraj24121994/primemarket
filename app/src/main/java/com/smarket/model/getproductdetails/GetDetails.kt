package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetDetails {
    @SerializedName("data")
    @Expose
    var data: Data? = null
}