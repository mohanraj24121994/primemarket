package com.smarket.model.getproductdetails

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Walmart {
    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("product_image")
    @Expose
    var productImage: String? = null

    @SerializedName("product_url")
    @Expose
    var productUrl: String? = null
}