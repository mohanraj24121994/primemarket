package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 11/7/18.
 */
class SubOffer() : Parcelable {
    var id = 0L

    @SerializedName("parent_id")
    var offerId = 0L

    @SerializedName("sub_offer_type")
    var subOfferType = ""

    @SerializedName("sub_offer_category")
    var subOfferCategory = ""

    @SerializedName("amount")
    var amount = 0.0

    var title = ""

    var conditions = ""

    @SerializedName("is_redemption")
    var isRedemptionRequired = ""

    @SerializedName("qr_code")
    var qrCode = ""

    var code = ""

    var status = ""

    var usageType = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        offerId = parcel.readLong()
        subOfferType = parcel.readString()!!
        subOfferCategory = parcel.readString()!!
        amount = parcel.readDouble()
        title = parcel.readString()!!
        conditions = parcel.readString()!!
        isRedemptionRequired = parcel.readString()!!
        qrCode = parcel.readString()!!
        code = parcel.readString()!!
        status = parcel.readString()!!
        usageType = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeLong(offerId)
        parcel.writeString(subOfferType)
        parcel.writeString(subOfferCategory)
        parcel.writeDouble(amount)
        parcel.writeString(title)
        parcel.writeString(conditions)
        parcel.writeString(isRedemptionRequired)
        parcel.writeString(qrCode)
        parcel.writeString(code)
        parcel.writeString(status)
        parcel.writeString(usageType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubOffer> {
        override fun createFromParcel(parcel: Parcel): SubOffer {
            return SubOffer(parcel)
        }

        override fun newArray(size: Int): Array<SubOffer?> {
            return arrayOfNulls(size)
        }
    }

}