package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 31/7/18.
 */
class Meta {

    var token = ""

    var status = 0

    var message = ""

    var timestamp = 0L

    @SerializedName("current_page")
    var currentPage = 0

    @SerializedName("last_page")
    var lastPage = 0

    var total = 0L

    @SerializedName("refferal_alert")
    var refferalAlert = 0

    @SerializedName("awaiting_rewards")
    var awaitingRewards = 0

    @SerializedName("store_credit")
    var storeCredit = 0.0

    var refcash = 0.0
}