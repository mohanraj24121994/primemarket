package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 24/8/18.
 */
class StoreCredit : BaseMerchantDetails(){
    var id = 0L

    var address = ""

    var latitude = 0.0

    var longitude = 0.0

    var website = ""

    @SerializedName("country_code")
    var countrycode = ""

    var mobile = ""

    var amount =  0.0

    @SerializedName("store_credit")
    var storeCredit = 0.0
}