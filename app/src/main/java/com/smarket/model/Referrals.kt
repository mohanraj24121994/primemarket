package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 29/8/18.
 */
class Referrals {

    var id = 0L

    @SerializedName("merchant_id")
    var merchantId = 0L

    @SerializedName("user_id")
    var userId = 0L

    @SerializedName("profile_pic")
    var profilePic = ""

    var name = ""

    @SerializedName("country_code")
    var countryCode = ""

    var mobile = ""

    var rating = 0F

    var review = ""

    var image = ""

    @SerializedName("offer_type")
    var offerType = ""

    @SerializedName("referred_date")
    var referredDate = ""

    @SerializedName("rate_on")
    var rateOn = ""

    @SerializedName("referral_status")
    var referralStatus = 0

    @SerializedName("qr_code")
    var qrCode = ""

    var code = ""
}