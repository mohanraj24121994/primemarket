package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 11/7/18.
 */
open class BaseMerchantDetails {

    @SerializedName("merchant_id")
    var merchantId = 0L

    @SerializedName("business_logo")
    var businessLogo = ""

    var distance = ""

    @SerializedName("business_name")
    var businessName = ""

    @SerializedName("tag_line")
    var tagLine = ""
}