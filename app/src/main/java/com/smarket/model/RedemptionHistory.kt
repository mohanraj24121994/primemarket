package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 24/8/18.
 */
open class RedemptionHistory : BaseMerchantDetails() {

    var id = 0L

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    var referrals = 0

    var amount = 0.0

    @SerializedName("sub_offer_type")
    var subOfferType = ""

    @SerializedName("sub_offer_category_id")
    var subOfferCategory = ""

    @SerializedName("expiry_date")
    var expiryDate = ""

    @SerializedName("redeemed_date")
    var redeemedDate = ""

    var title = ""

}