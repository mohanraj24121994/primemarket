package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 11/7/18.
 */
open class ReferralAlertAndAwaitingReward() : BaseMerchantDetails(), Parcelable {

    var id = 0L

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    @SerializedName("store_credit")
    var storeCredit = 0.0

    var referrals = 0

    var title = ""

    var amount = 0.0

    @SerializedName("sub_offer_category_id")
    var subOfferCategory = ""

    var conditions = ""

    @SerializedName("expiry_date")
    var expiryDate = ""

    @SerializedName("redeemed_date")
    var redeemedDate = ""

    var status = 1

    @SerializedName("purchased_by")
    var purchasedBy = ""

    @SerializedName("offer_id")
    var offerId = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        averageRating = parcel.readFloat()
        noOfRating = parcel.readLong()
        storeCredit = parcel.readDouble()
        referrals = parcel.readInt()
        title = parcel.readString()!!
        amount = parcel.readDouble()
        subOfferCategory = parcel.readString()!!
        conditions = parcel.readString()!!
        expiryDate = parcel.readString()!!
        redeemedDate = parcel.readString()!!
        status = parcel.readInt()
        purchasedBy = parcel.readString()!!
        offerId = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeFloat(averageRating)
        parcel.writeLong(noOfRating)
        parcel.writeDouble(storeCredit)
        parcel.writeInt(referrals)
        parcel.writeString(title)
        parcel.writeDouble(amount)
        parcel.writeString(subOfferCategory)
        parcel.writeString(conditions)
        parcel.writeString(expiryDate)
        parcel.writeString(redeemedDate)
        parcel.writeInt(status)
        parcel.writeString(purchasedBy)
        parcel.writeString(offerId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReferralAlertAndAwaitingReward> {
        override fun createFromParcel(parcel: Parcel): ReferralAlertAndAwaitingReward {
            return ReferralAlertAndAwaitingReward(parcel)
        }

        override fun newArray(size: Int): Array<ReferralAlertAndAwaitingReward?> {
            return arrayOfNulls(size)
        }
    }

}