package com.smarket.model

import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 11/7/18.
 */
class RefCashHistory {

    var id = 0L

    var name = ""

    @SerializedName("transaction_time")
    var transactionTime = ""

    var amount = 0.0
}