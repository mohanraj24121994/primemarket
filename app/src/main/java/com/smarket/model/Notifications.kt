package com.smarket.model

import com.google.gson.annotations.SerializedName


class Notifications {

    var id = 0L

    @SerializedName("send_to")
    var sendTo = ""

    var message = ""

    var date = ""
}