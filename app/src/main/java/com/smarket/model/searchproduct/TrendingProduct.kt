package com.smarket.model.searchproduct

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class TrendingProduct {

    @SerializedName("id")
    @Expose
    var id: Any? = null

    @SerializedName("phone_number")
    @Expose
    var phoneNumber: Any? = null

    @SerializedName("user_id")
    @Expose
    var userId: Any? = null

    @SerializedName("merchant_name")
    @Expose
    var merchantName: String? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("rewards_value")
    @Expose
    var rewardsValue: String? = null

    @SerializedName("product_image_url")
    @Expose
    var productImageUrl: String? = null

    @SerializedName("product_url")
    @Expose
    var productUrl: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("device_type")
    @Expose
    var deviceType: String? = null

    @SerializedName("clicked_at")
    @Expose
    var clickedAt: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
}