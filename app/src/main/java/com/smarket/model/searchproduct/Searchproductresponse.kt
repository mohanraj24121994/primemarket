package com.smarket.model.searchproduct

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Searchproductresponse {
    @SerializedName("offer_product")
    @Expose
    var offerProduct: List<OfferProduct?>? = null

    @SerializedName("trendingProducts")
    @Expose
    var trendingProducts: List<TrendingProduct?>? = null

    @SerializedName("collection")
    @Expose
    var collection: Collection? = null

}