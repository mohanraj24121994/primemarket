package com.smarket.model.searchproduct

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class OfferProduct {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: String? = null

    @SerializedName("product_url")
    @Expose
    var productUrl: String? = null

    @SerializedName("product_image")
    @Expose
    var productImage: String? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("deleted_at")
    @Expose
    var deletedAt: Int? = null

    @SerializedName("exp_date")
    @Expose
    var expDate: String? = null

    @SerializedName("currency")
    @Expose
    var currency: Int? = null

    @SerializedName("store")
    @Expose
    var store: Int? = null

    @SerializedName("cash_back")
    @Expose
    var cashBack: Int? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

    @SerializedName("store_name")
    @Expose
    var storeName: String? = null

    @SerializedName("cost_name")
    @Expose
    var costName: String? = null

    var earnValue: String? = null

    var cart: String? = null
    var count: String? = null


}