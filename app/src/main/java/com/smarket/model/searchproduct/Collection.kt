package com.smarket.model.searchproduct

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Collection {
    @SerializedName("ebay")
    @Expose
    var ebay: Ebay? = null
}