package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by MI-062 on 11/7/18.
 */
class Offer() : Parcelable {

    var id = 0L

    @SerializedName("offer_type")
    var offerType = ""

    @SerializedName("expiry_date")
    var expiryDate = ""

    @SerializedName("redeemed_date")
    var redeemedDate = ""

    @SerializedName("referred_date")
    var referredDate = ""

    @SerializedName("sub_offer")
    var subOffers = ArrayList<SubOffer>()

    var message = ""

    @SerializedName("mail_pdf")
    var mailPDF = ""

    var status = 1

    @SerializedName("referral_count")
    var referralCount = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        offerType = parcel.readString()!!
        expiryDate = parcel.readString()!!
        redeemedDate = parcel.readString()!!
        referredDate = parcel.readString()!!
        message = parcel.readString()!!
        mailPDF = parcel.readString()!!
        status = parcel.readInt()
        referralCount = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(offerType)
        parcel.writeString(expiryDate)
        parcel.writeString(redeemedDate)
        parcel.writeString(referredDate)
        parcel.writeString(message)
        parcel.writeString(mailPDF)
        parcel.writeInt(status)
        parcel.writeString(referralCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Offer> {
        override fun createFromParcel(parcel: Parcel): Offer {
            return Offer(parcel)
        }

        override fun newArray(size: Int): Array<Offer?> {
            return arrayOfNulls(size)
        }
    }
}