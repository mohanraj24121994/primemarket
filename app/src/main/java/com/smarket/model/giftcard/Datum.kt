package com.smarket.model.giftcard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class Datum(
        @SerializedName("id")
        @Expose
        var id: Int? = null ,

        @SerializedName("name")
        @Expose
        var name: String? = null ,

        @SerializedName("logo")
        @Expose
        var logo: String? = null ,

        @SerializedName("amount")
        @Expose
        var amount: Int? = null ,

        @SerializedName("status")
        @Expose
        var status: String? = null ,

        @SerializedName("expiry_date")
        @Expose
        var expiryDate: String? = null ,

        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null ,

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
)