package com.smarket.model.giftcard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class Meta {
    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("logo")
    @Expose
    private var logo: String? = null

    @SerializedName("amount")
    @Expose
    private var amount: Int? = null

    @SerializedName("status")
    @Expose
    private var status: String? = null

    @SerializedName("expiry_date")
    @Expose
    private var expiryDate: String? = null

    @SerializedName("created_at")
    @Expose
    private var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    private var updatedAt: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getLogo(): String? {
        return logo
    }

    fun setLogo(logo: String?) {
        this.logo = logo
    }

    fun getAmount(): Int? {
        return amount
    }

    fun setAmount(amount: Int?) {
        this.amount = amount
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String?) {
        this.status = status
    }

    fun getExpiryDate(): String? {
        return expiryDate
    }

    fun setExpiryDate(expiryDate: String?) {
        this.expiryDate = expiryDate
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String?) {
        this.updatedAt = updatedAt
    }
}