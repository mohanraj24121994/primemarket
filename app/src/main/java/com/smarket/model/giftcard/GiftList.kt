package com.smarket.model.giftcard

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GiftList {
    @SerializedName("data")
    @Expose
    private var data: List<Datum?>? = null

    @SerializedName("meta")
    @Expose
    private var meta: Meta? = null

    fun getData(): List<Datum?>? {
        return data
    }

    fun setData(data: List<Datum?>?) {
        this.data = data
    }

    fun getMeta(): Meta? {
        return meta
    }

    fun setMeta(meta: Meta?) {
        this.meta = meta
    }
}