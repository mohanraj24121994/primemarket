package com.smarket.model.referrallist

class ProductCompareList {
    var mProductName: String = ""
    var mPrice: String = ""
    var mImage: String = ""
    var mEarn: String = ""
    var mMerchantName: String = ""
    var mURL: String = ""
    var logo: String = ""
}