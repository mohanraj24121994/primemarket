package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 28/8/18.
 */
class OfferDetails() : BaseMerchantDetails(), Parcelable {

    var id = 0L

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    @SerializedName("store_credit")
    var storeCredit = 0.0

    @SerializedName("sub_offer_category")
    var subOfferCategory = ""

    @SerializedName("offer_used_in")
    var offerUsedIn = 0L

    @SerializedName("expires_date")
    var expiresDate = ""

    @SerializedName("referred_date")
    var referredDate = ""

    @SerializedName("redeemed_date")
    var redeemedDate = ""

    @SerializedName("qr_code")
    var qrCode = ""

    var  code = ""

    var status = 1

    var enter_storeCredit = ""

    var offer:Offer? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        averageRating = parcel.readFloat()
        noOfRating = parcel.readLong()
        storeCredit = parcel.readDouble()
        subOfferCategory = parcel.readString()!!
        offerUsedIn = parcel.readLong()
        expiresDate = parcel.readString()!!
        referredDate = parcel.readString()!!
        redeemedDate = parcel.readString()!!
        qrCode = parcel.readString()!!
        code = parcel.readString()!!
        status = parcel.readInt()
        offer = parcel.readParcelable(Offer::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeFloat(averageRating)
        parcel.writeLong(noOfRating)
        parcel.writeDouble(storeCredit)
        parcel.writeString(subOfferCategory)
        parcel.writeLong(offerUsedIn)
        parcel.writeString(expiresDate)
        parcel.writeString(referredDate)
        parcel.writeString(redeemedDate)
        parcel.writeString(qrCode)
        parcel.writeString(code)
        parcel.writeInt(status)
        parcel.writeParcelable(offer, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OfferDetails> {
        override fun createFromParcel(parcel: Parcel): OfferDetails {
            return OfferDetails(parcel)
        }

        override fun newArray(size: Int): Array<OfferDetails?> {
            return arrayOfNulls(size)
        }
    }

}