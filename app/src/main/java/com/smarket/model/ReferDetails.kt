package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.smarket.webservices.response.ContactDetails
import java.io.File

/**
 * Created by mind on 17/9/18.
 */
class ReferDetails() : Parcelable{

    var merchantId = 0L

    var rating = 0.0F

    var productName = ""
    var refersms = ""
    var referral_sms = ""
    var refermail = ""

    var review = ""

    var productImage:File? = null

    var smarketContactList = ArrayList<ContactDetails>()

    constructor(parcel: Parcel) : this() {
        merchantId = parcel.readLong()
        rating = parcel.readFloat()
        productName = parcel.readString()!!
        review = parcel.readString()!!
        refermail = parcel.readString()!!
        refersms = parcel.readString()!!
        referral_sms = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(merchantId)
        parcel.writeFloat(rating)
        parcel.writeString(productName)
        parcel.writeString(review)
        parcel.writeString(refermail)
        parcel.writeString(refersms)
        parcel.writeString(referral_sms)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReferDetails> {
        override fun createFromParcel(parcel: Parcel): ReferDetails {
            return ReferDetails(parcel)
        }

        override fun newArray(size: Int): Array<ReferDetails?> {
            return arrayOfNulls(size)
        }
    }

}