package com.smarket.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by mind on 27/8/18.
 */
class MerchantDetails() : BaseMerchantDetails(), Parcelable {

    var id = 0L

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    var website = ""

    var mobile = ""

    @SerializedName("store_credit")
    var storeCredit = 0.0

    @SerializedName("coupon_count")
    var coupon_count = 0

    @SerializedName("reward_count")
    var reward_count = 0


    var referrals = 0L

    var description = ""

    @SerializedName("product_and_services")
    var productAndServices = ""

    var address = ""

    var latitude = 0.0

    var longitude = 0.0

    @SerializedName("show_referral_alerts")
    var showReferralAlerts = ""

    @SerializedName("show_unclaimed_offers")
    var showUnclaimedOffers = ""

    @SerializedName("show_rate_merchant")
    var showRateMerchant = ""

    @SerializedName("show_refer_merchant")
    var showReferMerchant = ""

    @SerializedName("referral_message")
    var referralMessage = ""

    var offer = ArrayList<Offer>()

    constructor(parcel: Parcel) : this() {
        averageRating = parcel.readFloat()
        noOfRating = parcel.readLong()
        website = parcel.readString()!!
        mobile = parcel.readString()!!
        storeCredit = parcel.readDouble()
        coupon_count = parcel.readInt()
        reward_count = parcel.readInt()
        referrals = parcel.readLong()
        description = parcel.readString()!!
        productAndServices = parcel.readString()!!
        address = parcel.readString()!!
        latitude = parcel.readDouble()
        longitude = parcel.readDouble()
        showReferralAlerts = parcel.readString()!!
        showUnclaimedOffers = parcel.readString()!!
        showRateMerchant = parcel.readString()!!
        showReferMerchant = parcel.readString()!!
        referralMessage = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(averageRating)
        parcel.writeLong(noOfRating)
        parcel.writeString(website)
        parcel.writeString(mobile)
        parcel.writeDouble(storeCredit)
        parcel.writeInt(coupon_count)
        parcel.writeInt(reward_count)
        parcel.writeLong(referrals)
        parcel.writeString(description)
        parcel.writeString(productAndServices)
        parcel.writeString(address)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(showReferralAlerts)
        parcel.writeString(showUnclaimedOffers)
        parcel.writeString(showRateMerchant)
        parcel.writeString(showReferMerchant)
        parcel.writeString(referralMessage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MerchantDetails> {
        override fun createFromParcel(parcel: Parcel): MerchantDetails {
            return MerchantDetails(parcel)
        }

        override fun newArray(size: Int): Array<MerchantDetails?> {
            return arrayOfNulls(size)
        }
    }
}