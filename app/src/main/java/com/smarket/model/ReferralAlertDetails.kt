package com.smarket.model

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

/**
 * Created by mind on 29/8/18.
 */
class ReferralAlertDetails : BaseMerchantDetails() {

    var id = 0L

    @SerializedName("average_rating")
    var averageRating = 0.0F

    @SerializedName("no_of_rating")
    var noOfRating = 0L

    var offer:Offer? = null

    var referrals = ArrayList<Referrals>()
}