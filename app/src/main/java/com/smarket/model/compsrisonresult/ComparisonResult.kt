package com.smarket.model.compsrisonresult

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.smarket.webservices.response.Meta


class ComparisonResult {
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
}