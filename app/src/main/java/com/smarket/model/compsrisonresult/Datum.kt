package com.smarket.model.compsrisonresult

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class Datum {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("key_details")
    @Expose
    var keyDetails: KeyDetails? = null

    @SerializedName("country")
    @Expose
    var country: String? = null

    @SerializedName("percentage")
    @Expose
    var percentage: Int? = null
}