package com.smarket.model.compsrisonresult

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class KeyDetails {
    @SerializedName("key")
    @Expose
    var key: Key? = null

    @SerializedName("detail")
    @Expose
    var detail: Detail? = null

    @SerializedName("key2")
    @Expose
    var key2: Key2? = null

    @SerializedName("detail2")
    @Expose
    var detail2: Detail2? = null

    @SerializedName("detail3")
    @Expose
    var detail3: Detail3? = null
}