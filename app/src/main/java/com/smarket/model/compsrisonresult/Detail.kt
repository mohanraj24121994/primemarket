package com.smarket.model.compsrisonresult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Detail {
    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("price")
    @Expose
    var price: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null
}