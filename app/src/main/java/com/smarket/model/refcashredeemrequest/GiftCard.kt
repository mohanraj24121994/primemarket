package com.smarket.model.refcashredeemrequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GiftCard (

    @SerializedName("gift_amount")
    @Expose
    var gift_amount: String? = null,

    @SerializedName("merchant_name")
    @Expose
    var merchant_name: String? = null,

    @SerializedName("gift_quantity")
    @Expose
    var gift_quantity: String? = null,

    @SerializedName("gift_validity")
    @Expose
    var gift_validity:String?=null
)