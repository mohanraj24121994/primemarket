package com.smarket.model.refcashredeemrequest.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class RedeemRefcashResponse {
    @SerializedName("data")
    @Expose
    private var data: Any? = null

    @SerializedName("meta")
    @Expose
    var meta: Meta? = null

    fun getData(): Any? {
        return data
    }

    fun setData(data: Any?) {
        this.data = data
    }

}