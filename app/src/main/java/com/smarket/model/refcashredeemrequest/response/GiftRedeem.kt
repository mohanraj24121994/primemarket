package com.smarket.model.refcashredeemrequest.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GiftRedeem {
    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("customer_withdraw_request_id")
    @Expose
    private var customerWithdrawRequestId: Int? = null

    @SerializedName("gift_name")
    @Expose
    private var giftName: String? = null

    @SerializedName("gift_amount")
    @Expose
    private var giftAmount: Int? = null

    @SerializedName("gift_quantity")
    @Expose
    private var giftQuantity: Int? = null

    @SerializedName("expiry_date")
    @Expose
    private var expiryDate: String? = null

    @SerializedName("created_at")
    @Expose
    private var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    private var updatedAt: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getCustomerWithdrawRequestId(): Int? {
        return customerWithdrawRequestId
    }

    fun setCustomerWithdrawRequestId(customerWithdrawRequestId: Int?) {
        this.customerWithdrawRequestId = customerWithdrawRequestId
    }

    fun getGiftName(): String? {
        return giftName
    }

    fun setGiftName(giftName: String?) {
        this.giftName = giftName
    }

    fun getGiftAmount(): Int? {
        return giftAmount
    }

    fun setGiftAmount(giftAmount: Int?) {
        this.giftAmount = giftAmount
    }

    fun getGiftQuantity(): Int? {
        return giftQuantity
    }

    fun setGiftQuantity(giftQuantity: Int?) {
        this.giftQuantity = giftQuantity
    }

    fun getExpiryDate(): String? {
        return expiryDate
    }

    fun setExpiryDate(expiryDate: String?) {
        this.expiryDate = expiryDate
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String?) {
        this.updatedAt = updatedAt
    }
}