package com.smarket.model.refcashredeemrequest.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Meta {
    @SerializedName("refcash")
    @Expose
    var refcash: String? = null

    @SerializedName("min_withdraw_amount")
    @Expose
    private var minWithdrawAmount: String? = null

    @SerializedName("gift_redeem")
    @Expose
    private var giftRedeem: List<GiftRedeem?>? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    fun getMinWithdrawAmount(): String? {
        return minWithdrawAmount
    }

    fun setMinWithdrawAmount(minWithdrawAmount: String?) {
        this.minWithdrawAmount = minWithdrawAmount
    }

    fun getGiftRedeem(): List<GiftRedeem?>? {
        return giftRedeem
    }

    fun setGiftRedeem(giftRedeem: List<GiftRedeem?>?) {
        this.giftRedeem = giftRedeem
    }
}