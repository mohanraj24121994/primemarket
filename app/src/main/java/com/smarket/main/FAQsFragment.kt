package com.smarket.main

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.utils.convertDpToPixel
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.FAQ
import kotlinx.android.synthetic.main.fragment_faqs.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_faqs.view.*

/**
 * Created by MI-062 on 11/4/18.
 */
class FAQsFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): FAQsFragment {
            val fragment = FAQsFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var faqsAdapter: FAQsAdapter
    var faqsList = ArrayList<FAQ>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_faqs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_faqs.addItemDecoration(VerticalDividerDecoration(mActivity.convertDpToPixel(16F).toInt(), true))
        rv_faqs.layoutManager = linearLayoutManager
        faqsAdapter = FAQsAdapter()
        rv_faqs.adapter = faqsAdapter
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        if (mActivity.isUserTypeCustomer) {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        }
        tv_title.setAllCaps(false)
        tv_title.text = mActivity.resources.getString(R.string.faqs)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        faqsList = SMarket.appDB.faqDao().getData() as ArrayList<FAQ>
        faqsAdapter.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            else -> {
            }
        }
    }

    inner class FAQsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = inflater.inflate(R.layout.row_faqs, parent, false)

            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return faqsList.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            val faq = faqsList[holder.adapterPosition]

            with(faq) {

                holder.itemView.ctv_que.text = question
                holder.itemView.tv_ans.text = answer

                holder.itemView.ctv_que.isChecked = isChecked

                holder.itemView.tv_ans.visibility = if (isChecked) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                holder.itemView.setOnClickListener {
                    isChecked = !isChecked
                    notifyItemChanged(holder.adapterPosition)
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            if (!mActivity.isUserTypeCustomer) {
                mActivity.setDrawerEnable(true)
            }
        }
        super.onHiddenChanged(hidden)
    }
}