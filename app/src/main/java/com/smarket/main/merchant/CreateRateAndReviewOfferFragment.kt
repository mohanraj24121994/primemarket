package com.smarket.main.merchant

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.smarket.utils.*
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.model.SubOffer
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_create_offer.*
import kotlinx.android.synthetic.main.view_offer_type.*
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class CreateRateAndReviewOfferFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(offer: Offer): CreateRateAndReviewOfferFragment {
            val fragment = CreateRateAndReviewOfferFragment()
            val bundle = Bundle()
            bundle.putParcelable(OFFER_DETAILS, offer)
            fragment.arguments = bundle
            return fragment
        }
    }

    var subOfferCategory = ""
    var amount = ""
    var title = ""
    var conditions = ""
    var expiryDate = ""

    private var offer: Offer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_rate_and_review_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_skip.visibility = View.GONE

        getBundle()

        setHeader()

        val offerTypesList = resources.getStringArray(R.array.offer_types)

        val offerTypesAdapter = ArrayAdapter<String>(mActivity
                , R.layout.row_spinner_business_category
                , offerTypesList)
        offerTypesAdapter.setDropDownViewResource(R.layout.row_dropdown_business_category)
        sp_offer_category.adapter = offerTypesAdapter

        sp_offer_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                subOfferCategory = when (position) {
                    0 -> ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD
                    1 -> ApiParam.SUB_OFFER_CATEGORY_IN_STORE
                    2 -> ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT
                    else -> ""
                }
                if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {
                    et_amount.visibility = View.GONE
                    et_title.visibility = View.VISIBLE
                } else {
                    et_title.visibility = View.GONE
                    et_amount.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        et_offer_expiry_date.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(OFFER_DETAILS)) {
                offer = bundle.getParcelable(OFFER_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        iv_info.visibility = View.VISIBLE
        iv_info.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.create_rate_and_review_offer)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (!PreferenceUtil.getBooleanPref(PreferenceUtil.PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION)) {
            PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION, true)
            showCreateOfferInfoDialog()
        }

        if (offer != null && offer!!.id != 0L) {
            setOfferData()
        }
    }

    private fun setOfferData() {

        with(offer!!) {

            et_offer_expiry_date.setText(getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT))

            if (subOffers.isNotEmpty()) {
                val subOffer = subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_RATE }
                if (subOffer != null) {
                    with(subOffer) {
                        et_offer_conditions.setText(conditions)
                        when (subOfferCategory) {
                            ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                sp_offer_category.setSelection(1)
                                et_title.setText(title)
                            }
                            ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                sp_offer_category.setSelection(2)
                                et_amount.setText("$amount")
                            }
                            ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                sp_offer_category.setSelection(0)
                                et_amount.setText("$amount")
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_info -> showCreateOfferInfoDialog()
            R.id.et_offer_expiry_date -> {
                avoidDoubleClicks(et_offer_expiry_date)
                mActivity.hideKeyboard()
                mActivity.showDatePicker(et_offer_expiry_date)
            }
            R.id.btn_next -> {
                if (checkForValidations()) {
                    with(offer!!) {
                        offerType = ApiParam.OFFER_TYPE_RATE_AND_REVIEW
                        expiryDate = getChangedDateFormat(this@CreateRateAndReviewOfferFragment.expiryDate, APP_DATE_FORMAT, DATE_FORMAT)
                        var subOffer: SubOffer? = null
                        if (subOffers.isNotEmpty()) {
                            subOffer = subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_RATE }
                        }
                        if (subOffer == null) {
                            subOffer = SubOffer()
                        }
                        with(subOffer) {
                            subOfferType = ApiParam.SUB_OFFER_TYPE_RATE
                            subOfferCategory = this@CreateRateAndReviewOfferFragment.subOfferCategory
                            if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD ||
                                    subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT) {
                                amount = this@CreateRateAndReviewOfferFragment.amount.toDouble()
                            } else if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {
                                title = this@CreateRateAndReviewOfferFragment.title
                            }
                            conditions = this@CreateRateAndReviewOfferFragment.conditions
                        }
                        subOffers.clear()
                        subOffers.add(subOffer)
                    }

                    mActivity.addFragment(OfferPreviewFragment.getInstance(offer!! , null)
                            , true
                            , animationType = AnimationType.RightInZoomOut)
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {

            title = et_title.text.toString()

            if (!title.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.title_req))
                return false
            }
        } else {

            amount = et_amount.text.toString()

            if (!amount.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.amount_req))
                return false
            }
        }

        conditions = et_offer_conditions.text.toString()
        expiryDate = et_offer_expiry_date.text.toString()

        if (!expiryDate.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.expiry_date_req))
            return false
        }

        return true
    }

    private fun showCreateOfferInfoDialog() {
        var instructions = ""
        val cms = SMarket.appDB.cmsDao().getDataBySeoUrl("rate-review")
        if (cms != null) {
            instructions = Html.fromHtml(cms.content).toString()
            mActivity.showDialog(instructions, cms.title)
        }
    }
}