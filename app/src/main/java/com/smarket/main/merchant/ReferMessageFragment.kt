package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.utils.NetworkUtil
import com.smarket.utils.ProgressDialogUtil
import com.smarket.utils.showToast
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.UserActivityResponse
import kotlinx.android.synthetic.main.fragment_refer_message.*
import kotlinx.android.synthetic.main.header.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReferMessageFragment : BaseMainFragment() , View.OnClickListener {

    companion object {
        fun getInstance(): ReferMessageFragment {
            val fragment = ReferMessageFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        btn_save.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.add_referral_message)
        et_message?.setText(mActivity.userDetails?.referral_message)
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_refer_message , container , false)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_save -> {
                //if (et_message.text.toString().isNotEmpty()) {
                callMessageAPI(et_message.text.toString())
                /* } else {
                     Toast.makeText(mActivity , "Please Enter Message" , Toast.LENGTH_SHORT).show()
                 }*/
            }
        }
    }

    private fun callMessageAPI(message: String) {

        ProgressDialogUtil.showProgressDialog(mActivity)

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callMessage = WebApiClient.webApi().sendMessage(message)
        callMessage?.enqueue(object : Callback<UserActivityResponse?> {
            override fun onResponse(call: Call<UserActivityResponse?> , response: Response<UserActivityResponse?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        et_message.setText(response.body()?.getData()?.message)
                        et_message.setSelection(response.body()?.getData()?.message?.length ?: 0)
                        Toast.makeText(mActivity , response.body()?.getMeta()?.getMessage() , Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(mActivity , response.body()?.getMeta()?.getMessage() , Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<UserActivityResponse?> , t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
                Toast.makeText(mActivity , "Something went wrong" , Toast.LENGTH_LONG).show()
            }
        })
    }
}