package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.smarket.utils.hideKeyboard
import com.smarket.utils.isRequiredField
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.SubOffer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.view_create_offer.*
import kotlinx.android.synthetic.main.view_offer_type.*

/**
 * Created by MI-062 on 11/4/18.
 */
class ReferralOfferStep1Fragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(subOffer: SubOffer): ReferralOfferStep1Fragment {
            val fragment = ReferralOfferStep1Fragment()
            val bundle = Bundle()
            bundle.putParcelable(SUB_OFFER_DETAILS, subOffer)
            fragment.arguments = bundle
            return fragment
        }
    }

    var subOfferCategory = ""
    var amount = ""
    var title = ""
    var conditions = ""

    private var subOffer: SubOffer? = null

    private lateinit var mParentFragment: CreateReferralOfferFragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_create_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        et_offer_expiry_date.visibility = View.GONE

        getBundle()

        val offerTypesList = resources.getStringArray(R.array.offer_types)

        val offerTypesAdapter = ArrayAdapter<String>(mActivity
                , R.layout.row_spinner_business_category
                , offerTypesList)
        offerTypesAdapter.setDropDownViewResource(R.layout.row_dropdown_business_category)
        sp_offer_category.adapter = offerTypesAdapter

        sp_offer_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                subOfferCategory = when (position) {
                    0 -> ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD
                    1 -> ApiParam.SUB_OFFER_CATEGORY_IN_STORE
                    2 -> ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT
                    else -> ""
                }
                if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {
                    et_amount.visibility = View.GONE
                    et_title.visibility = View.VISIBLE
                } else {
                    et_title.visibility = View.GONE
                    et_amount.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        btn_next.setOnClickListener(this)
        btn_skip.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(SUB_OFFER_DETAILS)) {
                subOffer = bundle.getParcelable(SUB_OFFER_DETAILS)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mParentFragment = parentFragment as CreateReferralOfferFragment

        if (subOffer != null && subOffer!!.id != 0L) {

            btn_skip.visibility = View.GONE

            setSubOfferData()
        }
    }

    private fun setSubOfferData() {

        with(subOffer!!) {
            et_offer_conditions.setText(conditions)
            when (subOfferCategory) {
                ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                    sp_offer_category.setSelection(1)
                    et_title.setText(title)
                }
                ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                    sp_offer_category.setSelection(2)
                    et_amount.setText("$amount")
                }
                ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                    sp_offer_category.setSelection(0)
                    et_amount.setText("$amount")
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_next -> {
                if (checkForValidations()) {
                    with(subOffer!!) {
                        subOfferType = ApiParam.SUB_OFFER_TYPE_REFERRAL
                        subOfferCategory = this@ReferralOfferStep1Fragment.subOfferCategory
                        if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD ||
                                subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT) {
                            amount = this@ReferralOfferStep1Fragment.amount.toDouble()
                        } else if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {
                            title = this@ReferralOfferStep1Fragment.title
                        }
                        conditions = this@ReferralOfferStep1Fragment.conditions
                    }

                    if (subOffer!!.id == 0L) {
                        if (mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id && it.subOfferType == ApiParam.SUB_OFFER_TYPE_REFERRAL } != null) {
                            mParentFragment.offer!!.subOffers.set(
                                    mParentFragment.offer!!.subOffers.indexOf(
                                            mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id && it.subOfferType == ApiParam.SUB_OFFER_TYPE_REFERRAL }), subOffer!!)
                        } else {
                            mParentFragment.offer!!.subOffers.add(subOffer!!)
                        }
                    } else {
                        if (mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id } != null) {
                            mParentFragment.offer!!.subOffers.set(
                                    mParentFragment.offer!!.subOffers.indexOf(
                                            mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id }), subOffer!!)
                        }
                    }

                    mParentFragment.moveToNextStep()
                }
            }
            R.id.btn_skip -> {
                if ((et_amount.visibility == View.VISIBLE && et_amount.text.toString().isRequiredField())
                        || (et_title.visibility == View.VISIBLE && et_title.text.toString().isRequiredField())) {
                    mActivity.showDialogWithAction(mActivity.resources.getString(R.string.skip_referral)
                            , positiveButtonLabel = mActivity.resources.getString(R.string.skip), showNegativeButton = true) {
                        et_amount.setText("")
                        et_title.setText("")
                        et_offer_conditions.setText("")
                        if (subOffer!!.id == 0L && mParentFragment.offer!!.subOffers.isNotEmpty()) {
                            if (mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id && it.subOfferType == ApiParam.SUB_OFFER_TYPE_REFERRAL } != null) {
                                mParentFragment.offer!!.subOffers.removeAt(
                                        mParentFragment.offer!!.subOffers.indexOf(
                                                mParentFragment.offer!!.subOffers.find { it.id == subOffer!!.id && it.subOfferType == ApiParam.SUB_OFFER_TYPE_REFERRAL }))
                            } else {
                                mParentFragment.offer!!.subOffers.add(subOffer!!)
                            }
                        }
                        mParentFragment.moveToNextStep()
                    }
                } else {
                    mParentFragment.moveToNextStep()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        if (subOfferCategory == ApiParam.SUB_OFFER_CATEGORY_IN_STORE) {

            title = et_title.text.toString()

            if (!title.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.title_req))
                return false
            }
        } else {

            amount = et_amount.text.toString()

            if (!amount.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.amount_req))
                return false
            }
        }

        conditions = et_offer_conditions.text.toString()

        return true
    }
}