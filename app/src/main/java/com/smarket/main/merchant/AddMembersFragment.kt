package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.R
import com.smarket.interfaces.AddEditDeleteOfferManager
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_members.*
import kotlinx.android.synthetic.main.header.*


class AddMembersFragment : BaseMainFragment() , View.OnClickListener {

    private var disposable: Disposable? = null
    var id: Int? = 0
    var email = ""
    var isUpdate = false

    companion object {
        fun getInstance(email: String , id: Int , isUpdate: Boolean): AddMembersFragment {
            val fragment = AddMembersFragment()
            val bundle = Bundle()
            bundle.putString("email" , email)
            bundle.putInt("id" , id)
            bundle.putBoolean("isUpdate" , isUpdate)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        btn_save.setOnClickListener(this)

    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("email")) {
                email = bundle.getString("email") ?: ""
                et_Email.setText(email)
                et_ConfirmEmail.setText(email)
            }
            if (bundle.containsKey("id")) {
                id = bundle.getInt("id")
            }
            if (bundle.containsKey("isUpdate")) {
                isUpdate = bundle.getBoolean("isUpdate")
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.add_referral_member)
    }


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_members , container , false)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_save -> {
                if (checkForValidations()) {
                    if (! isUpdate) {
                        addMemberAPI(email = et_ConfirmEmail.text.toString())
                    } else {
                        updateAPI(email = et_ConfirmEmail.text.toString())
                    }
                }
            }
        }
    }

    private fun checkForValidations(): Boolean {
        val getEmail = et_Email.text.toString()
        val getConfirmEmail = et_ConfirmEmail.text.toString()
        when {
            ! getEmail.isRequiredField() -> {
                Toast.makeText(mActivity , mActivity.resources.getString(R.string.email_req) , Toast.LENGTH_SHORT).show()
                return false
            }
            ! getEmail.isValidEmail() -> {
                Toast.makeText(mActivity , mActivity.resources.getString(R.string.valid_email_req) , Toast.LENGTH_SHORT).show()
                return false
            }
            ! getConfirmEmail.isRequiredField() -> {
                Toast.makeText(mActivity , mActivity.resources.getString(R.string.confirm_email_req) , Toast.LENGTH_SHORT).show()
                return false
            }
            ! getConfirmEmail.isValidEmail() -> {
                Toast.makeText(mActivity , mActivity.resources.getString(R.string.valid_email_req) , Toast.LENGTH_SHORT).show()
                return false
            }
            getEmail != getConfirmEmail -> {
                Toast.makeText(mActivity , mActivity.resources.getString(R.string.email_mismatch) , Toast.LENGTH_SHORT).show()
                return false
            }
            else -> {
                return true
            }
        }
    }

    private fun addMemberAPI(email: String) {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.EMAIL] = email
        disposable = WebApiClient.webApi().addMemberAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    if (disposable == null) return@subscribe
                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (getMeta() != null) {
                                    mActivity.showDialogWithAction(getMeta()?.getMessage() ?: "") {
                                        mActivity.popBackStack(ManageMembersFragment::class.java.canonicalName)
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }
                    ProgressDialogUtil.hideProgressDialog()
                } , {
                    if (disposable == null) return@subscribe
                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                })

    }

    private fun updateAPI(email: String) {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)
        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.EMAIL] = email
        hashMap[ApiParam.MEMBER_ID] = id.toString()

        disposable = WebApiClient.webApi().updateMemberAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    if (disposable == null) return@subscribe
                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (getMeta() != null) {
                                    mActivity.showDialogWithAction(getMeta()?.getMessage() ?: "") {
                                        mActivity.popBackStack(ManageMembersFragment::class.java.canonicalName)
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }
                    ProgressDialogUtil.hideProgressDialog()
                } , {
                    if (disposable == null) return@subscribe
                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                })

    }
}