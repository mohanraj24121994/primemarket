package com.smarket.main.merchant

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.isRequiredField
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.utils.APP_DATE_FORMAT
import com.smarket.utils.DATE_FORMAT
import com.smarket.utils.getChangedDateFormat
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.dialog_read_more.view.*
import kotlinx.android.synthetic.main.divider.view.*
import kotlinx.android.synthetic.main.fragment_tab_merchant_offers.*
import kotlinx.android.synthetic.main.view_offer_details_with_read_more_cust.*
import kotlinx.android.synthetic.main.view_offer_details_with_read_more_cust.view.*
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import androidx.core.content.ContextCompat


/**
 * Created by MI-062 on 11/4/18.
 */
class TabMerchantOffersFragment : BaseMainFragment() {

    companion object {
        fun getInstance(): TabMerchantOffersFragment {
            val fragment = TabMerchantOffersFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var giftCardDrawable: Drawable
    lateinit var inStoreDrawable: Drawable
    lateinit var storeCreditDrawable: Drawable

    var offers = ArrayList<Offer>()

    var mParentFragment: MerchantDetailsFragment? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_merchant_offers , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        giftCardDrawable = mActivity.resources.getDrawable(R.drawable.gift_card)
        inStoreDrawable = mActivity.resources.getDrawable(R.drawable.in_store)
        storeCreditDrawable = mActivity.resources.getDrawable(R.drawable.store_credit)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mParentFragment = parentFragment as MerchantDetailsFragment

        if (mParentFragment != null) {
            if (mParentFragment !!.merchantDetails != null) {
                offers = mParentFragment !!.merchantDetails !!.offer
            }
        }

        setMerchantOfferData()
    }

    private fun setMerchantOfferData() {

        with(offers) {
            if (offers.isEmpty()) {
                tv_no_data_found.visibility = View.VISIBLE
                ll_referral_offer.visibility = View.GONE
                ll_rate_and_review_offer.visibility = View.GONE
            } else {

                val referralOfferData = offers.find { it.offerType == ApiParam.OFFER_TYPE_REFERRAL }
                if (referralOfferData != null) {
                    setReferralOfferData(referralOfferData)
                } else {
                    ll_referral_offer.visibility = View.GONE
                }

                val rateAndReviewOfferData = offers.find { it.offerType == ApiParam.OFFER_TYPE_RATE_AND_REVIEW }
                if (rateAndReviewOfferData != null) {
                    setRateAndReviewOfferData(rateAndReviewOfferData)
                } else {
                    ll_rate_and_review_offer.visibility = View.GONE
                }
            }
        }
    }

    private fun setRateAndReviewOfferData(offers: Offer) {

        ll_rate_and_review_offer_details.removeAllViews()

        with(offers) {

            tv_rate_and_review_offer_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"
            if (subOffers.isNotEmpty()) {
                subOffers.forEachIndexed { index , subOffer ->
                    if (subOffer.subOfferType == ApiParam.SUB_OFFER_TYPE_RATE) {
                        kotlin.with(subOffer) {
                            val rateAndReviewOfferView = LayoutInflater.from(mActivity).inflate(R.layout.view_offer_details_with_read_more_cust , ll_rate_and_review_offer_details , false)
                            rateAndReviewOfferView.tv_offer_type.visibility = View.GONE
                            when (subOfferCategory) {
                                ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} $$amount"
                                    } else {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }
//                                    rateAndReviewOfferView.tv_sub_offer_type.text = title
                                    rateAndReviewOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , inStoreDrawable , null)
                                }
                                ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} $$amount"
                                    } else {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }
                                    rateAndReviewOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , storeCreditDrawable , null)
                                }
                                ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} $$amount"
                                    } else {
                                        rateAndReviewOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} ₹$amount"
                                    }
                                    rateAndReviewOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , giftCardDrawable , null)
                                }
                            }

                            if (conditions.isRequiredField() && conditions.length > 30) {
                                rateAndReviewOfferView.txt_condition.text = conditions.substring(0 , 30) + "..."
                                rateAndReviewOfferView.tv_read_more.visibility = View.VISIBLE
                            } else {
                                rateAndReviewOfferView.txt_condition.text = conditions
                                rateAndReviewOfferView.tv_read_more.visibility = View.GONE
                            }
                            rateAndReviewOfferView.tv_read_more.setOnClickListener {
                                showReadMoreDialog(conditions)
                            }

                            ll_rate_and_review_offer_details.addView(rateAndReviewOfferView)

                        }
                    }
                }
            }
        }

        ll_rate_and_review_offer.visibility = View.VISIBLE
    }

    private fun setReferralOfferData(offers: Offer) {

        ll_referral_offer_details.removeAllViews()

        with(offers) {

            tv_referral_offer_expiry_date.text = "${mActivity.resources.getString(com.smarket.R.string.expires_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

            if (subOffers.isNotEmpty()) {
                subOffers.forEachIndexed { index , subOffer ->
                    if (subOffer.subOfferType != ApiParam.SUB_OFFER_TYPE_RATE) {
                        kotlin.with(subOffer) {
                            val referralOfferView = LayoutInflater.from(mActivity).inflate(R.layout.view_offer_details_with_read_more_cust , ll_referral_offer_details , false)
                            /*referralOfferView.tv_offer_type.text = when (subOfferType) {
                                ApiParam.SUB_OFFER_TYPE_REFERRAL -> resources.getString(R.string.referral)
                                ApiParam.SUB_OFFER_TYPE_BONUS -> resources.getString(R.string.bonus)
                                ApiParam.SUB_OFFER_TYPE_REWARD -> resources.getString(R.string.reward)
                                else -> ""
                            }*/

                            referralOfferView.tv_offer_type.text = when (subOfferType) {
                                ApiParam.SUB_OFFER_TYPE_REFERRAL -> {
                                    //to set rounded purple background
                                    val drawable = ContextCompat.getDrawable(mActivity , R.drawable.button_bg)
                                    drawable !!.colorFilter = PorterDuffColorFilter(mActivity.resources.getColor(R.color.purple) , PorterDuff.Mode.SRC)
                                    referralOfferView.tv_offer_type.background = drawable

                                    mActivity.resources.getString(R.string.smark_offer)
                                }
                                ApiParam.SUB_OFFER_TYPE_BONUS -> {
                                    //to set rounded light dark purple background
                                    val drawable = ContextCompat.getDrawable(mActivity , R.drawable.button_bg)
                                    drawable !!.colorFilter = PorterDuffColorFilter(mActivity.resources.getColor(R.color.light_dark_purple) , PorterDuff.Mode.SRC)
                                    referralOfferView.tv_offer_type.background = drawable

                                    mActivity.resources.getString(R.string.welcome_bonus_offer)
                                }
                                ApiParam.SUB_OFFER_TYPE_REWARD -> {
                                    //to set rounded  dark purple background
                                    val drawable = ContextCompat.getDrawable(mActivity , R.drawable.button_bg)
                                    drawable !!.colorFilter = PorterDuffColorFilter(mActivity.resources.getColor(R.color.dark_purple) , PorterDuff.Mode.SRC)
                                    referralOfferView.tv_offer_type.background = drawable

                                    mActivity.resources.getString(R.string.thank_u_reward)
                                }
                                else -> ""
                            }
                            when (subOfferCategory) {
                                ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                    referralOfferView.tv_sub_offer_type.text = title
                                    referralOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , inStoreDrawable , null)
                                }
                                ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        referralOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} $$amount"
                                    } else {
                                        referralOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }
                                    referralOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , storeCreditDrawable , null)
                                }
                                ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        referralOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} $$amount"
                                    } else {
                                        referralOfferView.tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} ₹$amount"
                                    }
                                    referralOfferView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(null , null , giftCardDrawable , null)
                                }
                            }
                            if (conditions.isRequiredField() && conditions.length > 30) {
                                referralOfferView.txt_condition.text = conditions.substring(0 , 30) + "..."
                                referralOfferView.tv_read_more.visibility = View.VISIBLE
                            } else {
                                referralOfferView.txt_condition.text = conditions
                                referralOfferView.tv_read_more.visibility = View.GONE
                            }
                            referralOfferView.tv_read_more.setOnClickListener {
                                showReadMoreDialog(conditions)
                            }

                            ll_referral_offer_details.addView(referralOfferView)

                            if (index < subOffers.size - 1) {
                                addDivider()
                            }
                        }

                    }
                }
            }
        }

        ll_referral_offer.visibility = View.VISIBLE
    }

    private fun addDivider() {

        val dividerView = LayoutInflater.from(mActivity).inflate(R.layout.divider , ll_referral_offer_details , false)
        dividerView.view_divider.setBackgroundColor(mActivity.resources.getColor(R.color.dim_gray_txt))

        ll_referral_offer_details.addView(dividerView)
    }

    private fun showReadMoreDialog(condition: String) {

        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_read_more , null , false)

        dialogView.tv_condition.text = condition

        AlertDialog.Builder(mActivity)
                .setView(dialogView)
                .setNegativeButton(mActivity.resources.getString(R.string.close)) { dialog , which ->
                    dialog.dismiss()
                }
                .show()
    }
}