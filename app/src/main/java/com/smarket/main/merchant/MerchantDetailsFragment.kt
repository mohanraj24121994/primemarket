package com.smarket.main.merchant

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.RedemptionHistoryFragment
import com.smarket.main.customer.topBar.ReferralAlertsFragment
import com.smarket.main.customer.UsersRateFragment
import com.smarket.model.MerchantDetails
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.frgament_merchant_details.*
import kotlinx.android.synthetic.main.header.*
import utils.AnimationType
import android.view.MotionEvent

/**
 * Created by MI-062 on 11/4/18.
 */
class MerchantDetailsFragment : BaseMainFragment() , View.OnClickListener {

    companion object {
        fun getInstance(merchantId: Long): MerchantDetailsFragment {
            val fragment = MerchantDetailsFragment()
            val bundle = Bundle()
            bundle.putLong(MERCHANT_ID , merchantId)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var tabTitleArray: Array<String>

    var merchantDetails: MerchantDetails? = null

    private var disposable: Disposable? = null

    var merchantId = 0L

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frgament_merchant_details , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        tabTitleArray = arrayOf(mActivity.resources.getString(R.string.details) , resources.getString(R.string.offers))

        tv_referral_alerts.setOnClickListener(this)
        tv_redemption_history.setOnClickListener(this)
        tv_rate_merchant.setOnClickListener(this)
        tv_refer_merchant.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(MERCHANT_ID)) {
                merchantId = bundle.getLong(MERCHANT_ID)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rating_bar.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View? , event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (merchantDetails?.averageRating !! > 0F) {
                            mActivity.addFragment(UsersRateFragment.getInstance(merchantId) , true , animationType = AnimationType.RightInZoomOut)
                        }
                    }
                }

                return v?.onTouchEvent(event) ?: true
            }
        })

        callMerchantDetailsAPI()
    }

    private fun setUpTabLayoutAndViewPager() {

        vp_merchant_details.adapter = ViewPagerAdapter(childFragmentManager , tabTitleArray.size)
        tab_layout.setupWithViewPager(vp_merchant_details)
        vp_merchant_details.offscreenPageLimit = 2

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                vp_merchant_details.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        tab_layout.getTabAt(1) !!.select()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_referral_alerts -> mActivity.addFragment(ReferralAlertsFragment.getInstance(merchantId = merchantId) , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_redemption_history -> mActivity.addFragment(RedemptionHistoryFragment.getInstance(merchantId = merchantId) , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_rate_merchant -> mActivity.addFragment(RateAndReferMerchantFragment.getInstance(merchantDetails = merchantDetails) , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_refer_merchant -> mActivity.addFragment(RateAndReferMerchantFragment.getInstance(true , merchantDetails = merchantDetails) , true , animationType = AnimationType.RightInZoomOut)
            else -> {
            }
        }
    }

    private inner class ViewPagerAdapter(fragmentManager: FragmentManager , internal var tabCount: Int) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> TabMerchantDetailsFragment.getInstance()
                1 -> TabMerchantOffersFragment.getInstance()
                else -> throw IllegalStateException("position $position is invalid for this viewpager")
            }
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitleArray[position]
        }
    }

    private fun callMerchantDetailsAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        progress_bar.visibility = View.VISIBLE

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.MERCHANT_ID] = "$merchantId"

        disposable = WebApiClient.webApi().merchantDetailsAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            kotlin.with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        merchantDetails = data

                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            setMerchantData()

                            progress_bar.visibility = View.GONE
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            setMerchantData()

                            progress_bar.visibility = View.GONE
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun setMerchantData() {

        if (merchantDetails == null) {

            tv_no_data_found.visibility = View.VISIBLE
            ll_all_details.visibility = View.GONE

        } else {

            tv_no_data_found.visibility = View.GONE
            ll_all_details.visibility = View.VISIBLE

            setUpTabLayoutAndViewPager()

            with(merchantDetails !!) {

                tv_title.text = businessName

                sdv_logo.loadFrescoImage(businessLogo)
                if (tagLine.isRequiredField()) {
                    tv_tag_line.visibility = View.VISIBLE
                    tv_tag_line.text = tagLine
                } else {
                    tv_tag_line.visibility = View.GONE
                }
                rating_bar.rating = averageRating
                tv_average_ratings.text = "$averageRating"
                tv_ratings.text = "($noOfRating)"
                if (mActivity.userDetails?.countryCode != "+91") {
                    tv_store_credit.setCustomText("$$storeCredit" , mActivity.resources.getString(R.string.store_credit) , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt) , fontFamilyId = R.font.poppins_regular)
                } else {
                    tv_store_credit.setCustomText("₹$storeCredit" , mActivity.resources.getString(R.string.store_credit) , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt) , fontFamilyId = R.font.poppins_regular)
                }

                tv_referrals.setCustomText("$referrals" , mActivity.resources.getString(R.string.referrals) , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt) , fontFamilyId = R.font.poppins_regular)

                iv_website.visibility = if (website.isRequiredField()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                iv_call.visibility = if (mobile.isRequiredField()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                iv_call.setOnClickListener {
                    mActivity.makeCall(mobile)
                }
                iv_website.setOnClickListener {
                    mActivity.showWeb(website)
                }

                tv_referral_alerts.visibility = if (showReferralAlerts == ApiParam.TRUE) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }

                tv_redemption_history.visibility = if (showUnclaimedOffers == ApiParam.TRUE) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }

                tv_rate_merchant.visibility = if (showRateMerchant == ApiParam.TRUE) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }

                /*tv_refer_merchant.visibility = if (showReferMerchant == ApiParam.TRUE) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }*/

                cl_nav_icons.visibility = if (showReferralAlerts != ApiParam.TRUE
                        && showUnclaimedOffers != ApiParam.TRUE
                        && showRateMerchant != ApiParam.TRUE
                        && showReferMerchant != ApiParam.TRUE) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        }
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}