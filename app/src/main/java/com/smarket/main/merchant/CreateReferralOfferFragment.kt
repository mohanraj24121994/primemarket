package com.smarket.main.merchant

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.model.SubOffer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.fragment_create_referral_offer.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 11/4/18.
 */
class CreateReferralOfferFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(offer: Offer): CreateReferralOfferFragment {
            val fragment = CreateReferralOfferFragment()
            val bundle = Bundle()
            bundle.putParcelable(OFFER_DETAILS, offer)
            fragment.arguments = bundle
            return fragment
        }
    }

    var offer: Offer? = null

    var dialog_title = ""
    var dialog_content = ""
    var step = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_referral_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        setUpViewPager()

        tv_step_1.setOnClickListener(this)
        ctv_step_1.setOnClickListener(this)
        tv_step_2.setOnClickListener(this)
        ctv_step_2.setOnClickListener(this)
        tv_step_3.setOnClickListener(this)
        ctv_step_3.setOnClickListener(this)
        tv_step_4.setOnClickListener(this)
        ctv_step_4.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(OFFER_DETAILS)) {
                offer = bundle.getParcelable(OFFER_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        iv_info.visibility = View.VISIBLE
        iv_info.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.create_referral_offer)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (!PreferenceUtil.getBooleanPref(PreferenceUtil.PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION)) {
            PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_HIDE_CREATE_OFFER_INSTRUCTION, true)
            showCreateOfferInfoDialog()
        }
    }

    private fun setUpViewPager() {

        /*to show 1st step as selected*/
        tv_step_1.text = ""
        tv_step_1.isSelected = true
        ctv_step_1.isChecked = true

        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, 4)
        view_pager.adapter = viewPagerAdapter
        view_pager.offscreenPageLimit = 4

        view_pager.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                updateStepBar(position)
            }
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_info -> showCreateOfferInfoDialog()
            R.id.tv_step_1,
            R.id.ctv_step_1 -> {
                if (tv_step_1.isSelected && ctv_step_1.isChecked) {
                    moveToStep(1)

                    iv_info.visibility = View.VISIBLE

                    step = 1
                }
            }
            R.id.tv_step_2,
            R.id.ctv_step_2 -> {
                if (tv_step_2.isSelected && ctv_step_2.isChecked) {
                    moveToStep(2)

                    iv_info.visibility = View.VISIBLE

                    step = 2
                }
            }
            R.id.tv_step_3,
            R.id.ctv_step_3 -> {
                if (tv_step_3.isSelected && ctv_step_3.isChecked) {
                    moveToStep(3)

                    iv_info.visibility = View.VISIBLE

                    step = 3
                }
            }
            R.id.tv_step_4,
            R.id.ctv_step_4 -> {
                if (tv_step_4.isSelected && ctv_step_4.isChecked) {
                    moveToStep(4)

                    iv_info.visibility = View.GONE

                    step = 4
                }
            }
            else -> {
            }
        }
    }

    private fun showCreateOfferInfoDialog() {

        when (step) {
            1 -> {
                val cms = SMarket.appDB.cmsDao().getDataBySeoUrl("SMARK-offer")
                if (cms != null) {
                    dialog_content = cms.content
                    dialog_title = cms.title
                }
            }
            2 -> {
                val cms = SMarket.appDB.cmsDao().getDataBySeoUrl("welcome-bonus")
                if (cms != null) {
                    dialog_content = cms.content
                    dialog_title = cms.title
                }
            }
            3 -> {
                val cms = SMarket.appDB.cmsDao().getDataBySeoUrl("thank-you-reward")
                if (cms != null) {
                    dialog_content = cms.content
                    dialog_title = cms.title
                }
            }
            4 -> {
                val cms = SMarket.appDB.cmsDao().getDataBySeoUrl("create-offer-instruction")
                if (cms != null) {
                    dialog_content = cms.content
                    dialog_title = cms.title
                }
            }
        }

        mActivity.showDialog(Html.fromHtml(dialog_content).toString(), dialog_title)
    }

    private inner class ViewPagerAdapter(fragmentManager: androidx.fragment.app.FragmentManager , internal var tabCount: Int) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    var subOffer: SubOffer? = null
                    if (offer!!.subOffers.isNotEmpty()) {
                        subOffer = offer!!.subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_REFERRAL }
                    }
                    if (subOffer == null) {
                        subOffer = SubOffer()
                    }
                    ReferralOfferStep1Fragment.getInstance(subOffer)
                }
                1 -> {
                    var subOffer: SubOffer? = null
                    if (offer!!.subOffers.isNotEmpty()) {
                        subOffer = offer!!.subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_BONUS }
                    }
                    if (subOffer == null) {
                        subOffer = SubOffer()
                    }
                    ReferralOfferStep2Fragment.getInstance(subOffer)
                }
                2 -> {
                    var subOffer: SubOffer? = null
                    if (offer!!.subOffers.isNotEmpty()) {
                        subOffer = offer!!.subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_REWARD }
                    }
                    if (subOffer == null) {
                        subOffer = SubOffer()
                    }
                    ReferralOfferStep3Fragment.getInstance(subOffer)
                }
                3 -> ReferralOfferStep4Fragment.getInstance()
                else -> throw IllegalStateException("position $position is invalid for this viewpager")
            }
        }

        override fun getCount(): Int {
            return tabCount
        }
    }

    fun moveToNextStep() {

        mActivity.hideKeyboard()

        view_pager.currentItem = view_pager.currentItem + 1
    }

    private fun moveToStep(stepNo: Int) {

        mActivity.hideKeyboard()

        view_pager.currentItem = stepNo - 1
    }

    private fun updateStepBar(selectedPage: Int) {

        when (selectedPage) {
            0 -> {
                tv_step_1.text = ""
                tv_step_1.isSelected = true
                ctv_step_1.isChecked = true
                tv_step_2.text = "2"
                tv_step_2.isSelected = false
                ctv_step_2.isChecked = false
                tv_step_3.text = "3"
                tv_step_3.isSelected = false
                ctv_step_3.isChecked = false
                tv_step_4.text = "4"
                tv_step_4.isSelected = false
                ctv_step_4.isChecked = false
                tv_step_5.text = "5"
                tv_step_5.isSelected = false
                ctv_step_5.isChecked = false

                iv_info.visibility = View.VISIBLE

                step = 1
            }
            1 -> {
                tv_step_1.text = ""
                tv_step_1.isSelected = true
                ctv_step_1.isChecked = true
                tv_step_2.text = ""
                tv_step_2.isSelected = true
                ctv_step_2.isChecked = true
                tv_step_3.text = "3"
                tv_step_3.isSelected = false
                ctv_step_3.isChecked = false
                tv_step_4.text = "4"
                tv_step_4.isSelected = false
                ctv_step_4.isChecked = false
                tv_step_5.text = "5"
                tv_step_5.isSelected = false
                ctv_step_5.isChecked = false

                iv_info.visibility = View.VISIBLE

                step = 2
            }
            2 -> {
                tv_step_1.text = ""
                tv_step_1.isSelected = true
                ctv_step_1.isChecked = true
                tv_step_2.text = ""
                tv_step_2.isSelected = true
                ctv_step_2.isChecked = true
                tv_step_3.text = ""
                tv_step_3.isSelected = true
                ctv_step_3.isChecked = true
                tv_step_4.text = "4"
                tv_step_4.isSelected = false
                ctv_step_4.isChecked = false
                tv_step_5.text = "5"
                tv_step_5.isSelected = false
                ctv_step_5.isChecked = false

                iv_info.visibility = View.VISIBLE

                step = 3
            }
            3 -> {
                tv_step_1.text = ""
                tv_step_1.isSelected = true
                ctv_step_1.isChecked = true
                tv_step_2.text = ""
                tv_step_2.isSelected = true
                ctv_step_2.isChecked = true
                tv_step_3.text = ""
                tv_step_3.isSelected = true
                ctv_step_3.isChecked = true
                tv_step_4.text = ""
                tv_step_4.isSelected = true
                ctv_step_4.isChecked = true
                tv_step_5.text = "5"
                tv_step_5.isSelected = false
                ctv_step_5.isChecked = false

                iv_info.visibility = View.GONE

                step = 4
            }
        }
    }
}