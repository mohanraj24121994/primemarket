package com.smarket.main.merchant

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.MerchantDetails
import kotlinx.android.synthetic.main.fragment_tab_merchant_details.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.smarket.utils.*
import kotlinx.android.synthetic.main.dialog_read_more.view.*
import kotlinx.android.synthetic.main.view_offer_details_with_read_more_cust.view.*


/**
 * Created by MI-062 on 11/4/18.
 */
class TabMerchantDetailsFragment : BaseMainFragment(), View.OnClickListener {
    companion object {
        fun getInstance(): TabMerchantDetailsFragment {
            val fragment = TabMerchantDetailsFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var merchantDetails: MerchantDetails? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_merchant_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setUpMap()

        tv_location.setOnClickListener(this)
//        sdv_map.setOnClickListener(this)
        ll_map.setOnClickListener(this)
    }

    private fun setUpMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFragment!!.getMapAsync { googleMap ->
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            //Disable Map Toolbar:
            googleMap.uiSettings.isMapToolbarEnabled = false
            googleMap.uiSettings.isZoomControlsEnabled = false
            googleMap.uiSettings.setAllGesturesEnabled(false)

            googleMap.clear() //clear old markers

            val latLong = LatLng(merchantDetails?.latitude!!, merchantDetails?.longitude!!)

            googleMap.addMarker(MarkerOptions()
                    .position(latLong))
            //Move the camera to the user's location and zoom in!
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLong, 16f))
        }
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        merchantDetails = (parentFragment as MerchantDetailsFragment).merchantDetails

        setMerchantDetails()
    }

    private fun setMerchantDetails() {

        if (merchantDetails != null) {
            with(merchantDetails!!) {

                tv_details.text = description
                tv_product_and_services.text = productAndServices
                tv_location.text = address

                if (description.isRequiredField() && description.length > 30) {
                    tv_details.text = description.substring(0, 30) + "..."
                    tv_read_more.visibility = View.VISIBLE
                } else {
                    tv_details.text = description
                    tv_read_more.visibility = View.GONE
                }
                tv_read_more.setOnClickListener {
                    showReadMoreDialog(description)
                }

            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_location,
                /*   R.id.sdv_map,*/
            R.id.ll_map -> {
                if (merchantDetails != null) {
                    navigateToGoogleMap(mActivity, merchantDetails!!.latitude, merchantDetails!!.longitude, merchantDetails!!.address)
                }
            }
            else -> {
            }
        }
    }

    private fun showReadMoreDialog(condition: String) {

        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_read_more, null, false)

        dialogView.tv_condition.text = condition

        AlertDialog.Builder(mActivity)
                .setView(dialogView)
                .setNegativeButton(mActivity.resources.getString(R.string.close)) { dialog, which ->
                    dialog.dismiss()
                }
                .show()
    }
}