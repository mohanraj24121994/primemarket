package com.smarket.main.merchant

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.interfaces.AddEditDeleteOfferManager
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.divider.view.*
import kotlinx.android.synthetic.main.fragment_offer_preview.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_offer_details.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Created by MI-062 on 11/4/18.
 */
class OfferPreviewFragment : BaseMainFragment() , View.OnClickListener {


    companion object {
        var couponPic: File? = null

        fun getInstance(offer: Offer , picture: File?): OfferPreviewFragment {
            val fragment = OfferPreviewFragment()
            val bundle = Bundle()
            bundle.putParcelable(OFFER_DETAILS , offer)
            fragment.arguments = bundle

            if (picture != null) {
                couponPic = picture
            }
            return fragment
        }
    }

    lateinit var giftCardDrawable: Drawable
    lateinit var inStoreDrawable: Drawable
    lateinit var storeCreditDrawable: Drawable

    private var offer: Offer? = null

    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offer_preview , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        giftCardDrawable = mActivity.resources.getDrawable(R.drawable.gift_card)
        inStoreDrawable = mActivity.resources.getDrawable(R.drawable.in_store)
        storeCreditDrawable = mActivity.resources.getDrawable(R.drawable.store_credit)

        btn_save.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(OFFER_DETAILS)) {
                offer = bundle.getParcelable(OFFER_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (offer != null) {
            setOfferData()
        }
    }

    private fun setOfferData() {

        if (mActivity.userDetails != null) {
            with(mActivity.userDetails !!) {
                sdv_logo.loadFrescoImage(businessLogo)
                tv_business_name.text = businessName
                if (tagLine.isRequiredField()) {
                    tv_tag_line.visibility = View.VISIBLE
                    tv_tag_line.text = tagLine
                } else {
                    tv_tag_line.visibility = View.GONE
                }
                rating_bar.rating = averageRating
                tv_average_ratings.text = "$averageRating"
                tv_ratings.text = "($noOfRating)"
            }
        }

        with(offer !!) {

            tv_title.text = mActivity.resources.getString(R.string.preview)

            ll_offer_details.removeAllViews()

            tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${
                getChangedDateFormat(
                    expiryDate ,
                    DATE_FORMAT ,
                    APP_DATE_FORMAT
                )
            }"

            if (status == 3) {
                tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${
                    getChangedDateFormat(
                        expiryDate ,
                        DATE_FORMAT ,
                        APP_DATE_FORMAT
                    )
                }"

                tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
            }

            if (subOffers.isNotEmpty()) {

                subOffers.sortBy { it.subOfferType }

                subOffers.forEachIndexed { index , subOffer ->
                    with(subOffer) {
                        val offerView = LayoutInflater.from(mActivity)
                            .inflate(R.layout.view_offer_details , ll_offer_details , false)
                        offerView.tv_offer_type.text = when (subOfferType) {
                            ApiParam.SUB_OFFER_TYPE_REFERRAL -> mActivity.resources.getString(R.string.smark_offer)
                            ApiParam.SUB_OFFER_TYPE_REWARD -> mActivity.resources.getString(R.string.thank_u_reward)
                            ApiParam.SUB_OFFER_TYPE_BONUS -> mActivity.resources.getString(R.string.welcome_bonus_offer)
                            ApiParam.SUB_OFFER_TYPE_RATE -> mActivity.resources.getString(R.string.rate_and_review)
                            ApiParam.SUB_OFFER_TYPE_COUPON_RATE -> mActivity.resources.getString(R.string.coupon_offer)
                            else -> ""
                        }
                        if (conditions.isRequiredField()) {
                            offerView.tv_condition.text = conditions
                        } else {
                            offerView.ll_condition.visibility = View.GONE
                        }
                        when (subOfferCategory) {
                            ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                offerView.tv_sub_offer_type.text = title
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    null ,
                                    null ,
                                    inStoreDrawable ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                offerView.tv_sub_offer_type.text =
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        "${mActivity.resources.getString(R.string.store_credit_of)} $$amount"
                                    } else {
                                        "${mActivity.resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    null ,
                                    null ,
                                    storeCreditDrawable ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                offerView.tv_sub_offer_type.text =
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        "${mActivity.resources.getString(R.string.gift_card_worth)} $$amount"
                                    } else {
                                        "${mActivity.resources.getString(R.string.gift_card_worth)} ₹$amount"
                                    }
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    null ,
                                    null ,
                                    giftCardDrawable ,
                                    null
                                )
                            }
                        }

                        ll_offer_details.addView(offerView)

                        if (index < subOffers.size - 1) {
                            addDivider()
                        }
                    }
                }
            }
        }
    }

    private fun addDivider() {

        val dividerView =
            LayoutInflater.from(mActivity).inflate(R.layout.divider , ll_offer_details , false)
        dividerView.view_divider.setBackgroundColor(mActivity.resources.getColor(R.color.dim_gray_txt))

        ll_offer_details.addView(dividerView)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_save -> {
                avoidDoubleClicks(btn_save)
                if (offer?.offerType == ApiParam.OFFER_TYPE_COUPON) {
                    callCouponAddEdit()
                } else {
                    callAddEditOfferAPI()
                }

            }
            else -> {
            }
        }
    }

    private fun callAddEditOfferAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        offer?.mailPDF = if (tv_create_pdf_and_send_mail.isChecked) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        disposable = WebApiClient.webApi().addEditOfferAPI(offer !!).subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        if (status == ApiParam.META_SUCCESS) {

                                            mActivity.showDialogWithAction(message) {
                                                AddEditDeleteOfferManager.executeCallBacks(data)
                                                mActivity.popBackStack(MyOffersFragment::class.java.canonicalName)
                                            }

                                        } else {
                                            mActivity.showDialog(message)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun callCouponAddEdit() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        offer?.mailPDF = if (tv_create_pdf_and_send_mail.isChecked) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val requestFile: RequestBody =
            RequestBody.create(MediaType.parse("multipart/form-data") , couponPic !!)
        val body: MultipartBody.Part = MultipartBody.Part.createFormData(
            ApiParam.COUPON_PIC , couponPic !!.name , requestFile
        )

        val offerDetail = ApiParam.getHashMapCoupon()
        offerDetail[ApiParam.ID] = prepareTextRequestBody("${offer?.id}")
        offerDetail[ApiParam.OFFER_TYPE] = prepareTextRequestBody(offer?.offerType ?: "")
        offerDetail[ApiParam.EXPIRY_DATE] = prepareTextRequestBody(offer?.expiryDate ?: "")
        offerDetail[ApiParam.REDEEMED_DATE] = prepareTextRequestBody(offer?.redeemedDate ?: "")
        offerDetail[ApiParam.REFERRED_DATE] = prepareTextRequestBody(offer?.referredDate ?: "")
        offerDetail[ApiParam.MAIL_PDF] = prepareTextRequestBody(offer?.mailPDF ?: "")
        offerDetail[ApiParam.REFERRAL_COUNT] = prepareTextRequestBody(offer?.referralCount ?: "")

        val hashMap = ApiParam.getHashMapCoupon()
        for ((index , sub_offer) in offer?.subOffers?.withIndex() !!) {
            hashMap["sub_offer[${index}][id]"] = prepareTextRequestBody("${sub_offer.id}")
            hashMap["sub_offer[${index}][parent_id]"] =
                prepareTextRequestBody("${sub_offer.offerId}")
            hashMap["sub_offer[${index}][sub_offer_type]"] =
                prepareTextRequestBody(sub_offer.subOfferType)
            hashMap["sub_offer[${index}][sub_offer_category]"] =
                prepareTextRequestBody(sub_offer.subOfferCategory)
            hashMap["sub_offer[${index}][amount]"] = prepareTextRequestBody("${sub_offer.amount}")
            hashMap["sub_offer[${index}][title]"] = prepareTextRequestBody(sub_offer.title)
            hashMap["sub_offer[${index}][conditions]"] =
                prepareTextRequestBody(sub_offer.conditions)
            hashMap["sub_offer[${index}][is_redemption]"] =
                prepareTextRequestBody(sub_offer.isRedemptionRequired)
            hashMap["sub_offer[${index}][qr_code]"] = prepareTextRequestBody(sub_offer.qrCode)
            hashMap["sub_offer[${index}][code]"] = prepareTextRequestBody(sub_offer.code)
            hashMap["sub_offer[${index}][status]"] = prepareTextRequestBody(sub_offer.status)
            hashMap["sub_offer[${index}][coupon_type]"] =
                prepareTextRequestBody(sub_offer.usageType)
        }

        /*hashMap.keys.forEach {
            hashMap[it]
        }*/

        disposable =
            WebApiClient.webApi().addEditOfferAPI(offerDetail , body , hashMap)
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                    {
                        if (disposable == null) return@subscribe

                        with(it) {
                            if (isSuccessful && body() != null) {
                                with(body() !!) {
                                    if (meta != null) {
                                        with(meta !!) {
                                            if (status == ApiParam.META_SUCCESS) {

                                                mActivity.showDialogWithAction(message) {
                                                    AddEditDeleteOfferManager.executeCallBacks(data)
                                                    mActivity.popBackStack(MyOffersFragment::class.java.canonicalName)
                                                }

                                            } else {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    } else {
                                        ErrorUtil.showError(it , mActivity)
                                    }
                                }
                            } else {
                                ErrorUtil.showError(it , mActivity)
                            }
                        }

                        ProgressDialogUtil.hideProgressDialog()
                    } ,
                    {
                        if (disposable == null) return@subscribe

                        ProgressDialogUtil.hideProgressDialog()
                        ErrorUtil.setExceptionMessage(it)
                    }
                )
    }

    private fun prepareTextRequestBody(data: String?): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain") , data ?: "")
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}