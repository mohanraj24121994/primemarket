package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.utils.*
import com.smarket.R
import com.smarket.adapter.BannersAdapter
import com.smarket.interfaces.UpdateRefCash
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.main.BaseMainFragment
import com.smarket.model.Banner
import com.smarket.utils.setCustomText
import com.smarket.utils.showDialog
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.BannersResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_mer_home.*
import kotlinx.android.synthetic.main.header.*
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class MerHomeFragment : BaseMainFragment() , View.OnClickListener , androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener , UpdateRefCash {

    companion object {
        fun getInstance(): MerHomeFragment {
            val fragment = MerHomeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var bannersAdapter: BannersAdapter

    var banners = ArrayList<Banner>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    private var disposable: Disposable? = null
    var observable: Observable<Response<BannersResponse>>? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mer_home , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_banners.addItemDecoration(VerticalDividerDecoration(mActivity.convertDpToPixel(16F).toInt() , true))
        rv_banners.layoutManager = linearLayoutManager
        bannersAdapter = BannersAdapter(banners)
        rv_banners.adapter = bannersAdapter

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        tv_refcash.setOnClickListener(this)
        tv_my_offers.setOnClickListener(this)
        tv_add_message.setOnClickListener(this)
        tv_add_members.setOnClickListener(this)
        tv_claim_offer.setOnClickListener(this)
        btn_scan_offer.setOnClickListener(this)

        if (mActivity.userDetails?.login_user.equals("member")) {
            tv_refcash.isClickable = false
            tv_add_message.isClickable = false
            tv_refcash.setCompoundDrawablesWithIntrinsicBounds(R.drawable.refcash , 0 , 0 , 0)
        } else {
            tv_refcash.isClickable = true
            tv_add_message.isClickable = true
            tv_refcash.setCompoundDrawablesWithIntrinsicBounds(R.drawable.refcash , 0 , R.drawable.add , 0)
        }
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        if (mActivity.userDetails != null) {
            tv_title.text = mActivity.userDetails?.businessName
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        UpdateRefCashManager.addCallBack(this)

        if (mActivity.userDetails != null) {
            setRefCash(mActivity.userDetails !!.refcash)
        }

        rv_banners.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView , newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == bannersAdapter.itemCount - 1 &&
                        page <= lastPage && ! isLoading) {
                    callBannersAPI()
                }
            }
        })

        callBannersAPI()
    }

    private fun setRefCash(refCash: Double) {
        if (refCash > 0 && ! mActivity.userDetails?.login_user.equals("member")) {
            if (mActivity.userDetails?.countryCode != "+91") {
                tv_refcash.setCustomText(mActivity.resources.getString(R.string.refcash_c) , "$$refCash" , fontColor = mActivity.resources.getColor(R.color.green))
            } else {
                tv_refcash.setCustomText(mActivity.resources.getString(R.string.refcash_c) , "₹$refCash" , fontColor = mActivity.resources.getColor(R.color.green))
            }
        } else {
            tv_refcash.text = mActivity.resources.getString(R.string.add_refcash_to_promote_business)
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_refcash -> mActivity.addFragment(AddRefcashFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_my_offers -> mActivity.addFragment(MyOffersFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.btn_scan_offer -> mActivity.addFragment(RedeemCodeFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_add_message -> mActivity.addFragment(ReferMessageFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_add_members -> mActivity.addFragment(ManageMembersFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            R.id.tv_claim_offer -> mActivity.addFragment(ClaimOffersFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        callBannersAPI()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && ! hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    private fun callBannersAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (banners.find { it.id == 0L } == null) {
                banners.add(Banner())
                bannersAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.USER_TYPE] = ApiParam.USER_TYPE_MERCHANT
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().bannersAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            banners.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            banners.addAll(data)
                                                        }

                                                        page ++

                                                        this@MerHomeFragment.lastPage = lastPage
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (banners.isNotEmpty()) {
                                    banners.removeAll(banners.filter { it.id == 0L })
                                }
                            }

                            bannersAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (banners.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (banners.isNotEmpty()) {
                                    banners.removeAll(banners.filter { it.id == 0L })
                                    bannersAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (banners.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        UpdateRefCashManager.removeCallBack(this)
        super.onDestroy()
    }

    override fun onUpdateRefCash(refCash: Double) {
        setRefCash(refCash)
    }
}