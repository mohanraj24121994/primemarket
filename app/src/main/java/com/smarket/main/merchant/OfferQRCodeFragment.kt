package com.smarket.main.merchant

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.topBar.AwaitingRewardsFragment
import com.smarket.main.customer.CouponListFragment
import com.smarket.main.customer.RecommendFriendsFragment
import com.smarket.main.customer.topBar.StoreCreditsFragment
import com.smarket.model.OfferDetails
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.fragment_offer_qr_code.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.*
import utils.AnimationType


/**
 * Created by MI-062 on 11/4/18.
 */
class OfferQRCodeFragment : BaseMainFragment() {

    companion object {
        fun getInstance(
            isFrom: String = "" ,
            offerDetails: OfferDetails? = null ,
            receiversList: String = "" ,
            merchantMessage: String
        ): OfferQRCodeFragment {
            val fragment = OfferQRCodeFragment()
            val bundle = Bundle()
            bundle.putParcelable(OFFER_DETAILS , offerDetails)
            bundle.putString(IS_FROM , isFrom)
            bundle.putString("receiversList" , receiversList)
            bundle.putString("merchantMessage" , merchantMessage)
            fragment.arguments = bundle
            return fragment
        }
    }

    var isFrom = ""
    var offerDetails: OfferDetails? = null
    var receiversList = ""
    var merchantMessage = ""

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offer_qr_code , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }

            if (bundle.containsKey(OFFER_DETAILS)) {
                offerDetails = bundle.getParcelable(OFFER_DETAILS)
            }
            if (bundle.containsKey("receiversList")) {
                receiversList = bundle.getString("receiversList") ?: ""
            }
            if (bundle.containsKey("merchantMessage")) {
                merchantMessage = bundle.getString("merchantMessage") ?: ""
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = if (isFrom == RateAndReferMerchantFragment::class.java.canonicalName) {
            if (offerDetails !!.offer !!.offerType == ApiParam.OFFER_TYPE_REFERRAL) {
                mActivity.resources.getString(R.string.referral_qr_code)
            } else {
                mActivity.resources.getString(R.string.rate_review_qr_code)
            }
        } else if (isFrom == StoreCreditsFragment::class.java.canonicalName) {
            mActivity.resources.getString(R.string.store_credit_qr_code)
        } else if (isFrom == CouponListFragment::class.java.canonicalName) {
            mActivity.resources.getString(R.string.coupon_qr_code)
        } else {
            mActivity.resources.getString(R.string.offer_qr_code)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //  if (mActivity.userDetails?.countryCode.equals("+91")) {
        if (isFrom == RateAndReferMerchantFragment::class.java.canonicalName && receiversList.isRequiredField()) {
            val sendIntent = Intent(Intent.ACTION_VIEW)
            sendIntent.data = Uri.parse("sms:" + receiversList)
            if (offerDetails != null) {
                if (merchantMessage.isEmpty()) {
                    sendIntent.putExtra(
                        "sms_body" ,
                        mActivity.resources.getString(
                            R.string.smarket_invite_message ,
                            "I got great deal from " + offerDetails?.businessName + ", " + "I would recommend you to try it." ,
                            mActivity.userDetails?.id.toString() ,
                            mActivity.userDetails?.referral_code
                        )
                    )
                } else {
                    sendIntent.putExtra(
                        "sms_body" ,
                        mActivity.resources.getString(
                            R.string.smarket_invite_message ,
                            merchantMessage ,
                            mActivity.userDetails?.id.toString() ,
                            mActivity.userDetails?.referral_code
                        )
                    )
                }
            }
            mActivity.startActivity(sendIntent)
        }
        // }

        if (isFrom == AwaitingRewardsFragment::class.java.canonicalName) {
            mActivity.showDialogWithActions(getString(R.string.invite_friends) ,
                positiveButtonLabel = getString(R.string.yes) ,
                negativeButtonLabel = getString(R.string.no) ,
                requiredAlign = true ,
                showNegativeButton = true , negativeFunction = { } , positiveFunction = {
                    mActivity.addFragment(
                        RecommendFriendsFragment.getInstance() ,
                        true ,
                        animationType = AnimationType.RightInZoomOut
                    )
                })
        }

        if (offerDetails != null) {
            setOfferDetails()
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
        }
    }

    private fun setOfferDetails() {

        with(offerDetails !!) {

            /*if (isFrom != StoreCreditsFragment::class.java.canonicalName) {
                if (this.status == 1) {
                    mActivity.showDialogWithActions(mActivity.resources.getString(R.string.refer_confirmation_message),
                            positiveButtonLabel = mActivity.resources.getString(R.string.proceed),
                            negativeButtonLabel = mActivity.resources.getString(R.string.cancel),
                            showNegativeButton = true,
                            positiveFunction = {
                                alertDialog?.dismiss()
                            },
                            negativeFunction = {
                                mActivity.onBackPressed()
                                alertDialog?.dismiss()
                            })
                }
            }*/

            sdv_logo.loadFrescoImage(businessLogo)
            if (distance.isRequiredField()) {
                tv_distance.text = "($distance ${mActivity.resources.getString(R.string.mi)})"
            }
            tv_business_name.text = businessName
            if (tagLine.isRequiredField()) {
                tv_tag_line.visibility = View.VISIBLE
                tv_tag_line.text = tagLine
            } else {
                tv_tag_line.visibility = View.GONE
            }
            rating_bar.rating = averageRating
            tv_average_ratings.text = "$averageRating"
            tv_ratings.text = "($noOfRating)"
            sdv_offer_code.loadFrescoImage(qrCode)
            tv_offer_code.text = code
            tv_offer_code.setOnClickListener {

                val myClip: ClipData

                val myClipboard:ClipboardManager
                myClipboard= context?.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;


                        myClip = ClipData.newPlainText("text", tv_offer_code.text);
                        myClipboard.setPrimaryClip(myClip);

                        Toast.makeText(context, "Code Copied",
                            Toast.LENGTH_SHORT).show();
                    }

            if (status == 1) {                 //pending i.e no label
                tv_redeemed_expired.visibility = View.GONE
            } else if (status == 2) {         //offer redeemed
                tv_redeemed_expired.visibility = View.VISIBLE
                tv_redeemed_expired.text = mActivity.resources.getString(R.string.redeemed)
                tv_redeemed_expired.setTextColor(mActivity.resources.getColor(R.color.green_redeem))

            } else if (status == 3) {         //offer expired
                tv_redeemed_expired.visibility = View.VISIBLE
                tv_redeemed_expired.text = mActivity.resources.getString(R.string.expired)
                tv_redeemed_expired.setTextColor(mActivity.resources.getColor(R.color.red_expired))
            }
            if (referredDate.isRequiredField()) {
                tv_referred_date.text = "${mActivity.resources.getString(R.string.referred_on)} ${
                    getChangedDateFormat(
                        referredDate ,
                        DATE_FORMAT ,
                        APP_DATE_FORMAT
                    )
                }"
            } else {
                tv_referred_date.visibility = View.GONE
            }

            if (status == 2 && redeemedDate.isRequiredField()) {
                tv_redeemed_date.text = "${mActivity.resources.getString(R.string.redeemed_on)} ${
                    getChangedDateFormat(
                        this !!.redeemedDate ,
                        DATE_FORMAT ,
                        APP_DATE_FORMAT
                    )
                }"
            } else {
                tv_redeemed_date.visibility = View.GONE
            }
            if (offer != null) {

                with(offer) {

                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${
                        getChangedDateFormat(
                            this !!.expiryDate ,
                            DATE_FORMAT ,
                            APP_DATE_FORMAT
                        )
                    }"

                    if (status == 3) {
                        tv_expiry_date.text =
                            "${mActivity.resources.getString(R.string.expired_on)} ${
                                getChangedDateFormat(
                                    expiryDate ,
                                    DATE_FORMAT ,
                                    APP_DATE_FORMAT
                                )
                            }"

                        tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    if (this.subOffers.isNotEmpty()) {
                        subOffers.forEachIndexed { index , subOffer ->
                            with(subOffer) {
                                when (subOfferCategory) {
                                    ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                        tv_offer_value.text = ""
                                        tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                                        tv_sub_offer_type.text =
                                            mActivity.resources.getString(R.string.exclusive)
                                        if (conditions.isRequiredField()) {
                                            tv_condition_label.visibility = View.VISIBLE
                                            tv_condition.visibility = View.VISIBLE
                                            tv_condition.text = conditions
                                        } else {
                                            tv_condition_label.visibility = View.GONE
                                            tv_condition.visibility = View.GONE
                                        }
                                    }
                                    ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                        if (mActivity.userDetails?.countryCode != "+91") {
                                            tv_offer_value.text = "$$amount"
                                        } else {
                                            tv_offer_value.text = "₹$amount"
                                        }
                                        tv_sub_offer_type.text =
                                            "${mActivity.resources.getString(R.string.store_credit)}"
                                        if (conditions.isRequiredField()) {
                                            tv_condition_label.visibility = View.VISIBLE
                                            tv_condition.visibility = View.VISIBLE
                                            tv_condition.text = conditions
                                        } else {
                                            tv_condition_label.visibility = View.GONE
                                            tv_condition.visibility = View.GONE
                                        }
                                    }
                                    ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                        if (mActivity.userDetails?.countryCode != "+91") {
                                            tv_offer_value.text = "$$amount"
                                        } else {
                                            tv_offer_value.text = "₹$amount"
                                        }
                                        tv_sub_offer_type.text =
                                            "${mActivity.resources.getString(R.string.gift_card)}"
                                        if (conditions.isRequiredField()) {
                                            tv_condition_label.visibility = View.VISIBLE
                                            tv_condition.visibility = View.VISIBLE
                                            tv_condition.text = conditions
                                        } else {
                                            tv_condition_label.visibility = View.GONE
                                            tv_condition.visibility = View.GONE
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (mActivity.userDetails?.countryCode != "+91") {
                    tv_available_store_credit.text =
                        "${mActivity.resources.getString(R.string.available_store_credit)} $$storeCredit"
                    tv_redemption_amount.text =
                        "${mActivity.resources.getString(R.string.redeem_amount)} $$enter_storeCredit"
                } else {
                    tv_available_store_credit.text =
                        "${mActivity.resources.getString(R.string.available_store_credit)} ₹$storeCredit"
                    tv_redemption_amount.text =
                        "${mActivity.resources.getString(R.string.redeem_amount)} ₹$enter_storeCredit"
                }
                view_end.visibility = View.GONE
                tv_expiry_date.visibility = View.GONE
                tv_referred_date.visibility = View.GONE
            }
        }
    }
}
