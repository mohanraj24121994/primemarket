package com.smarket.main.merchant

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bolts.Task
import com.mi.mycontacts.Contact
import com.mi.mycontacts.Contacts
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.ReferContactsFragment
import com.smarket.model.MerchantDetails
import com.smarket.model.ReferDetails
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ContactDetails
import com.smarket.webservices.response.GenerateRedemptionCodeForStoreCreditResponse
import com.smarket.webservices.response.MyContact
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_rate_and_refer_merchant.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import utils.AnimationType
import java.io.File

/**
 * Created by MI-062 on 11/4/18.
 */
class RateAndReferMerchantFragment : BaseMainFragment() , View.OnClickListener {

    companion object {
        fun getInstance(isForReferMerchant: Boolean = false , merchantDetails: MerchantDetails? = null): RateAndReferMerchantFragment {
            val fragment = RateAndReferMerchantFragment()
            val bundle = Bundle()
            bundle.putBoolean("isForReferMerchant" , isForReferMerchant)
            bundle.putParcelable(MERCHANT_DETAILS , merchantDetails)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var rateAndReferObservable: Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>
    lateinit var rateReferralMerchantObservable: Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>
    private var disposable: Disposable? = null

    var isForReferMerchant = false
    var merchantDetails: MerchantDetails? = null
    var picturePath = ""
    var image: File? = null

    var READ_CONTACTS_PERMISSION = Manifest.permission.READ_CONTACTS
    val READ_CONTACTS_PERMISSION_REQUEST_CODE = 76
    val READ_CONTACTS_PERMISSION_GRANTED = 1
    val READ_CONTACTS_PERMISSION_DENIED = 2
    val READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS = 3

    var myContactsList = ArrayList<Contact>()

    var referDetails = ReferDetails()

    var isWhatsappContacts = false

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rate_and_refer_merchant , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        btn_rate_and_refer.text = if (isForReferMerchant) {
            mActivity.resources.getString(R.string.submit_rate_and_refer_now)
        } else {
            mActivity.resources.getString(R.string.rate_now)
        }

        sdv_image.setOnClickListener(this)
        btn_rate_and_refer.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("isForReferMerchant")) {
                isForReferMerchant = bundle.getBoolean("isForReferMerchant")
            }

            if (bundle.containsKey(MERCHANT_DETAILS)) {
                merchantDetails = bundle.getParcelable(MERCHANT_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = if (isForReferMerchant) {
            mActivity.resources.getString(R.string.refer_merchant)
        } else {
            mActivity.resources.getString(R.string.rate_merchant)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (merchantDetails != null) {
            setMerchantData()
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.sdv_image -> {
                val intent = Intent(mActivity , ImageChooserActivity::class.java)
                startActivityForResult(intent , ImageChooserActivity.REQUEST_CODE)
            }
            R.id.btn_rate_and_refer -> {
                if (checkForValidations()) {
                    avoidDoubleClicks(btn_rate_and_refer)

                    referDetails.merchantId = merchantDetails?.id !!
                    if (image != null) {
                        referDetails.productImage = image
                    }
                    if (et_item_name.text.toString().isRequiredField()) {
                        referDetails.productName = et_item_name.text.toString()
                    }
                    if (et_review.text.toString().isRequiredField()) {
                        referDetails.review = et_review.text.toString()
                    }
                    referDetails.rating = rb_merchant.rating

                    if (isForReferMerchant) {
                        checkForReadContactsPermission()
                    } else {
                        callRateAndReferAPI()
                    }
                }
            }
            else -> {
            }
        }
    }

    private fun checkForReadContactsPermission() {
        if (isReadContactsPermissionGranted()) {
            // Android version is below Marshmallow or read myContactsList permission is already allowed.
            getContacts()
        } else {
            // Android version is above Marshmallow and read myContactsList permission is not allowed, ask user to allow.
            // Check the result in onActivityResult().
            requestPermissions(arrayOf(READ_CONTACTS_PERMISSION) , READ_CONTACTS_PERMISSION_REQUEST_CODE)
        }
    }

    private fun isReadContactsPermissionGranted(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        when (requestCode) {
            READ_CONTACTS_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Read myContactsList permission is allowed.
                getContacts()
            } else {
                if (! shouldShowRequestPermissionRationale(READ_CONTACTS_PERMISSION)) {
                    // Read myContactsList permission is denied for always, ask user to turn it on from settings.
                    setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS)
                } else {
                    // Read myContactsList permission is denied for now.
                    setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_DENIED)
                }
            }
            else -> {
            }
        }

        if (requestCode == READ_CONTACTS_PERMISSION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getContacts()
        }
    }


    private fun getContacts() {

        setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_GRANTED)

        isWhatsappContacts = appInstalledOrNot("com.whatsapp" , mActivity)

        Task.callInBackground {
            myContactsList = Contacts.getQuery(isWhatsappContacts).find() as ArrayList<Contact>

            setUpMyContactsListToPass()
        }.continueWith { task ->
            null
        }
    }

    private fun setUpMyContactsListToPass() {

        val myContactsListToPass = ArrayList<ContactDetails>()

        if (myContactsList.isNotEmpty()) {
            var myContactToPass: ContactDetails
            for (contact in myContactsList) {
                myContactToPass = ContactDetails()
                myContactToPass.id = contact.id
                myContactToPass.name = contact.displayName
                if (contact.phoneNumbers.isNotEmpty()) {
                    if (contact.phoneNumbers[0].normalizedNumber != null &&
                            contact.phoneNumbers[0].normalizedNumber.isRequiredField()) {
                        myContactToPass.number = contact.phoneNumbers[0].normalizedNumber
                    } else if (contact.phoneNumbers[0].number != null &&
                            contact.phoneNumbers[0].number.isRequiredField()) {
                        myContactToPass.number = contact.phoneNumbers[0].number
                    }
                }
                myContactsListToPass.add(myContactToPass)
            }
            mActivity.runOnUiThread {

                /* if (isWhatsappContacts) {
                     callContactListAPI(myContactsListToPass.filter { it.number.startsWith("${SMarket.appDB.userDao().getData() !!.countryCode}") } as ArrayList<ContactDetails>)
                 } else {
                     callContactListAPI(myContactsListToPass)
                 }*/
                callContactListAPI(myContactsListToPass)
            }

        } else {
            mActivity.runOnUiThread {
                mActivity.showDialog(mActivity.resources.getString(R.string.not_able_to_use_offer))
            }
        }
    }

    private fun setUpReadContactsPermissionStatusMessage(permissionStatus: Int) {
        when (permissionStatus) {
            READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS -> mActivity.showDialog(mActivity.resources.getString(R.string.contacts_permission_denied))
            READ_CONTACTS_PERMISSION_DENIED -> mActivity.showDialogWithAction(mActivity.resources.getString(R.string.contacts_permission_pending) , showNegativeButton = true) {
                checkForReadContactsPermission()
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        if (rb_merchant.rating == 0F) {
            mActivity.showDialog(mActivity.resources.getString(R.string.rating_req))
            return false
        }
        return true
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        if (data != null) {
            when (requestCode) {
                ImageChooserActivity.REQUEST_CODE -> {
                    picturePath = data.getStringExtra(ImageChooserActivity.KEY_OF_URI) !!
                    image = File(picturePath)
                    sdv_image.loadFrescoImageFromFile(image)
                }
                else -> {
                }
            }
        }
    }

    private fun callRateAndReferAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.MERCHANT_ID] = merchantDetails?.id.toString()
        hashMap[ApiParam.RateAndRefer.RATING] = rb_merchant.rating.toString()
        if (et_item_name.text.toString().isRequiredField()) {
            hashMap[ApiParam.RateAndRefer.PRODUCT_NAME] = et_item_name.text.toString()
        }
        if (et_review.text.toString().isRequiredField()) {
            hashMap[ApiParam.RateAndRefer.REVIEW] = et_review.text.toString()
        }

        rateAndReferObservable = if (image != null) {
            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data") , image !!)
            val body: MultipartBody.Part = MultipartBody.Part.createFormData(ApiParam.RateAndRefer.PRODUCT_IMAGE , image !!.name , requestFile)
            WebApiClient.webApi().rateAndReferAPI(hashMap , body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        } else {
            WebApiClient.webApi().rateAndReferAPI(hashMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        }

        disposable = rateAndReferObservable.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                mActivity.addFragment(OfferQRCodeFragment.getInstance(RateAndReferMerchantFragment::class.java.canonicalName
                                                        ?: "" , data , merchantMessage = merchantDetails?.referralMessage
                                                        ?: "") , true , animationType = AnimationType.RightInZoomOut)
                                                mActivity.showDialog(message)
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
        )
    }

    private fun callContactListAPI(myContactsListToPass: ArrayList<ContactDetails>) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val myContact = MyContact()
        myContact.contactList.addAll(myContactsListToPass)

        disposable = WebApiClient.webApi().contactListAPI(myContact).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        with(myContact) {
                                                            smarketContactList.addAll(data.smarketContactList)
                                                            otherContactList.addAll(data.otherContactList)
                                                            if (smarketContactList.isNotEmpty()) {
                                                                referDetails.smarketContactList.addAll(smarketContactList)
                                                            }

                                                            if (otherContactList.isNotEmpty()) {
                                                                /* mActivity.showDialogWithActions(mActivity.resources.getString(R.string.invite_friends),
                                                                         positiveButtonLabel = mActivity.resources.getString(R.string.yes),
                                                                         negativeButtonLabel = mActivity.resources.getString(R.string.no), showNegativeButton = true,
                                                                         requiredAlign = true,
                                                                         negativeFunction = {

                                                                             callRateReferralMerchantAPI()

                                                                         }, positiveFunction = {

                                                                     //To combine both smarketContactList & otherContactList and for to show s-mark icon
                                                                     //on smarketContactList we are using isSmarketContact param
                                                                     data.smarketContactList.forEach { it.isSmarketContact = true }
                                                                     val contactsTopass = ArrayList<ContactDetails>()
                                                                     contactsTopass.addAll(data.smarketContactList)
                                                                     contactsTopass.addAll(otherContactList)
                                                                     contactsTopass.sortBy { it.name }

                                                                     mActivity.addFragment(ReferContactsFragment.getInstance(contactsTopass
                                                                             , referDetails)
                                                                             , true, animationType = AnimationType.RightInZoomOut)
                                                                 })*/

                                                                data.smarketContactList.forEach { it.isSmarketContact = true }
                                                                val contactsTopass = ArrayList<ContactDetails>()
                                                                contactsTopass.addAll(data.smarketContactList)
                                                                contactsTopass.addAll(otherContactList)
                                                                contactsTopass.sortBy { it.name }

                                                                mActivity.addFragment(ReferContactsFragment.getInstance(contactsTopass , referDetails , merchantDetails) , true , animationType = AnimationType.RightInZoomOut)
                                                                /* mActivity.addFragment(ReferContactsFragment.getInstance(otherContactList, referDetails)
                                                                         , true, animationType = AnimationType.RightInZoomOut)*/
                                                            } else {
                                                                callRateReferralMerchantAPI()
                                                            }
                                                        }
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    fun callRateReferralMerchantAPI() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }
        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.MERCHANT_ID] = referDetails.merchantId.toString()
        hashMap[ApiParam.RateAndRefer.RATING] = referDetails.rating.toString()
        hashMap[ApiParam.RateAndRefer.PRODUCT_NAME] = referDetails.productName
        hashMap[ApiParam.RateAndRefer.REVIEW] = referDetails.review
        hashMap[ApiParam.RateAndRefer.SMARKET_CONTACT_LIST] = referDetails.smarketContactList.map { it.number }.toString()

        rateReferralMerchantObservable = if (referDetails.productImage != null) {
            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data") , referDetails.productImage !!)
            val body: MultipartBody.Part = MultipartBody.Part.createFormData(ApiParam.RateAndRefer.PRODUCT_IMAGE , referDetails.productImage !!.name , requestFile)
            WebApiClient.webApi().rateReferralMerchantAPI(hashMap , body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        } else {
            WebApiClient.webApi().rateReferralMerchantAPI(hashMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        }

        disposable = rateReferralMerchantObservable.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                mActivity.addFragment(OfferQRCodeFragment.getInstance(RateAndReferMerchantFragment::class.java.canonicalName , offerDetails = data , merchantMessage = merchantDetails?.referralMessage
                                                        ?: "") , true , animationType = AnimationType.RightInZoomOut)
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
        )

    }

    private fun setMerchantData() {
        with(merchantDetails !!) {
            sdv_logo.loadFrescoImage(businessLogo)
            if (distance.isRequiredField()) {
                tv_distance.text = "($distance ${mActivity.resources.getString(R.string.mi)})"
            }
            tv_business_name.text = businessName
            if (tagLine.isRequiredField()) {
                tv_tag_line.visibility = View.VISIBLE
                tv_tag_line.text = tagLine
            } else {
                tv_tag_line.visibility = View.GONE
            }
            rating_bar.rating = averageRating
            tv_average_ratings.text = "$averageRating"
            tv_ratings.text = "($noOfRating)"
//            tv_store_credit.text = "${resources.getString(R.string.available_store_credit)} $$storeCredit"
            with(offer) {
                if (! isForReferMerchant) {
                    setRateAndReviewOfferData()
                } else {
                    setReferralOfferData()
                }
            }
        }
    }

    private fun setRateAndReviewOfferData() {
        val rateAndReviewOfferData = merchantDetails !!.offer.find { it.offerType == ApiParam.OFFER_TYPE_RATE_AND_REVIEW }
        if (rateAndReviewOfferData != null) {
            with(rateAndReviewOfferData) {
                tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

                if (status == 3) {
                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

                    tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                }

                if (referredDate.isRequiredField()) {
                    tv_referred_date.text = "${mActivity.resources.getString(R.string.referred_on)} ${getChangedDateFormat(referredDate , DATE_FORMAT , APP_DATE_FORMAT)}"
                } else {
                    tv_referred_date.visibility = View.GONE
                }
                if (subOffers.isNotEmpty()) {
                    subOffers.forEachIndexed { index , subOffer ->
                        kotlin.with(subOffer) {
                            when (subOfferCategory) {
                                ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                    tv_offer_value.text = ""
                                    tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                                    tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                                }
                                ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        tv_offer_value.text = "$$amount"
                                    } else {
                                        tv_offer_value.text = "₹$amount"
                                    }
                                    tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit)}"
                                }
                                ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        tv_offer_value.text = "$$amount"
                                    } else {
                                        tv_offer_value.text = "₹$amount"
                                    }
                                    tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card)}"
                                }
                            }

                            if (conditions.isRequiredField()) {
                                tv_condition.text = conditions
                            } else {
                                tv_condition_label.visibility = View.GONE
                                tv_condition.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setReferralOfferData() {
        val referralOfferData = merchantDetails !!.offer.find { it.offerType == ApiParam.OFFER_TYPE_REFERRAL }
        if (referralOfferData != null) {
            with(referralOfferData) {
                tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

                if (status == 3) {
                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

                    tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                }

                if (referredDate.isRequiredField()) {
                    tv_referred_date.text = "${mActivity.resources.getString(R.string.referred_on)} ${getChangedDateFormat(referredDate , DATE_FORMAT , APP_DATE_FORMAT)}"
                } else {
                    tv_referred_date.visibility = View.GONE
                }
                if (subOffers.isNotEmpty()) {
                    subOffers.forEachIndexed { index , subOffer ->
                        if (subOffer.subOfferType != ApiParam.SUB_OFFER_TYPE_RATE) {
                            when (subOffer.subOfferType) {
                                ApiParam.SUB_OFFER_TYPE_REFERRAL ->
                                    with(subOffer) {
                                        when (subOfferCategory) {
                                            ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                                tv_offer_value.text = ""
                                                tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                                                tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                                            }
                                            ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                                if (mActivity.userDetails?.countryCode != "+91") {
                                                    tv_offer_value.text = "$$amount"
                                                } else {
                                                    tv_offer_value.text = "₹$amount"
                                                }
                                                tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit)}"
                                            }
                                            ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                                if (mActivity.userDetails?.countryCode != "+91") {
                                                    tv_offer_value.text = "$$amount"
                                                } else {
                                                    tv_offer_value.text = "₹$amount"
                                                }
                                                tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card)}"
                                            }
                                        }

                                        if (conditions.isRequiredField()) {
                                            tv_condition.text = conditions
                                        } else {
                                            tv_condition_label.visibility = View.GONE
                                            tv_condition.visibility = View.GONE
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}