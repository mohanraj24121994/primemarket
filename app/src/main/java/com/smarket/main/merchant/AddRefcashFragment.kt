package com.smarket.main.merchant

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import com.smarket.utils.*
import com.smarket.utils.showDialog
import com.smarket.R
import com.smarket.interfaces.UpdateRefCash
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.main.BaseMainFragment
import com.smarket.stripepayment.StripePaymentActivity
import com.smarket.utils.setCustomText
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_refcash.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 11/4/18.
 */
class AddRefcashFragment : BaseMainFragment(), View.OnClickListener, UpdateRefCash {

    companion object {
        fun getInstance(): AddRefcashFragment {
            val fragment = AddRefcashFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var tabTitleArray: Array<String>
    lateinit var checkedTextViewArray: Array<CheckedTextView>

    var amount = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_refcash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        tabTitleArray = arrayOf(mActivity.resources.getString(R.string.debit), mActivity.resources.getString(R.string.credit))
        checkedTextViewArray = arrayOf(ctv_add_10, ctv_add_25, ctv_add_50, ctv_add_100, ctv_add_200)

        ctv_add_10.setOnClickListener(this)
        ctv_add_25.setOnClickListener(this)
        ctv_add_50.setOnClickListener(this)
        ctv_add_100.setOnClickListener(this)
        ctv_add_200.setOnClickListener(this)
        btn_add_money.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.add_refcash)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        UpdateRefCashManager.addCallBack(this)

        if (mActivity.userDetails != null) {
            setAvailableBalance(mActivity.userDetails!!.refcash)
        }

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                setUpMoneySelector(when (s.toString()) {
                    "10" -> R.id.ctv_add_10
                    "25" -> R.id.ctv_add_25
                    "50" -> R.id.ctv_add_50
                    "100" -> R.id.ctv_add_100
                    "200" -> R.id.ctv_add_200
                    else -> 0
                })
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        setUpTabLayoutAndViewPager()
    }

    private fun setAvailableBalance(refCash: Double) {

        tv_available_balance.setCustomText(resources.getString(R.string.available_balance)
                , "$$refCash"
                , fontColor = mActivity.resources.getColor(R.color.green))
    }

    private fun setUpTabLayoutAndViewPager() {

        vp_refcash_history.adapter = ViewPagerAdapter(childFragmentManager, tabTitleArray.size)
        tab_layout.setupWithViewPager(vp_refcash_history)
        vp_refcash_history.offscreenPageLimit = 2

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                vp_refcash_history.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.ctv_add_10,
            R.id.ctv_add_25,
            R.id.ctv_add_50,
            R.id.ctv_add_100,
            R.id.ctv_add_200 -> setUpMoneySelector(v.id)
            R.id.btn_add_money -> {
                if (checkForValidations()) {
                    avoidDoubleClicks(btn_add_money)

                    /**for temporary we are removing the stripe Integration when adding amount..
                     * and just showing dialog with some message.
                    */
                    mActivity.showDialog(mActivity.resources.getString(R.string.added_money_without_stripe))

                    /* val intent = Intent(mActivity, StripePaymentActivity::class.java)
                     intent.putExtra(StripePaymentActivity.STRIPE_PAYMENT_AMOUNT, amount)
                     startActivityForResult(intent, StripePaymentActivity.STRIPE_PAYMENT_REQUEST_CODE)*/
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        amount = et_amount.text.toString()

        if (!amount.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.amount_req))
            return false
        }

        return true
    }

    private fun setUpMoneySelector(selectedViewId: Int) {

        for (i in checkedTextViewArray.indices) {
            if (checkedTextViewArray[i].id == selectedViewId) {
                if (!checkedTextViewArray[i].isChecked) {
                    checkedTextViewArray[i].isChecked = true
                    et_amount.setText(checkedTextViewArray[i].text.substring(2))
                    et_amount.setSelection(et_amount.text.length)
                } else {
                    break
                }
            } else {
                checkedTextViewArray[i].isChecked = false
            }
        }
    }

    private inner class ViewPagerAdapter(fragmentManager: androidx.fragment.app.FragmentManager , internal var tabCount: Int) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> RefcashHistoryFragment.getInstance(ApiParam.HISTORY_TYPE_DEBIT)
                1 -> RefcashHistoryFragment.getInstance(ApiParam.HISTORY_TYPE_CREDIT)
                else -> throw IllegalStateException("position $position is invalid for this viewpager")
            }
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitleArray[position]
        }
    }

    private fun callAddRefCashAPI(stripe_token: String) {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.AMOUNT] = amount
        hashMap[ApiParam.STRIPE_TOKEN] = stripe_token

        disposable = WebApiClient.webApi().addRefCashAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    UpdateRefCashManager.executeCallBacks(mActivity, refcash)

                                                    et_amount.setText("")

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        UpdateRefCashManager.removeCallBack(this)
        super.onDestroy()
    }

    override fun onUpdateRefCash(refCash: Double) {
        setAvailableBalance(refCash)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == StripePaymentActivity.STRIPE_PAYMENT_REQUEST_CODE) {
            if (data != null) {
                // we are checking that whether Stripe Token generation process has been done sucessfully or not.
                val isTokenGenerationSucceed = data.getBooleanExtra(StripePaymentActivity.STRIPE_TRANSACTION_RESULT, false)
                if (isTokenGenerationSucceed) {
                    // now we are fetching toke data to share with server.
                    val stripe_token = data.getStringExtra(StripePaymentActivity.STRIPE_TOKEN)
                    if (stripe_token!!.isRequiredField()) {
                        println("######stripe_token-->$stripe_token")
                        //TODO: call api to server with stripe token.
                        callAddRefCashAPI(stripe_token)
                    } else {
                        Log.d(StripePaymentActivity.TAG, "Stripe Token Generation Error: Empty or null payment token found. Please retry.")
                    }
                } else {
                    val errorMessage = data.getStringExtra(StripePaymentActivity.STRIPE_TRANSACTION_ERROR_MESSAGE)
                    Log.d(StripePaymentActivity.TAG, "Stripe Token Generation failure message: $errorMessage")
                    showToast(errorMessage)
                }
            }
        }
    }
}