package com.smarket.main.merchant

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import com.smarket.R
import com.smarket.interfaces.AddEditDeleteOffer
import com.smarket.interfaces.AddEditDeleteOfferManager
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.model.SubOffer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ApiUtil
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.memberlist.Datum
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_read_more.view.*
import kotlinx.android.synthetic.main.divider.view.*
import kotlinx.android.synthetic.main.fragment_my_offers.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_offer_details_with_read_more_mer.view.*
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class MyOffersFragment : BaseMainFragment() , View.OnClickListener ,
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener , AddEditDeleteOffer {

    companion object {
        fun getInstance(): MyOffersFragment {
            val fragment = MyOffersFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var giftCardDrawable: Drawable
    lateinit var inStoreDrawable: Drawable
    lateinit var storeCreditDrawable: Drawable

    var offers = ArrayList<Offer>()
    var addMembers = ArrayList<Datum>()

    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_offers , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        giftCardDrawable = resources.getDrawable(R.drawable.gift_card)
        inStoreDrawable = resources.getDrawable(R.drawable.in_store)
        storeCreditDrawable = resources.getDrawable(R.drawable.store_credit)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        iv_edit_rate_and_review_offer.setOnClickListener(this)
        iv_edit_referral_offer.setOnClickListener(this)
        iv_edit_coupon_offer.setOnClickListener(this)
        btn_create_pdf_and_send_mail.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        iv_add.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.my_offers)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AddEditDeleteOfferManager.addCallBack(this)

        callMyOffersAPI()
    }

    private fun setOffersData() {

        if (offers.isEmpty()) {
            btn_create_pdf_and_send_mail.visibility = View.GONE
            tv_no_data_found.visibility = View.VISIBLE
            /*if (! mActivity.userDetails?.login_user.equals("member")) {
                iv_add.visibility = View.VISIBLE
            }*/
            ll_referral_offer.visibility = View.GONE
            ll_rate_and_review_offer.visibility = View.GONE
            ll_coupon_offer.visibility = View.GONE

            rr_rate_review.visibility = View.VISIBLE
            rr_referral_offer.visibility = View.VISIBLE
            rr_coupon_offer.visibility = View.VISIBLE

            rr_rate_review.setOnClickListener {
                mActivity.addFragment(
                    CreateRateAndReviewOfferFragment.getInstance(Offer()) ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }

            rr_referral_offer.setOnClickListener {
                mActivity.addFragment(
                    CreateReferralOfferFragment.getInstance(Offer()) ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }

            rr_coupon_offer.setOnClickListener {
                mActivity.addFragment(
                    CreateCouponOfferFragment.getInstance(Offer()) ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }

        } else {

            tv_no_data_found.visibility = View.GONE
            btn_create_pdf_and_send_mail.visibility = View.VISIBLE

            /*if (! mActivity.userDetails?.login_user.equals("member")) {
                iv_add.visibility = if (offers.size > 1) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }*/

            val referralOfferData = offers.find { it.offerType == ApiParam.OFFER_TYPE_REFERRAL }
            if (referralOfferData != null) {
                rr_referral_offer.visibility = View.GONE
                setReferralOfferData(referralOfferData)
            } else {
                ll_referral_offer.visibility = View.GONE

                rr_referral_offer.visibility = View.VISIBLE
                rr_referral_offer.setOnClickListener {
                    mActivity.addFragment(
                        CreateReferralOfferFragment.getInstance(Offer()) ,
                        true ,
                        animationType = AnimationType.RightInZoomOut
                    )
                }
            }

            val rateAndReviewOfferData =
                offers.find { it.offerType == ApiParam.OFFER_TYPE_RATE_AND_REVIEW }
            if (rateAndReviewOfferData != null) {
                rr_rate_review.visibility = View.GONE
                setRateAndReviewOfferData(rateAndReviewOfferData)
            } else {
                ll_rate_and_review_offer.visibility = View.GONE

                rr_rate_review.visibility = View.VISIBLE
                rr_rate_review.setOnClickListener {
                    mActivity.addFragment(
                        CreateRateAndReviewOfferFragment.getInstance(Offer()) ,
                        true ,
                        animationType = AnimationType.RightInZoomOut
                    )
                }
            }

            val couponOfferData = offers.find { it.offerType == ApiParam.OFFER_TYPE_COUPON }
            if (couponOfferData != null) {
                rr_coupon_offer.visibility = View.GONE
                setCouponOfferData(couponOfferData)
            } else {
                ll_coupon_offer.visibility = View.GONE

                rr_coupon_offer.visibility = View.VISIBLE
                rr_coupon_offer.setOnClickListener {
                    mActivity.addFragment(
                        CreateCouponOfferFragment.getInstance(Offer()) ,
                        true ,
                        animationType = AnimationType.RightInZoomOut
                    )
                }
            }
        }
    }

    private fun setCouponOfferData(couponOfferData: Offer) {
        ll_coupon_offer_details.removeAllViews()

        with(couponOfferData) {
            tv_coupon_offer_expiry_date.text =
                "${resources.getString(R.string.expires_on)} ${
                    getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)
                }"

            if (subOffers.isNotEmpty()) {
                val subOffer =
                    subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_COUPON_RATE }
                if (subOffer != null) {
                    with(subOffer) {
                        val offerView = LayoutInflater.from(mActivity).inflate(
                            R.layout.view_offer_details_with_read_more_mer ,
                            ll_coupon_offer_details ,
                            false
                        )
                        offerView.tv_offer_type.visibility = View.GONE
                        when (subOfferCategory) {
                            ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                offerView.tv_sub_offer_type.text = title
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    inStoreDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                offerView.tv_sub_offer_type.text =
                                    if (mActivity.userDetails?.countryCode != "+91") {
                                        "${resources.getString(R.string.store_credit_of)} $$amount"
                                    } else {
                                        "${resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }

                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    storeCreditDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {

                                if (mActivity.userDetails?.countryCode != "+91") {
                                    offerView.tv_sub_offer_type.text =
                                        "${resources.getString(R.string.gift_card_worth)} $$amount"
                                }
                                else {
                                    offerView.tv_sub_offer_type.text =
                                        "${resources.getString(R.string.gift_card_worth)} ₹$amount"
                                }
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    giftCardDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                        }

                        offerView.iv_delete.setOnClickListener {
                            showConfirmationDialog(id , offerId)
                        }
                        if (conditions.isRequiredField()) {
                            offerView.tv_read_more.setOnClickListener {
                                showReadMoreDialog(conditions)
                            }
                        } else {
                            offerView.tv_read_more.visibility = View.GONE
                        }

                        ll_coupon_offer_details.addView(offerView)
                    }
                }
            }

            iv_edit_coupon_offer.setOnClickListener {
                val referralOfferToPass = Offer()
                referralOfferToPass.id = id
                referralOfferToPass.offerType = offerType
                referralOfferToPass.expiryDate = expiryDate
                referralOfferToPass.referredDate = referredDate
                val referralSubOffersToPass = ArrayList<SubOffer>()
                for (i in subOffers.indices) {
                    val subOfferToPass = SubOffer()
                    subOfferToPass.id = subOffers[i].id
                    subOfferToPass.offerId = subOffers[i].offerId
                    subOfferToPass.subOfferType = subOffers[i].subOfferType
                    subOfferToPass.subOfferCategory = subOffers[i].subOfferCategory
                    subOfferToPass.amount = subOffers[i].amount
                    subOfferToPass.title = subOffers[i].title
                    subOfferToPass.conditions = subOffers[i].conditions
                    subOfferToPass.isRedemptionRequired = subOffers[i].isRedemptionRequired
                    subOfferToPass.qrCode = subOffers[i].qrCode
                    subOfferToPass.code = subOffers[i].code
                    subOfferToPass.status = subOffers[i].status
                    referralSubOffersToPass.add(subOfferToPass)
                }
                referralOfferToPass.subOffers = referralSubOffersToPass
                mActivity.addFragment(
                    CreateCouponOfferFragment.getInstance(referralOfferToPass) ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }
        }

        ll_coupon_offer.visibility = View.VISIBLE
    }

    private fun setReferralOfferData(offer: Offer) {

        ll_referral_offer_details.removeAllViews()

        with(offer) {

            tv_referral_offer_expiry_date.text = "${resources.getString(R.string.expires_on)} ${
                getChangedDateFormat(
                    expiryDate ,
                    DATE_FORMAT ,
                    APP_DATE_FORMAT
                )
            }"

            if (subOffers.isNotEmpty()) {
                subOffers.forEachIndexed { index , subOffer ->
                    if (subOffer.subOfferType != ApiParam.SUB_OFFER_TYPE_RATE) {
                        with(subOffer) {
                            val offerView = LayoutInflater.from(mActivity).inflate(
                                R.layout.view_offer_details_with_read_more_mer ,
                                ll_referral_offer_details ,
                                false
                            )
                            offerView.tv_offer_type.text = when (subOfferType) {
                                ApiParam.SUB_OFFER_TYPE_REFERRAL -> resources.getString(R.string.smark_offer)
                                ApiParam.SUB_OFFER_TYPE_BONUS -> resources.getString(R.string.welcome_bonus_offer)
                                ApiParam.SUB_OFFER_TYPE_REWARD -> resources.getString(R.string.thank_u_reward)
                                else -> ""
                            }
                            when (subOfferCategory) {
                                ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                    offerView.tv_sub_offer_type.text = title
                                    offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                        inStoreDrawable ,
                                        null ,
                                        null ,
                                        null
                                    )
                                }
                                ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {

                                        offerView.tv_sub_offer_type.text =
                                            "${resources.getString(R.string.store_credit_of)} $$amount"
                                    }else {
                                        offerView.tv_sub_offer_type.text =
                                            "${resources.getString(R.string.store_credit_of)} ₹$amount"
                                    }
                                    offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                        storeCreditDrawable ,
                                        null ,
                                        null ,
                                        null
                                    )
                                }
                                ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                    if (mActivity.userDetails?.countryCode != "+91") {

                                        offerView.tv_sub_offer_type.text =
                                            "${resources.getString(R.string.gift_card_worth)} $$amount"
                                    }
                                    else
                                    {
                                        offerView.tv_sub_offer_type.text =
                                            "${resources.getString(R.string.gift_card_worth)} ₹$amount"
                                    }
                                    offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                        giftCardDrawable ,
                                        null ,
                                        null ,
                                        null
                                    )
                                }
                            }

                            if (mActivity.userDetails?.login_user.equals("member")) {
                                offerView.iv_delete.visibility = View.INVISIBLE
                            } else {
                                offerView.iv_delete.visibility = View.VISIBLE
                            }

                            offerView.iv_delete.setOnClickListener {
                                showConfirmationDialog(id , offerId)
                            }
                            offerView.tv_read_more.setOnClickListener {
                                showReadMoreDialog(conditions)
                            }
                            if (conditions.isRequiredField()) {
                                offerView.tv_read_more.setOnClickListener {
                                    showReadMoreDialog(conditions)
                                }
                            } else {
                                offerView.tv_read_more.visibility = View.GONE
                            }

                            ll_referral_offer_details.addView(offerView)

                            if (index < subOffers.size - 1) {
                                addDivider()
                            }
                        }
                    }
                }
            }

            if (mActivity.userDetails?.login_user.equals("member")) {
                iv_edit_referral_offer.visibility = View.INVISIBLE
            } else {
                iv_edit_referral_offer.visibility = View.VISIBLE
            }

            iv_edit_referral_offer.setOnClickListener {
                val referralOfferToPass = Offer()
                referralOfferToPass.id = id
                referralOfferToPass.offerType = offerType
                referralOfferToPass.expiryDate = expiryDate
                referralOfferToPass.referredDate = referredDate
                val referralSubOffersToPass = ArrayList<SubOffer>()
                for (i in subOffers.indices) {
                    val subOfferToPass = SubOffer()
                    subOfferToPass.id = subOffers[i].id
                    subOfferToPass.offerId = subOffers[i].offerId
                    subOfferToPass.subOfferType = subOffers[i].subOfferType
                    subOfferToPass.subOfferCategory = subOffers[i].subOfferCategory
                    subOfferToPass.amount = subOffers[i].amount
                    subOfferToPass.title = subOffers[i].title
                    subOfferToPass.conditions = subOffers[i].conditions
                    subOfferToPass.isRedemptionRequired = subOffers[i].isRedemptionRequired
                    subOfferToPass.qrCode = subOffers[i].qrCode
                    subOfferToPass.code = subOffers[i].code
                    subOfferToPass.status = subOffers[i].status
                    referralSubOffersToPass.add(subOfferToPass)
                }
                referralOfferToPass.subOffers = referralSubOffersToPass
                mActivity.addFragment(
                    CreateReferralOfferFragment.getInstance(referralOfferToPass) ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }
        }

        ll_referral_offer.visibility = View.VISIBLE
    }

    private fun setRateAndReviewOfferData(offer: Offer) {

        ll_rate_and_review_offer_details.removeAllViews()

        with(offer) {

            tv_rate_and_review_offer_expiry_date.text =
                "${resources.getString(R.string.expires_on)} ${
                    getChangedDateFormat(
                        expiryDate ,
                        DATE_FORMAT ,
                        APP_DATE_FORMAT
                    )
                }"

            if (subOffers.isNotEmpty()) {
                val subOffer = subOffers.find { it.subOfferType == ApiParam.SUB_OFFER_TYPE_RATE }
                if (subOffer != null) {
                    with(subOffer) {
                        val offerView = LayoutInflater.from(mActivity).inflate(
                            R.layout.view_offer_details_with_read_more_mer ,
                            ll_rate_and_review_offer_details ,
                            false
                        )
                        offerView.tv_offer_type.visibility = View.GONE
                        when (subOfferCategory) {
                            ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                offerView.tv_sub_offer_type.text = title
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    inStoreDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                if (mActivity.userDetails?.countryCode != "+91") {
                                        offerView.tv_sub_offer_type.text =
                                            "${resources.getString(R.string.store_credit_of)} $$amount"
                                    }
                                    else {
                                    offerView.tv_sub_offer_type.text =
                                        "${resources.getString(R.string.store_credit_of)} ₹$amount"
                                }
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    storeCreditDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                            ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                if (mActivity.userDetails?.countryCode != "+91") {

                                    offerView.tv_sub_offer_type.text =
                                        "${resources.getString(R.string.gift_card_worth)} $$amount"
                                }
                                else {
                                    offerView.tv_sub_offer_type.text =
                                        "${resources.getString(R.string.gift_card_worth)} ₹$amount"
                                }
                                offerView.tv_sub_offer_type.setCompoundDrawablesWithIntrinsicBounds(
                                    giftCardDrawable ,
                                    null ,
                                    null ,
                                    null
                                )
                            }
                        }

                        if (mActivity.userDetails?.login_user.equals("member")) {
                            offerView.iv_delete.visibility = View.INVISIBLE
                        } else {
                            offerView.iv_delete.visibility = View.VISIBLE
                        }

                        offerView.iv_delete.setOnClickListener {
                            showConfirmationDialog(id , offerId)
                        }
                        if (conditions.isRequiredField()) {
                            offerView.tv_read_more.setOnClickListener {
                                showReadMoreDialog(conditions)
                            }
                        } else {
                            offerView.tv_read_more.visibility = View.GONE
                        }

                        ll_rate_and_review_offer_details.addView(offerView)
                    }
                }
            }

            if (mActivity.userDetails?.login_user.equals("member")) {
                iv_edit_rate_and_review_offer.visibility = View.INVISIBLE
            } else {
                iv_edit_rate_and_review_offer.visibility = View.VISIBLE
            }

            iv_edit_rate_and_review_offer.setOnClickListener {
                val rateAndReviewOfferToPass = Offer()
                rateAndReviewOfferToPass.id = id
                rateAndReviewOfferToPass.offerType = offerType
                rateAndReviewOfferToPass.expiryDate = expiryDate
                rateAndReviewOfferToPass.referredDate = referredDate
                val rateAndReviewSubOffersToPass = ArrayList<SubOffer>()
                for (i in subOffers.indices) {
                    val subOfferToPass = SubOffer()
                    subOfferToPass.id = subOffers[i].id
                    subOfferToPass.offerId = subOffers[i].offerId
                    subOfferToPass.subOfferType = subOffers[i].subOfferType
                    subOfferToPass.subOfferCategory = subOffers[i].subOfferCategory
                    subOfferToPass.amount = subOffers[i].amount
                    subOfferToPass.title = subOffers[i].title
                    subOfferToPass.conditions = subOffers[i].conditions
                    subOfferToPass.isRedemptionRequired = subOffers[i].isRedemptionRequired
                    subOfferToPass.qrCode = subOffers[i].qrCode
                    subOfferToPass.code = subOffers[i].code
                    subOfferToPass.status = subOffers[i].status
                    rateAndReviewSubOffersToPass.add(subOfferToPass)
                }
                rateAndReviewOfferToPass.subOffers = rateAndReviewSubOffersToPass
                mActivity.addFragment(
                    CreateRateAndReviewOfferFragment.getInstance(
                        rateAndReviewOfferToPass
                    ) , true , animationType = AnimationType.RightInZoomOut
                )
            }
        }

        ll_rate_and_review_offer.visibility = View.VISIBLE
    }

    private fun showConfirmationDialog(subOfferId: Long , offerId: Long) {
        AlertDialog.Builder(mActivity)
            .setMessage(resources.getString(R.string.delete_offer_confirmation))
            .setPositiveButton(resources.getString(R.string.delete)) { dialog , which ->
                callDeleteOfferAPI(subOfferId , offerId)
                dialog.dismiss()
            }
            .setNegativeButton(resources.getString(R.string.cancel)) { dialog , which ->
                dialog.dismiss()
            }
            .show()
    }

    private fun showReadMoreDialog(condition: String) {

        val dialogView =
            LayoutInflater.from(mActivity).inflate(R.layout.dialog_read_more , null , false)

        dialogView.tv_condition.text = condition

        AlertDialog.Builder(mActivity)
            .setView(dialogView)
            .setNegativeButton(resources.getString(R.string.close)) { dialog , which ->
                dialog.dismiss()
            }
            .show()
    }

    private fun addDivider() {

        val dividerView = LayoutInflater.from(mActivity)
            .inflate(R.layout.divider , ll_referral_offer_details , false)
        dividerView.view_divider.setBackgroundColor(mActivity.resources.getColor(R.color.dim_gray_txt))

        ll_referral_offer_details.addView(dividerView)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_add -> {
                val popupMenu = PopupMenu(mActivity , iv_add)
                if (ll_referral_offer.visibility == View.GONE) {
                    popupMenu.menu.add(resources.getString(R.string.add_referral_offer))
                }
                if (ll_rate_and_review_offer.visibility == View.GONE) {
                    popupMenu.menu.add(resources.getString(R.string.add_rate_and_review_offer))
                }

                if (ll_coupon_offer.visibility == View.GONE) {
                    popupMenu.menu.add(resources.getString(R.string.add_coupon_offer))
                }

                popupMenu.setOnMenuItemClickListener {
                    when (it.title) {
                        resources.getString(R.string.add_referral_offer) -> {
                            mActivity.addFragment(
                                CreateReferralOfferFragment.getInstance(Offer()) ,
                                true ,
                                animationType = AnimationType.RightInZoomOut
                            )
                        }
                        resources.getString(R.string.add_rate_and_review_offer) -> {
                            mActivity.addFragment(
                                CreateRateAndReviewOfferFragment.getInstance(Offer()) ,
                                true ,
                                animationType = AnimationType.RightInZoomOut
                            )
                        }
                        resources.getString(R.string.add_coupon_offer) -> {
                            mActivity.addFragment(
                                CreateCouponOfferFragment.getInstance(Offer()) ,
                                true ,
                                animationType = AnimationType.RightInZoomOut
                            )
                        }
                        else -> {
                        }
                    }
                    true
                }
                popupMenu.show()
            }
            R.id.btn_create_pdf_and_send_mail -> ApiUtil.callCreatePDFAndSendEmail(mActivity)
            else -> {
            }
        }
    }

    override fun onRefresh() {
        callMyOffersAPI()
    }

    private fun callMyOffersAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        swipe_refresh.isRefreshing = true

        disposable = WebApiClient.webApi().myOffersAPI().subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                offers.clear()

                                                if (data.isNotEmpty()) {
                                                    offers.addAll(data)
                                                }

                                                setOffersData()
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    }
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    }
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun callDeleteOfferAPI(subOfferId: Long , offerId: Long) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.OFFER_ID] = "$offerId"
        hashMap[ApiParam.SUB_OFFER_ID] = "$subOfferId"

        disposable = WebApiClient.webApi().deleteOfferAPI(hashMap).subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    kotlin.with(meta !!) {
                                        if (status == ApiParam.META_SUCCESS) {

                                            offers.clear()

                                            if (data.isNotEmpty()) {
                                                offers.addAll(data)
                                            }

                                            setOffersData()

                                        } else {
                                            mActivity.showDialog(message)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        AddEditDeleteOfferManager.removeCallBack(this)
        super.onDestroy()
    }

    override fun onAddEditDeleteOffer(updatedOffers: ArrayList<Offer>) {
        offers = updatedOffers
        setOffersData()
    }
}