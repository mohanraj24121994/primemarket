package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.addFragment
import com.smarket.utils.avoidDoubleClicks
import com.smarket.utils.hideKeyboard
import com.smarket.utils.isRequiredField
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import kotlinx.android.synthetic.main.view_create_offer.*
import kotlinx.android.synthetic.main.view_offer_type.*
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class ReferralOfferStep4Fragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): ReferralOfferStep4Fragment {
            val fragment = ReferralOfferStep4Fragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var expiryDate = ""

    private var offer: Offer? = null

    private lateinit var mParentFragment: CreateReferralOfferFragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_create_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cl_business_category.visibility = View.GONE
        et_amount.visibility = View.GONE
        et_offer_conditions.visibility = View.GONE
        btn_skip.visibility = View.GONE

        et_offer_expiry_date.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mParentFragment = parentFragment as CreateReferralOfferFragment

        offer = (parentFragment as CreateReferralOfferFragment).offer

        if (offer != null && offer!!.id != 0L) {
            setOfferData()
        }
    }

    private fun setOfferData() {

        et_offer_expiry_date.setText(getChangedDateFormat(offer!!.expiryDate, DATE_FORMAT, APP_DATE_FORMAT))
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.et_offer_expiry_date -> {
                avoidDoubleClicks(et_offer_expiry_date)
                mActivity.hideKeyboard()
                mActivity.showDatePicker(et_offer_expiry_date)
            }
            R.id.btn_next -> {
                if (checkForValidations()) {

                    with(mParentFragment.offer!!) {
                        offerType = ApiParam.OFFER_TYPE_REFERRAL
                        expiryDate = getChangedDateFormat(this@ReferralOfferStep4Fragment.expiryDate, APP_DATE_FORMAT, DATE_FORMAT)
                    }

                    mActivity.addFragment(OfferPreviewFragment.getInstance(
                        mParentFragment.offer!! , null)
                            , true
                            , animationType = AnimationType.RightInZoomOut)
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        expiryDate = et_offer_expiry_date.text.toString()

        if (!expiryDate.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.expiry_date_req))
            return false
        }
        return true
    }
}