package com.smarket.main.merchant

import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.NetworkUtil
import com.smarket.utils.getPrimaryColor
import com.smarket.utils.showToast
import com.smarket.R
import com.smarket.interfaces.UpdateRefCash
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.main.BaseMainFragment
import com.smarket.model.RefCashHistory
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.RefCashHistoryResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_refcash_history.*
import kotlinx.android.synthetic.main.row_refcash_history.view.*
import retrofit2.Response

/**
 * Created by MI-062 on 11/4/18.
 */
class RefcashHistoryFragment : BaseMainFragment(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener, UpdateRefCash {

    companion object {
        fun getInstance(historyType: String): RefcashHistoryFragment {
            val fragment = RefcashHistoryFragment()
            val bundle = Bundle()
            bundle.putString(HISTORY_TYPE, historyType)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var refCashHistoryAdapter: RefCashHistoryAdapter

    var refCashHistoryList = ArrayList<RefCashHistory>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    var historyType = ""

    private var disposable: Disposable? = null
    var observable: Observable<Response<RefCashHistoryResponse>>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_refcash_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_refcash_history.layoutManager = linearLayoutManager
        rv_refcash_history.addItemDecoration(androidx.recyclerview.widget.DividerItemDecoration(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL))
        refCashHistoryAdapter = RefCashHistoryAdapter()
        rv_refcash_history.adapter = refCashHistoryAdapter

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(HISTORY_TYPE)) {
                historyType = bundle.getString(HISTORY_TYPE)!!
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        UpdateRefCashManager.addCallBack(this)

        rv_refcash_history.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == refCashHistoryAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callRefCashHistoryAPI()
                }
            }
        })

        callRefCashHistoryAPI()
    }

    override fun onRefresh() {
        refreshRefCashHistory()
    }

    private fun refreshRefCashHistory() {
        page = 1
        callRefCashHistoryAPI()
    }

    inner class RefCashHistoryAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_refcash_history, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (refCashHistoryList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return refCashHistoryList.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(refCashHistoryList[holder.adapterPosition]) {
                    holder.itemView.tv_title.text = name
                    holder.itemView.tv_time.text = "${getLocalTimeFromUTC(transactionTime)}"
                    holder.itemView.tv_amount.text = "$amount ${mActivity.resources.getString(R.string.ref)}"
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }


    private fun callRefCashHistoryAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (refCashHistoryList.find { it.id == 0L } == null) {
                refCashHistoryList.add(RefCashHistory())
                refCashHistoryAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val tag: String

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_REFCASH_HISTORY
        } else {
            tag = ApiParam.Merchant.TAG_REFCASH_HISTORY
        }

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.HISTORY_TYPE] = historyType
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().refCashHistoryAPI(tag, hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            refCashHistoryList.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            refCashHistoryList.addAll(data)
                                                        }

                                                        page++

                                                        this@RefcashHistoryFragment.lastPage = lastPage
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (refCashHistoryList.isNotEmpty()) {
                                    refCashHistoryList.removeAll(refCashHistoryList.filter { it.id == 0L })
                                }
                            }

                            refCashHistoryAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (refCashHistoryList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (refCashHistoryList.isNotEmpty()) {
                                    refCashHistoryList.removeAll(refCashHistoryList.filter { it.id == 0L })
                                    refCashHistoryAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (refCashHistoryList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        UpdateRefCashManager.removeCallBack(this)
        super.onDestroy()
    }

    override fun onUpdateRefCash(refCash: Double) {
        refreshRefCashHistory()
    }
}