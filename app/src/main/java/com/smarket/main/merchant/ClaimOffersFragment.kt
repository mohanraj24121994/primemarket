package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_claim_offers.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_create_offer.*


class ClaimOffersFragment : BaseMainFragment() , View.OnClickListener {

    var amount = ""
    var conditions = ""
    private var disposable: Disposable? = null

    companion object {
        fun getInstance(): ClaimOffersFragment {
            val fragment = ClaimOffersFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_claim_offers , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        btn_skip.visibility = View.GONE
        view_offer.visibility = View.GONE
        et_offer_expiry_date.visibility = View.GONE

        getBundle()

        setHeader()

        btn_next.setOnClickListener(this)

        listCashback()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.create_claim_offer)
        btn_next.text = mActivity.resources.getString(R.string.save)
        et_amount.hint = mActivity.resources.getString(R.string.enter_percentage)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_next -> {
                if (checkForValidations()) {
                    createCashBackOffer()
                }
            }
        }
    }

    private fun createCashBackOffer() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.CreateCashBackOffer.OFFER_PERCENTAGE] = amount
        hashMap[ApiParam.CreateCashBackOffer.OFFER_CONDITION] = conditions
        hashMap[ApiParam.CreateCashBackOffer.STATUS] = "1"

        disposable = WebApiClient.webApi().createCashBackAPI(hashMap).subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                if (disposable == null) return@subscribe
                with(it) {
                    if (isSuccessful && body() != null) {
                        with(body() !!) {
                            if (! status.equals("error")) {
                                mActivity.showDialog(message ?: "")
                            } else {
                                mActivity.showDialog(message ?: "")
                            }
                        }
                    } else {
                        ErrorUtil.showError(it , mActivity)
                    }
                }
                ProgressDialogUtil.hideProgressDialog()

            } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )

    }

    private fun listCashback() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        disposable = WebApiClient.webApi().listCashBackAPI().subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                if (disposable == null) return@subscribe
                with(it) {
                    if (isSuccessful && body() != null) {
                        with(body() !!) {
                            if (status.equals("success")) {
                                if (data.isNullOrEmpty()) {
                                    create_offer_ll.visibility = View.VISIBLE
                                    list_offer_rr.visibility = View.GONE
                                } else {
                                    create_offer_ll.visibility = View.GONE
                                    list_offer_rr.visibility = View.VISIBLE
                                    data.forEach {
                                        tv_offer_percentage.text = it.offerPercentage.toString()
                                        tv_offer_conditions.text = it.offerCondition
                                    }
                                }
                            }
                        }
                    } else {
                        ErrorUtil.showError(it , mActivity)
                    }
                }
                ProgressDialogUtil.hideProgressDialog()

            } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )

    }

    private fun checkForValidations(): Boolean {
        mActivity.hideKeyboard()

        amount = et_amount.text.toString()

        if (! amount.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.percentage_req))
            return false
        }

        conditions = et_offer_conditions.text.toString()

        return true
    }
}