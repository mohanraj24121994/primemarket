package com.smarket.main.merchant

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.smarket.R
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.main.BaseMainFragment
import com.smarket.model.Offer
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_offer_details.view.*
import kotlinx.android.synthetic.main.fragment_redeem_code.*
import kotlinx.android.synthetic.main.header.*
import org.json.JSONObject
import java.io.IOException

/**
 * Created by MI-062 on 11/4/18.
 */
class RedeemCodeFragment : BaseMainFragment() , View.OnClickListener {

    companion object {
        fun getInstance(): RedeemCodeFragment {
            val fragment = RedeemCodeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    var CAMERA_PERMISSION = Manifest.permission.CAMERA
    val CAMERA_PERMISSION_REQUEST_CODE = 401
    val CAMERA_PERMISSION_GRANTED = 1
    val CAMERA_PERMISSION_DENIED = 2
    val CAMERA_PERMISSION_DENIED_FOR_ALWAYS = 3
    var isLoading = false

    var code = ""
    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_redeem_code , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        btn_submit.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.redeem_code)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        checkForCameraPermission()

        // Initializing barcode detector
        barcodeDetector = BarcodeDetector.Builder(mActivity)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build()

        // Initializing camera source
        cameraSource = CameraSource.Builder(mActivity , barcodeDetector)
                .setRequestedPreviewSize(1024 , 1024)
                .setAutoFocusEnabled(true) //you should add this feature
                .build()

        // Setting up barcode detector
        barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {}

            override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() != 0 && ! isLoading) {
                    isLoading = true
                    tv_camera_permission_status.post {
                        tv_camera_permission_status.post {

                            val barcodeContent = barcodes.valueAt(0).displayValue

                            var qrCode = ""

                            if (barcodeContent.isRequiredField() && barcodeContent.contains("SMARKET")) {
                                try {
                                    val jsonObject = JSONObject(barcodeContent)
                                    if (jsonObject.has("app")) {
                                        if (jsonObject.getString("app") == "SMARKET") {
                                            if (jsonObject.has("value")) {
                                                qrCode = jsonObject.getString("value")
                                            }
                                        }
                                    }
                                } catch (t: Throwable) {
                                    print(t.stackTrace)
                                }
                            }

                            if (qrCode.isRequiredField()) {
                                callRedeemCodeAPI(qrCode)
                            } else {
                                mActivity.showDialog(mActivity.resources.getString(R.string.invalid_qr_code))
                            }
                        }
                    }
                }
            }
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_submit -> {
                if (checkForValidations()) {
                    avoidDoubleClicks(btn_submit)
                    callRedeemCodeAPI(code)
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        code = et_code.text.toString()

        if (! code.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.code_req))
            return false
        }

        return true
    }

    private fun checkForCameraPermission() {

        if (isCameraPermissionGranted()) {
            // Android version is below Marshmallow or camera permission is already allowed.
            startCamera()
        } else {
            // Android version is above Marshmallow and camera permission is not allowed, ask user to allow.
            // Check the result in onActivityResult().
            requestPermissions(arrayOf(CAMERA_PERMISSION) , CAMERA_PERMISSION_REQUEST_CODE)
        }
    }

    private fun isCameraPermissionGranted(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
        when (requestCode) {
            CAMERA_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission is allowed.
                startCamera()
            } else {
                if (! shouldShowRequestPermissionRationale(CAMERA_PERMISSION)) {
                    // Camera permission is denied for always, ask user to turn it on from settings.
                    setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_DENIED_FOR_ALWAYS)
                } else {
                    // Camera permission is denied for now.
                    setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_DENIED)
                }
            }
            else -> {
            }
        }
    }

    private fun startCamera() {

        setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_GRANTED)

        Handler().postDelayed({
            try {
                iv_qr_code.visibility = View.GONE
                cameraSource?.start(surface_view.holder)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } , 300)
    }

    private fun stopCamera() {
        iv_qr_code.visibility = View.VISIBLE
        cameraSource?.stop()
    }

    private fun setUpCameraPermissionStatusMessage(permissionStatus: Int) {
        when (permissionStatus) {
            CAMERA_PERMISSION_DENIED_FOR_ALWAYS -> tv_camera_permission_status.text = mActivity.resources.getString(R.string.camera_permission_denied)
            CAMERA_PERMISSION_DENIED -> {

                val prefixMessage = mActivity.resources.getString(R.string.camera_permission_pending_prefix_message) + " "
                val clickableMessage = mActivity.resources.getString(R.string.click_here)
                val postfixMessage = " " + mActivity.resources.getString(R.string.camera_permission_pending_postfix_message)
                val cameraPermissionStatusMessage = prefixMessage + clickableMessage + postfixMessage

                val ss = SpannableString(cameraPermissionStatusMessage)

                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(textView: View) {
                        checkForCameraPermission()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = true
                        ds.color = mActivity.resources.getColor(R.color.green)
                    }
                }

                ss.setSpan(clickableSpan , prefixMessage.length ,
                        cameraPermissionStatusMessage.length - postfixMessage.length ,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                tv_camera_permission_status.movementMethod = LinkMovementMethod.getInstance()
                tv_camera_permission_status.text = ss
            }
            CAMERA_PERMISSION_GRANTED -> tv_camera_permission_status.text = ""
            else -> {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isVisible && isCameraPermissionGranted()) {
            startCamera()
        }
    }

    override fun onPause() {
        if (isCameraPermissionGranted()) {
            stopCamera()
        }
        super.onPause()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (isAdded && isCameraPermissionGranted()) {
            if (hidden) {
                stopCamera()
            } else {
                startCamera()
            }
        }
    }

    private fun callRedeemCodeAPI(code: String) {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.CODE] = code

        disposable = WebApiClient.webApi().scanOfferAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        showOfferDetailsDialog(data , message , refcash)

                                                        et_code.setText("")
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun showOfferDetailsDialog(offer: Offer? , metaMessage: String , refcash: Double) {
        val builder = AlertDialog.Builder(mActivity)
        val view = LayoutInflater.from(mActivity).inflate(R.layout.dialog_offer_details , null , false)
        with(view) {
            if (offer != null && offer.offerType.isRequiredField()) {
                with(offer) {
                    if (offerType == ApiParam.OFFER_TYPE_REFERRAL) {
                        tv_offer_type.text = mActivity.resources.getString(R.string.referral_offer)
                        tv_referrals.visibility = View.VISIBLE
                        // tv_referrals.text = mActivity.resources.getString(R.string.referrals_total, referralCount)

                    } else if (offerType == ApiParam.OFFER_TYPE_RATE_AND_REVIEW) {
                        tv_offer_type.text = mActivity.resources.getString(R.string.rate_and_review_offer)
                    }

                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"
                    if (status == 3) {
                        tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate , DATE_FORMAT , APP_DATE_FORMAT)}"

                        tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }
                    tv_total_refcash.text = mActivity.resources.getString(R.string.total_refcash , refcash.toString())

                    if (subOffers.isNotEmpty()) {
                        subOffers.forEachIndexed { index , subOffer ->
                            with(subOffer) {
                                when (subOfferCategory) {
                                    ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                                        tv_sub_offer_type.text = title
                                    }
                                    ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                                        if (mActivity.userDetails?.countryCode != "+91") {
                                            tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} $$amount"
                                        } else {
                                            tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.store_credit_of)} ₹$amount"
                                        }
                                    }
                                    ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                                        if (mActivity.userDetails?.countryCode != "+91") {
                                            tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} $$amount"
                                        } else {
                                            tv_sub_offer_type.text = "${mActivity.resources.getString(R.string.gift_card_worth)} ₹$amount"
                                        }
                                    }
                                }
                                if (conditions.isRequiredField()) {
                                    tv_condition_label.visibility = View.VISIBLE
                                    tv_condition.visibility = View.VISIBLE
                                    tv_condition.text = conditions
                                } else {
                                    tv_condition_label.visibility = View.GONE
                                    tv_condition.visibility = View.GONE
                                }
                            }
                        }
                    } else {
                        tv_offer_type.text = mActivity.resources.getString(R.string.offer_details)
                        tv_sub_offer_type.visibility = View.GONE
                        tv_condition_label.text = message
                        tv_condition.visibility = View.GONE
                    }
                }
            } else {
                tv_offer_type.text = mActivity.resources.getString(R.string.store_credit)
//                tv_total_refcash.text = mActivity.resources.getString(R.string.total_refcash, refcash.toString())
                tv_sub_offer_type.text = if (offer?.message !!.isRequiredField()) offer.message else metaMessage
                tv_expiry_date.visibility = View.GONE
                tv_condition_label.visibility = View.GONE
                tv_condition.visibility = View.GONE
            }
        }
        builder.setView(view)
        builder.setCancelable(false)
        builder.setPositiveButton(mActivity.resources.getString(R.string.confirm)) { dialog , i ->
            if (offer != null) {
                callRedeemOfferAPI(offer.id)
            }
            dialog.dismiss()
        }
        builder.setNegativeButton(mActivity.resources.getString(R.string.cancel)) { dialog , i ->
            dialog.dismiss()
        }
        builder.setOnDismissListener { isLoading = false }
        builder.show()
    }

    private fun callRedeemOfferAPI(id: Long) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = "$id"

        disposable = WebApiClient.webApi().redeemOfferAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        UpdateRefCashManager.executeCallBacks(mActivity , refcash)

                                                        mActivity.showDialogWithBackNavigation(mActivity , message)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        cameraSource?.release()
        super.onDestroy()
    }
}