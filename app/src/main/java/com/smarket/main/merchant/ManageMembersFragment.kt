package com.smarket.main.merchant

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.memberlist.Datum
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.divider.view.*
import kotlinx.android.synthetic.main.fragment_manage_members.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_offer_details_with_read_more_mer.view.*
import utils.AnimationType

class ManageMembersFragment : BaseMainFragment() , View.OnClickListener , SwipeRefreshLayout.OnRefreshListener {

    private var disposable: Disposable? = null
    var addMembers = ArrayList<Datum>()

    companion object {
        fun getInstance(): ManageMembersFragment {
            val fragment = ManageMembersFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        Log.e("call" , "onViewCreated")

    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.manage_members)
        iv_add.setOnClickListener(this)
        if (mActivity.userDetails?.login_user.equals("member")) {
            iv_add.visibility = View.INVISIBLE
        } else {
            iv_add.visibility = View.VISIBLE
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_manage_members , container , false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Log.e("call" , "onActivityCreated")

        getMembersList()
    }

    override fun onRefresh() {
        getMembersList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("call" , "onCreate")
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_add -> {
                val popupMenu = PopupMenu(mActivity , iv_add)
                popupMenu.menu.add(resources.getString(R.string.add_referral_member))

                popupMenu.setOnMenuItemClickListener {
                    when (it.title) {
                        resources.getString(R.string.add_referral_member) -> {
                            mActivity.addFragment(AddMembersFragment.getInstance("" , 0 , false) , true ,
                                    animationType = AnimationType.RightInZoomOut)
                        }
                        else -> {
                        }
                    }
                    true
                }
                popupMenu.show()
            }
        }
    }

    private fun getMembersList() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        swipe_refresh.isRefreshing = true

        disposable = WebApiClient.webApi().listMembersAPI().subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        addMembers.clear()
                                                        data?.forEach {
                                                            addMembers.add(it)
                                                        }

                                                        setMembersData()
                                                    }
                                                    else -> {
                                                        Toast.makeText(mActivity , message , Toast.LENGTH_SHORT).show()
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            }
                        } , {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    }
                    ErrorUtil.setExceptionMessage(it)
                })
    }

    private fun setMembersData() {
        ll_members_details.removeAllViews()

        if (addMembers.isNotEmpty()) {
            tv_no_data_found.visibility = View.GONE
            addMembers.forEachIndexed { index , datum ->
                with(datum) {
                    val membersView = LayoutInflater.from(mActivity).inflate(R.layout.view_offer_details_with_read_more_mer , ll_members_details , false)
                    membersView.tv_sub_offer_type.text = email
                    membersView.tv_read_more.visibility = View.GONE

                    if (mActivity.userDetails?.login_user.equals("member")) {
                        membersView.iv_edit.visibility = View.INVISIBLE
                        membersView.iv_delete.visibility = View.INVISIBLE
                    } else {
                        membersView.iv_edit.visibility = View.VISIBLE
                        membersView.iv_delete.visibility = View.VISIBLE
                    }

                    membersView.iv_delete.setOnClickListener {
                        showConfirmationDialogMembers(id)
                    }

                    ll_members_details.addView(membersView)

                    if (index < addMembers.size - 1) {
                        addDividerMember()
                    }

                    membersView.iv_edit.setOnClickListener {
                        mActivity.addFragment(AddMembersFragment.getInstance(email ?: "" , id
                                ?: 0 , true
                        ) , true , animationType = AnimationType.RightInZoomOut)
                    }
                }
            }


        } else {
            tv_no_data_found.visibility = View.VISIBLE
        }
        ll_members.visibility = View.VISIBLE
    }

    private fun showConfirmationDialogMembers(membersId: Int?) {
        AlertDialog.Builder(mActivity)
                .setMessage(resources.getString(R.string.delete_members_confirmation))
                .setPositiveButton(resources.getString(R.string.delete)) { dialog , which ->
                    callDeleteMembersAPI(membersId)
                    dialog.dismiss()
                }
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog , which ->
                    dialog.dismiss()
                }
                .show()
    }

    private fun callDeleteMembersAPI(membersId: Int?) {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.MEMBER_ID] = "$membersId"

        disposable = WebApiClient.webApi().deleteMembersAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (getMeta() != null) {
                                            with(getMeta() !!) {
                                                if (getStatus() == ApiParam.META_SUCCESS) {
                                                    getMembersList()
                                                } else {
                                                    Toast.makeText(mActivity , getMessage() , Toast.LENGTH_SHORT).show()
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun addDividerMember() {

        val dividerView = LayoutInflater.from(mActivity).inflate(R.layout.divider , ll_members_details , false)
        dividerView.view_divider.setBackgroundColor(mActivity.resources.getColor(R.color.dim_gray_txt))

        ll_members_details.addView(dividerView)
    }
}