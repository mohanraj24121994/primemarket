package com.smarket.main.merchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.smarket.utils.*
import com.smarket.utils.showDialog
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_refer_merchant.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_mobile_number.*

/**
 * Created by MI-062 on 11/4/18.
 */
class ReferMerchantFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): ReferMerchantFragment {
            val fragment = ReferMerchantFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    private var disposable: Disposable? = null
    var email = ""
    var countryCode = ""
    var mobile = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_refer_merchant, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        mActivity.setCountryCodeSpinner(sp_country_code)

        sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                countryCode = mActivity.countryCodeList[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        btn_all_set.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.refer_merchant)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_all_set -> {
                if (checkForValidations()) {
                    callReferMerchantAPI()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        email = et_email.text.toString()
        mobile = et_mobile.text.toString()

        if (!email.isRequiredField() && !mobile.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.email_or_mobile_req))
            return false
        } else if (email.isRequiredField() && !email.isValidMobile()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
            return false
        } else if (mobile.isRequiredField() && !mobile.isValidMobile()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
            return false
        }

        return true
    }

    private fun callReferMerchantAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        if (email.isRequiredField()) {
            hashMap[ApiParam.EMAIL] = email
        } else if (mobile.isRequiredField()) {
            hashMap[ApiParam.MOBILE] = mobile
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
        }

        disposable = WebApiClient.webApi().referMerchantAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        clearFilledData()

                                                        mActivity.showDialog(message)

                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun clearFilledData() {
        et_email.setText("")
        et_mobile.setText("")
        mActivity.setCurrentCountryCodeAsDefault(sp_country_code)
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}