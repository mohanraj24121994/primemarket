package com.smarket.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.smarket.R

/**
 * Created by MI-062 on 20/6/18.
 */
open class BaseMainFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    lateinit var mActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = activity as MainActivity
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_drawer -> mActivity.openDrawer()
            R.id.iv_back -> mActivity.onBackPressed()
            else -> {
            }
        }
    }
}