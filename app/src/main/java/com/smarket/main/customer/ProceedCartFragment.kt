package com.smarket.main.customer

import android.R.id.message
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import kotlinx.android.synthetic.main.fragment_cart_product.*
import kotlinx.android.synthetic.main.fragment_proceed_cart.*
import kotlinx.android.synthetic.main.fragment_proceed_cart.et_address
import kotlinx.android.synthetic.main.fragment_proceed_cart.et_email
import kotlinx.android.synthetic.main.fragment_proceed_cart.et_name
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_mobile_number.*


class ProceedCartFragment : BaseMainFragment(), View.OnClickListener {
    var isFrom = ""
    var name = ""
    var email = ""
    var mobile = ""
    var address = ""

    companion object {
        fun getInstance(isFrom: String = ""): ProceedCartFragment {
            val fragment = ProceedCartFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_proceed_cart, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundle()

        setHeader()


        btn_proceed.setOnClickListener {
            name = et_name.text.toString()
            email = et_email.text.toString()
            mobile = et_mobile.text.toString()
            address = et_address.text.toString()
            if (!name.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.name_req))
            } else if (!email.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.email_req))
            } else if (email.isRequiredField() && !email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
            } else if (!mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
            } else if (!address.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.address_req))
            } else {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "email@email.com", null))
                startActivity(Intent.createChooser(intent, "Choose an Email client :"))

            }

        }

        //  mActivity.updateUserDetailsObject()

        // callGiftListAPI()

        /* val btn = view.findViewById<Button>(R.id.btn_redeem)
         btn.setOnClickListener {
             callRedeemRefCashAPI()
         }*/
    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.proceedfragment)

    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM)!!
            }
        }
    }


}