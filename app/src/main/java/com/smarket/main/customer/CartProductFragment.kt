package com.smarket.main.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.smarket.R
import com.smarket.adapter.CartItemModel
import com.smarket.adapter.CartProductAdapter
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import kotlinx.android.synthetic.main.fragment_cart_product.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.iv_back
import utils.AnimationType


class CartProductFragment : BaseMainFragment(), View.OnClickListener {

    var isFrom = ""
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cartAdapter: CartProductAdapter

    companion object {
        fun getInstance(isFrom: String = ""): CartProductFragment {
            val fragment = CartProductFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart_product, container, false)

    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rvfun()
        cartAdapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundle()
        setHeader()

     //   cartAdapter.notifyDataSetChanged()
    }

    fun rvfun() {
      var dup = Singleton.addOfferProduct.distinct().toList()

        rv_cart.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        cartAdapter = CartProductAdapter(dup, mActivity)
       // cartAdapter.setHasStableIds(true)
        rv_cart.adapter = cartAdapter
        cartAdapter.notifyDataSetChanged()
        
       // cartAdapter.notifyItemRemoved(dup)

        //cartAdapter.notifyDataSetChanged()






    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.cartitem)
        btn_proceedpayment.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM)!!
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_back -> mActivity.onBackPressed()
            R.id.btn_proceedpayment -> mActivity.addFragment(ProceedCartFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)

        }

    }


}