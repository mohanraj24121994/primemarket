package com.smarket.main.customer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.ProductComparisonAdapter
import com.smarket.database.User
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.main.BaseMainFragment
import com.smarket.model.compsrisonresult.ComparisonResult
import com.smarket.model.compsrisonresult.Datum
import com.smarket.model.compsrisonresult.KeyDetails
import com.smarket.model.referrallist.ProductCompareList
import com.smarket.utils.*
import com.smarket.webservices.*
import com.smarket.webservices.response.UserActivityResponse
import com.smarket.webservices.response.VigilinkProductResponse
import com.smarket.webservices.response.adminpercentage.AdminPercentage
import com.smarket.webservices.response.refferedlist.ReferredList
import com.tap.app.utils.CustomTypefaceSpan
import kotlinx.android.synthetic.main.fragment_product_comparison.*
import kotlinx.android.synthetic.main.header.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.AnimationType
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ProductComparisonFragment : BaseMainFragment(), View.OnClickListener {

    private var userID: String? = null
    var count: Int = 0
    var referPercentage = 60
    var mComparisonResult: ArrayList<Datum> = ArrayList()
    private val progressDialog = CustomProgressDialog()
    private var mProductCompareList = ArrayList<ProductCompareList>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_product_comparison, container, false)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userID = SMarket.appDB.userDao().getData()?.id.toString()

        getBundle()

        setHeader()

        mActivity.updateUserDetailsObject()

        if (mActivity.userDetails?.countryCode != "+91") {
            rewards_tv.text = getString(R.string.dollar).plus(mActivity.userDetails?.refcash)
        } else {
            rewards_tv.text = getString(R.string.rupees).plus(mActivity.userDetails?.refcash)
        }
        callReferralAPI()
        friends_fl.setOnClickListener(this)
        txt_contact.setOnClickListener(this)
        animateBackground()
        val shareURL = PreferenceUtil.getStringPref("shareURL")
        val checkingLink = modifyShareLink(shareURL)

        /*if (checkingLink.isNotEmpty()) {
            GlobalScope.launch(Dispatchers.IO) {
                callProductListAPI(checkingLink)
            }
        }*/
        // if (mComparisonResult.isEmpty()) {
        calReferralPercentAPI(checkingLink)
        // }
        Log.e("checkingLink", "checkingLink==>$checkingLink")
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            Log.d("GetBundle", bundle.toString())
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        tv_title.visibility = View.GONE
    }

    private fun callReferralAPI() {
        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }
        val callReferralApi = WebApiClient.webApi().referList(userID ?: "")
        callReferralApi?.enqueue(object : Callback<ReferredList?> {
            override fun onFailure(call: Call<ReferredList?>, t: Throwable) {
                Log.e("onFailure", "onFailure" + t.message)
            }

            override fun onResponse(call: Call<ReferredList?>, response: Response<ReferredList?>) {
                if (response.isSuccessful) {
                    if (response.body() != null)
                        Log.d("callReferralAPI", response.body().toString())
                    if (response.body()?.getData()?.getRefered()?.size!! > 0) {
                        referral_count_tv.text =
                            response.body()?.getData()?.getRefered()?.size.toString()
                    } else {
                        try {
                            referral_count_tv.text = "0"
                        } catch (e: Exception) {
                            Log.e("referFriends", "referFriends" + e.message)
                        }
                    }
                }
            }
        })
    }

    private fun animateBackground() {
        val animLay = mActivity.findViewById<ConstraintLayout>(R.id.root)
        val drawable = intArrayOf(R.drawable.new_banner, R.drawable.referral_banner)
        val backgroundAnim = MyAnim(animLay, drawable[count])
        backgroundAnim.duration = 3000
        animLay.startAnimation(backgroundAnim)
        backgroundAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                if (count == drawable.size - 1) {
                    count = 0
                } else {
                    count++
                }
                backgroundAnim.setBackground(drawable[count])
                animLay.startAnimation(backgroundAnim)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
    }

    private fun modifyShareLink(url: String): String {
        try {
            Log.d("modifyShareLink", "")
            return when {
                url.contains("\n\n") -> {
                    val sharedURL = url.split("\n\n")
                    val urlModify = sharedURL[1]
                    urlModify
                }
                url.contains("http") -> {
                    val sharedURL = url.split("http")
                    val urlModify = sharedURL[1]
                    "http$urlModify"
                }
                else -> {
                    url
                }
            }
        } catch (e: Exception) {
            Log.e("modifyShareLink", "exception" + e.message)
        }
        return url
    }

    private fun calReferralPercentAPI(checkingLink: String) {
        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        val callAdminPercentage = WebApiClient.webApi().adminPercentage()
        callAdminPercentage?.enqueue(object : Callback<AdminPercentage?> {
            override fun onResponse(
                call: Call<AdminPercentage?>,
                response: Response<AdminPercentage?>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Log.d("calReferralPercentAPI", response.body().toString())
                        referPercentage = response.body()?.data?.rewardpercent?.customerPercent ?: 0
                        callMerchantKeyAPI(checkingLink)
                    }
                }
            }

            override fun onFailure(call: Call<AdminPercentage?>, t: Throwable) {
            }
        })
    }

    private fun callMerchantKeyAPI(shareUrl: String) {
        mActivity.runOnUiThread {
            progressDialog.show(mActivity)
        }

        val apiInterface = WebApiClient.webApi().comparisonResult("android")
        apiInterface?.enqueue(object : Callback<ComparisonResult?> {
            override fun onFailure(call: Call<ComparisonResult?>, t: Throwable) {
                progressDialog.dialog.dismiss()
                Log.d("callMerchantKeyAPI", t.toString())
            }

            override fun onResponse(
                call: Call<ComparisonResult?>,
                response: Response<ComparisonResult?>
            ) {
                progressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    Log.d("callMerchantKeyAPI", response.body().toString())
                    if (response.body() != null) {
                        mComparisonResult.clear()

                        val countryList = if (mActivity.userDetails?.countryCode.equals("+91")) {
                            response.body()?.data?.filter {
                                it.country!!.contains("India", true) || shareUrl.contains(
                                    it.keyDetails?.detail3?.title
                                        ?: "", true
                                )
                            }
                        } else {
                            response.body()?.data?.filter {
                                it.country!!.contains("usa", true) || shareUrl.contains(
                                    it.keyDetails?.detail3?.title
                                        ?: "", true
                                )
                            }
                        }
                        countryList?.let { mComparisonResult.addAll(it) }

                        if (mComparisonResult.isNotEmpty()) {
                            @Suppress("DEPRECATION")
                            MyAsyncTask(
                                mActivity,
                                progressDialog,
                                mComparisonResult,
                                rvComparison,
                                referPercentage,
                                mActivity.userDetails
                            ).execute(shareUrl)
                        }
                    }
                }
            }
        })
    }

    @Suppress("DEPRECATION")
    class MyAsyncTask(
        private val activity: Activity,
        private var progressDialog: CustomProgressDialog,
        private var mComparisonResult: ArrayList<Datum>,
        private var rvComparison: RecyclerView,
        var referPercentage: Int,
        var user: User?
    ) : AsyncTask<String, String, ArrayList<ProductCompareList>>() {

        private var productName = ""
        private var productPrice = ""
        private var productImages = ""
        var mProductCompareList: ArrayList<ProductCompareList> = ArrayList()

        override fun doInBackground(vararg params: String?): ArrayList<ProductCompareList> {
            val shareUrl = params[0]
            Log.d("PAram1", params[0].toString())
            try {
                val isCheckUrl = mComparisonResult.filter {
                    shareUrl?.contains(it.keyDetails?.detail3?.title ?: "", true) ?: false
                }
                if (isCheckUrl.isEmpty()) {
                    callProductListAPI(shareUrl ?: "", mComparisonResult)
                } else {
                    val amazonShare = Jsoup.connect(shareUrl)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                        .get()
                    println("ShareUrl::" + shareUrl)
                    for (i in mComparisonResult.indices) {
                        if (shareUrl!!.contains(
                                mComparisonResult[i].name
                                    ?: "", ignoreCase = true
                            )
                        ) {
                            getProductTitle(
                                amazonShare,
                                mComparisonResult,
                                shareUrl,
                                referPercentage
                            )
                        }
                    }
                }

            } catch (e: Exception) {
            }
            return mProductCompareList
        }

        override fun onPreExecute() {
            super.onPreExecute()
            progressDialog.show(activity)
        }

        override fun onPostExecute(result: ArrayList<ProductCompareList>?) {
            super.onPostExecute(result)
            if (progressDialog.dialog.isShowing) {
                progressDialog.dialog.dismiss()
            }
            productAdapter(result!!)
        }

        private fun callProductListAPI(url: String, mComparisonResult: ArrayList<Datum>) {
            activity?.runOnUiThread {
                progressDialog.show(activity!!)
            }
            val apiInterface = WebServiceVigiLink.client?.create(WebApi::class.java)
            val callProductDetail = apiInterface?.vigiLinkProductDetails(url)

            callProductDetail?.enqueue(object : Callback<VigilinkProductResponse?> {
                override fun onFailure(call: Call<VigilinkProductResponse?>, t: Throwable) {
                    progressDialog.dialog.dismiss()
                    Log.d("callProductListAPI", t.toString())
                }

                override fun onResponse(
                    call: Call<VigilinkProductResponse?>,
                    response: Response<VigilinkProductResponse?>
                ) {
                    Log.d("callProductListAPI", response.body().toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            mProductCompareList.clear()
                            val mProductCompare = ProductCompareList()
                            mProductCompare.mImage = response.body()?.imageUrl ?: ""
                            mProductCompare.mPrice = response.body()?.price ?: ""
                            mProductCompare.mProductName = response.body()?.title ?: ""
                            mProductCompare.mURL = response.body()?.url ?: ""
                            mProductCompare.mMerchantName = response.body()?.merchantName ?: ""

                            mProductCompareList.add(mProductCompare)

                            response.body()?.price?.let { Log.d("Price", it) }
                            response.body()?.merchantName?.let { Log.d("merchantName", it) }
                            response.body()?.brandName?.let { Log.d("brandName", it) }
                            val amazonShare = Jsoup.connect(url)
                                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                                .get()
                            println("ShareUrl::" + url)
                            for (i in mComparisonResult.indices) {
                     /*           if (url.contains(
                                        mComparisonResult[i].name
                                            ?: "", ignoreCase = true
                                    )
                                ) {*/
                                    getProductTitleOther(
                                        amazonShare,mProductCompare,
                                        mComparisonResult,
                                        url,
                                        referPercentage
                                    )
//                                }
                            }
//                            productVigiLinkAdapter(mProductCompareList)

//
                        }
                    }
                }
            })
        }

        private fun getProductTitleOther(
            amazonShare: Document,mProductCompareList:ProductCompareList,
            mComparisonResult: ArrayList<Datum>,
            shareUrl: String,
            referPercentage: Int
        ) {

       /*     val sharedWebSiteUrl =
                mComparisonResult.filter { shareUrl.contains(it.name.toString(), true) }

            for (i in sharedWebSiteUrl.indices) {
                getDirectLinkValues(
                    amazonShare, sharedWebSiteUrl[i].keyDetails, sharedWebSiteUrl[i].name,
                    sharedWebSiteUrl[i].percentage, referPercentage,
                    sharedWebSiteUrl[i].image, shareUrl
                )
            }

            val notSharedWebSiteUrl =
                mComparisonResult.filterNot { shareUrl.contains(it.name.toString(), true) }
*/
            val notSharedWebSiteUrl ="https://www.flipkart.com/search?q="
            var  getHrefVal: Document? =null
            for (j in notSharedWebSiteUrl.indices) {

                    val product = "https://www.flipkart.com/search?q=".plus( mProductCompareList.mProductName)
                  getHrefVal = Jsoup.connect(product) .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36").get()

                }
            var title="\"https://www.flipkart.com/"
               var getRedirect =
                    getHrefVal?.getElementsByClass("https://www.flipkart.com/")

//            var getRedirect="_1fQZEK"


                val link = getRedirect?.select("a")
                val redirectURL = link?.attr("href")
                println("redirectURL " + redirectURL)

                val hrefUrl =
                    title.plus(redirectURL)

            Log.d("Link Ref", hrefUrl)
                }

//                println("hrefUrl " + hrefUrl)

      /*          val getLink = Jsoup.connect(hrefUrl)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
                    .get()
                getDirectLinkValues(
                    getLink,
                    notSharedWebSiteUrl[j].keyDetails,
                    notSharedWebSiteUrl[j].name,
                    notSharedWebSiteUrl[j].percentage,
                    referPercentage,
                    notSharedWebSiteUrl[j].image,
                    hrefUrl
                )*/
        private fun getProductTitle(
            amazonShare: Document,
            mComparisonResult: ArrayList<Datum>,
            shareUrl: String,
            referPercentage: Int
        ) {

            val sharedWebSiteUrl =
                mComparisonResult.filter { shareUrl.contains(it.name.toString(), true) }

            for (i in sharedWebSiteUrl.indices) {
                getDirectLinkValues(
                    amazonShare, sharedWebSiteUrl[i].keyDetails, sharedWebSiteUrl[i].name,
                    sharedWebSiteUrl[i].percentage, referPercentage,
                    sharedWebSiteUrl[i].image, shareUrl
                )
            }

            val notSharedWebSiteUrl =
                mComparisonResult.filterNot { shareUrl.contains(it.name.toString(), true) }

            for (j in notSharedWebSiteUrl.indices) {
                var getHrefVal: Document? = null
                try {
                    getHrefVal = if (notSharedWebSiteUrl[j].name.equals("newegg")) {
                        val name = getFirstNStrings(productName.replace("&nbsp;", ""), 5)
                        val product = notSharedWebSiteUrl[j].url.plus(name)
                        Jsoup.connect(product)
                            .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
                            .timeout(5000).get()
                    } else {
                        val name = getFirstNStrings(productName.replace("&nbsp;", ""), 5)
                        val product = notSharedWebSiteUrl[j].url.plus(name)
                        Jsoup.connect(product).get()
                    }
                } catch (e: IOException) {
                    Log.e("targetException", "targetException" + e.message)
                }

                var getRedirect =
                    getHrefVal?.getElementsByClass(notSharedWebSiteUrl[j].keyDetails?.key?.title)

                if (getRedirect?.size == 0) {
                    getRedirect =
                        getHrefVal?.getElementsByClass(notSharedWebSiteUrl[j].keyDetails?.key2?.title)
                }

                val link = getRedirect?.select("a")
                val redirectURL = link?.attr("href")
                println("redirectURL " + redirectURL)

                val hrefUrl = if (notSharedWebSiteUrl[j].name.equals(
                        "ebay",
                        true
                    ) || notSharedWebSiteUrl[j].name.equals("newegg", true)
                ) {
                    redirectURL
                } else {
                    notSharedWebSiteUrl[j].keyDetails?.key?.image.plus(redirectURL)
                }
                println("hrefUrl " + hrefUrl)

                val getLink = Jsoup.connect(hrefUrl)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
                    .get()
                getDirectLinkValues(
                    getLink,
                    notSharedWebSiteUrl[j].keyDetails,
                    notSharedWebSiteUrl[j].name,
                    notSharedWebSiteUrl[j].percentage,
                    referPercentage,
                    notSharedWebSiteUrl[j].image,
                    hrefUrl
                )
            }
        }

        /*private fun otherSiteUrl(url: String) {
            val notSharedWebSiteUrl =
                mComparisonResult.filterNot { url.contains(it.name.toString(), true) }

            for (j in notSharedWebSiteUrl.indices) {
                var getHrefVal: Document? = null
                try {
                    getHrefVal = if (notSharedWebSiteUrl[j].name.equals("newegg")) {
                        val name = getFirstNStrings(productName.replace("&nbsp;", ""), 5)
                        val product = notSharedWebSiteUrl[j].url.plus(name)
                        Jsoup.connect(product)
                            .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
                            .timeout(5000).get()
                    } else {
                        val name = getFirstNStrings(productName.replace("&nbsp;", ""), 5)
                        val product = notSharedWebSiteUrl[j].url.plus(name)
                        Jsoup.connect(product).get()
                    }
                } catch (e: IOException) {
                    Log.e("targetException", "targetException" + e.message)
                }

                var getRedirect =
                    getHrefVal?.getElementsByClass(notSharedWebSiteUrl[j].keyDetails?.key?.title)

                if (getRedirect?.size == 0) {
                    getRedirect =
                        getHrefVal?.getElementsByClass(notSharedWebSiteUrl[j].keyDetails?.key2?.title)
                }

                val link = getRedirect?.select("a")
                val redirectURL = link?.attr("href")
                println("redirectURL " + redirectURL)

                val hrefUrl = if (notSharedWebSiteUrl[j].name.equals(
                        "ebay",
                        true
                    ) || notSharedWebSiteUrl[j].name.equals("newegg", true)
                ) {
                    redirectURL
                } else {
                    notSharedWebSiteUrl[j].keyDetails?.key?.image.plus(redirectURL)
                }
                println("hrefUrl " + hrefUrl)

                val getLink = Jsoup.connect(hrefUrl)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
                    .get()
                getDirectLinkValues(
                    getLink,
                    notSharedWebSiteUrl[j].keyDetails,
                    notSharedWebSiteUrl[j].name,
                    notSharedWebSiteUrl[j].percentage,
                    referPercentage,
                    notSharedWebSiteUrl[j].image,
                    hrefUrl
                )
            }
        }
*/
        private fun getFirstNStrings(str: String, n: Int): String {
            return try {
                val sArr = str.split(" ").toTypedArray()
                var firstStrs = ""
                for (i in 0 until n) firstStrs += sArr[i] + " "
                firstStrs.trim { it <= ' ' }
            } catch (e: Exception) {
                str
            }
        }

        private fun getDirectLinkValues(
            amazonShare: Document,
            keyDetail: KeyDetails?,
            webSiteKey: String?,
            percentage: Int?,
            referPercentage: Int,
            logo: String?,
            shareURL: String?
        ) {

            if (amazonShare.getElementById(keyDetail?.detail?.title) != null) {
                amazonShare.getElementById(keyDetail?.detail?.title).childNodes().forEach {
                    productName = it.toString()
                    println("productName " + productName)
                }
            } else if (amazonShare.getElementsByClass(keyDetail?.detail?.title) != null) {
                amazonShare.getElementsByClass(keyDetail?.detail?.title).forEach {
                    productName = it.text()
                    println("productName::: " + productName)
                }
            }

            if (productName.isEmpty()) {
                amazonShare.getElementsByClass(keyDetail?.detail2?.title).forEach {
                    productName = it.text()
                    println("productName::: " + productName)
                }
            }

            if (amazonShare.getElementById(keyDetail?.detail?.price) != null) {
                amazonShare.getElementById(keyDetail?.detail?.price).childNodes().forEach {
                    productPrice = it.toString().replace("₹&nbsp;", "")
                    println("productPrice " + productPrice)
                }
            } else if (amazonShare.getElementsByClass(keyDetail?.detail?.price) != null) {
                amazonShare.getElementsByClass(keyDetail?.detail?.price).forEach {
                    productPrice = it.text()
                    println("productPrice " + productPrice)
                }
            }

            if (productPrice.isEmpty()) {
                if (amazonShare.getElementsByClass(keyDetail?.detail2?.price) != null) {
                    amazonShare.getElementsByClass(keyDetail?.detail2?.price).forEach {
                        productPrice = it.text()
                    }
                } else if (amazonShare.getElementsByClass(keyDetail?.detail3?.price) != null) {
                    amazonShare.getElementsByClass(keyDetail?.detail3?.price).forEach {
                        productPrice = it.text()
                    }
                }
            }

            if (amazonShare.getElementById(keyDetail?.detail?.image) != null) {
                val productImage = amazonShare.getElementById(keyDetail?.detail?.image)
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    if (webSiteKey.equals("ebay", true)) {
                        val srcValue = imageElement.attr("src")
                        productImages = srcValue
                    } else {
                        val srcValue = imageElement.attr("data-a-dynamic-image")
                        val imageValue = srcValue.split(".jpg")
                        val url1 = imageValue[0]
                        val finalURL = url1.trim().trim().replace("{", "") + ".jpg"
                        productImages = finalURL.replace("\"", "")
                        println("productImages " + productImages)
                    }
                }
            } else if (amazonShare.getElementsByClass(keyDetail?.detail?.image) != null) {
                val productImage = amazonShare.getElementsByClass(keyDetail?.detail?.image)
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    when {
                        webSiteKey.equals("reliancedigital", true) -> {
                            val srcValue = imageElement.attr("data-srcset")
                            productImages = keyDetail?.key?.image.plus(srcValue)
                            println("productImages " + productImages)
                        }
                        webSiteKey.equals("walmart", true) -> {
                            val srcValue = imageElement.attr("src")
                            productImages = "https:".plus(srcValue)
                            println("productImages " + productImages)
                        }
                        else -> {
                            val srcValue = imageElement.attr("src")
                            productImages = srcValue
                            println("productImages " + productImages)
                        }
                    }
                }
            }

            if (productImages.isEmpty()) {
                val productImage = amazonShare.getElementsByClass(keyDetail?.detail2?.image)
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    productImages = imageElement.attr("src")
                }
            }

            val mProductCompare = ProductCompareList()
            mProductCompare.mImage = productImages
            mProductCompare.mPrice = productPrice
            mProductCompare.mProductName = productName
            mProductCompare.mMerchantName = webSiteKey ?: ""
            mProductCompare.logo = logo ?: ""
            mProductCompare.mURL = shareURL ?: ""

            if (productPrice.isNotEmpty()) {
                val priceCalculation = percentageCalculator(
                    productPrice.replace("₹", "").replace("/month", "").replace("$", "")
                        .replace("-", "").replace("INR", "").replace(",", "").replace("\u20B9", "")
                        .replace("Rs.", "").replace("\"", "").trim().toDouble(), percentage
                        ?: 0
                )
                val totalPrice = percentageCalculator(priceCalculation, referPercentage)
                mProductCompare.mEarn = String.format("%.1f", totalPrice)
            }
            mProductCompareList.add(mProductCompare)

            println("mProductCompareList" + mProductCompareList.size)
        }

        private fun percentageCalculator(price: Double, percentage: Int): Double {
            val amount: Double = price
            return amount / 100.0f * percentage
        }

        @SuppressLint("SimpleDateFormat")
        private fun productAdapter(productCompareList: ArrayList<ProductCompareList>) {
            val linearLayoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            rvComparison.addItemDecoration(
                VerticalDividerDecoration(
                    activity.convertDpToPixel(12F).toInt(), true
                )
            )
            rvComparison.layoutManager = linearLayoutManager
            val mProductComparisonAdapter =
                ProductComparisonAdapter(productCompareList, activity) { itemList, i, view ->
                    when (view.id) {
                        R.id.cvProductRoot -> {
                            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                            val currentDate = sdf.format(Date())
                            if (SMarket.appDB.userDao().getData()?.countryCode != "+91") {
                                sharePopUpOutside(
                                    productTittle = itemList[i].mProductName,
                                    price = itemList[i].mPrice,
                                    productUrl = itemList[i].mURL,
                                    clicked = currentDate,
                                    userID = user?.id.toString(),
                                    earn = itemList[i].mEarn,
                                    merchantName = itemList[i].mMerchantName,
                                    imageURL = itemList[i].mImage
                                )
                            } else {
                                sharePopUp(
                                    productTittle = itemList[i].mProductName,
                                    price = itemList[i].mPrice,
                                    productUrl = itemList[i].mURL,
                                    clicked = currentDate,
                                    userID = user?.id.toString(),
                                    earn = itemList[i].mEarn,
                                    merchantName = itemList[i].mMerchantName,
                                    imageURL = itemList[i].mImage
                                )
                            }
                        }
                    }
                }
            rvComparison.adapter = mProductComparisonAdapter

        }

        @SuppressLint("SimpleDateFormat")
        private fun productVigiLinkAdapter(productCompareList: ArrayList<ProductCompareList>) {
            val linearLayoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            rvComparison.addItemDecoration(
                VerticalDividerDecoration(
                    activity.convertDpToPixel(12F).toInt(), true
                )
            )
            rvComparison.layoutManager = linearLayoutManager
            val mProductComparisonAdapter =
                ProductComparisonAdapter(productCompareList, activity) { itemList, i, view ->
                    when (view.id) {
                        R.id.cvProductRoot -> {
                            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                            val currentDate = sdf.format(Date())
                            if (SMarket.appDB.userDao().getData()?.countryCode != "+91") {
                                sharePopUpOutside(
                                    productTittle = itemList[i].mProductName,
                                    price = itemList[i].mPrice,
                                    productUrl = itemList[i].mURL,
                                    clicked = currentDate,
                                    userID = user?.id.toString(),
                                    earn = itemList[i].mEarn,
                                    merchantName = itemList[i].mMerchantName,
                                    imageURL = itemList[i].mImage
                                )
                            } else {
                                sharePopUp(
                                    productTittle = itemList[i].mProductName,
                                    price = itemList[i].mPrice,
                                    productUrl = itemList[i].mURL,
                                    clicked = currentDate,
                                    userID = user?.id.toString(),
                                    earn = itemList[i].mEarn,
                                    merchantName = itemList[i].mMerchantName,
                                    imageURL = itemList[i].mImage
                                )
                            }
                        }
                    }
                }
            rvComparison.adapter = mProductComparisonAdapter

        }

        private fun sharePopUpOutside(
            productTittle: String, price: String, productUrl: String,
            clicked: String, userID: String, earn: String, merchantName: String, imageURL: String
        ) {

            activity.runOnUiThread {
                val alertDialog = AlertDialog.Builder(activity).create()
                val dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_share, null)
                alertDialog.setView(dialogView)
                alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                val messanger = dialogView.findViewById<Button>(R.id.btnShare)
                val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)

                val adobe = ResourcesCompat.getFont(activity, R.font.adobe_devanagari_bold)!!
                val adobeFont = CustomTypefaceSpan("", adobe)

                val typeface = ResourcesCompat.getFont(activity, R.font.poppins_italic)!!
                val spannable = CustomTypefaceSpan("", typeface)

                val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
                val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
                val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

                val second = SpannableString("₹20\nBonus Cash")
                second.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                second.setSpan(spannable, 3, 14, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                second.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                second.setSpan(
                    ForegroundColorSpan(Color.YELLOW),
                    0,
                    3,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
                tvSecond.text = second

                val third = SpannableString("30%\nCash Rewards")
                third.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                third.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                third.setSpan(RelativeSizeSpan(2.2f), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                third.setSpan(RelativeSizeSpan(1.7f), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                third.setSpan(
                    ForegroundColorSpan(Color.YELLOW),
                    0,
                    3,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
                tvThird.text = third

                messanger.setOnClickListener {
                    val intent = Intent()
                    intent.action = Intent.ACTION_SEND
                    intent.type = "text/plain"
                    val shareMessage =
                        user?.referral_message?.replace("XXXXXX", user?.referral_code.toString())
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    activity.startActivity(Intent.createChooser(intent, "Share"))
                }

                tvFAQ.setOnClickListener {
                    alertDialog.dismiss()
                    //activity.addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
                }

                btnViewSite.setOnClickListener {
                    alertDialog.dismiss()
                    callUserActivityAPI(
                        productTittle,
                        price,
                        productUrl,
                        clicked,
                        userID,
                        earn,
                        merchantName,
                        imageURL
                    )
                }

                alertDialog.show()

            }
        }

        private fun sharePopUp(
            productTittle: String, price: String, productUrl: String,
            clicked: String, userID: String, earn: String, merchantName: String, imageURL: String
        ) {

            val alertDialog = AlertDialog.Builder(activity).create()
            val dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_share_new, null)
            alertDialog.setView(dialogView)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)

            val adobe = ResourcesCompat.getFont(activity, R.font.adobe_devanagari_bold)
            val adobeFont = adobe?.let { CustomTypefaceSpan("", it) }

            val typeface = ResourcesCompat.getFont(activity, R.font.poppins_italic)
            val spannable = typeface?.let { CustomTypefaceSpan("", it) }

            val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val first = SpannableString("₹50\nCash Rewards")
            first.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(
                ForegroundColorSpan(Color.YELLOW),
                0,
                3,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvFirst.text = first

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable, 3, 14, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(
                ForegroundColorSpan(Color.YELLOW),
                0,
                3,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(
                ForegroundColorSpan(Color.YELLOW),
                0,
                3,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage =
                    user?.referral_message?.replace("XXXXXX", user?.referral_code.toString())
                intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                activity.startActivity(Intent.createChooser(intent, "Share"))

            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                /*mActivity.addFragment(FAQsFragment.getInstance()
                        , true , animationType = AnimationType.RightInZoomOut)*/
            }

            btnViewSite.setOnClickListener {
                alertDialog.dismiss()
                callUserActivityAPI(
                    productTittle,
                    price,
                    productUrl,
                    clicked,
                    userID,
                    earn,
                    merchantName,
                    imageURL
                )
            }

            alertDialog.show()
        }

        private fun callUserActivityAPI(
            productName: String, price: String, productUrl: String,
            clicked: String, userID: String, earn: String, merchantName: String, imageURL: String
        ) {
            activity.runOnUiThread {
                ProgressDialogUtil.showProgressDialog(activity)
            }

            if (!NetworkUtil.isNetworkAvailable()) {
                showToast(ErrorUtil.NO_INTERNET)
                ProgressDialogUtil.hideProgressDialog()
                return
            }

            val hashMap = ApiParam.getHashMap()

            if (user != null) {
                with(user!!) {
                    hashMap[ApiParam.PHONE_NUMBER] = mobile
                    hashMap[ApiParam.MERCHANT_NAME] = merchantName
                }
            }
            hashMap[ApiParam.PRODUCT_NAME] = productName
            hashMap[ApiParam.PRODUCT_PRICE] = price
            hashMap[ApiParam.REWARD_VALUE] = earn
            hashMap[ApiParam.STATUS] = "1"
            hashMap[ApiParam.DEVICE_TYPE] = "Android"
            hashMap[ApiParam.PRODUCT_URL] = productUrl
            hashMap[ApiParam.PRODUCT_IMAGE_URL] = imageURL
            hashMap[ApiParam.CLICKED_AT] = clicked
            hashMap[ApiParam.USER_ID] = userID

            val callUserActivity = WebApiClient.webApi().userActivity(hashMap)

            callUserActivity?.enqueue(object : Callback<UserActivityResponse?> {
                override fun onFailure(call: Call<UserActivityResponse?>, t: Throwable) {
                    ProgressDialogUtil.hideProgressDialog()
                    Log.d("callUserActivityAPI", t.toString())
                }

                override fun onResponse(
                    call: Call<UserActivityResponse?>,
                    response: Response<UserActivityResponse?>
                ) {
                    ProgressDialogUtil.hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            Log.d("callUserActivityAPI", response.body().toString())
                            if (!merchantName.contains("eBay", true)) {
                                if (merchantName == "Amazon.in") {
                                    val browserIntent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("$productUrl&linkCode=ll1&tag=smarketindia-21")
                                    )
                                    activity.startActivity(browserIntent)
                                } else if (merchantName == "Amazon.com") {
                                    val browserIntent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("$productUrl/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarket0f-20")
                                    )
                                    activity.startActivity(browserIntent)
                                } else if (merchantName.contains("ajio.com") || merchantName.contains(
                                        "Pharmeasy (CPAT)"
                                    )
                                    || merchantName.contains("Nykaaman.com") || merchantName.contains(
                                        "paytmmall"
                                    )
                                    || merchantName.contains("tatacliq.com") || merchantName.contains(
                                        "AliExpress"
                                    )
                                    || merchantName.contains("Macy's") || merchantName.contains("Walmart")
                                ) {

                                    val browserIntent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://linksredirect.com/?cid=125043&source=linkkit&url=$productUrl")
                                    )
                                    activity.startActivity(browserIntent)
                                } else {
                                    Log.d("callUserActivityAPI", "Else VigiLink")
                                    val browserIntent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("http://redirect.viglink.com?u=$productUrl&key=f5e38af288c412320cf9ff35e2299c98")
                                    )
                                    activity.startActivity(browserIntent)
                                }
                            } else {
                                val browserIntent = Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://rover.ebay.com/rover/1/711-53200-19255-0/1?campid=5338693049&customid=" + user?.mobile + "!&toolid=20006&mpre=" + productUrl)
                                )
                                activity.startActivity(browserIntent)
                            }
                        } else {
                            Toast.makeText(activity, "No product found!", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(activity, "No product found!", Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.friends_fl -> {
                mActivity.addFragment(
                    FriendsOnSmarketFragment.getInstance(),
                    addToBackStack = true,
                    animationType = AnimationType.RightInZoomOut
                )
            }
            R.id.txt_contact -> {
                mActivity.sendMail(
                    mActivity.resources.getString(R.string.app_name),
                    emailId = "support@smarketdeals.com"
                )
            }
        }
    }


    companion object {
        fun getInstance(isFrom: String = ""): ProductComparisonFragment {
            val fragment = ProductComparisonFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }
}

