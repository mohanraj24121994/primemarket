package com.smarket.main.customer

import android.graphics.drawable.Drawable
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetBehavior
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import com.smarket.utils.*
import com.smarket.R
import com.smarket.location.LocationUpdates
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.topBar.SearchFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.model.RedemptionHistory
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.RedemptionHistoryResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_redemeption_history.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header_filter.*
import kotlinx.android.synthetic.main.row_redemption_history.view.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class RedemptionHistoryFragment : BaseMainFragment(), View.OnClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(isFrom: String = "", merchantId: Long = 0L): RedemptionHistoryFragment {
            val fragment = RedemptionHistoryFragment()
            val bundle = Bundle()
            bundle.putLong(MERCHANT_ID, merchantId)
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    var sheetBehavior: BottomSheetBehavior<*>? = null

    lateinit var checkedTextViewArray: Array<CheckedTextView>

    lateinit var giftCardDrawable: Drawable
    lateinit var inStoreDrawable: Drawable
    lateinit var storeCreditDrawable: Drawable

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var redemptionHistoryAdapter: RedemptionHistoryAdapter

    var redemptionHistory = ArrayList<RedemptionHistory>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    var selected_filter_id = -1

    var mLatitude = ""
    var mLongitude = ""

    private var disposable: Disposable? = null
    var observable: Observable<Response<RedemptionHistoryResponse>>? = null

    var isFrom = ""
    var merchantId = 0L

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_redemeption_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        sheetBehavior = BottomSheetBehavior.from(ll_filter)
        sheetBehavior?.peekHeight = 0

        checkedTextViewArray = arrayOf(ctv_referral,ctv_reward,ctv_bonus,ctv_rate_review)

        giftCardDrawable = resources.getDrawable(R.drawable.gift_card)
        inStoreDrawable = resources.getDrawable(R.drawable.in_store)
        storeCreditDrawable = resources.getDrawable(R.drawable.store_credit)

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_redemption_history.layoutManager = linearLayoutManager
        redemptionHistoryAdapter = RedemptionHistoryAdapter()
        rv_redemption_history.adapter = redemptionHistoryAdapter

        iv_apply.setOnClickListener(this)
        iv_cancel.setOnClickListener(this)
        iv_reset.setOnClickListener(this)
        ctv_referral.setOnClickListener(this)
        ctv_reward.setOnClickListener(this)
        ctv_bonus.setOnClickListener(this)
        ctv_rate_review.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
            if (bundle.containsKey(MERCHANT_ID)) {
                merchantId = bundle.getLong(MERCHANT_ID)
            }
        }
    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
            iv_search.visibility = View.VISIBLE
            iv_search.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
            iv_search.visibility = View.GONE
        }
        iv_filter.visibility = View.VISIBLE
        iv_filter.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.redemption_history)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        rv_redemption_history.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == redemptionHistoryAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callRedemptionHistoryAPI()
                }
            }
        })
        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_filter -> openFilter()
            R.id.iv_search -> mActivity.addFragment(
                SearchFragment.getInstance()
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.iv_apply -> {
                closeFilter()
                selected_filter_id = when {
                    ctv_referral.isChecked -> ctv_referral.id
                    ctv_reward.isChecked -> ctv_reward.id
                    ctv_bonus.isChecked -> ctv_bonus.id
                    ctv_rate_review.isChecked ->ctv_rate_review.id
                    else -> -1
                }
                page = 1
                callRedemptionHistoryAPI()
            }
            R.id.iv_cancel -> closeFilter()
            R.id.ctv_referral,
            R.id.ctv_reward,
            R.id.ctv_bonus,
            R.id.ctv_rate_review-> setUpFilterSelector(v.id)
            R.id.iv_reset -> unSelectFilterSelector()
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    private fun openFilter() {
        sheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        setUpFilterSelector(selected_filter_id)
    }

    private fun closeFilter() {
        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun setUpFilterSelector(selectedViewId: Int) {
        for (i in checkedTextViewArray.indices) {
            if (checkedTextViewArray[i].id == selectedViewId) {
                if (!checkedTextViewArray[i].isChecked) {
                    checkedTextViewArray[i].isChecked = true
                } else {
                    break
                }
            } else {
                checkedTextViewArray[i].isChecked = false
            }
        }
    }

    private fun unSelectFilterSelector() {
        for (i in checkedTextViewArray.indices) {
            if (checkedTextViewArray[i].isChecked) {
                checkedTextViewArray[i].isChecked = false
            }
        }
    }

    inner class RedemptionHistoryAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_redemption_history, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (redemptionHistory[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return redemptionHistory.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(redemptionHistory[holder.adapterPosition]) {
                    holder.itemView.sdv_logo.loadFrescoImage(businessLogo)
                    if (distance.isRequiredField()) {
                        holder.itemView.tv_distance.text = "($distance ${mActivity.resources.getString(R.string.mi)})"
                    }
                    holder.itemView.tv_business_name.text = businessName
                    if (tagLine.isRequiredField()) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = tagLine
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }
                    holder.itemView.rating_bar.rating = averageRating
                    holder.itemView.tv_average_ratings.text = "$averageRating"
                    holder.itemView.tv_ratings.text = "($noOfRating)"
                    holder.itemView.tv_redeemed_date.text = "${mActivity.resources.getString(R.string.redeemed_on)} ${getChangedDateFormat(redeemedDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                    when (subOfferType) {
                        ApiParam.SUB_OFFER_TYPE_REFERRAL -> {
                            holder.itemView.tv_main_offer.text = mActivity.resources.getString(R.string.referral)
                        }
                        ApiParam.SUB_OFFER_TYPE_BONUS -> {
                            holder.itemView.tv_main_offer.text = mActivity.resources.getString(R.string.bonus)
                        }
                        ApiParam.SUB_OFFER_TYPE_REWARD -> {
                            holder.itemView.tv_main_offer.text = mActivity.resources.getString(R.string.reward)
                        }
                        ApiParam.SUB_OFFER_TYPE_RATE -> {
                            holder.itemView.tv_main_offer.text = mActivity.resources.getString(R.string.rate_and_review)
                        }
                    }
                    when (subOfferCategory) {
                        ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                            holder.itemView.iv_offer_type.setImageDrawable(inStoreDrawable)
                            holder.itemView.tv_offer_type.text = title
                        }
                        ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                            holder.itemView.iv_offer_type.setImageDrawable(storeCreditDrawable)
                            holder.itemView.tv_offer_type.text = mActivity.resources.getString(R.string.store_credit_of) + " " + "$$amount"
                        }
                        ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                            holder.itemView.iv_offer_type.setImageDrawable(giftCardDrawable)
                            holder.itemView.tv_offer_type.text = mActivity.resources.getString(R.string.gift_card_worth) + " " + "$$amount"
                        }
                    }
                    holder.itemView.setOnClickListener {

                        callOfferDetailAPI(id)

                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            if (isFrom == IS_FROM_DRAWER) {
                mActivity.setDrawerEnable(true)
            }
        }
        super.onHiddenChanged(hidden)
    }

    private fun callRedemptionHistoryAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (redemptionHistory.find { it.id == 0L } == null) {
                redemptionHistory.add(RedemptionHistory())
                redemptionHistoryAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude

        if (selected_filter_id != -1) {
            hashMap[ApiParam.FILTER_BY] = when (selected_filter_id) {
                R.id.ctv_referral -> ApiParam.SUB_OFFER_TYPE_REFERRAL
                R.id.ctv_bonus -> ApiParam.SUB_OFFER_TYPE_BONUS
                R.id.ctv_rate_review -> ApiParam.SUB_OFFER_TYPE_RATE
                R.id.ctv_reward -> ApiParam.SUB_OFFER_TYPE_REWARD
                else -> ""
            }
        }

        if (isFrom != IS_FROM_DRAWER && merchantId != 0L) {
            hashMap[ApiParam.MERCHANT_ID] = "$merchantId"
        }

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().redemptionHistoryAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            redemptionHistory.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            redemptionHistory.addAll(data)
                                                        }

                                                        page++

                                                        this@RedemptionHistoryFragment.lastPage = lastPage
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (redemptionHistory.isNotEmpty()) {
                                    redemptionHistory.removeAll(redemptionHistory.filter { it.id == 0L })
                                }
                            }

                            redemptionHistoryAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (redemptionHistory.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (redemptionHistory.isNotEmpty()) {
                                    redemptionHistory.removeAll(redemptionHistory.filter { it.id == 0L })
                                    redemptionHistoryAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (redemptionHistory.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callOfferDetailAPI(id: Long) {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = "$id"

        disposable = WebApiClient.webApi().offerDetailAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        mActivity.addFragment(OfferQRCodeFragment.getInstance(offerDetails = data,
                                                                merchantMessage = "")
                                                                , true, animationType = AnimationType.RightInZoomOut)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )

    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        locationUpdates = null
        super.onDestroy()
    }

    private var locationUpdates: LocationUpdates? = LocationUpdates { isLatest, latLng ->
        if (isAdded && !isHidden) {

            if (swipe_refresh.isRefreshing) {
                swipe_refresh.isRefreshing = false
            }

            if (latLng != null) {

                mLatitude = latLng.latitude.toString()
                mLongitude = latLng.longitude.toString()

                if (isLatest) {
                    callRedemptionHistoryAPI()
                } else {
                    mActivity.showDialogWithAction(mActivity.resources.getString(R.string.location_not_found_ask_for_next_action),
                            positiveButtonLabel = mActivity.resources.getString(R.string.Continue), showNegativeButton = true) {
                        callRedemptionHistoryAPI()
                    }
                }

            } else {
                mActivity.showDialog(mActivity.resources.getString(R.string.location_not_found))
            }
        }

    }
}