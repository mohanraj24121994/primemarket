package com.smarket.main.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.addFragment
import com.smarket.utils.rateApp
import com.smarket.utils.sendMail
import com.smarket.utils.share
import com.smarket.BuildConfig
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.CMSFragment
import com.smarket.main.ChangePasswordFragment
import com.smarket.main.FAQsFragment
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ApiUtil.callNotificationOnOffAPI
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.header.*
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class SettingsFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): SettingsFragment {
            val fragment = SettingsFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        sw_push_notification.typeface = tv_change_password.typeface
        tv_version.text = "${mActivity.resources.getString(R.string.version)} ${BuildConfig.VERSION_NAME}"

        tv_change_password.setOnClickListener(this)
        tv_how_to_use_this_app.setOnClickListener(this)
        tv_terms_and_conditions.setOnClickListener(this)
        tv_contact_us.setOnClickListener(this)
        tv_privacy_policy.setOnClickListener(this)
        tv_about_us.setOnClickListener(this)
        tv_share_an_app.setOnClickListener(this)
        tv_rate_app.setOnClickListener(this)
        tv_faqs.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.settings)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        addCheckedStateListener()
    }

    private fun addCheckedStateListener() {
        sw_push_notification.setOnCheckedChangeListener { buttonView, isChecked ->

            callNotificationOnOffAPI(mActivity,if (isChecked) {
                ApiParam.TRUE
            } else {
                ApiParam.FALSE
            })
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_change_password -> mActivity.addFragment(ChangePasswordFragment.getInstance()
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.tv_how_to_use_this_app -> mActivity.addFragment(CMSFragment.getInstance(CMSFragment.HOW_TO_USE)
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.tv_terms_and_conditions -> mActivity.addFragment(CMSFragment.getInstance(CMSFragment.TERMS_CONDITIONS)
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.tv_contact_us -> {
                mActivity.sendMail(mActivity.resources.getString(R.string.app_name), emailId = "support@smarketdeals.com")
            }
            R.id.tv_privacy_policy -> mActivity.addFragment(CMSFragment.getInstance(CMSFragment.PRIVACY_POLICY)
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.tv_about_us -> mActivity.addFragment(CMSFragment.getInstance(CMSFragment.ABOUT_US)
                    , true, animationType = AnimationType.RightInZoomOut)
            R.id.tv_share_an_app -> mActivity.share(mActivity, "https://play.google.com/store/apps/details?id=${mActivity.packageName}")
            R.id.tv_rate_app -> mActivity.rateApp()
            R.id.tv_faqs -> mActivity.addFragment(FAQsFragment.getInstance()
                    , true, animationType = AnimationType.RightInZoomOut)
            else -> {
            }
        }
    }
}