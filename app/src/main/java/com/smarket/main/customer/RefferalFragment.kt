package com.smarket.main.customer

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Base64
import android.util.Base64.DEFAULT
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.facebook.drawee.view.SimpleDraweeView
import com.skyhope.showmoretextview.ShowMoreTextView
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.main.BaseMainFragment
import com.smarket.main.FAQsFragment
import com.smarket.utils.*
import com.smarket.webservices.*
import com.smarket.webservices.response.ProductResponse
import com.smarket.webservices.response.UserActivityResponse
import com.smarket.webservices.response.adminpercentage.AdminPercentage
import com.smarket.webservices.response.refferedlist.ReferredList
import com.tap.app.utils.CustomTypefaceSpan
import kotlinx.android.synthetic.main.fragment_refferal.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.AnimationType
import java.text.SimpleDateFormat
import java.util.*


class RefferalFragment : BaseMainFragment() , View.OnClickListener , GetProductDetails , GeteBayProductDetails , GetProductDetailsWalmart ,
        GetProductFlipkart , GetProductNewEgg , GetProductBandH , GetProductBestBuy {

    private var productTittle: String? = null
    private var merchantName: String? = null
    private var price: String? = null
    private var baseURL: String? = null
    private var userID: String? = null
    private var isCheckUrl: Boolean = false
    private var isCheckPopUp: Boolean = true
    var userDetails: User? = null
    var getProductDetails: GetProductDetails? = null
    var getProductWalmart: GetProductDetailsWalmart? = null
    var geteBayProductDetails: GeteBayProductDetails? = null
    var getFlipkartProductDetails: GetProductFlipkart? = null
    var getNewEggProductDetails: GetProductNewEgg? = null
    var getProductBandH: GetProductBandH? = null
    var getProductBestBuy: GetProductBestBuy? = null
    private val progressDialog = CustomProgressDialog()
    var count: Int = 0
    private var backgroundAnim: MyAnim? = null

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        userID = SMarket.appDB.userDao().getData() !!.id.toString()

        getBundle()

        setHeader()

        val shareURL = PreferenceUtil.getStringPref("shareURL")
        baseURL = shareURL

        mActivity.updateUserDetailsObject()

        if (mActivity.userDetails?.countryCode != "+91") {
            rewards_tv.text = "$" + mActivity.userDetails?.refcash
        } else {
            rewards_tv.text = "₹" + mActivity.userDetails?.refcash
        }

        if (shareURL != "") {
            try {
                baseURL = when {
                    shareURL.contains("\n\n") -> {
                        val currentstring = shareURL.split("\n\n")
                        val url1 = currentstring[1]
                        url1
                    }
                    shareURL.contains("http") -> {
                        val currentstring = shareURL.split("http")
                        val url1 = currentstring[1]
                        "http$url1"
                    }
                    else -> {
                        shareURL
                    }
                }
            } catch (e: Exception) {
                Log.e("exception" , "exception" + e.message)
            }

            isCheckPopUp = false

            if (baseURL !!.contains("https://www.amazon.com/") || baseURL !!.contains("https://www.amazon.in/")) {
                mActivity.runOnUiThread {
                    earn_frame.visibility = View.VISIBLE
                }
                getProductDetails = this
                MyAsyncTask(activity !! , getProductDetails !! , progressDialog).execute(baseURL)
            } else if (baseURL?.contains("https://www.walmart.com/") !! || baseURL?.contains("http://www.walmart.com/") !!) {
                getProductWalmart = this
                getDetailFromWalmart(activity !! , getProductWalmart !! , progressDialog).execute(baseURL)
            } else if (baseURL !!.contains("https://www.ebay.com/")) {
                geteBayProductDetails = this
                getDetaileBay(activity !! , geteBayProductDetails !! , progressDialog , mActivity.userDetails?.countryCode !!).execute(baseURL)
            } else if (baseURL !!.contains("http://dl.flipkart.com/") || baseURL !!.contains("https://dl.flipkart.com/")) {
                getFlipkartProductDetails = this
                getDetailFlipKart(activity !! , getFlipkartProductDetails !! , progressDialog).execute(baseURL !!.replace("&cmpid=product.share.pp" , ""))
            } else if (baseURL !!.contains("https://www.newegg.com/")) {
                getNewEggProductDetails = this
                getDetailNewEgg(activity !! , getNewEggProductDetails !! , progressDialog).execute(baseURL !!)
            } else if (baseURL !!.contains("https://www.bhphotovideo.com/")) {
                getProductBandH = this
                getDetailBandH(activity !! , getProductBandH !! , progressDialog).execute(baseURL !!)
            } /*else if (baseURL !!.contains("https://www.bestbuy.com/")) {
               // getBestBuyDetails()
                getProductBestBuy = this
                getDetailBestBuy(activity !! , getProductBestBuy !! , progressDialog).execute(baseURL !!)
            } */ else {
                // mActivity.runOnUiThread { progressDialog.show(mActivity) }
                GlobalScope.launch(Dispatchers.IO) {
                    callProductListAPI(baseURL !!)
                }
            }
            /* if (baseURL !!.contains("http://dl.flipkart.com/") || baseURL !!.contains("https://dl.flipkart.com/")) {
                 baseURL = baseURL !!.replace("&cmpid=product.share.pp" , "")
             }

             if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                 getProductListAPI(baseURL !! , "others")
             } else {
                 getProductListAPI(baseURL !! , "india")
             }*/

            warning_txt.visibility = View.VISIBLE
            txt_contact.visibility = View.VISIBLE
            txt_contact.setOnClickListener {
                isCheckPopUp = true
                mActivity.sendMail(mActivity.resources.getString(R.string.app_name) , emailId = "support@smarketdeals.com")
            }
        }

        callReferralAPI()

        friends_fl.setOnClickListener(this)

        animateBackground()

    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_refferal , container , false)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        return v
    }

    override fun onResume() {
        super.onResume()

        val shareURL = PreferenceUtil.getStringPref("shareURL")
        baseURL = shareURL

        Log.e("baseURL" , "baseURL$baseURL")
        Log.e("baseURL" , "baseURL==>" + "onResume")

    }

    override fun onDestroy() {
        super.onDestroy()

        Log.e("baseURL" , "baseURL==>" + "onDestroy")
    }

    override fun onStop() {
        super.onStop()

        /* if (! isCheckPopUp) {
             mActivity.finish()
         }*/
    }

    override fun onDetach() {
        super.onDetach()
        Log.e("baseURL" , "baseURL==>" + "onDetach")
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        tv_title.visibility = View.GONE
    }

    private fun animateBackground() {

        val animLay = mActivity.findViewById<ConstraintLayout>(R.id.root)
        val drawable = intArrayOf(R.drawable.new_banner , R.drawable.referral_banner)
        backgroundAnim = MyAnim(animLay !! , drawable[count])
        backgroundAnim !!.duration = 10000
        animLay.startAnimation(backgroundAnim)
        backgroundAnim !!.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                if (count == drawable.size - 1) {
                    count = 0
                } else {
                    count ++
                }
                backgroundAnim !!.setBackground(drawable[count])
                animLay.startAnimation(backgroundAnim)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })

        animLay.setOnClickListener {
            if (mActivity.userDetails?.countryCode == "+91") {
                sharePopUpWithoutHeader()
            } else {
                sharePopUpWithoutHeaderOutsideIndia()
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.friends_fl -> {
                mActivity.addFragment(FriendsOnSmarketFragment.getInstance()
                        , true , animationType = AnimationType.RightInZoomOut)
            }
        }
    }

    private fun percentageCalculator(price: Double , percentage: Int): Double {
        val amount: Double = price
        return amount / 100.0f * percentage
    }

    private fun percentageCalculatorAdmin(price: Double , percentage: Int): Double {
        val amount: Double = price
        return amount / 100.0f * percentage
    }

    /*  private fun getProductListAPI(url: String , country: String) {

          activity !!.runOnUiThread { progressDialog.show(mActivity) }

          if (! NetworkUtil.isNetworkAvailable()) {
              showToast(ErrorUtil.NO_INTERNET)
              progressDialog.dialog.dismiss()
              return
          }

          val getProductDetail = WebApiClient.webApi().getProductDetailAPI(url , country)
          getProductDetail?.enqueue(object : Callback<GetDetails?> {
              override fun onResponse(call: Call<GetDetails?> , response: Response<GetDetails?>) {
                  progressDialog.dialog.dismiss()
                  if (response.isSuccessful) {

                      description_tv.visibility = View.VISIBLE


                      if (response.body() != null) {
                          if (response.body() !!.data !!.other_sites() != null) {
                              cv_product.visibility = View.VISIBLE
                              productTittle = response.body() !!.data !!.other_sites() !!.title
                              product_name.text = response.body() !!.data !!.other_sites() !!.title !!.replace("&nbsp;" , "")
                              product_name.setShowingLine(3)
                              product_name.addShowMoreText("more")
                              product_name.addShowLessText("Less")
                              product_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                              product_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                              price_txt.text = response.body() !!.data !!.other_sites() !!.price

                              if (response.body() !!.data !!.other_sites() !!.merchantName != null) {
                                  merchantName = response.body() !!.data !!.other_sites() !!.merchantName !!
                              } else {
                                  merchantName = "Merchant new"
                              }

                              if (price !!.isNotEmpty()) {
                                  callPercentageAPI(price_txt.text.toString() , earn_txt , merchantName !!)
                              }

                              if (response.body() !!.data !!.other_sites() !!.imageUrl != null) {
                                  Glide.with(activity !!).load(response.body() !!.data !!.other_sites() !!.imageUrl !!).into(ivProduct)
                              }

                              cv_product.setOnClickListener {
                                  val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                                  val currentDate = sdf.format(Date())
                                  if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                      sharePopUpOutside(response.body() !!.data !!.other_sites() !!.price !! , response.body() !!.data !!.other_sites() !!.canonicalUrl !! ,
                                              currentDate , userID !! , earn_txt.text.toString() , response.body() !!.data !!.other_sites() !!.merchantName !!)
                                  } else {
                                      sharePopUp(response.body() !!.data !!.other_sites() !!.price !! , response.body() !!.data !!.other_sites() !!.canonicalUrl !! ,
                                              currentDate , userID !! , earn_txt.text.toString() , response.body() !!.data !!.other_sites() !!.merchantName !!)
                                  }
                              }
                          }

                          if (response.body() !!.data !!.flipkart() != null) {
                              setValueFlipKart(response.body() !!.data !!.flipkart())
                          }
                          if (response.body() !!.data !!.ebay() != null) {
                              setValueeBay(response.body() !!.data !!.ebay())
                          }
                          if (response.body() !!.data !!.amazonCom() != null) {
                              setValueAmazon(response.body() !!.data !!.amazonCom() !!)
                          }
                          if (response.body() !!.data !!.getAmazonIn() != null) {
                              setValueAmazonIndia(response.body() !!.data !!.getAmazonIn() !!)
                          }
                      }
                  }
              }

              override fun onFailure(call: Call<GetDetails?> , t: Throwable) {
                  progressDialog.dialog.dismiss()
                  Log.e("progressbar" , "progressbar" + t.message)
              }
          })
      }

      private fun setValueAmazonIndia(amazonIn: AmazonIn) {
          cv_amazon_in.visibility = View.VISIBLE
          productTittle = amazonIn.getProductName()
          amazon_in_name.text = amazonIn.getProductName()
          amazon_in_name.setShowingLine(3)
          amazon_in_name.addShowMoreText("more")
          amazon_in_name.addShowLessText("Less")
          amazon_in_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
          amazon_in_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

          amazon_inprice_txt.text = amazonIn.getProductPrice()

          val price = amazon_inprice_txt.text.toString()
          if (price.isNotEmpty()) {
              callPercentageAPI(amazon_inprice_txt.text.toString() , earn_amazon_in , "Amazon")
          }

          cv_amazon_in.setOnClickListener {
              val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
              val currentDate = sdf.format(Date())
              if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                  sharePopUpOutside(amazon_inprice_txt.text.toString() , amazonIn.getProductUrl() !! , currentDate , userID !! , earn_amazon_in.text.toString() , "Amazon.in")
              } else {
                  sharePopUp(amazon_inprice_txt.text.toString() , amazonIn.getProductUrl() !! , currentDate , userID !! , earn_amazon_in.text.toString() , "Amazon.in")
              }
          }

          val decodedString = Base64.decode(amazonIn.getProductImage() , Base64.DEFAULT)
          val decodedByte = BitmapFactory.decodeByteArray(decodedString , 0 , decodedString.size)
          ivamazon_inProduct.setImageBitmap(decodedByte)
      }

      private fun setValueAmazon(amazonCom: AmazonCom) {

          cv_amazon.visibility = View.VISIBLE
          productTittle = amazonCom.getProductName()
          amazon_name.text = amazonCom.getProductName()
          amazon_name.setShowingLine(3)
          amazon_name.addShowMoreText("more")
          amazon_name.addShowLessText("Less")
          amazon_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
          amazon_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

          amazonprice_txt.text = amazonCom.getProductPrice()

          val price = amazonprice_txt.text.toString()
          if (price.isNotEmpty()) {
              callPercentageAPI(amazonprice_txt.text.toString() , amazon_earn , "Amazon")
          }

          cv_amazon.setOnClickListener {
              val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
              val currentDate = sdf.format(Date())
              if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                  sharePopUpOutside(amazonprice_txt.text.toString() , amazonCom.getProductUrl() !! ,
                          currentDate , userID !! , amazon_name.text.toString() , "Amazon")
              } else {
                  sharePopUp(amazonprice_txt.text.toString() , amazonCom.getProductUrl() !! ,
                          currentDate , userID !! , amazon_name.text.toString() , "Amazon")
              }
          }

      }

      private fun setValueeBay(ebay: Ebay?) {

          cv_ebuy.visibility = View.VISIBLE
          productTittle = ebay !!.getProductName()
          ebuy_name.text = ebay.getProductName()
          ebuy_name.setShowingLine(3)
          ebuy_name.addShowMoreText("more")
          ebuy_name.addShowLessText("Less")
          ebuy_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
          ebuy_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

          ebuyprice_txt.text = ebay.getProductPrice()

          val price = ebuyprice_txt.text.toString()
          if (price.isNotEmpty() && price != null) {
              callPercentageAPI(ebuyprice_txt.text.toString() , ebay_earn , "eBay")
          }

          cv_ebuy.setOnClickListener {
              val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
              val currentDate = sdf.format(Date())
              if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                  sharePopUpOutside(ebuyprice_txt.text.toString() , ebay.getProductUrl() !! ,
                          currentDate , userID !! , ebay_earn.text.toString() , "eBay")
              } else {
                  sharePopUp(ebuyprice_txt.text.toString() , ebay.getProductUrl() !! ,
                          currentDate , userID !! , ebay_earn.text.toString() , "eBay")
              }
          }
      }

      private fun setValueFlipKart(flipkart: Flipkart?) {
          cv_flip.visibility = View.VISIBLE
          flipname.text = flipkart !!.getProductName()

          productTittle = flipkart.getProductName()

          flipname.setShowingLine(3)
          flipname.addShowMoreText("more")
          flipname.addShowLessText("Less")
          flipname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
          flipname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

          flipprice_txt.text = flipkart.getProductPrice()

          val price = flipprice_txt.text.toString()
          if (price.isNotEmpty()) {
              callPercentageAPI(flipprice_txt.text.toString() , earn_flip_tv , "Flipkart")
          }

          cv_flip.setOnClickListener {
              val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
              val currentDate = sdf.format(Date())
              if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                  sharePopUpOutside(flipprice_txt.text.toString() , flipkart.getProductUrl() !! ,
                          currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart")
              } else {
                  sharePopUp(flipprice_txt.text.toString() , flipkart.getProductUrl() !! ,
                          currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart")
              }
          }

          //image
      }*/

    private suspend fun callProductListAPI(url: String) {

        mActivity.runOnUiThread {
            progressDialog.show(mActivity)
            earn_frame.visibility = View.VISIBLE
        }

        withContext(Dispatchers.Default) {
            try {
                baseURL = when {
                    url.contains("\n\n") -> {
                        val currentstring = url.split("\n\n")
                        val url1 = currentstring[1]
                        url1
                    }
                    url.contains("http") -> {
                        val currentstring = url.split("http")
                        val url1 = currentstring[1]
                        "http$url1"
                    }
                    else -> {
                        url
                    }
                }
            } catch (e: Exception) {
                Log.e("exception" , "exception" + e.message)
            }

            val apiInterface = WebServiceVigiLink.client !!.create(WebApi::class.java)
            val callProductDetail = apiInterface.productDetails(baseURL !!)

            callProductDetail?.enqueue(object : Callback<ProductResponse?> {
                override fun onFailure(call: Call<ProductResponse?> , t: Throwable) {
                    progressDialog.dialog.dismiss()
                    Log.e("progressbar" , "progressbar" + t.message)
                }

                override fun onResponse(call: Call<ProductResponse?> , response: Response<ProductResponse?>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            progressDialog.dialog.dismiss()
                            cv_product.visibility = View.VISIBLE
                            description_tv.visibility = View.VISIBLE

                            product_name.text = response.body() !!.getTitle()
                            product_name.setShowingLine(3)
                            product_name.addShowMoreText("more")
                            product_name.addShowLessText("Less")
                            product_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                            product_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                            when {
                                baseURL !!.contains("https://www.amazon.in/") -> {
                                    val doc = Jsoup.connect(baseURL).get()
                                    if (doc.getElementById("priceblock_ourprice") != null) {
                                        doc.getElementById("priceblock_ourprice").childNodes().forEach {
                                            price = it.toString().replace("&nbsp;" , "")
                                        }
                                    } else if (doc.getElementById("priceblock_dealprice") != null) {
                                        doc.getElementById("priceblock_dealprice").childNodes().forEach {
                                            price = it.toString().replace("&nbsp;" , "")
                                        }
                                    }
                                    price_txt.text = price
                                }
                                baseURL !!.contains("https://www.ebay.com/") -> {
                                    try {
                                        if (SMarket.appDB.userDao().getData() !!.countryCode == "+91") {
                                            val doc = Jsoup.connect(baseURL).get()
                                            if (doc.getElementById("convbinPrice") != null) {
                                                for (i in 0 until doc.getElementById("convbinPrice").childNodes().size) {
                                                    price = doc.getElementById("convbinPrice").childNodes()[0].toString()
                                                }
                                            } else if (doc.getElementById("convbidPrice") != null) {
                                                for (i in 0 until doc.getElementById("convbidPrice").childNodes().size) {
                                                    price = doc.getElementById("convbidPrice").childNodes()[0].toString()
                                                }
                                            } else {
                                                val productNamePrice = doc.getElementsByClass("display-price")
                                                for (i in 0 until productNamePrice.size) {
                                                    price = productNamePrice[0].text()
                                                }
                                            }
                                            price_txt.text = price
                                        } else {
                                            price_txt.text = response.body() !!.getPrice()
                                        }
                                    } catch (e: Exception) {
                                        price_txt.text = response.body() !!.getPrice()
                                    }
                                }
                                else -> {
                                    price_txt.text = response.body() !!.getPrice()
                                }
                            }

                            val price = price_txt.text.toString()

                            if (response.body() !!.getMerchantName() != null) {
                                merchantName = response.body() !!.getMerchantName() !!
                            } else {
                                merchantName = "Merchant new"
                            }

                            Log.e("merchantName" , "merchantName==>$merchantName")

                            // if (baseURL !!.contains("https://www.amazon")) {
                            if (price.isNotEmpty()) {
                                callPercentageAPI(price_txt.text.toString() , earn_txt , merchantName !!)
                            }
                            // }

                            if (response.body() !!.getImageUrl() != null) {
                                Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivProduct)
                            }
                            productTittle = response.body() !!.getTitle()?.replace("&nbsp;" , "")

                            if (response.body() !!.getCanonicalUrl() != null) {
                                cv_product.setOnClickListener {
                                    val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                                    val currentDate = sdf.format(Date())
                                    if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                        sharePopUpOutside(response.body() !!.getPrice() !! , response.body() !!.getCanonicalUrl() !! ,
                                                currentDate , userID !! , earn_txt.text.toString() , response.body() !!.getMerchantName() !! , response.body() !!.getImageUrl() !!)
                                    } else {
                                        sharePopUp(response.body() !!.getPrice() !! , response.body() !!.getCanonicalUrl() !! ,
                                                currentDate , userID !! , earn_txt.text.toString() , response.body() !!.getMerchantName() !! , response.body() !!.getImageUrl() !!)
                                    }
                                }
                            }

                            if (response.body() !!.getMerchantName() != null) {
                                setLogoForImage(response.body() !!.getMerchantName() !!)
                            }

                            if (response.body() !!.getImageUrl() != null) {
                                Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivflipProduct)
                            }


                            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                if (baseURL !!.contains("https://www.ebay.com/")) {
                                    getProductDetailFromWalmarT("https://www.walmart.com/search/?query=$productTittle")

                                } else if (baseURL !!.contains("https://www.walmart.com/") || baseURL !!.contains(
                                                "http://www.walmart.com/")) {

                                    getProductDEtailFromAmazon(
                                            "https://www.amazon.com/s?k=$productTittle".replace(
                                                    "(AT&T)" , ""
                                            ).replace("(GSM)" , "")
                                                    .replace("(CDMA + GSM)" , "").replace(
                                                            "AT&T" ,
                                                            ""
                                                    ).replace("Verizon" , "")
                                    )

                                } else if (baseURL !!.contains("https://www.flipkart.com/")) {
                                    cv_product.visibility = View.GONE
                                    description_tv.visibility = View.GONE
                                    flip_iv.visibility = View.VISIBLE
                                } else if (baseURL !!.equals("https://www.reliancedigital.in/")) {
                                    cv_product.visibility = View.GONE
                                    description_tv.visibility = View.GONE
                                    flip_iv.visibility = View.VISIBLE
                                } else {
                                    isCheckUrl = true

                                    val getTittle = getFirstNStrings(productTittle !! , 5)
                                    getTargetLink("https://www.target.com/s?searchTerm=$getTittle")

                                    // getBestBuyLink("https://www.bestbuy.com/site/searchpage.jsp?st=$getTittle")

                                    getProductDEtailFromAmazon(
                                            "https://www.amazon.com/s?k=$productTittle".replace(
                                                    "(AT&T)" ,
                                                    ""
                                            ).replace("(GSM)" , "")
                                                    .replace("(CDMA + GSM)" , "").replace(
                                                            "AT&T" , ""
                                                    ).replace("Verizon" , "")
                                    )

                                }
                            } else {

                                if (baseURL !!.contains("https://www.amazon.in/")) {
                                    getProductFlipkart("https://www.flipkart.com/search?q=$productTittle")
                                } else if (baseURL !!.contains("http://dl.flipkart.com/") || baseURL !!.contains("https://dl.flipkart.com/")) {
                                    val getTittle = getFirstNStrings(productTittle !! , 5)
                                    getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
                                } else if (baseURL !!.contains("https://www.flipkart.com/")) {
                                    cv_product.visibility = View.GONE
                                    description_tv.visibility = View.GONE
                                    flip_iv.visibility = View.VISIBLE
                                    progressDialog.dialog.dismiss()
                                } else {
                                    isCheckUrl = true
                                    getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")

                                    if (! baseURL !!.contains("https://www.reliancedigital.in")) {
                                        isCheckUrl = true
                                        getRelianceDigitalRedirect("https://www.reliancedigital.in/search?q=$productTittle")
                                    } else {
                                        isCheckUrl = true
                                        getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
                                    }
                                }

                            }
                            cv_cashpack.visibility = View.VISIBLE
                            /*if (! baseURL?.contains("http://dl.flipkart.com/") !! && ! baseURL !!.contains("https://dl.flipkart.com/")) {
                                GlobalScope.launch(Dispatchers.IO) {
                                    val getTittle = getFirstNStrings(productTittle !! , 3)
                                    callVigiLinkAPIFlipkart("https://www.flipkart.com/search?q=$getTittle")
                                }
                            }*/

                        } else {
                            Toast.makeText(activity , response.message() , Toast.LENGTH_LONG)
                                    .show()
                            progressDialog.dialog.dismiss()
                        }
                    } else {
                        progressDialog.dialog.dismiss()
                        Toast.makeText(activity , response.message() , Toast.LENGTH_LONG).show()
                    }
                }

            })
        }
    }

    private fun callProductListAPIAmazon(url: String) {

        mActivity.runOnUiThread { progressDialog.show(mActivity) }

        val apiInterface = WebServiceVigiLink.client !!.create(WebApi::class.java)
        val callProductDetail = apiInterface.productDetails(url)

        callProductDetail?.enqueue(object : Callback<ProductResponse?> {
            override fun onFailure(call: Call<ProductResponse?> , t: Throwable) {
                progressDialog.dialog.dismiss()
                Log.e("progressbar" , "progressbar" + t.message)
            }

            override fun onResponse(call: Call<ProductResponse?> , response: Response<ProductResponse?>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        progressDialog.dialog.dismiss()
                        cv_product.visibility = View.VISIBLE
                        description_tv.visibility = View.VISIBLE

                        product_name.text = response.body() !!.getTitle()
                        product_name.setShowingLine(3)
                        product_name.addShowMoreText("more")
                        product_name.addShowLessText("Less")
                        product_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                        product_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                        /*when {
                            url.contains("https://www.amazon.in/") -> {
                                val doc = Jsoup.connect(url).get()
                                if (doc.getElementById("priceblock_ourprice") != null) {
                                    doc.getElementById("priceblock_ourprice").childNodes().forEach {
                                        price = it.toString().replace("&nbsp;" , "")
                                    }
                                } else if (doc.getElementById("priceblock_dealprice") != null) {
                                    doc.getElementById("priceblock_dealprice").childNodes().forEach {
                                        price = it.toString().replace("&nbsp;" , "")
                                    }
                                } else if (doc.getElementById("priceblock_saleprice") != null) {
                                    doc.getElementById("priceblock_saleprice").childNodes().forEach {
                                        price = it.toString().replace("&nbsp;" , "")
                                    }
                                } else if (doc.getElementsByClass("a-size-medium a-color-price inlineBlock-display offer-price a-text-normal price3P") != null) {
                                    val productPr = doc.getElementsByClass("a-size-medium a-color-price inlineBlock-display offer-price a-text-normal price3P")
                                    for (i in 0 until productPr.size) {
                                        price = productPr[0].text()
                                    }
                                }
                                else if (doc.getElementsByClass("a-size-medium a-color-price priceBlockDealPriceString") != null) {
                                    val productPr = doc.getElementsByClass("a-size-medium a-color-price priceBlockDealPriceString")
                                    for (i in 0 until productPr.size) {
                                        price = productPr[0].text()
                                    }
                                }
                                price_txt.text = price
                            }
                            url.contains("https://www.ebay.com/") -> {
                                try {
                                    if (SMarket.appDB.userDao().getData() !!.countryCode == "+91") {
                                        val doc = Jsoup.connect(url).get()
                                        if (doc.getElementById("convbinPrice") != null) {
                                            for (i in 0 until doc.getElementById("convbinPrice").childNodes().size) {
                                                price = doc.getElementById("convbinPrice").childNodes()[0].toString()
                                            }
                                        } else if (doc.getElementById("convbidPrice") != null) {
                                            for (i in 0 until doc.getElementById("convbidPrice").childNodes().size) {
                                                price = doc.getElementById("convbidPrice").childNodes()[0].toString()
                                            }
                                        } else {
                                            val productNamePrice = doc.getElementsByClass("display-price")
                                            for (i in 0 until productNamePrice.size) {
                                                price = productNamePrice[0].text()
                                            }
                                        }
                                        price_txt.text = price
                                    } else {
                                        price_txt.text = response.body() !!.getPrice()
                                    }
                                } catch (e: Exception) {
                                    price_txt.text = response.body() !!.getPrice()
                                }
                            }
                            else -> {
                                price_txt.text = response.body() !!.getPrice()
                            }
                        }*/

                        price_txt.text = response.body() !!.getPrice()

                        val price = price_txt.text.toString()

                        if (response.body() !!.getMerchantName() != null) {
                            merchantName = response.body() !!.getMerchantName() !!
                        } else {
                            merchantName = "Merchant new"
                        }

                        if (price.isNotEmpty()) {
                            callPercentageAPI(price_txt.text.toString() , earn_txt , merchantName !!)
                        }

                        if (response.body() !!.getImageUrl() != null) {
                            Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivProduct)
                        }
                        productTittle = response.body() !!.getTitle()?.replace("&nbsp;" , "")

                        if (response.body() !!.getCanonicalUrl() != null) {
                            cv_product.setOnClickListener {
                                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                                val currentDate = sdf.format(Date())
                                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                    sharePopUpOutside(response.body() !!.getPrice() !! , response.body() !!.getCanonicalUrl() !! ,
                                            currentDate , userID !! , earn_txt.text.toString() , response.body() !!.getMerchantName() !! , response.body() !!.getImageUrl() !!)
                                } else {
                                    sharePopUp(response.body() !!.getPrice() !! , response.body() !!.getCanonicalUrl() !! ,
                                            currentDate , userID !! , earn_txt.text.toString() , response.body() !!.getMerchantName() !! , response.body() !!.getImageUrl() !!)
                                }
                            }
                        }

                        if (response.body() !!.getMerchantName() != null) {
                            setLogoForImage(response.body() !!.getMerchantName() !!)
                        }

                        if (response.body() !!.getImageUrl() != null) {
                            Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivflipProduct)
                        }


                        if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                            if (url.contains("https://www.ebay.com/")) {
                                getProductDetailFromWalmarT("https://www.walmart.com/search/?query=$productTittle")

                            } else if (url.contains("https://www.walmart.com/") || url.contains(
                                            "http://www.walmart.com/")) {

                                getProductDEtailFromAmazon(
                                        "https://www.amazon.com/s?k=$productTittle".replace(
                                                "(AT&T)" , ""
                                        ).replace("(GSM)" , "")
                                                .replace("(CDMA + GSM)" , "").replace(
                                                        "AT&T" ,
                                                        ""
                                                ).replace("Verizon" , "")
                                )

                            } else if (url.contains("https://www.flipkart.com/")) {
                                cv_product.visibility = View.GONE
                                description_tv.visibility = View.GONE
                                flip_iv.visibility = View.VISIBLE
                            } else if (url.contains("https://www.reliancedigital.in/")) {
                                cv_product.visibility = View.GONE
                                description_tv.visibility = View.GONE
                                flip_iv.visibility = View.VISIBLE
                            } else {
                                isCheckUrl = true

                                val getTittle = getFirstNStrings(productTittle !! , 5)
                                getTargetLink("https://www.target.com/s?searchTerm=$getTittle")

                                // getBestBuyLink("https://www.bestbuy.com/site/searchpage.jsp?st=$getTittle")

                                getProductDEtailFromAmazon(
                                        "https://www.amazon.com/s?k=$productTittle".replace(
                                                "(AT&T)" ,
                                                ""
                                        ).replace("(GSM)" , "")
                                                .replace("(CDMA + GSM)" , "").replace(
                                                        "AT&T" , ""
                                                ).replace("Verizon" , "")
                                )

                            }
                        } else {

                            if (url.contains("https://www.amazon.in/")) {
                                getProductFlipkart("https://www.flipkart.com/search?q=$productTittle")
                            } else if (url.contains("http://dl.flipkart.com/") || url.contains("https://dl.flipkart.com/")) {
                                val getTittle = getFirstNStrings(productTittle !! , 5)
                                getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
                            } else if (url.contains("https://www.flipkart.com/")) {
                                cv_product.visibility = View.GONE
                                description_tv.visibility = View.GONE
                                flip_iv.visibility = View.VISIBLE
                                progressDialog.dialog.dismiss()
                            } else {
                                isCheckUrl = true
                                getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")

                                if (! url.contains("https://www.reliancedigital.in")) {
                                    isCheckUrl = true
                                    getRelianceDigitalRedirect("https://www.reliancedigital.in/search?q=$productTittle")
                                } else {
                                    isCheckUrl = true
                                    getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
                                }
                            }

                        }


                        cv_cashpack.visibility = View.VISIBLE

                        /*if (! baseURL?.contains("http://dl.flipkart.com/") !! && ! baseURL !!.contains("https://dl.flipkart.com/")) {
                            GlobalScope.launch(Dispatchers.IO) {
                                val getTittle = getFirstNStrings(productTittle !! , 3)
                                callVigiLinkAPIFlipkart("https://www.flipkart.com/search?q=$getTittle")
                            }
                        }*/

                    } else {
                        Toast.makeText(activity , response.message() , Toast.LENGTH_LONG)
                                .show()
                        progressDialog.dialog.dismiss()
                    }
                } else {
                    progressDialog.dialog.dismiss()
                    Toast.makeText(activity , response.message() , Toast.LENGTH_LONG).show()
                }
            }

        })

    }

    private suspend fun callVigiLinkAPIFlipkart(url: String) {

        withContext(Dispatchers.Default) {

            val apiInterface = WebServiceVigiLink.client !!.create(WebApi::class.java)
            val callProductDetail = apiInterface.productDetails(url)

            callProductDetail?.enqueue(object : Callback<ProductResponse?> {
                override fun onFailure(call: Call<ProductResponse?> , t: Throwable) {
                    progressDialog.dialog.dismiss()
                    Log.e("progressbar" , "progressbar" + t.message)
                }

                override fun onResponse(call: Call<ProductResponse?> , response: Response<ProductResponse?>) {

                    mActivity.runOnUiThread {
                        if (progressDialog.dialog.isShowing) {
                            progressDialog.dialog.dismiss()
                        }
                    }

                    if (response.isSuccessful) {
                        if (response.body() != null) {

                            cv_flip.visibility = View.VISIBLE

                            flipname.text = response.body()?.getTitle()
                            flipname.setShowingLine(3)
                            flipname.addShowMoreText("more")
                            flipname.addShowLessText("Less")
                            flipname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                            flipname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                            flipprice_txt.text = response.body() !!.getPrice()

                            /* val price = flipprice_txt.text.toString()
                             if (price.isNotEmpty()) {
                                 callPercentageAPI(flipprice_txt.text.toString() , earn_flip_tv , "Flipkart")
                             }*/

                            if (response.body() !!.getImageUrl() != null) {
                                Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivflipProduct)
                            }

                            cv_flip.setOnClickListener {
                                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                                val currentDate = sdf.format(Date())
                                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                    /* sharePopUpOutside(response.body()?.getPrice() !! , url ,
                                             currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart" , response.body()?.getImageUrl() !!)*/

                                    sharePopUpOutside(response.body()?.getPrice() !! , url ,
                                            currentDate , userID !! , "0" , "Flipkart" , response.body()?.getImageUrl() !!)
                                } else {
                                    sharePopUp(response.body()?.getPrice() !! , url ,
                                            currentDate , userID !! , "0" , "Flipkart" , response.body()?.getImageUrl() !!)

                                    /*sharePopUp(response.body()?.getPrice() !! , url ,
                                            currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart" , response.body()?.getImageUrl() !!)*/
                                }
                            }


                        } else {
                            mActivity.runOnUiThread {
                                if (progressDialog.dialog.isShowing) {
                                    progressDialog.dialog.dismiss()
                                }
                            }
                            Toast.makeText(activity , response.message() , Toast.LENGTH_LONG).show()
                        }
                    } else {
                        mActivity.runOnUiThread {
                            if (progressDialog.dialog.isShowing) {
                                progressDialog.dialog.dismiss()
                            }
                        }
                        Toast.makeText(activity , response.message() , Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    private fun getRelianceDigitalRedirect(url: String) {
        activity?.runOnUiThread {

            try {
                val reliance = Jsoup.connect(url).get()
                val relianceRedirect = reliance.getElementsByClass("sp grid")

                val link = relianceRedirect.select("a")
                val redirectURL = link.attr("href")

                if (redirectURL != null) {

                    callRelianceDigitalAPI("https://www.reliancedigital.in$redirectURL")
                } else {
                    if (! isCheckUrl) {
                        if (! baseURL?.contains("https://www.amazon.in/") !! && ! baseURL?.contains("https://www.amazon.com/") !!) {//! baseURL?.contains("https://www.amazon.com/") !!
                            getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
                        }
                    }
                    /* isCheckUrl = true
                     getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")*/
                }
            } catch (e: Exception) {
                Log.e("exception" , "exception" + e.localizedMessage)
            }
        }
    }

    private fun callRelianceDigitalAPI(url: String) {

        mActivity.runOnUiThread {
            if (progressDialog.dialog.isShowing) {
                progressDialog.dialog.dismiss()
            }
        }

        val progressDialog = CustomProgressDialog()
        progressDialog.show(mActivity)

        val apiInterface = WebServiceVigiLink.client !!.create(WebApi::class.java)
        val callProductDetail = apiInterface.productDetails(url)

        callProductDetail?.enqueue(object : Callback<ProductResponse?> {
            override fun onFailure(call: Call<ProductResponse?> , t: Throwable) {
                progressDialog.dialog.dismiss()
                Log.e("progressbar" , "progressbar" + t.message)
            }

            override fun onResponse(call: Call<ProductResponse?> , response: Response<ProductResponse?>) {
                if (response.isSuccessful) {
                    progressDialog.dialog.dismiss()
                    if (response.body() != null) {
                        cv_reliance.visibility = View.VISIBLE

                        reliancename.text = response.body() !!.getTitle()
                        reliancename.setShowingLine(3)
                        reliancename.addShowMoreText("more")
                        reliancename.addShowLessText("Less")
                        reliancename.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                        reliancename.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                        if (response.body() !!.getImageUrl() != null) {
                            Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivrelianceProduct)
                        }

                        if (response.body() !!.getMerchantName() != null) {
                            merchantName = response.body() !!.getMerchantName() !!
                        }

                        productTittle = response.body() !!.getTitle()?.replace("&nbsp;" , "")

                        relianceprice_txt.text = response.body() !!.getPrice()
                        val price = relianceprice_txt.text.toString()
                        if (price.isNotEmpty()) {
                            callPercentageAPI(relianceprice_txt.text.toString() , earn_reliance_tv , merchantName !!)
                        }

                        /*isCheckUrl = true
                        getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")*/

                        if (! isCheckUrl) {
                            if (! baseURL?.contains("https://www.amazon.in/") !! && ! baseURL?.contains("https://www.amazon.com/") !!) {//! baseURL?.contains("https://www.amazon.com/") !!
                                getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
                            } else {
                                if (progressDialog.dialog.isShowing) {
                                    progressDialog.dialog.dismiss()
                                }
                            }
                        }

                        cv_reliance.setOnClickListener {
                            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                            val currentDate = sdf.format(Date())
                            sharePopUp(response.body() !!.getPrice() !! , response.body() !!.getCanonicalUrl() !! ,
                                    currentDate , userID !! , earn_reliance_tv.text.toString() , response.body() !!.getMerchantName() !! , response.body() !!.getImageUrl() !!)

                        }
                    }
                }
            }
        })
    }

    private fun callImageURLAPI(url: String) {

        activity !!.runOnUiThread {
            progressDialog.show(mActivity)
        }

        val apiInterface = WebServiceVigiLink.client !!.create(WebApi::class.java)
        val callProductDetail = apiInterface.productDetails(url)

        callProductDetail?.enqueue(object : Callback<ProductResponse?> {
            override fun onFailure(call: Call<ProductResponse?> , t: Throwable) {
                progressDialog.dialog.dismiss()
            }

            override fun onResponse(call: Call<ProductResponse?> , response: Response<ProductResponse?>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {

                        product_name.text = response.body() !!.getTitle()
                        product_name.setShowingLine(3)
                        product_name.addShowMoreText("more")
                        product_name.addShowLessText("Less")
                        product_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                        product_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                        if (response.body() !!.getMerchantName() != null) {
                            merchantName = response.body() !!.getMerchantName() !!
                        }

                        productTittle = response.body() !!.getTitle()?.replace("&nbsp;" , "")

                        price_txt.text = response.body() !!.getPrice()

                        val name = productTittle
                        val price = price_txt.text.toString()


                        cv_product?.setOnClickListener {
                            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                            val currentDate = sdf.format(Date())
                            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "Amazon.com" , imageURLAmazon !!)
                            } else {
                                sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "Amazon India" , imageURLAmazon !!)
                            }
                        }

                        baseURL = url
                        productTittle = name !!.replace("&nbsp;" , "")

                        if (price.isNotEmpty()) {
                            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                                callPercentageAPI(price , earn_txt , "Amazon.com")
                            } else {
                                callPercentageAPI(price , earn_txt , "Amazon India")
                            }
                        }

                        if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                            if (! baseURL?.contains("https://www.ebay.com/") !!) {
                                if (productTittle != null) {
                                    val getTittle = getFirstNStrings(productTittle !! , 5)
                                    getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
                                }
                            }
                        } else {
                            val getTittle = getFirstNStrings(productTittle !! , 5)
                            getProductFlipkart("https://www.flipkart.com/search?q=$getTittle")
                        }

                        if (response.body() !!.getImageUrl() != null) {
                            imageURLAmazon = response.body() !!.getImageUrl()
                            Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivProduct)
                            Glide.with(activity !!).load(response.body() !!.getImageUrl() !!).into(ivflipProduct)
                        }
                    }
                }
            }
        })
    }

    private fun callUserActivityAPI(productName: String , price: String , productUrl: String ,
                                    clicked: String , userID: String , earn: String , merchantName: String , imageURL: String) {
        activity !!.runOnUiThread {
            ProgressDialogUtil.showProgressDialog(mActivity)
        }

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val hashMap = ApiParam.getHashMap()

        if (mActivity.userDetails != null) {
            with(mActivity.userDetails !!) {
                hashMap[ApiParam.PHONE_NUMBER] = mobile
                hashMap[ApiParam.MERCHANT_NAME] = merchantName
            }
        }
        hashMap[ApiParam.PRODUCT_NAME] = productName
        hashMap[ApiParam.PRODUCT_PRICE] = price
        hashMap[ApiParam.REWARD_VALUE] = earn
        hashMap[ApiParam.STATUS] = "1"
        hashMap[ApiParam.DEVICE_TYPE] = "Android"
        hashMap[ApiParam.PRODUCT_URL] = productUrl
        hashMap[ApiParam.PRODUCT_IMAGE_URL] = imageURL
        hashMap[ApiParam.CLICKED_AT] = clicked
        hashMap[ApiParam.USER_ID] = userID

        val callUserActivity = WebApiClient.webApi().userActivity(hashMap)

        callUserActivity?.enqueue(object : Callback<UserActivityResponse?> {
            override fun onFailure(call: Call<UserActivityResponse?> , t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<UserActivityResponse?> , response: Response<UserActivityResponse?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (! merchantName.contains("eBay" , true)) {
                            if (merchantName == "Amazon India") {
                                val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("$productUrl/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarketindia-21"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon.in") {
                                val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("$productUrl&linkCode=ll1&tag=smarketindia-21"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon.com") {
                                val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("$productUrl/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarket0f-20"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon") {
                                val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("$productUrl&linkCode=ll1&tag=smarket0f-20"))
                                startActivity(browserIntent)
                            } else if (merchantName.contains("ajio.com") || merchantName.contains("Pharmeasy (CPAT)")
                                    || merchantName.contains("Nykaaman.com") || merchantName.contains("paytmmall")
                                    || merchantName.contains("tatacliq.com") || merchantName.contains("AliExpress")
                                    || merchantName.contains("Macy's") || merchantName.contains("Walmart")) {

                                val browserIntent = Intent(
                                        Intent.ACTION_VIEW ,
                                        Uri.parse("https://linksredirect.com/?cid=125043&source=linkkit&url=$productUrl"))

                                Log.e("redirect" , "redirect==>https://linksredirect.com/?cid=125043&source=linkkit&url=$productUrl")
                                startActivity(browserIntent)
                            } else {
                                val browserIntent = Intent(
                                        Intent.ACTION_VIEW ,
                                        Uri.parse("http://redirect.viglink.com?u=$productUrl&key=f5e38af288c412320cf9ff35e2299c98"))
                                startActivity(browserIntent)
                            }
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("https://rover.ebay.com/rover/1/711-53200-19255-0/1?campid=5338693049&customid=" + mActivity.userDetails !!.mobile + "!&toolid=20006&mpre=" + productUrl))
                            startActivity(browserIntent)
                        }

                    } else {
                        Toast.makeText(activity , "No product found!" , Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(activity , "No product found!" , Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun callPercentageAPI(price: String , textView: TextView , merchantName: String) {

        /* activity !!.runOnUiThread {
             ProgressDialogUtil.showProgressDialog(mActivity)
         }*/

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            // ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callAdminPercentage = WebApiClient.webApi().adminPercentage()
        callAdminPercentage !!.enqueue(object : Callback<AdminPercentage?> {
            override fun onResponse(call: Call<AdminPercentage?> , response: Response<AdminPercentage?>) {
                // ProgressDialogUtil.hideProgressDialog()
                progressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        var priceUpdate = price.replace("/month" , "").replace("$" , "").replace("-" , "").replace("INR" , "").replace("," , "").replace("\u20B9" , "").replace("Rs." , "").replace("\"" , "").trim()
                        if (priceUpdate.contains("Your price for this item is")) {
                            val getPrice = price.split("Your")
                            priceUpdate = getPrice[0]
                        }
                        if (priceUpdate.contains("The previous price for this item was ")) {
                            val getPrice = price.split("The")
                            priceUpdate = getPrice[0]
                        }
                        if (priceUpdate.contains("to")) {
                            val getPrice = price.split("to")
                            priceUpdate = getPrice[0].replace("/month" , "").replace("$" , "").replace("INR" , "").replace("," , "").replace("\u20B9" , "").replace("Rs." , "").replace("\"" , "")
                        }
                        if (priceUpdate.contains("-")) {
                            val getPrice = price.split("-")
                            priceUpdate = getPrice[0].replace("/month" , "").replace("-" , "").replace("$" , "").replace("INR" , "").replace("," , "").replace("\u20B9" , "").replace("Rs." , "").replace("\"" , "")
                        }
                        if (priceUpdate.isNotEmpty()) {
                            if (merchantName != "Amazon" && merchantName != "Amazon India" && merchantName != "Amazon.com") {
                                val earnValue = percentageCalculator(priceUpdate.replace("/month" , "").replace("-" , "")
                                        .replace("$" , "").replace("INR" , "")
                                        .replace("US" , "").replace("," , "")
                                        .replace("\u20B9" , "").replace("Rs." , "")
                                        .replace("\"" , "").toDouble() , 6)
                                val percentageValue = percentageCalculatorAdmin(earnValue , response.body() !!.data !!.rewardpercent !!.customerPercent !!)
                                if (price.contains("INR") || price.contains("₹")) {
                                    textView.text = "₹" + String.format("%.1f" , percentageValue)
                                } else {
                                    textView.text = "$" + String.format("%.1f" , percentageValue)
                                }
                            } else {
                                if (price.contains("Currently unavailable.") || price.contains("Temporarily out of stock.") || price.contains("order soon")) {
                                    if (SMarket.appDB.userDao().getData() !!.countryCode == "+91") {
                                        textView.text = "₹" + "0.0"
                                    } else {
                                        textView.text = "$" + "0.0"
                                    }
                                } else {
                                    val earnValue = percentageCalculator(priceUpdate.toDouble() , 10)
                                    val percentageValue = percentageCalculatorAdmin(earnValue , response.body() !!.data !!.rewardpercent !!.customerPercent !!)
                                    if (price.contains("INR") || price.contains("₹")) {
                                        textView.text = "₹" + String.format("%.1f" , percentageValue)
                                    } else {
                                        textView.text = "$" + String.format("%.1f" , percentageValue)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<AdminPercentage?> , t: Throwable) {
                progressDialog.dialog.dismiss()
            }
        })
    }

    private fun callReferralAPI() {

        /*activity !!.runOnUiThread {
            //ProgressDialogUtil.showProgressDialog(mActivity)
        }
*/
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            // ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callRefferalApi = WebApiClient.webApi().referList(userID !!)
        callRefferalApi?.enqueue(object : Callback<ReferredList?> {
            override fun onFailure(call: Call<ReferredList?> , t: Throwable) {
                // ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<ReferredList?> , response: Response<ReferredList?>) {
                // ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null)
                        if (response.body() !!.getData()?.getRefered()?.size !! > 0) {
                            referral_count_tv.text = response.body() !!.getData()?.getRefered()?.size.toString()
                        } else {
                            try {
                                referral_count_tv.text = "0"
                            } catch (e: Exception) {
                                Log.e("referFriends" , "referFriends" + e.message)
                            }
                        }
                }
            }
        })
    }

    private fun setLogoForImage(merchantName: String) {

        if (merchantName.contains("eBay")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.ebay)).into(logo)
        } else if (merchantName.contains("Walmart")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.walmart)).into(logo)
        } else if (merchantName.contains("Amazon India")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.amazon_india)).into(logo)
        } else if (merchantName.contains("Best Buy")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.bestbuy)).into(logo)
        } else if (merchantName.contains("NewEgg")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.target)).into(logo)
        } else if (merchantName.contains("Flipkart")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.flip)).into(logo)
        } else if (merchantName.contains("Reliancedigital.in") || merchantName.contains("Reliance Digital IN")) {
            Glide.with(activity !!).load(resources.getDrawable(R.drawable.reliance)).into(logo)
        }
    }

    private fun getFirstNStrings(str: String , n: Int): String? {
        try {
            val sArr = str.split(" ").toTypedArray()
            var firstStrs = ""
            for (i in 0 until n) firstStrs += sArr[i] + " "
            return firstStrs.trim { it <= ' ' }
        } catch (e: Exception) {
            return str
        }
    }

    private fun getProductDEtailebay(url: String) {
        activity?.runOnUiThread {
            cv_ebuy.visibility = View.VISIBLE
            //progressDialog.show(mActivity)
            var srcValue: String? = null

            val amazon = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36")
                    .get()
            val productNameAmzon = amazon.getElementsByClass("s-item__title")

            for (i in 0 until productNameAmzon.size) {
                ebuy_name.text = productNameAmzon[0].text()
            }

            ebuy_name.setShowingLine(3)
            ebuy_name.addShowMoreText("more")
            ebuy_name.addShowLessText("Less")
            ebuy_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
            ebuy_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))


            val productNamePrice = amazon.getElementsByClass("s-item__price")

            for (i in 0 until productNamePrice.size) {
                ebuyprice_txt.text = productNamePrice[0].text()
            }

            if (amazon.getElementsByClass("s-item__image-img") != null) {
                val productImage = amazon.getElementsByClass("s-item__image-img")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    srcValue = imageElement.attr("src")
                    activity?.runOnUiThread {
                        Glide.with(activity !!).load(srcValue).into(ivebuyProduct)
                    }
                }
            }

            if (productNameAmzon.size == 0 && productNamePrice.size == 0) {
                // Glide.with(activity !!).load(resources.getDrawable(R.drawable.noproduct)).into(ivebuyProduct)
                cv_ebuy.visibility = View.GONE
            }

            val price = ebuyprice_txt.text.toString()
            if (price.isNotEmpty() && price != null) {
                callPercentageAPI(ebuyprice_txt.text.toString() , ebay_earn , "eBay")
            }

            cv_ebuy.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(ebuyprice_txt.text.toString() , url ,
                            currentDate , userID !! , ebay_earn.text.toString() , "eBay" , srcValue !!)
                } else {
                    sharePopUp(ebuyprice_txt.text.toString() , url ,
                            currentDate , userID !! , ebay_earn.text.toString() , "eBay" , srcValue !!)
                }
            }

            if (! baseURL?.contains("https://www.walmart.com/") !! && ! baseURL?.contains("http://www.walmart.com/") !!) {
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    getProductDetailFromWalmarT("https://www.walmart.com/search/?query=$productTittle")
                } else {
                    //Redirect to reliance digital

                    /*if (! isCheckUrl) {
                        if (! baseURL?.contains("https://www.amazon.in/") !! && ! baseURL?.contains("https://www.amazon.com/") !!) {//! baseURL?.contains("https://www.amazon.com/") !!
                            getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
                        }
                    }*/

                    if (! baseURL?.contains("https://www.reliancedigital.in/") !!) {

                        /*mActivity.runOnUiThread {
                            if (progressDialog.dialog.isShowing) {
                                progressDialog.dialog.dismiss()
                            }
                        }*/

                        getRelianceDigitalRedirect("https://www.reliancedigital.in/search?q=$productTittle")
                    }
                }
            }
            mActivity.runOnUiThread {
                if (progressDialog.dialog.isShowing) {
                    progressDialog.dialog.dismiss()
                }
            }
        }
    }

    private fun getProductDetailFromWalmarT(tittle: String) {

        activity?.runOnUiThread {
            cv_walmart.visibility = View.VISIBLE

            var srcValue: String? = null

            val docMacys = Jsoup.connect(tittle).get()
            val brandname =
                    docMacys.getElementsByClass("product-title-link line-clamp line-clamp-2 truncate-title")
            for (i in 0 until brandname.size) {
                walmart_name.text = brandname[0].text()
            }

            if (! walmart_name.text.toString().isEmpty()) {
                walmart_name.setShowingLine(3)
                walmart_name.addShowMoreText("more")
                walmart_name.addShowLessText("Less")
                walmart_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                walmart_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))
            }

            val price = docMacys.getElementsByClass("price-group")
            for (i in 0 until price.size) {
                walmartyprice_txt.text = price[0].text()
            }

            if (docMacys.getElementsByClass("orientation-square") != null) {
                val productImage = docMacys.getElementsByClass("orientation-square")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    srcValue = imageElement.attr("src")
                    activity?.runOnUiThread {
                        Glide.with(activity !!).load(srcValue).into(ivwalmartProduct)
                    }
                }
            } else if (docMacys.getElementsByClass("s-item__image-wrapper") != null) {
                val productImage = docMacys.getElementsByClass("s-item__image-wrapper")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()

                if (imageElement != null) {
                    val srcValue = imageElement.attr("src")
                    activity?.runOnUiThread {
                        Glide.with(activity !!).load(srcValue).into(ivwalmartProduct)
                    }
                }
            }

            val earnprice = walmartyprice_txt.text.toString()
            if (earnprice.isNotEmpty() && earnprice != null) {
                callPercentageAPI(walmartyprice_txt.text.toString() , walmart_earn , "Walmart")
            }

            if (walmart_name.text.toString().isEmpty() && walmartyprice_txt.text.toString().isEmpty()) {
                cv_walmart.visibility = View.GONE
            }

            cv_walmart.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(walmartyprice_txt.text.toString() , tittle ,
                            currentDate , userID !! , walmart_earn.text.toString() , "Walmart" , srcValue !!)
                } else {
                    sharePopUp(walmartyprice_txt.text.toString() , tittle ,
                            currentDate , userID !! , walmart_earn.text.toString() , "Walmart" , srcValue !!)
                }
            }

            if (! baseURL?.contains("https://www.newegg.com/") !!) {
                getNewEgg("https://www.newegg.com/p/pl?d=$productTittle")
            }

            if (progressDialog.dialog.isShowing) {
                progressDialog.dialog.dismiss()
            }
        }
    }

    private fun getNewEgg(tittle: String) {

        activity?.runOnUiThread {
            cv_newEgg.visibility = View.VISIBLE

            var srcValue: String? = null

            val docMacys = Jsoup.connect(tittle).get()
            val brandname =
                    docMacys.getElementsByClass("item-title")
            for (i in 0 until brandname.size) {
                newEggname.text = brandname[0].text()
            }

            if (newEggname.text.toString().isNotEmpty()) {
                newEggname.setShowingLine(3)
                newEggname.addShowMoreText("more")
                newEggname.addShowLessText("Less")
                newEggname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                newEggname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))
            }

            val price = docMacys.getElementsByClass("price-current ")
            for (i in 0 until price.size) {
                newEggprice_txt.text = price[0].text().trim().replace("–" , "")
            }

            if (docMacys.getElementsByClass("item-img") != null) {
                val productImage = docMacys.getElementsByClass("item-img")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    srcValue = imageElement.attr("src")
                    activity?.runOnUiThread {
                        Glide.with(activity !!).load(srcValue).into(ivnewEggProduct)
                    }
                }
            }

            var earnprice = newEggprice_txt.text.toString()

            if (earnprice.contains("(")) {
                val currentstring = earnprice.split("(")
                val url1 = currentstring[0]
                earnprice = url1.trim().trim().replace("(" , "")
            }

            if (earnprice.isNotEmpty() && earnprice != null) {
                callPercentageAPI(earnprice , earn_newEgg_tv , "newegg")
            }

            if (newEggname.text.toString().isEmpty() && newEggprice_txt.text.toString().isEmpty()) {
                cv_newEgg.visibility = View.GONE
            }

            cv_newEgg.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(newEggprice_txt.text.toString() , tittle ,
                            currentDate , userID !! , earn_newEgg_tv.text.toString() , "newegg" , srcValue !!)
                } else {
                    sharePopUp(newEggprice_txt.text.toString() , tittle ,
                            currentDate , userID !! , earn_newEgg_tv.text.toString() , "newegg" , srcValue !!)
                }
            }


            if (! baseURL?.contains("https://www.bhphotovideo.com/") !!) {
                getBandH("https://www.bhphotovideo.com/c/search?Ntt=$productTittle")
            }

            if (progressDialog.dialog.isShowing) {
                progressDialog.dialog.dismiss()
            }
        }


    }

    private fun getBandH(tittle: String) {

        activity?.runOnUiThread {
            var srcValue: String? = null
            var bandh: Document? = null
            try {
                bandh = Jsoup.connect(tittle).userAgent("Mozilla").get()
            } catch (e: Exception) {
                Log.e("targetException" , "targetException" + e.localizedMessage)
            }

            if (bandh != null) {
                cv_BandH.visibility = View.VISIBLE

                if (bandh.getElementsByClass("title_ip0F69brFR7q991bIVYh1") != null) {
                    val brandname = bandh.getElementsByClass("title_ip0F69brFR7q991bIVYh1")
                    for (i in 0 until brandname.size) {
                        BandHname.text = brandname[0].text()
                    }
                }

                if (BandHname.text.toString().isNotEmpty()) {
                    BandHname.setShowingLine(3)
                    BandHname.addShowMoreText("more")
                    BandHname.addShowLessText("Less")
                    BandHname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                    BandHname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))
                }

                if (bandh.getElementsByClass("container_14EdEmSSsYmuetz3imKuAI") != null) {
                    val price = bandh.getElementsByClass("container_14EdEmSSsYmuetz3imKuAI")
                    for (i in 0 until price.size) {
                        //BandHprice_txt.text = price[0].text()

                        val s = price[0].text()
                        BandHprice_txt.text = s.substring(0 , s.length - 2)
                        // println(s.substring(0 , s.length - 2))
                    }
                }

                if (bandh.getElementsByClass("image_1RwLvcJ70jHSVLOALM4UoW") != null) {
                    val productImage = bandh.getElementsByClass("image_1RwLvcJ70jHSVLOALM4UoW")
                    val docs = Jsoup.parse(productImage.toString())
                    val imageElement = docs.select("img").first()
                    if (imageElement != null) {
                        srcValue = imageElement.attr("src")
                        activity?.runOnUiThread {
                            Glide.with(activity !!).load(srcValue).into(ivBandHProduct)
                        }
                    }
                }

                val earnprice = BandHprice_txt.text.toString()
                if (earnprice.isNotEmpty() && earnprice != null) {
                    callPercentageAPI(BandHprice_txt.text.toString() , earn_BandH_tv , "B&H")
                }

                if (BandHname.text.toString().isEmpty() && BandHprice_txt.text.toString().isEmpty()) {
                    cv_BandH.visibility = View.GONE
                }

                cv_BandH.setOnClickListener {
                    val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                    val currentDate = sdf.format(Date())
                    if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                        sharePopUpOutside(BandHprice_txt.text.toString() , tittle ,
                                currentDate , userID !! , earn_BandH_tv.text.toString() , "B&H" , srcValue !!)
                    } else {
                        sharePopUp(BandHprice_txt.text.toString() , tittle ,
                                currentDate , userID !! , earn_BandH_tv.text.toString() , "B&H" , srcValue !!)

                    }
                }
            }

            if (! isCheckUrl) {
                if (! baseURL?.contains("https://www.amazon.com/") !!) {
                    getProductDEtailFromAmazon(
                            "https://www.amazon.com/s?k=$productTittle".replace(
                                    "(AT&T)" ,
                                    ""
                            ).replace("(GSM)" , "").replace("(CDMA + GSM)" , "").replace(
                                    "AT&T" ,
                                    ""
                            ).replace("Verizon" , "")
                    )
                }
            }

            if (progressDialog.dialog.isShowing) {
                progressDialog.dialog.dismiss()
            }
        }
    }

    private fun getTargetLink(url: String) {

        activity?.runOnUiThread {
            val targetLink = Jsoup.connect(url).get()

            val targetLRedirectLink = targetLink.getElementsByClass("Link-sc-1khjl8b-0 dJwaza h-display-block")
            val link = targetLink.select("a")
            val urllink = link.attr("href")

            Log.e("urllink" , "urllink==>$urllink")

            for (i in 0 until targetLRedirectLink.size) {
                val baseUR = targetLRedirectLink[0].text()
                Log.e("targetLink" , "targetLink==>$baseUR")
            }
        }
    }

    private fun getBestBuyLink(url: String) {

        activity?.runOnUiThread {
            val targetLink = Jsoup.connect(url).get()

            val targetLRedirectLink = targetLink.getElementsByClass("image-link")
            for (i in 0 until targetLRedirectLink.size) {
                val baseUR = targetLRedirectLink[0].text()
                Log.e("targetLink" , "targetLink==>$baseUR")
            }
        }
    }

    private fun getProductDetailTarget(url: String) {

        activity?.runOnUiThread {
            cv_target.visibility = View.VISIBLE

            var srcValue: String? = null

            val target = Jsoup.connect(url).get()

            val productNameAmzon = target.getElementsByClass("Link-sc-1khjl8b-0 styles__StyledTitleLink-mkgs8k-5 dJwaza jqiYMz h-display-block h-text-bold h-text-bs flex-grow-one")

            for (i in 0 until productNameAmzon.size) {
                targetname.text = productNameAmzon[0].text()
            }

            targetname.setShowingLine(3)
            targetname.addShowMoreText("more")
            targetname.addShowLessText("Less")
            targetname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
            targetname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

            val productNamePrice = target.getElementsByClass("h-text-bs h-text-red")

            for (i in 0 until productNamePrice.size) {
                targetprice_txt.text = productNamePrice[0].text()
            }

            if (target.getElementsByClass("ItemPictureContainer-quh9jv-0 jDXhgT") != null) {
                val productImage = target.getElementsByClass("ItemPictureContainer-quh9jv-0 jDXhgT")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    srcValue = imageElement.attr("src")
                    Glide.with(activity !!).load(srcValue).into(ivtargetProduct)
                }
            }

            cv_target.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                sharePopUpOutside(targetprice_txt.text.toString() , url ,
                        currentDate , userID !! , targetname.text.toString() , "NewEgg" , srcValue !!)
            }

            if (targetname.text.isEmpty() && targetprice_txt.text.isEmpty()) {
                cv_target.visibility = View.GONE
            }

            val price = targetprice_txt.text.toString()
            if (price.isNotEmpty()) {
                callPercentageAPI(targetprice_txt.text.toString() , earn_target_tv , "NewEgg")
            }

            // getProductDEtailNewEgg("https://www.target.com/s?searchTerm=$productTittle")
            if (! baseURL?.contains("https://www.ebay.com/") !!) {
                val getTittle = getFirstNStrings(productTittle !! , 5)
                getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
            }

        }
    }

    private fun getProductDEtailFromAmazon(url: String) {

        activity?.runOnUiThread {
            cv_amazon.visibility = View.VISIBLE

            var srcValue: String? = null

            val amazon = Jsoup.connect(url).get()

            val productNameAmzon = amazon.getElementsByClass("a-link-normal a-text-normal")

            for (i in 0 until productNameAmzon.size) {
                amazon_name.text = productNameAmzon[0].text()
            }

            amazon_name.setShowingLine(3)
            amazon_name.addShowMoreText("more")
            amazon_name.addShowLessText("Less")
            amazon_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
            amazon_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

            val productNamePrice = amazon.getElementsByClass("a-offscreen")

            for (i in 0 until productNamePrice.size) {
                amazonprice_txt.text = productNamePrice[0].text()
            }

            if (amazon.getElementsByClass("s-image") != null) {
                val productImage = amazon.getElementsByClass("s-image")
                val docs = Jsoup.parse(productImage.toString())
                val imageElement = docs.select("img").first()
                if (imageElement != null) {
                    srcValue = imageElement.attr("src")
                    Glide.with(activity !!).load(srcValue).into(ivamazonProduct)
                }
            }

            cv_amazon.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(amazonprice_txt.text.toString() , url ,
                            currentDate , userID !! , amazon_name.text.toString() , "Amazon" , srcValue !!)
                } else {
                    sharePopUp(amazonprice_txt.text.toString() , url ,
                            currentDate , userID !! , amazon_name.text.toString() , "Amazon" , srcValue !!)
                }
            }

            if (amazon_name.text.isEmpty() && amazonprice_txt.text.isEmpty()) {
                cv_amazon.visibility = View.GONE
            }

            val price = amazonprice_txt.text.toString()
            if (price.isNotEmpty()) {
                callPercentageAPI(amazonprice_txt.text.toString() , amazon_earn , "Amazon")
            }

            if (! baseURL?.contains("https://www.ebay.com/") !!) {
                val getTittle = getFirstNStrings(productTittle !! , 5)
                getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
            }

        }
    }

    companion object {

        fun getInstance(): RefferalFragment {
            val fragment = RefferalFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }

        class MyAsyncTask(applicationContext: Activity , getProductDetails: GetProductDetails , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {

            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null
            private var cardRefer: androidx.cardview.widget.CardView? = null
            var getProductDetails: GetProductDetails? = getProductDetails
            private var logo: ImageView? = null
            private var flip_iv: ImageView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                flip_iv = activity.findViewById(R.id.ivflipProduct)
                cardProduct = activity.findViewById(R.id.cv_product)
                cardRefer = activity.findViewById(R.id.cv_refer)
                logo = activity.findViewById(R.id.logo)
                description = activity.findViewById(R.id.description_tv)

            }

            override fun onPostExecute(result: String?) {
                /* if (progressDialog !!.dialog.isShowing) {
                     progressDialog !!.dialog.dismiss()
                 }*/


                cardProduct?.visibility = View.VISIBLE
                description?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    if (imageUrl !!.contains("base64")) {
                        Glide.with(activity).load(Base64.decode(imageUrl , DEFAULT)).into(image !!)
                        Glide.with(activity).load(Base64.decode(imageUrl , DEFAULT)).into(flip_iv !!)

                    } else {
                        Glide.with(activity).load(imageUrl).into(image !!)
                        Glide.with(activity).load(imageUrl).into(flip_iv !!)
                    }
                } else {
                    imageUrl = "No Image Found"
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductURlFromAmazon(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , pricetv?.text.toString() , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.amazon)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {

                    val doc = Jsoup.connect(baseURL)
                            .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36").get()

                    if (doc.getElementById("productTitle") != null) {
                        doc.getElementById("productTitle").childNodes().forEach {
                            productname = it.toString()
                        }
                    }

                    when {
                        doc.getElementById("priceblock_ourprice") != null -> {
                            doc.getElementById("priceblock_ourprice").childNodes().forEach {
                                productPrice = it.toString().replace("&nbsp;" , "")
                            }
                        }
                        doc.getElementById("priceblock_dealprice") != null -> {
                            doc.getElementById("priceblock_dealprice").childNodes().forEach {
                                productPrice = it.toString().replace("&nbsp;" , "")
                            }
                        }
                        doc.getElementById("priceblock_saleprice") != null -> {
                            doc.getElementById("priceblock_saleprice").childNodes().forEach {
                                productPrice = it.toString().replace("&nbsp;" , "")
                            }
                        }
                    }

                    if (doc.getElementById("img-canvas") != null) {
                        val image = doc.getElementById("img-canvas")
                        val docs = Jsoup.parse(image.toString())
                        val imageElement = docs.select("img").first()
                        if (imageElement != null) {
                            val srcValue = imageElement.attr("data-a-dynamic-image")

                            val imageValue = srcValue.split(".jpg")
                            val url1 = imageValue[0]
                            val finalURL = url1.trim().trim().replace("{" , "") + ".jpg"
                            imageUrl = finalURL.replace("\"" , "")
                        }
                    } else if (doc.getElementsByClass("a-column a-span3 a-spacing-micro imageThumb thumb") != null) {
                        val productImage = doc.getElementsByClass("a-column a-span3 a-spacing-micro imageThumb thumb")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        if (imageElement != null) {
                            val srcValue = imageElement.attr("src")
                            imageUrl = srcValue
                        }
                    } else if (doc.getElementById("imgTagWrapperId") != null) {
                        val image = doc.getElementById("imgTagWrapperId")
                        val docs = Jsoup.parse(image.toString())

                        val imageElement = docs.select("img").first()
                        if (imageElement != null) {
                            val srcValue = imageElement.attr("data-old-hires")
                            imageUrl = srcValue
                        }
                    } else if (doc.getElementsByClass("mainImage") != null) {
                        val productImage = doc.getElementsByClass("mainImage")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        if (imageElement != null) {
                            val srcValue = imageElement.attr("src")
                            imageUrl = srcValue
                        }
                    } else if (doc.getElementsByClass("a-image-wrapper a-lazy-loaded a-manually-loaded immersive-carousel-img-manual-load") != null) {
                        val productImage = doc.getElementsByClass("a-image-wrapper a-lazy-loaded a-manually-loaded immersive-carousel-img-manual-load")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        if (imageElement != null) {
                            val srcValue = imageElement.attr("src")
                            imageUrl = srcValue
                        }
                    }

                    if (imageUrl == null) {
                        if (doc.getElementsByClass("imgTagWrapper") != null) {
                            val productImage = doc.getElementsByClass("imgTagWrapper")
                            val docs = Jsoup.parse(productImage.toString())
                            val imageElement = docs.select("img").first()
                            if (imageElement != null) {
                                val srcValue = imageElement.attr("data-a-dynamic-image")

                                val imageValue = srcValue.split(".jpg")
                                val url1 = imageValue[0]
                                val finalURL = url1.trim().trim().replace("{" , "") + ".jpg"
                                imageUrl = finalURL.replace("\"" , "")
                            }
                        }
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetailFromWalmart(applicationContext: Activity , getProductDetails: GetProductDetailsWalmart , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {

            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GetProductDetailsWalmart? = getProductDetails
            private var logo: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {
                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                Log.e("productname" , "productname" + productname)

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductURlFromWalmart(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.walmart)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {

                    val WalMartConnect = Jsoup.connect(baseURL).get()

                    val productNameWalMart = WalMartConnect.getElementsByClass("prod-ProductTitle prod-productTitle-buyBox font-bold")

                    for (i in 0 until productNameWalMart.size) {
                        productname = productNameWalMart[0].text()
                    }

                    val productPrice1 = WalMartConnect.getElementsByClass("price-group")
                    for (i in 0 until productPrice1.size) {
                        productPrice = productPrice1[0].text()
                    }

                    if (WalMartConnect.getElementsByClass("larger-hero-image-carousel-image") != null) {
                        val productImage = WalMartConnect.getElementsByClass("larger-hero-image-carousel-image")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")
                        imageUrl = "https:$srcValue"
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetaileBay(applicationContext: Activity , geteBayProductDetails: GeteBayProductDetails , progressDialog: CustomProgressDialog , countryCode: String) : AsyncTask<String , String , String?>() {

            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var country: String? = countryCode
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GeteBayProductDetails? = geteBayProductDetails
            private var logo: ImageView? = null
            private var flip_iv: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                flip_iv = activity.findViewById(R.id.ivflipProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {
                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                    Glide.with(activity).load(imageUrl).into(flip_iv !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductURlFromeBay(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , country !! , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.ebay)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {
                    val ebayConnect = Jsoup.connect(baseURL).get()

                    val productNameEbay = ebayConnect.getElementsByClass("product-title")

                    for (i in 0 until productNameEbay.size) {
                        productname = productNameEbay[0].text()
                    }

                    if (ebayConnect.getElementById("itemTitle") != null) {
                        ebayConnect.getElementById("itemTitle").childNodes().forEach {
                            productname = it.toString()
                        }
                    }

                    if (country != "+91") {

                        val productPrice1 = ebayConnect.getElementsByClass("original-price")
                        for (i in 0 until productPrice1.size) {
                            productPrice = productPrice1[0].text()
                        }

                        if (ebayConnect.getElementById("prcIsum") != null) {
                            ebayConnect.getElementById("prcIsum").childNodes().forEach {
                                productPrice = it.toString()
                            }
                        } else if (ebayConnect.getElementById("prcIsum_bidPrice") != null) {
                            ebayConnect.getElementById("prcIsum_bidPrice").childNodes().forEach {
                                productPrice = it.toString()
                            }
                        }
                    } else {
                        when {
                            ebayConnect.getElementById("convbinPrice") != null -> {
                                for (i in 0 until ebayConnect.getElementById("convbinPrice").childNodes().size) {
                                    productPrice = ebayConnect.getElementById("convbinPrice").childNodes()[0].toString()
                                }
                            }
                            ebayConnect.getElementById("convbidPrice") != null -> {
                                for (i in 0 until ebayConnect.getElementById("convbidPrice").childNodes().size) {
                                    productPrice = ebayConnect.getElementById("convbidPrice").childNodes()[0].toString()
                                }
                            }
                            ebayConnect.getElementsByClass("display-price") != null -> {
                                val productNamePrice = ebayConnect.getElementsByClass("display-price")
                                for (i in 0 until productNamePrice.size) {
                                    productPrice = productNamePrice[0].text()
                                }
                            }
                        }
                    }

                    if (ebayConnect.getElementById("icImg") != null) {
                        val productImage = ebayConnect.getElementById("icImg")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")

                        imageUrl = srcValue

                    } else if (ebayConnect.getElementsByClass("vi-image-gallery__image vi-image-gallery__image--absolute-center") != null) {
                        val productImage =
                                ebayConnect.getElementsByClass("vi-image-gallery__image vi-image-gallery__image--absolute-center")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")
                        imageUrl = srcValue
                    }
                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetailFlipKart(applicationContext: Activity , getFlipkartProductDetails: GetProductFlipkart , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {

            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GetProductFlipkart? = getFlipkartProductDetails
            private var logo: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {
                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductURlFlipkart(nametv?.text.toString() , pricetv?.text.toString() , baseURL !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.flip)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {

                    val flipkartDoc = Jsoup.connect(baseURL).get()

                    val productName = flipkartDoc.getElementsByClass("_35KyD6")
                    for (i in 0 until productName.size) {
                        productname = productName[0].text()
                    }

                    val productName1 = flipkartDoc.getElementsByClass("B_NuCI")
                    for (i in 0 until productName1.size) {
                        productname = productName1[0].text()
                    }

                    val productName2 = flipkartDoc.getElementsByClass("_4rR01T")
                    for (i in 0 until productName2.size) {
                        productname = productName2[0].text()
                    }

                    val productPrice1 = flipkartDoc.getElementsByClass("_1vC4OE _3qQ9m1")

                    for (i in 0 until productPrice1.size) {
                        productPrice = productPrice1[0].text()
                    }

                    val productPrice2 = flipkartDoc.getElementsByClass("30jeq3 1_WHN1")
                    for (i in 0 until productPrice2.size) {
                        productPrice = productPrice2[0].text()
                    }

                    val productPrice3 = flipkartDoc.getElementsByClass("_30jeq3 _16Jk6d")
                    for (i in 0 until productPrice3.size) {
                        productPrice = productPrice3[0].text()
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetailNewEgg(applicationContext: Activity , getProductDetails: GetProductNewEgg , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {
            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GetProductNewEgg? = getProductDetails
            private var logo: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {
                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductURlNewEgg(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.newegg)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {
                    val targetDoc = Jsoup.connect(baseURL).get()
                    val productName = targetDoc.getElementsByClass("product-title")
                    for (i in 0 until productName.size) {
                        productname = productName[0].text()
                    }

                    val productPrice1 = targetDoc.getElementsByClass("price-current")
                    for (i in 0 until productPrice1.size) {
                        productPrice = productPrice1[0].text().replace(" –" , "").trim()
                    }

                    if (targetDoc.getElementsByClass("product-view-img-original") != null) {
                        val productImage = targetDoc.getElementsByClass("product-view-img-original")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")
                        imageUrl = srcValue
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetailBandH(applicationContext: Activity , getProductDetails: GetProductBandH , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {
            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GetProductBandH? = getProductDetails
            private var logo: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {

                // progressDialog !!.dialog.dismiss()

                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductBandH(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.bandh)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {
                    val targetDoc = Jsoup.connect(baseURL).get()
                    val productName = targetDoc.getElementsByClass("title1_17KKS47kFEQb7ynVBsRb_5 reset_gKJdXkYBaMDV-W3ignvsP primary_ELb2ysditdCtk24iMBTUs")
                    for (i in 0 until productName.size) {
                        productname = productName[0].text()
                    }

                    val productPrice1 = targetDoc.getElementsByClass("price_1DPoToKrLP8uWvruGqgtaY")
                    for (i in 0 until productPrice1.size) {
                        productPrice = productPrice1[0].text()
                    }

                    if (targetDoc.getElementsByClass("image_2AnnrdYk1brbXAJDpSPf_y") != null) {
                        val productImage = targetDoc.getElementsByClass("image_2AnnrdYk1brbXAJDpSPf_y")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")
                        imageUrl = srcValue
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

        class getDetailBestBuy(applicationContext: Activity , getProductDetails: GetProductBestBuy , progressDialog: CustomProgressDialog) : AsyncTask<String , String , String?>() {
            private var productname: String? = null
            private var productPrice: String? = null
            private var imageUrl: String? = null
            private var baseURL: String? = null
            private var progressDialog: CustomProgressDialog? = progressDialog
            private val activity: Activity = applicationContext
            private var nametv: ShowMoreTextView? = null
            private var description: TextView? = null
            private var pricetv: TextView? = null
            private var image: SimpleDraweeView? = null
            var getProductDetails: GetProductBestBuy? = getProductDetails
            private var logo: ImageView? = null
            private var cardProduct: androidx.cardview.widget.CardView? = null

            override fun onPreExecute() {
                super.onPreExecute()
                progressDialog !!.show(activity)
                nametv = activity.findViewById(R.id.product_name)
                pricetv = activity.findViewById(R.id.price_txt)
                image = activity.findViewById(R.id.ivProduct)
                description = activity.findViewById(R.id.description_tv)
                logo = activity.findViewById(R.id.logo)
                cardProduct = activity.findViewById(R.id.cv_product)
            }

            override fun onPostExecute(result: String?) {

                progressDialog !!.dialog.dismiss()

                description?.visibility = View.VISIBLE
                cardProduct?.visibility = View.VISIBLE
                nametv?.text = productname
                pricetv?.text = productPrice

                nametv !!.setShowingLine(3)
                nametv !!.addShowMoreText("more")
                nametv !!.addShowLessText("Less")
                nametv !!.setShowMoreColor(activity.resources.getColor(R.color.customer_app_color))
                nametv !!.setShowLessTextColor(activity.resources.getColor(R.color.customer_app_color))

                if (imageUrl != null) {
                    Glide.with(activity).load(imageUrl).into(image !!)
                }

                if (getProductDetails != null) {
                    getProductDetails !!.getProductBestBuy(nametv?.text.toString() , pricetv?.text.toString() , baseURL !! , imageUrl !!)
                }

                Glide.with(activity).load(ContextCompat.getDrawable(activity , R.drawable.bestbuy)).into(logo !!)
            }

            override fun doInBackground(vararg params: String?): String? {
                baseURL = params[0]
                try {
                    val targetDoc = Jsoup.connect(baseURL)
                            .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36").get()
                    val productName = targetDoc.getElementsByClass("heading-5 v-fw-regular")
                    for (i in 0 until productName.size) {
                        productname = productName[0].text()
                    }

                    val productPrice1 = targetDoc.getElementsByClass("priceView-hero-price priceView-customer-price")
                    for (i in 0 until productPrice1.size) {
                        productPrice = productPrice1[0].text()
                    }

                    if (targetDoc.getElementsByClass("primary-image") != null) {
                        val productImage = targetDoc.getElementsByClass("primary-image")
                        val docs = Jsoup.parse(productImage.toString())
                        val imageElement = docs.select("img").first()
                        val srcValue = imageElement.attr("src")
                        imageUrl = srcValue
                    }

                } catch (e: Exception) {
                    Log.e("exception" , "exception" + e.message)
                }
                return null
            }
        }

    }

    var srcValueAmazon: String? = null

    private fun getProductAmazonIndia(url: String) {

        activity?.runOnUiThread {
            //progressDialog.show(mActivity)
            cv_amazon_in.visibility = View.VISIBLE



            try {
                val amazon = Jsoup.connect(url).get()

                val productNameAmzon = amazon.getElementsByClass("a-link-normal a-text-normal")
                for (i in 0 until productNameAmzon.size) {
                    amazon_in_name.text = productNameAmzon[0].text()
                }

                val productNameAmzon1 = amazon.getElementsByClass("a-size-medium a-color-base a-text-normal")
                for (i in 0 until productNameAmzon1.size) {
                    amazon_in_name.text = productNameAmzon1[0].text()
                }

                if (amazon_in_name.text.toString().isNotEmpty()) {
                    amazon_in_name.setShowingLine(3)
                    amazon_in_name.addShowMoreText("more")
                    amazon_in_name.addShowLessText("Less")
                    amazon_in_name.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                    amazon_in_name.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))
                }

                val productNamePrice = amazon.getElementsByClass("a-offscreen")

                for (i in 0 until productNamePrice.size) {
                    amazon_inprice_txt.text = productNamePrice[0].text()
                }

                if (amazon.getElementsByClass("s-image") != null) {
                    val productImage = amazon.getElementsByClass("s-image")
                    val docs = Jsoup.parse(productImage.toString())
                    val imageElement = docs.select("img").first()
                    if (imageElement != null) {
                        srcValueAmazon = imageElement.attr("src")
                        Glide.with(activity !!).load(srcValueAmazon).into(ivamazon_inProduct)
                        Glide.with(activity !!).load(srcValueAmazon).into(ivProduct)
                        imageURLAmazon = srcValueAmazon
                    }
                }

                val price = amazon_inprice_txt.text.toString()
                if (price.isNotEmpty()) {
                    callPercentageAPI(amazon_inprice_txt.text.toString() , earn_amazon_in , "Amazon")
                }

                if (amazon_in_name.text.isEmpty() && amazon_inprice_txt.text.isEmpty()) {
                    cv_amazon_in.visibility = View.GONE
                }

            } catch (e: Exception) {
                Log.e("amazonException" , "amazonException" + e.message)
            }
            cv_amazon_in.setOnClickListener {
                val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                val currentDate = sdf.format(Date())
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(amazon_inprice_txt.text.toString() , url , currentDate , userID !! , earn_amazon_in.text.toString() , "Amazon.in" , srcValueAmazon !!)
                } else {
                    sharePopUp(amazon_inprice_txt.text.toString() , url , currentDate , userID !! , earn_amazon_in.text.toString() , "Amazon.in" , srcValueAmazon !!)
                }
            }
        }

        if (progressDialog.dialog.isShowing) {
            progressDialog.dialog.dismiss()
        }


        if (! baseURL?.contains("http://dl.flipkart.com/") !! && ! baseURL !!.contains("https://dl.flipkart.com/")) {
            getProductFlipkart("https://www.flipkart.com/search?q=$productTittle")

        }
    }

    private fun getProductFlipkart(url: String) {
        try {
            activity?.runOnUiThread {
                //progressDialog.show(mActivity)
                cv_flip.visibility = View.VISIBLE

                val flipkart = Jsoup.connect(url).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36").get()
                val productName = flipkart.getElementsByClass("_2cLu-l")

                for (i in 0 until productName.size) {
                    flipname.text = productName[0].text()
                }

                flipname.setShowingLine(3)
                flipname.addShowMoreText("more")
                flipname.addShowLessText("Less")
                flipname.setShowMoreColor(activity !!.resources.getColor(R.color.customer_app_color))
                flipname.setShowLessTextColor(activity !!.resources.getColor(R.color.customer_app_color))

                val productName1 = flipkart.getElementsByClass("_3wU53n")

                for (i in 0 until productName1.size) {
                    activity?.runOnUiThread {
                        flipname.text = productName1[0].text()
                    }
                }
                val productName2 = flipkart.getElementsByClass("s1Q9rs")

                for (i in 0 until productName2.size) {
                    activity?.runOnUiThread {
                        flipname.text = productName2[0].text()
                    }
                }

                val productName3 = flipkart.getElementsByClass("_4rR01T")

                for (i in 0 until productName3.size) {
                    activity?.runOnUiThread {
                        flipname.text = productName3[0].text()
                    }
                }

                val productPrice = flipkart.getElementsByClass("_1vC4OE")

                for (i in 0 until productPrice.size) {
                    activity?.runOnUiThread {
                        flipprice_txt.text = productPrice[0].text()
                    }
                }

                val productPrice1 = flipkart.getElementsByClass("_1vC4OE _2rQ-NK")

                for (i in 0 until productPrice1.size) {
                    activity?.runOnUiThread {
                        flipprice_txt.text = productPrice1[0].text()
                    }
                }

                val productPrice2 = flipkart.getElementsByClass("_30jeq3")

                for (i in 0 until productPrice2.size) {
                    activity?.runOnUiThread {
                        flipprice_txt.text = productPrice2[0].text()
                    }
                }

                val price = flipprice_txt.text.toString()
                if (price.isNotEmpty()) {
                    callPercentageAPI(flipprice_txt.text.toString() , earn_flip_tv , "Flipkart")
                }

                cv_flip.setOnClickListener {
                    val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                    val currentDate = sdf.format(Date())
                    if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                        sharePopUpOutside(flipprice_txt.text.toString() , url ,
                                currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart" , srcValueAmazon !!)
                    } else {
                        sharePopUp(flipprice_txt.text.toString() , url ,
                                currentDate , userID !! , earn_flip_tv.text.toString() , "Flipkart" , srcValueAmazon !!)
                    }
                }


                if (productName.size == 0 && productName1.size == 0 && productName2.size == 0 && productPrice.size == 0 && productPrice1.size == 0 && productName3.size == 0 && productPrice2.size == 0) {
                    cv_flip.visibility = View.GONE
                }

                if (! baseURL?.contains("https://www.ebay.com/") !! && ! baseURL?.contains("https://www.amazon.com/") !!) {
                    if (productTittle != null) {
                        val getTittle = getFirstNStrings(productTittle !! , 5)
                        // val s = URLDecoder.decode("https://www.ebay.com/sch/i.html?_nkw=${getTittle}" , "UTF-8")
                        getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
                    }
                }

                mActivity.runOnUiThread {
                    if (progressDialog.dialog.isShowing) {
                        progressDialog.dialog.dismiss()
                    }
                }
            }
        } catch (e: Exception) {
            cv_flip.visibility = View.INVISIBLE
            Log.e("exception" , "exception" + e.message)
        }
    }

    var imageURLAmazon: String? = null
    override fun getProductURlFromAmazon(name: String , price: String , url: String , earn: String , image: String) {


        /* if (image.equals("No Image Found")){
             callImageURLAPI(url)
         }else{
             imageURLAmazon = image
         }*/

        imageURLAmazon = image
        srcValueAmazon = image

        val newPrice: String
        val earnValue: String

        if (price.isEmpty()) {
            newPrice = "0"
            price_txt.visibility = View.GONE
            no_price_txt.visibility = View.VISIBLE
            no_price_txt_nxt.visibility = View.VISIBLE
        } else {
            newPrice = price
        }

        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (! price.isEmpty()) {
                if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                    sharePopUpOutside(newPrice , url , currentDate , userID !! , earn_txt.text.toString() , "Amazon.com" , imageURLAmazon !!)
                } else {
                    sharePopUp(newPrice , url , currentDate , userID !! , earn_txt.text.toString() , "Amazon India" , imageURLAmazon !!)
                }
            } else {
                val browserIntent = Intent(Intent.ACTION_VIEW , Uri.parse("$url/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarketindia-21"))
                startActivity(browserIntent)
            }
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        if (price.isNotEmpty()) {
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                callPercentageAPI(price , earn_txt , "Amazon.com")
            } else {
                callPercentageAPI(price , earn_txt , "Amazon India")
            }
        }

        if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
            if (! baseURL?.contains("https://www.ebay.com/") !!) {
                //  progressDialog.show(mActivity)
                if (productTittle != null) {
                    val getTittle = getFirstNStrings(productTittle !! , 5)
                    getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
                }
            }
        } else {
            //val getTittle = getFirstNStrings(productTittle !! , 3)
            if (name.isNotEmpty()) {
                //val getTittle = getFirstNStrings(productTittle !! , 3)
                getProductFlipkart("https://www.flipkart.com/search?q=" + productTittle?.replace("%" , "%25")?.replace("(" , "")?.replace(")" , ""))
            } else {
                cv_product.visibility = View.INVISIBLE
                callProductListAPIAmazon(baseURL !!)
            }
        }
    }

    private fun sharePopUpOutside(price: String , productUrl: String ,
                                  clicked: String , userID: String , earn: String , merchantName: String , imageURL: String) {
        isCheckPopUp = true
        /* val alertDialog = AlertDialog.Builder(mActivity).create()
         val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
         alertDialog.setView(dialogView)
         alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

         val text = dialogView.findViewById<TextView>(R.id.tv_text)
         val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
         val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
         val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
         val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)

         val typeface = ResourcesCompat.getFont(mActivity , R.font.stencil_std_bold) !!
         val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari) !!

         val spannable = CustomTypefaceSpan("" , typeface)
         val adobeFont = CustomTypefaceSpan("" , adobe)

         val spannableString = SpannableString(resources.getString(R.string.each_time))
         val rewardString = SpannableString(resources.getString(R.string.refer_friends))

         spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
         spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
         spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
         spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

         rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
         rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

         tv_life.text = spannableString
         reward.text = rewardString

         text.setOnClickListener {
             try {
                 mActivity.addFragment(RecommendFriendsFragment.getInstance()
                         , true , animationType = AnimationType.RightInZoomOut)
             } catch (e: Exception) {
                 Log.e("" , "" + e.message)
             }
             alertDialog.dismiss()
         }
         messanger.setOnClickListener {
             val intent = Intent()
             intent.action = Intent.ACTION_SEND
             intent.type = "text/plain"
             intent.putExtra(Intent.EXTRA_TEXT ,
                     mActivity.resources.getString(R.string.general_invite_flow_message ,
                             "https://rb.gy/0otkjs" , mActivity.userDetails?.name ,
                             mActivity.userDetails?.countryCode , mActivity.userDetails?.mobile , mActivity.userDetails?.referral_code))

             *//*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                    "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*//*
            val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            //startActivity(intent)
            startActivity(Intent.createChooser(intent , "Share"));
        }

        btnViewSite.setOnClickListener {
            alertDialog.dismiss()
            callUserActivityAPI(productTittle !! , price , productUrl , clicked , userID , earn , merchantName , imageURL)
        }

        alertDialog.show()*/

        mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)

            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))
            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            }

            btnViewSite.setOnClickListener {
                alertDialog.dismiss()
                callUserActivityAPI(productTittle !! , price , productUrl , clicked , userID , earn , merchantName , imageURL)
            }

            alertDialog.show()
        }
    }

    private fun sharePopUp(price: String , productUrl: String ,
                           clicked: String , userID: String , earn: String , merchantName: String , imageURL: String) {
        //isCheckPopUp = true
        /*val alertDialog = AlertDialog.Builder(mActivity).create()
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
        alertDialog.setView(dialogView)
        val text = dialogView.findViewById<TextView>(R.id.tv_text)
        val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
        val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
        val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
        val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)

        val typeface = ResourcesCompat.getFont(mActivity , R.font.stencil_std_bold) !!
        val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari) !!

        val spannable = CustomTypefaceSpan("" , typeface)
        val adobeFont = CustomTypefaceSpan("" , adobe)

        val spannableString = SpannableString(resources.getString(R.string.each_time))
        val rewardString = SpannableString(resources.getString(R.string.refer_friends))

        spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        tv_life.text = spannableString
        reward.text = rewardString

        text.setOnClickListener {
            try {
                mActivity.addFragment(RecommendFriendsFragment.getInstance()
                        , true , animationType = AnimationType.RightInZoomOut)
            } catch (e: Exception) {
                Log.e("" , "" + e.message)
            }
            alertDialog.dismiss()
        }
        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"
            *//* intent.putExtra(Intent.EXTRA_TEXT ,
                     mActivity.resources.getString(R.string.general_invite_flow_message ,
                             "https://rb.gy/0otkjs" , mActivity.userDetails?.name ,
                             mActivity.userDetails?.countryCode , mActivity.userDetails?.mobile , mActivity.userDetails?.referral_code))*//*

            val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                    "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            //startActivity(intent)
            startActivity(Intent.createChooser(intent , "Share"));
        }*/
        isCheckPopUp = true

        val alertDialog = AlertDialog.Builder(mActivity).create()
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share_new , null)
        alertDialog.setView(dialogView)
        alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val messanger = dialogView.findViewById<Button>(R.id.btnShare)
        val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)

        val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
        val adobeFont = CustomTypefaceSpan("" , adobe)

        val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
        val spannable = CustomTypefaceSpan("" , typeface)

        val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
        val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
        val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
        val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

        val first = SpannableString("₹50\nCash Rewards")
        first.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvFirst.text = first

        val second = SpannableString("₹20\nBonus Cash")
        second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvSecond.text = second

        val third = SpannableString("30%\nCash Rewards")
        third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvThird.text = third

        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"

            /*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                    "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*/

            val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            startActivity(Intent.createChooser(intent , "Share"))

        }

        tvFAQ.setOnClickListener {
            alertDialog.dismiss()
            mActivity.addFragment(FAQsFragment.getInstance()
                    , true , animationType = AnimationType.RightInZoomOut)
        }

        btnViewSite.setOnClickListener {
            alertDialog.dismiss()
            callUserActivityAPI(productTittle !! , price , productUrl , clicked , userID , earn , merchantName , imageURL)
        }

        alertDialog.show()
    }

    private fun sharePopUpWithoutHeader() {
        mActivity.runOnUiThread {
            isCheckPopUp = true
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share_new , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)
            btnViewSite.visibility = View.GONE

            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val first = SpannableString("₹50\nCash Rewards")
            first.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvFirst.text = first

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"

                /*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                        "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*/

                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))

            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance()
                        , true , animationType = AnimationType.RightInZoomOut)
            }

            alertDialog.show()
        }
    }

    private fun sharePopUpWithoutHeaderOutsideIndia() {
        /* mActivity.runOnUiThread {
             isCheckPopUp = true
             val alertDialog = AlertDialog.Builder(mActivity).create()
             val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
             alertDialog.setView(dialogView)
             alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
             val text = dialogView.findViewById<TextView>(R.id.tv_text)
             val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
             val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
             val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
             val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)
             btnViewSite.visibility = View.GONE

             val typeface = ResourcesCompat.getFont(mActivity , R.font.stencil_std_bold) !!
             val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari) !!

             val spannable = CustomTypefaceSpan("" , typeface)
             val adobeFont = CustomTypefaceSpan("" , adobe)

             val spannableString = SpannableString(resources.getString(R.string.each_time))
             val rewardString = SpannableString(resources.getString(R.string.refer_friends))

             spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
             spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
             spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
             spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

             rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
             rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

             tv_life.text = spannableString
             reward.text = rewardString

             text.setOnClickListener {
                 try {
                     mActivity.addFragment(RecommendFriendsFragment.getInstance(IS_FROM_DRAWER)
                             , true , animationType = AnimationType.RightInZoomOut)
                 } catch (e: Exception) {
                     Log.e("" , "" + e.message)
                 }
                 alertDialog.dismiss()
             }
             messanger.setOnClickListener {
                 val intent = Intent()
                 intent.action = Intent.ACTION_SEND
                 intent.type = "text/plain"

                 *//*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                        "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*//*
                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))

            }
            alertDialog.show()
        }*/

        mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)
            btnViewSite.visibility = View.GONE

            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))
            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            }

            alertDialog.show()
        }
    }

    override fun getProductURlFromeBay(name: String , price: String , url: String , country: String , image: String) {
        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "eBay" , image)
            } else {
                sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "eBay" , image)
            }
        }

        if (price.isNotEmpty()) {
            callPercentageAPI(price , earn_txt , "eBay")
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        if (country != "+91") {
            getProductDetailFromWalmarT("https://www.walmart.com/search/?query=$productTittle")
        } else {
            //getRelianceDigitalRedirect("https://www.reliancedigital.in/search?q=$productTittle")
            isCheckUrl = true
            getProductAmazonIndia("https://www.amazon.in/s?k=$productTittle")
        }
    }

    override fun getProductURlFromWalmart(name: String , price: String , url: String , image: String) {

        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "Walmart" , image)
            } else {
                sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "Walmart" , image)
            }
        }

        if (price.isNotEmpty()) {
            callPercentageAPI(price , earn_txt , "Walmart")
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        if (! baseURL?.contains("https://www.newegg.com/") !!) {
            getNewEgg("https://www.newegg.com/p/pl?d=$productTittle")
        }
    }

    override fun getProductURlFlipkart(name: String , price: String , url: String) {
        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                if (imageURLAmazon != null) {
                    sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "Flipkart" , imageURLAmazon !!)
                } else {
                    sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "Flipkart" , "null")
                }
            } else {
                if (imageURLAmazon != null) {
                    sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "Flipkart" , imageURLAmazon !!)
                } else {
                    sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "Flipkart" , "null")
                }
            }
        }

        if (price.isNotEmpty()) {
            callPercentageAPI(price , earn_txt , "Flipkart")
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        val getTittle = getFirstNStrings(productTittle !! , 5)
        getProductDEtailebay("https://www.ebay.com/sch/i.html?_nkw=${getTittle}")
    }

    override fun getProductURlNewEgg(name: String , price: String , url: String , image: String) {
        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "newegg" , image)
            } else {
                sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "newegg" , image)
            }
        }

        if (price.isNotEmpty()) {
            callPercentageAPI(price , earn_txt , "newegg")
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
            getBandH("https://www.bhphotovideo.com/c/search?Ntt=$productTittle")
        }
    }

    override fun getProductBandH(name: String , price: String , url: String , image: String) {
        cv_product?.setOnClickListener {
            val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
            val currentDate = sdf.format(Date())
            if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
                sharePopUpOutside(price , url , currentDate , userID !! , earn_txt.text.toString() , "B&H" , image)
            } else {
                sharePopUp(price , url , currentDate , userID !! , earn_txt.text.toString() , "B&H" , image)
            }
        }

        if (price.isNotEmpty()) {
            callPercentageAPI(price , earn_txt , "B&H")
        }

        baseURL = url
        productTittle = name.replace("&nbsp;" , "")

        isCheckUrl = true

        if (SMarket.appDB.userDao().getData() !!.countryCode != "+91") {
            getProductDEtailFromAmazon(
                    "https://www.amazon.com/s?k=$productTittle".replace(
                            "(AT&T)" , ""
                    ).replace("(GSM)" , "")
                            .replace("(CDMA + GSM)" , "").replace(
                                    "AT&T" ,
                                    ""
                            ).replace("Verizon" , "")
            )
        }
    }

    override fun getProductBestBuy(name: String , price: String , url: String , image: String) {

    }
}
