package com.smarket.main.customer

import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.couponlist.CouponListResponse
import com.smarket.webservices.response.couponlist.CouponOffer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_coupon_list.*
import kotlinx.android.synthetic.main.fragment_coupon_list.swipe_refresh
import kotlinx.android.synthetic.main.fragment_coupon_list.tv_no_data_found
import kotlinx.android.synthetic.main.fragment_referral_alerts.*
import kotlinx.android.synthetic.main.product_compare_list.view.*
import kotlinx.android.synthetic.main.row_coupon_list.view.*
import kotlinx.android.synthetic.main.topbar_search_header.*
import kotlinx.android.synthetic.main.view_merchant_contact_details.view.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import retrofit2.Response
import utils.AnimationType

class CouponListFragment : BaseMainFragment() , View.OnClickListener ,
    SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(): CouponListFragment {
            val fragment = CouponListFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var couponListAdapter: CouponListAdapter
    var searchText = ""

    var page = 1
    var lastPage = 1
    var isLoading = false

    var couponOfferList = ArrayList<CouponOffer>()

    var observable: Observable<Response<CouponListResponse>>? = null
    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_coupon_list , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.VERTICAL , false)
        rv_coupon_list.layoutManager = linearLayoutManager
        couponListAdapter = CouponListAdapter()
        rv_coupon_list.adapter = couponListAdapter
        SearchData()
    }


    private fun SearchData() {
        et_search_top.requestFocus()
        if (et_search_top.requestFocus()) {
            mActivity.showKeyboard(et_search_top)
        }

        et_search_top.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                searchText = s.toString().trim()
                page = 1
                callSearchCouponOfferAPI()
            }
        })

    }

    private fun callSearchCouponOfferAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (couponOfferList.find { it.id == 0L } == null) {
                couponOfferList.add(CouponOffer())
                couponListAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
//        hashMap[ApiParam.LATITUDE] = mLatitude
//        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.SearchMerchant.SEARCH_TEXT] = searchText
//        hashMap[ApiParam.SearchMerchant.POST_CODE] = zipCode
//        hashMap[ApiParam.SearchMerchant.DISTANCE] = "$radius"
        /*hashMap[ApiParam.SearchMerchant.SHOW_TOP_MERCHANT] = if (showTopMerchants) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }*/
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().searchCouponOfferAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    couponOfferList.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    couponOfferList.addAll(data)
                                                }

                                                if (links != null && links !!.next != null
                                                    && links !!.next !!.isRequiredField()) {
                                                    this@CouponListFragment.lastPage ++
                                                } else {
                                                    this@CouponListFragment.lastPage = page
                                                }

                                                page ++
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (couponOfferList.isNotEmpty()) {
                            couponOfferList.removeAll(couponOfferList.filter { it.id == 0L })
                        }
                    }

                    couponListAdapter.notifyDataSetChanged()

                    if (couponOfferList.isEmpty()) {
                        rv_coupon_list.visibility = View.GONE
//                        btn_view_merchants.visibility = View.GONE
                        tv_no_data_found.visibility = View.VISIBLE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                        rv_coupon_list.visibility = View.VISIBLE
                    }

                    isLoading = false
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (couponOfferList.isNotEmpty()) {
                            couponOfferList.removeAll(couponOfferList.filter { it.id == 0L })
                            couponListAdapter.notifyDataSetChanged()
                        }
                    }

                    if (couponOfferList.isEmpty()) {
                        tv_no_data_found.visibility = View.VISIBLE
//                        btn_view_merchants.visibility = View.GONE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }



    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {

        }
    }

    private fun setHeader() {
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        iv_search.visibility = View.VISIBLE
        iv_search.setOnClickListener(this)
        cancelText.setOnClickListener(this)
        iv_clearHeader.setOnClickListener(this)
        tv_title.text = resources.getString(R.string.coupon_offer)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.cancelText -> {
                et_search_top.visibility = View.GONE
                iv_clearHeader.visibility = View.GONE
                cancelText.visibility = View.GONE
                iv_search.visibility = View.VISIBLE
                tv_title.visibility = View.VISIBLE
                et_search_top.setText("")
            }
            R.id.iv_clearHeader -> et_search_top.setText("")
            R.id.iv_search -> {
                et_search_top.visibility = View.VISIBLE
                iv_clearHeader.visibility = View.VISIBLE
                cancelText.visibility = View.VISIBLE
                tv_title.visibility = View.GONE
                iv_search.visibility = View.GONE
            } else -> {
        }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        /*rv_coupon_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView , newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == couponListAdapter.itemCount - 1 && ! isLoading) {
                    callCouponListAPI()
                }
            }
        })*/
        callCouponListAPI()
    }

    private fun callCouponListAPI() {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        swipe_refresh.isRefreshing = true

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().couponListAPI().subscribeOn(Schedulers.io())
        disposable = observable?.observeOn(AndroidSchedulers.mainThread())?.subscribe(
            {
                if (disposable == null) return@subscribe
                with(it) {
                    if (isSuccessful && body() != null) {
                        with(body() !!) {
                            if (meta != null) {
                                with(meta !!) {
                                    when (status) {
                                        ApiParam.META_SUCCESS -> {
                                            couponOfferList.clear()
                                            if (data.isNotEmpty()) {
                                                this@CouponListFragment.couponOfferList.addAll(data)
                                            }
                                        }
                                        else -> {
                                            mActivity.showDialog(message)
                                        }
                                    }
                                }
                            } else {
                                ErrorUtil.showError(it , mActivity)
                            }
                        }
                    } else {
                        ErrorUtil.showError(it , mActivity)
                    }
                }

                if (swipe_refresh.isRefreshing) {
                    swipe_refresh.isRefreshing = false
                } else {
                    if (couponOfferList.isNotEmpty()) {
                        couponOfferList.removeAll(couponOfferList.filter { it.id == 0L })
                    }
                }

                couponListAdapter.notifyDataSetChanged()

                tv_no_data_found.visibility = if (couponOfferList.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                isLoading = false
            } ,
            {
                if (disposable == null) return@subscribe

                if (swipe_refresh.isRefreshing) {
                    swipe_refresh.isRefreshing = false
                } else {
                    if (couponOfferList.isNotEmpty()) {
                        couponOfferList.removeAll(couponOfferList.filter { it.id == 0L })
                        couponListAdapter.notifyDataSetChanged()
                    }
                }

                tv_no_data_found.visibility = if (couponOfferList.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                isLoading = false
                ErrorUtil.setExceptionMessage(it)
            }
        )

    }

    override fun onRefresh() {
        callCouponListAPI()
    }

    inner class CouponListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup , viewType: Int
        ): RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress , parent , false)
            } else {
                inflater.inflate(R.layout.row_coupon_list , parent , false)
            }
            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (couponOfferList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return couponOfferList.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder , position: Int) {
            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(couponOfferList[holder.adapterPosition]) {
                    if (subOffer.isNotEmpty()) {
                            holder.itemView.ivProfile.loadFrescoImage(subOffer[0].couponImage ?: "")

                        if (mActivity.userDetails?.countryCode != "+91") {
                            val referNameColor =
                                "Offer: " + "<font color='#f50057'>" + "$" + subOffer[0].amount?.replace(
                                    ".00" ,
                                    ""
                                ) + "</font>" +
                                        mActivity.resources.getString(R.string.coupon)
                            holder.itemView.tv_store_credit.text =
                                Html.fromHtml(referNameColor , Html.FROM_HTML_MODE_LEGACY)
                        } else
                        {
                            val referNameColor =
                                "Offer: " + "<font color='#f50057'>" + "₹" + subOffer[0].amount?.replace(
                                    ".00" ,
                                    ""
                                ) + "</font>" +
                                        mActivity.resources.getString(R.string.coupon)
                            holder.itemView.tv_store_credit.text =
                                Html.fromHtml(referNameColor , Html.FROM_HTML_MODE_LEGACY)
                        }

                        holder.itemView.tv_conditions.text = subOffer[0].conditions ?: ""
                        holder.itemView.tv_conditions.setShowingLine(2)
                        holder.itemView.tv_conditions.addShowMoreText("more")
                        holder.itemView.tv_conditions.addShowLessText("Less")
                        holder.itemView.tv_conditions.setShowMoreColor(
                            ContextCompat.getColor(
                                mActivity ,
                                R.color.customer_app_color
                            )
                        )
                        holder.itemView.tv_conditions.setShowLessTextColor(
                            ContextCompat.getColor(
                                mActivity ,
                                R.color.customer_app_color
                            )
                        )

                        if (usedCoupon.equals("1")) {
                            holder.itemView.btnRedeem.text = resources.getString(R.string.used)
                            holder.itemView.btnRedeem.isEnabled = false
                            holder.itemView.btnRedeem.background =
                                mActivity.resources.getDrawable(R.drawable.edit_text_bg)
                        } else {
                            holder.itemView.btnRedeem.text = resources.getString(R.string.redeem)
                            holder.itemView.btnRedeem.isEnabled = true
                            holder.itemView.btnRedeem.background =
                                mActivity.resources.getDrawable(R.drawable.button_background)
                        }
                    }
                    else{

                        holder.itemView.ivProfile.visibility=View.GONE
                        holder.itemView.view_coupon_iv.visibility=View.GONE
                    }

                    if (merchant.isNotEmpty()) {
                        val businesslogo=merchant[0].businessLogo
                        if(businesslogo.equals("")){
                            holder.itemView.sdv_logo.setImageResource(R.drawable.customer_type)
                        }
                        else {
                            holder.itemView.sdv_logo.loadFrescoImage(merchant[0].businessLogo ?: "")
                        }
                    }

                    holder.itemView.tv_business_name.text = merchant[0].businessName
                    holder.itemView.tv_distance.setCustomText(
                        "Exp.Date: \n" , expiryDate ?: "" ,
                        fontColor = mActivity.resources.getColor(R.color.black) ,
                        fontFamilyId = R.font.poppins_medium
                    )
                    holder.itemView.tv_address.text = merchant[0].address

                    if (merchant[0].tagline?.isRequiredField() == true) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = merchant[0].tagline
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }

                    if (merchant[0].website?.isRequiredField() == true) {
                        holder.itemView.iv_website.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_website.visibility = View.GONE
                    }

                    if (merchant[0].mobileNumber?.isRequiredField() == true) {
                        holder.itemView.iv_call.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_call.visibility = View.GONE
                    }

                    if (merchant[0].address?.isRequiredField() == true) {
                        holder.itemView.iv_direction.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_direction.visibility = View.GONE
                    }

                    holder.itemView.iv_call.setOnClickListener {
                        merchant[0].mobileNumber?.let { it1 -> mActivity.makeCall(it1) }
                    }


                    holder.itemView.iv_direction.setOnClickListener {
                        navigateToGoogleMap(
                            mActivity ,
                            merchant[0].latitude?.toDouble() ?: 0.0 ,
                            merchant[0].longitude?.toDouble() ?: 0.0 ,
                            merchant[0].address ?: ""
                        )
                    }
                    holder.itemView.iv_website.setOnClickListener {
                        mActivity.showWeb(merchant[0].website ?: "")
                    }

                    holder.itemView.btnRedeem.setOnClickListener {
                        callCouponQRAPI(id)
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }

    private fun callCouponQRAPI(id: Long) {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.OFFER_ID] = id.toString()

        disposable = WebApiClient.webApi().generateRedemptionCodeForCouponOfferAPI(hashMap)
            .subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe
                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                mActivity.addFragment(
                                                    OfferQRCodeFragment.getInstance(
                                                        CouponListFragment::class.java.canonicalName ,
                                                        offerDetails = data , merchantMessage = ""
                                                    ) ,
                                                    true ,
                                                    animationType = AnimationType.RightInZoomOut
                                                )
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } , {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }
}