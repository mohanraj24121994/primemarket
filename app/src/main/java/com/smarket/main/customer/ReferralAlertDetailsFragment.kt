package com.smarket.main.customer

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.ReferralAlertAndAwaitingReward
import com.smarket.model.Referrals
import com.smarket.utils.*
import com.smarket.utils.ProgressDialogUtil.hideProgressDialog
import com.smarket.utils.ProgressDialogUtil.showProgressDialog
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_offer_details.*
import kotlinx.android.synthetic.main.fragment_offer_details.rating_bar
import kotlinx.android.synthetic.main.fragment_offer_details.tv_average_ratings
import kotlinx.android.synthetic.main.fragment_offer_details.tv_business_name
import kotlinx.android.synthetic.main.fragment_offer_details.tv_condition
import kotlinx.android.synthetic.main.fragment_offer_details.tv_condition_label
import kotlinx.android.synthetic.main.fragment_offer_details.tv_expiry_date
import kotlinx.android.synthetic.main.fragment_offer_details.tv_ratings
import kotlinx.android.synthetic.main.fragment_offer_details.tv_tag_line
import kotlinx.android.synthetic.main.fragment_offer_qr_code.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_referrals.view.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.*
import utils.AnimationType
import java.util.*

/**
 * Created by MI-062 on 11/4/18.
 */
class ReferralAlertDetailsFragment : BaseMainFragment() {

    companion object {
        fun getInstance(merchantDetails: ReferralAlertAndAwaitingReward? = null): ReferralAlertDetailsFragment {
            val fragment = ReferralAlertDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable(MERCHANT_DETAILS, merchantDetails)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var referralsAdapter: ReferralsAdapter

    var page = 1
    var lastPage = 1
    var isLoading = false

    private var disposable: Disposable? = null

    var referrals = ArrayList<Referrals>()
    var merchantDetails: ReferralAlertAndAwaitingReward? = null

    var mLatitude = ""
    var mLongitude = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_offer_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_referrals.layoutManager = linearLayoutManager
        referralsAdapter = ReferralsAdapter()
        rv_referrals.adapter = referralsAdapter
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(MERCHANT_DETAILS)) {
                merchantDetails = bundle.getParcelable(MERCHANT_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.offer_details)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setMerchantDetails()

        rv_referrals.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == referralsAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callReferralDetailsAPI()
                }
            }
        })

        callReferralDetailsAPI()
    }

    private fun setMerchantDetails() {

        if (merchantDetails == null) {
            cl_merchant_and_offer_details.visibility = View.GONE
        } else {
            cl_merchant_and_offer_details.visibility = View.VISIBLE

            with(merchantDetails!!) {

                sdv_logo.loadFrescoImage(businessLogo)
                if (distance.isRequiredField()) {
                    tv_distance.text = "($distance ${mActivity.resources.getString(R.string.mi)})"
                }
                tv_business_name.text = businessName
                if (tagLine.isRequiredField()) {
                    tv_tag_line.visibility = View.VISIBLE
                    tv_tag_line.text = tagLine
                } else {
                    tv_tag_line.visibility = View.GONE
                }
                rating_bar.rating = averageRating
                tv_average_ratings.text = "$averageRating"
                tv_ratings.text = "($noOfRating)"
                tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                if (status == 3) {
                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                    tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                }

                if (status == 2) {
                    tv_expiry_date.text = "${mActivity.resources.getString(R.string.redeemed_on)} ${getChangedDateFormat(this!!.redeemedDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                    tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                }

                if (conditions.isRequiredField()) {
                    tv_condition_label.visibility = View.VISIBLE
                    tv_condition.visibility = View.VISIBLE
                    divider3.visibility = View.VISIBLE
                    tv_condition.text = conditions
                } else {
                    tv_condition_label.visibility = View.GONE
                    tv_condition.visibility = View.GONE
                    divider3.visibility = View.GONE
                }

                when (subOfferCategory) {
                    ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                        tv_offer_value.text = ""
                        tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                        tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                    }
                    ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                        tv_offer_value.text = "$$amount"
                        tv_sub_offer_type.text = mActivity.resources.getString(R.string.store_credit)
                    }
                    ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                        tv_offer_value.text = "$$amount"
                        tv_sub_offer_type.text = mActivity.resources.getString(R.string.gift_card)
                    }
                }
            }
        }
    }

    inner class ReferralsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_referrals, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (referrals[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return referrals.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(referrals[position]) {
                    holder.itemView.sdv_profile_pic.loadFrescoImage(profilePic)
                    holder.itemView.tv_name.text = name

                    //To mask mobile number as xxxxx12345
                    val maskMobile = mobile.replace("\\w(?=\\w{5})".toRegex(), "x")
                    holder.itemView.tv_mobile.text = maskMobile

                    holder.itemView.rating_bar.rating = rating
                    holder.itemView.tv_average_ratings.text = "$rating"
                    holder.itemView.tv_referred_date.text = "${mActivity.resources.getString(R.string.referred_on)} ${getChangedDateFormat(referredDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                    if (image.isRequiredField()) {
                        holder.itemView.sdv_offer.visibility = View.VISIBLE
                        holder.itemView.sdv_offer.loadFrescoImage(image)
                    } else {
                        holder.itemView.sdv_offer.visibility = View.GONE
                    }
                    if (review.isRequiredField()) {
                        holder.itemView.tv_review_label.visibility = View.VISIBLE
                        holder.itemView.tv_review.text = review
                    } else {
                        holder.itemView.tv_review_label.visibility = View.GONE
                        holder.itemView.tv_review.text = ""
                    }
                    if (referralStatus == ApiParam.REFERRAL_STATUS_ACCEPTED) {
                        holder.itemView.ll_offer_code.visibility = View.VISIBLE
                        holder.itemView.sdv_offer_code.loadFrescoImage(qrCode)
                        holder.itemView.tv_offer_code.text = code
                        holder.itemView.tv_offer_code.setOnClickListener {

                            val myClip: ClipData

                            val myClipboard: ClipboardManager
                            myClipboard= context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager;


                            myClip = ClipData.newPlainText("text", tv_offer_code.text);
                            myClipboard.setPrimaryClip(myClip);

                            Toast.makeText(context, "Code Copied",
                                Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        holder.itemView.ll_offer_code.visibility = View.GONE
                    }
                    holder.itemView.tv_generate_code.visibility = if (referralStatus == ApiParam.REFERRAL_STATUS_PENDING) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    holder.itemView.tv_generate_code.setOnClickListener {
                        callGenerateRedemptionCodeAPI(id, merchantId, userId)
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    private fun callReferralDetailsAPI() {
        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        isLoading = true

        referrals.add(Referrals())
        referralsAdapter.notifyDataSetChanged()

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.ID] = "${merchantDetails!!.merchantId}"
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.OFFER_ID] = merchantDetails!!.offerId

        disposable = WebApiClient.webApi().referralDetailsAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            referrals.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            referrals.addAll(data)
                                                        }

                                                        page++

                                                        this@ReferralAlertDetailsFragment.lastPage = lastPage

                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }

                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (referrals.isNotEmpty()) {
                                referrals.removeAll(referrals.filter { it.id == 0L })
                            }

                            referralsAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (referrals.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (referrals.isNotEmpty()) {
                                referrals.removeAll(referrals.filter { it.id == 0L })
                                referralsAdapter.notifyDataSetChanged()
                            }

                            tv_no_data_found.visibility = if (referrals.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callGenerateRedemptionCodeAPI(id: Long, merchantId: Long, userId: Long) {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = "$id"
        hashMap[ApiParam.MERCHANT_ID] = "$merchantId"

        disposable = WebApiClient.webApi().generateRedemptionCodeAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            hideProgressDialog()

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        if (data != null) {

                                                            val updatedReferralIndex = referrals.indexOf(
                                                                    referrals.find {
                                                                        it.userId == userId
                                                                    }
                                                            )

                                                            if (updatedReferralIndex != -1) {
                                                                referrals.forEach { it.referralStatus = ApiParam.REFERRAL_STATUS_REJECTED }
                                                                referrals[updatedReferralIndex] = data!!
                                                                referralsAdapter.notifyDataSetChanged()
                                                            }

                                                        }
                                                        mActivity.showDialogWithActions(mActivity.resources.getString(R.string.invite_friends),
                                                                positiveButtonLabel = mActivity.resources.getString(R.string.yes),
                                                                negativeButtonLabel = mActivity.resources.getString(R.string.no),
                                                                requiredAlign = true,
                                                                showNegativeButton = true, negativeFunction = {}, positiveFunction = {
                                                            mActivity.addFragment(RecommendFriendsFragment.getInstance()
                                                                    , true, animationType = AnimationType.RightInZoomOut)
                                                        })
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }
                        },
                        {
                            if (disposable == null) return@subscribe

                            hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}