package com.smarket.main.customer

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.ViewPager
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.BannersAdapter
import com.smarket.database.User
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.main.BaseMainFragment
import com.smarket.main.FAQsFragment
import com.smarket.main.customer.topBar.AwaitingRewardsFragment
import com.smarket.main.customer.topBar.ReferralAlertsFragment
import com.smarket.main.customer.topBar.SearchFragment
import com.smarket.main.customer.topBar.StoreCreditsFragment
import com.smarket.main.merchant.MerchantDetailsFragment
import com.smarket.model.Banner
import com.smarket.model.Meta
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.BannersResponse
import com.smarket.webservices.response.refferedlist.ReferredList
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import com.tap.app.utils.CustomTypefaceSpan
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cust_home.*
import kotlinx.android.synthetic.main.header.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class CustHomeFragment : BaseMainFragment() , View.OnClickListener ,
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(): CustHomeFragment {
            val fragment = CustHomeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var bannersAdapter: BannersAdapter
    var refer_per = ArrayList<User>()
    var banners = ArrayList<Banner>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    var searchText = ""

    private var disposable: Disposable? = null
    var observable: Observable<Response<BannersResponse>>? = null

    var tutorialImages = intArrayOf(
        R.drawable.ic_merchant_detail ,
        R.drawable.ic_refer_merchant ,
        R.drawable.ic_referal_offer
    )

    override fun onCreateView(
        inflater: LayoutInflater ,
        container: ViewGroup? ,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_cust_home , container , false)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        //  Redirect to yooutube
        val imageViewSlider: CardView = v.findViewById(R.id.flFirstCard)
        val flFrame: CardView = v.findViewById(R.id.cardTutorial)
        val secondImageCard: CardView = v.findViewById(R.id.flSecondCardView)
        val tvNext: AppCompatTextView = v.findViewById(R.id.tv_next_new)
        val carouselViewSecondPopUp: CarouselView = v.findViewById(R.id.carouselTutorial)
        secondImageCard.setOnClickListener {
            flFrame.visibility = View.VISIBLE
            imageViewSlider.visibility = View.GONE
            secondImageCard.visibility = View.GONE
            tb.visibility = View.GONE

            val imageListener = ImageListener { position , imageView ->
                imageView.setImageResource(tutorialImages[position])
            }
            carouselViewSecondPopUp.setImageListener(imageListener)
            carouselViewSecondPopUp.pageCount = tutorialImages.size

            carouselViewSecondPopUp.addOnPageChangeListener(object :
                ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(p0: Int) {
                }

                override fun onPageScrolled(p0: Int , p1: Float , p2: Int) {
                }

                override fun onPageSelected(p0: Int) {
                    when (p0) {
                        2 -> {
                            textViewPopUp.text = "Step 3: Redeem Offer Code"
                            iv_Tutorialend.visibility = View.GONE
                        }
                        1 -> {
                            textViewPopUp.text = "Step 2: Refer Merchant"
                            iv_Tutorialstart.visibility = View.VISIBLE
                            iv_Tutorialend.visibility = View.VISIBLE
                        }
                        else -> {
                            iv_Tutorialstart.visibility = View.GONE
                            textViewPopUp.text = "Step 1: Find a Merchant"
                        }
                    }
                }
            })

            /*mActivity.runOnUiThread {
                val alertDialog = AlertDialog.Builder(mActivity).create()

                val dialogView =
                    LayoutInflater.from(mActivity).inflate(R.layout.home_popup_dialog , null)

                alertDialog.setView(dialogView)
                alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                val flSecondCard = dialogView.findViewById<CardView>(R.id.flSecondCard)
                val textViewPopUp = dialogView.findViewById<TextView>(R.id.textViewPopUp)
                val carouselViewSecondPopUp =
                    dialogView.findViewById<CarouselView>(R.id.carouselViewSecondPopUp)
                val tv_nextPopUp = dialogView.findViewById<TextView>(R.id.tv_nextPopUp)
                val imageListener = ImageListener { position , imageView ->
                    imageView.setImageResource(tutorialImages[position])
//                    imageView.scaleType = ImageView.ScaleType.FIT_XY
//                    imageView.scaleType = ImageView.ScaleType.FIT_CENTER
                }
                carouselViewSecondPopUp.setImageListener(imageListener)
                carouselViewSecondPopUp?.pageCount = tutorialImages.size
//        carouselViewSecond.fitsSystemWindows

                swipe_refresh.visibility = View.GONE
                // iv_search.visibility = View.GONE
                // et_search_any.visibility = View.GONE

                carouselViewSecondPopUp.addOnPageChangeListener(object :
                    ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(p0: Int) {
                    }

                    override fun onPageScrolled(p0: Int , p1: Float , p2: Int) {
                    }

                    override fun onPageSelected(p0: Int) {
                        when (p0) {
                            2 -> {
                                textViewPopUp.text = " Redeem Offer Code"
                                tv_nextPopUp.text = activity !!.getString(R.string.next_under)
                                iv_end.visibility = View.GONE
                            }
                            1 -> {
                                tv_next.text = activity !!.getString(R.string.skip)
                                textViewPopUp.text = "Refer Merchant"
                                iv_start.visibility = View.VISIBLE
                                iv_end.visibility = View.VISIBLE
                            }
                            else -> {
                                iv_start.visibility = View.GONE
                                tv_next.text = activity !!.getString(R.string.skip)
                                textViewPopUp.text = "Find a Merchant"
                            }
                        }
                    }
                })
*//*
            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            }
*//*

                alertDialog.show()
            }*/
        }

        tvNext.setOnClickListener {
            flFrame.visibility = View.GONE
            imageViewSlider.visibility = View.VISIBLE
            secondImageCard.visibility = View.VISIBLE
            tb.visibility = View.VISIBLE
        }
        imageViewSlider.setOnClickListener {
            val url = "https://youtu.be/4VM4d5kBsXk"
            val intent = Intent(Intent.ACTION_VIEW , Uri.parse(url))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.setPackage("com.google.android.youtube")
            startActivity(intent)
        }

        return v
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        setKeyBoardDoneAction()

        if (mActivity.userDetails?.refered_person_mer.equals("merchant")) {

            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dailog_home , null)
            alertDialog.setView(dialogView)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val btnOk = dialogView.findViewById<Button>(R.id.btnOk)
            val tvReferMerchantName = dialogView.findViewById<TextView>(R.id.tvViewMerchant)

            val referNameColor =
                "Your refer by " + "<font color='#f50057'>" + mActivity.userDetails?.refered_business_name + "</font>" + "\n" + "Go to the merchant offer"
            tvReferMerchantName.text = Html.fromHtml(referNameColor , Html.FROM_HTML_MODE_LEGACY)

            btnCancel.setOnClickListener {
                alertDialog.dismiss()
            }
            btnOk.setOnClickListener {
                alertDialog.dismiss()
                val referredBy = mActivity.userDetails?.refered_person_id
                mActivity.addFragment(
                    MerchantDetailsFragment.getInstance(referredBy !!.toLong()) ,
                    true , animationType = AnimationType.RightInZoomOut
                )

            }
            alertDialog.show()
        } else {
            if (mActivity.userDetails?.countryCode == "+91") {
                sharePopUpWithoutHeader()
            } else {
                sharePopUpWithoutHeaderOutsideIndia()
            }
        }

        tv_referral.text = refer_per.toString()

        val carouselView = view.findViewById<CarouselView>(R.id.carouselView)
        val carouselViewSecond = view.findViewById<CarouselView>(R.id.carouselViewSecond)

        val imageListener = ImageListener { position , imageView ->
            imageView.setImageResource(tutorialImages[position])
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
        }
        carouselView.setImageListener(imageListener)

        carouselView?.pageCount = tutorialImages.size

        carouselViewSecond.setImageListener(imageListener)
        carouselViewSecond?.pageCount = tutorialImages.size
//        carouselViewSecond.fitsSystemWindows

        swipe_refresh.visibility = View.GONE
        // iv_search.visibility = View.GONE
        // et_search_any.visibility = View.GONE

        carouselViewSecond.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int , p1: Float , p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                when (p0) {
                    2 -> {
                        textView.text = " Redeem Offer Code"
                        tv_next.text = activity !!.getString(R.string.next_under)
                        iv_end.visibility = View.GONE
                    }
                    1 -> {
                        tv_next.text = activity !!.getString(R.string.skip)
                        textView.text = "Refer Merchant"
                        iv_start.visibility = View.VISIBLE
                        iv_end.visibility = View.VISIBLE
                    }
                    else -> {
                        iv_start.visibility = View.GONE
                        tv_next.text = activity !!.getString(R.string.skip)
                        textView.text = "Find a Merchant"
                    }
                }
            }
        })
        carouselView.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int , p1: Float , p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                when (p0) {
                    2 -> {
                        tv_next.text = activity !!.getString(R.string.next_under)
                        iv_end_second.visibility = View.GONE
                    }
                    1 -> {
                        tv_next.text = activity !!.getString(R.string.skip)
                        iv_start_second.visibility = View.GONE
                        iv_end_second.visibility = View.GONE
                    }
                    else -> {
                        iv_start_second.visibility = View.GONE
                        tv_next.text = activity !!.getString(R.string.skip)
                    }
                }
            }
        })

        tv_next.setOnClickListener {
            swipe_refresh.visibility = View.VISIBLE
            card_image.visibility = View.GONE
            // iv_search.visibility = View.GONE
            //  et_search_any.visibility = View.GONE
        }

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            mActivity ,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL ,
            false
        )
        rv_banners.addItemDecoration(
            VerticalDividerDecoration(
                mActivity.convertDpToPixel(16F).toInt() , true
            )
        )
        rv_banners.layoutManager = linearLayoutManager
        bannersAdapter = BannersAdapter(banners)
        rv_banners.adapter = bannersAdapter

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        ll_refcash.setOnClickListener(this)
        ll_referral_alerts.setOnClickListener(this)
        ll_awaiting_rewards.setOnClickListener(this)
        ll_store_credit.setOnClickListener(this)
        btn_scan_qr_code.setOnClickListener(this)
        ll_rfriends.setOnClickListener(this)
        //callReferralAPI()
    }


    private fun callReferralAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        val callRefferalApi =
            WebApiClient.webApi().referList(SMarket.appDB.userDao().getData() !!.id.toString())
        callRefferalApi?.enqueue(object : Callback<ReferredList?> {
            override fun onFailure(call: Call<ReferredList?> , t: Throwable) {
                Log.e("referFriends" , "referFriends" + t.message)
            }

            override fun onResponse(call: Call<ReferredList?> , response: Response<ReferredList?>) {
                if (response.isSuccessful) {
                    if (response.body() != null)
                        if (response.body() !!.getData()?.getRefered()?.size !! > 0) {
                            tv_rfriends.text =
                                response.body() !!.getData()?.getRefered()?.size.toString()
                        } else {
                            try {
                                tv_rfriends.text = "0"
                            } catch (e: Exception) {
                                Log.e("referFriends" , "referFriends" + e.message)
                            }
                        }
                }
            }
        })
    }

    private fun setKeyBoardDoneAction() {

        /*  et_search_any.setOnKeyListener(View.OnKeyListener { v , keyCode , event ->
              if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                  searchText = et_search_any.text.toString()
                  mActivity.addFragment(SearchFragment.getInstance(searchText)
                          , true , animationType = AnimationType.ZoomInZoomOut)
                  *//*mActivity.addFragment(SearchProductFragment.getInstance(searchText) , true , animationType = AnimationType.ZoomInZoomOut)
*//*
                return@OnKeyListener true
            }
            false
        })*/

        et_search_any.setOnClickListener {

            searchText = et_search_any.text.toString()
            mActivity.addFragment(
                SearchFragment.getInstance(searchText) , true , animationType = AnimationType.None
            )
        }
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        et_search_any.visibility = View.VISIBLE
        et_search_any.setOnClickListener(this)
        iv_search.visibility = View.VISIBLE
        iv_search.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rv_banners.addOnScrollListener(object :
            androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: androidx.recyclerview.widget.RecyclerView ,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView , newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == bannersAdapter.itemCount - 1 &&
                    page <= lastPage && ! isLoading
                ) {
                    callBannersAPI()
                }
            }
        })

        callBannersAPI()

        callUserDetailAPI()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.iv_search -> {
                searchText = et_search_any.text.toString()
                mActivity.addFragment(
                    SearchFragment.getInstance(searchText) ,
                    true ,
                    animationType = AnimationType.ZoomInZoomOut
                )

                /*mActivity.addFragment(SearchProductFragment.getInstance(searchText)
                        , true , animationType = AnimationType.ZoomInZoomOut)*/
            }
            R.id.btn_scan_qr_code -> mActivity.addFragment(
                ScanQRCodeFragment.getInstance() ,
                true ,
                animationType = AnimationType.RightInZoomOut
            )
            //  mActivity.addFragment(OfferQRCodeFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            R.id.ll_refcash -> mActivity.addFragment(
                RedeemRefcashFragment.getInstance() ,
                true ,
                animationType = AnimationType.RightInZoomOut
            )
            R.id.ll_referral_alerts -> mActivity.addFragment(
                ReferralAlertsFragment.getInstance(
                    IS_FROM_HOME
                ) , true , animationType = AnimationType.RightInZoomOut
            )
            R.id.ll_awaiting_rewards -> mActivity.addFragment(
                AwaitingRewardsFragment.getInstance() ,
                true ,
                animationType = AnimationType.RightInZoomOut
            )
            R.id.ll_store_credit -> mActivity.addFragment(
                StoreCreditsFragment.getInstance() ,
                true ,
                animationType = AnimationType.RightInZoomOut
            )
            R.id.ll_rfriends ->
                //mActivity.addFragment(FriendsOnSmarketFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
                mActivity.addFragment(
                    CouponListFragment.getInstance() ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        callBannersAPI()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && ! hidden) {
            callUserDetailAPI()
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    private fun callBannersAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (banners.find { it.id == 0L } == null) {
                banners.add(Banner())
                bannersAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.USER_TYPE] = ApiParam.USER_TYPE_CUSTOMER
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().bannersAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    kotlin.with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                UpdateRefCashManager.executeCallBacks(
                                                    mActivity ,
                                                    refcash
                                                )

                                                setCounterValues(meta !!)

                                                if (page == 1) {
                                                    banners.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    banners.addAll(data)
                                                }

                                                page ++

                                                this@CustHomeFragment.lastPage = lastPage
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (banners.isNotEmpty()) {
                            banners.removeAll(banners.filter { it.id == 0L })
                        }
                    }

                    bannersAdapter.notifyDataSetChanged()

                    tv_no_data_found.visibility = if (banners.isEmpty()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    isLoading = false
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (banners.isNotEmpty()) {
                            banners.removeAll(banners.filter { it.id == 0L })
                            bannersAdapter.notifyDataSetChanged()
                        }
                    }

                    tv_no_data_found.visibility = if (banners.isEmpty()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun callUserDetailAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            return
        }

        disposable = WebApiClient.webApi().userDetailAPI().subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                if (data != null) {

                                                    setUserDetails(data !!)
                                                }
                                            }
                                            ApiParam.META_UNVERIFIED_USER -> {
                                            }
                                            else -> {
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                } ,
                {
                    if (disposable == null) return@subscribe

                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun setUserDetails(user: User) {
        with(user) {
            tv_referral.text = refferalAlert.toString()
            tv_awaiting_rewards.text = awaitingRewards.toString()
            if (mActivity.userDetails?.countryCode != "+91") {
                tv_store_credit.text = "$$storeCredit"
                tv_refcash.text = "$$refcash"
            } else {
                tv_store_credit.text = "₹$storeCredit"
                tv_refcash.text = "₹$refcash"
            }
        }
    }

    private fun setCounterValues(meta: Meta) {
        with(meta) {
            tv_referral.text = refferalAlert.toString()
            tv_awaiting_rewards.text = awaitingRewards.toString()
            if (mActivity.userDetails?.countryCode != "+91") {
                tv_store_credit.text = "$$storeCredit"
                tv_refcash.text = "$$refcash"
            } else {
                tv_store_credit.text = "₹$storeCredit"
                tv_refcash.text = "₹$refcash"
            }
        }
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }

    private fun sharePopUpWithoutHeader() {
        mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView =
                LayoutInflater.from(mActivity).inflate(R.layout.dialog_share_new , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)
            btnViewSite.visibility = View.GONE

            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT ,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0 , 0 , 0 , 50)
            messanger.layoutParams = params

            val first = SpannableString("₹50\nCash Rewards")
            first.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            first.setSpan(
                ForegroundColorSpan(Color.YELLOW) ,
                0 ,
                3 ,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvFirst.text = first

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(
                ForegroundColorSpan(Color.YELLOW) ,
                0 ,
                3 ,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(
                ForegroundColorSpan(Color.YELLOW) ,
                0 ,
                3 ,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"

                /* val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                         "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*/

                val shareMessage = mActivity.userDetails?.referral_message !!.replace(
                    "XXXXXX" ,
                    mActivity.userDetails?.referral_code !!
                )
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))

            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(
                    FAQsFragment.getInstance() ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }

            alertDialog.show()
        }
        /* mActivity.runOnUiThread {
             val alertDialog = AlertDialog.Builder(mActivity).create()
             val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
             alertDialog.setView(dialogView)
             alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

             val messanger = dialogView.findViewById<Button>(R.id.btnShare)
             val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)
             btnViewSite.visibility = View.GONE

             val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
             val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))
            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
            }

            alertDialog.show()
        }*/
    }

    private fun sharePopUpWithoutHeaderOutsideIndia() {
        /*mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val text = dialogView.findViewById<TextView>(R.id.tv_text)
            val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
            val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
            val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
            val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)
            btnViewSite.visibility = View.GONE

            val typeface = ResourcesCompat.getFont(mActivity , R.font.stencil_std_bold) !!
            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari) !!

            val spannable = CustomTypefaceSpan("" , typeface)
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val spannableString = SpannableString(resources.getString(R.string.each_time))
            val rewardString = SpannableString(resources.getString(R.string.refer_friends))

            spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            tv_life.text = spannableString
            reward.text = rewardString

            text.setOnClickListener {
                try {
                    mActivity.addFragment(RecommendFriendsFragment.getInstance(IS_FROM_DRAWER)
                            , true , animationType = AnimationType.RightInZoomOut)
                } catch (e: Exception) {
                    Log.e("" , "" + e.message)
                }
                alertDialog.dismiss()
            }
            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"

                *//*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                        "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*//*
                val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))

            }
            alertDialog.show()
        }*/

        mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
            alertDialog.setView(dialogView)
            alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)
            btnViewSite.visibility = View.GONE

            val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari_bold) !!
            val adobeFont = CustomTypefaceSpan("" , adobe)

            val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_italic) !!
            val spannable = CustomTypefaceSpan("" , typeface)

            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(
                ForegroundColorSpan(Color.YELLOW) ,
                0 ,
                3 ,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(
                ForegroundColorSpan(Color.YELLOW) ,
                0 ,
                3 ,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage = mActivity.userDetails?.referral_message !!.replace(
                    "XXXXXX" ,
                    mActivity.userDetails?.referral_code !!
                )
                intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
                startActivity(Intent.createChooser(intent , "Share"))
            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(
                    FAQsFragment.getInstance() ,
                    true ,
                    animationType = AnimationType.RightInZoomOut
                )
            }

            alertDialog.show()
        }
    }

}