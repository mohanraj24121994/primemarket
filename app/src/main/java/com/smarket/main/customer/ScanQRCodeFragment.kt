package com.smarket.main.customer

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.merchant.MerchantDetailsFragment
import com.smarket.utils.addFragment
import com.smarket.utils.isRequiredField
import com.smarket.utils.showDialog
import kotlinx.android.synthetic.main.fragment_redeem_code.*
import kotlinx.android.synthetic.main.header.*
import org.json.JSONObject
import utils.AnimationType
import java.io.IOException

/**
 * Created by MI-062 on 11/4/18.
 */
class ScanQRCodeFragment : BaseMainFragment() {

    companion object {
        fun getInstance(): ScanQRCodeFragment {
            val fragment = ScanQRCodeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    var CAMERA_PERMISSION = Manifest.permission.CAMERA
    val CAMERA_PERMISSION_REQUEST_CODE = 401
    val CAMERA_PERMISSION_GRANTED = 1
    val CAMERA_PERMISSION_DENIED = 2
    val CAMERA_PERMISSION_DENIED_FOR_ALWAYS = 3
    var isLoading = false

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scan_qr_code , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.scan_qr_code)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        checkForCameraPermission()

        // Initializing barcode detector
        barcodeDetector = BarcodeDetector.Builder(mActivity)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build()

        // Initializing camera source
        cameraSource = CameraSource.Builder(mActivity , barcodeDetector)
                .setRequestedPreviewSize(1024 , 1024)
                .setAutoFocusEnabled(true) //you should add this feature
                .build()

        // Setting up barcode detector
        barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {}

            override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() != 0 && ! isLoading) {
                    isLoading = true
                    tv_camera_permission_status.post {
                        tv_camera_permission_status.post {

                            val barcodeContent = barcodes.valueAt(0).displayValue

                            var merchantId = ""

                            //if (barcodeContent.isRequiredField() && barcodeContent.contains("SMARKET")) {
                            if (barcodeContent.isRequiredField() && barcodeContent.contains("merchant")) {
                                try {
                                    /*val jsonObject = JSONObject(barcodeContent)
                                    if (jsonObject.has("app")) {
                                        if (jsonObject.getString("app") == "SMARKET") {
                                            if (jsonObject.has("merchant_id")) {
                                                merchantId = jsonObject.getString("merchant_id")
                                            }
                                        }
                                    }*/

                                    if (barcodeContent.contains("merchant")) {
                                        val getMerchantId = barcodeContent.split("merchant/")
                                        val secondString = getMerchantId[1]

                                        val firstString = secondString.split("/")
                                        merchantId = firstString[0].replace("/" , "")
                                        Log.e("merchantId" , "merchantId$merchantId")
                                    }

                                } catch (t: Throwable) {
                                    print(t.stackTrace)
                                }
                            }

                            if (merchantId.isRequiredField()) {
                                mActivity.addFragment(MerchantDetailsFragment.getInstance(merchantId.toLong())
                                        , true , animationType = AnimationType.RightInZoomOut)
                            } else {
                                mActivity.showDialog(mActivity.resources.getString(R.string.invalid_qr_code))
                            }
                        }
                    }
                }
            }
        })
    }

    private fun checkForCameraPermission() {

        if (isCameraPermissionGranted()) {
            // Android version is below Marshmallow or camera permission is already allowed.
            startCamera()
        } else {
            // Android version is above Marshmallow and camera permission is not allowed, ask user to allow.
            // Check the result in onActivityResult().
            requestPermissions(arrayOf(CAMERA_PERMISSION) , CAMERA_PERMISSION_REQUEST_CODE)
        }
    }

    private fun isCameraPermissionGranted(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
        when (requestCode) {
            CAMERA_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission is allowed.
                startCamera()
            } else {
                if (! shouldShowRequestPermissionRationale(CAMERA_PERMISSION)) {
                    // Camera permission is denied for always, ask user to turn it on from settings.
                    setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_DENIED_FOR_ALWAYS)
                } else {
                    // Camera permission is denied for now.
                    setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_DENIED)
                }
            }
            else -> {
            }
        }
    }

    private fun startCamera() {

        setUpCameraPermissionStatusMessage(CAMERA_PERMISSION_GRANTED)

        Handler().postDelayed({
            try {
                iv_qr_code.visibility = View.GONE
                cameraSource?.start(surface_view.holder)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } , 300)
    }

    private fun stopCamera() {
        iv_qr_code.visibility = View.VISIBLE
        cameraSource?.stop()
    }

    private fun setUpCameraPermissionStatusMessage(permissionStatus: Int) {
        when (permissionStatus) {
            CAMERA_PERMISSION_DENIED_FOR_ALWAYS -> tv_camera_permission_status.text = mActivity.resources.getString(R.string.camera_permission_denied)
            CAMERA_PERMISSION_DENIED -> {

                val prefixMessage = mActivity.resources.getString(R.string.camera_permission_pending_prefix_message) + " "
                val clickableMessage = mActivity.resources.getString(R.string.click_here)
                val postfixMessage = " " + mActivity.resources.getString(R.string.camera_permission_pending_postfix_message)
                val cameraPermissionStatusMessage = prefixMessage + clickableMessage + postfixMessage

                val ss = SpannableString(cameraPermissionStatusMessage)

                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(textView: View) {
                        checkForCameraPermission()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = true
                        ds.color = mActivity.resources.getColor(R.color.green)
                    }
                }

                ss.setSpan(clickableSpan , prefixMessage.length ,
                        cameraPermissionStatusMessage.length - postfixMessage.length ,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                tv_camera_permission_status.movementMethod = LinkMovementMethod.getInstance()
                tv_camera_permission_status.text = ss
            }
            CAMERA_PERMISSION_GRANTED -> tv_camera_permission_status.text = ""
            else -> {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isVisible && isCameraPermissionGranted()) {
            startCamera()
        }
    }

    override fun onPause() {
        if (isCameraPermissionGranted()) {
            stopCamera()
        }
        super.onPause()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (isAdded && isCameraPermissionGranted()) {
            if (hidden) {
                stopCamera()
            } else {
                startCamera()
            }
        }
    }

    override fun onDestroy() {
        cameraSource?.release()
        super.onDestroy()
    }
}