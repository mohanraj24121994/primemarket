package com.smarket.main.customer

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.ReferFriendsAdapter
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.refferedlist.Refered
import com.smarket.webservices.response.refferedlist.ReferredList
import kotlinx.android.synthetic.main.fragment_friends_of_smarket.*
import kotlinx.android.synthetic.main.header.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FriendsOnSmarketFragment : BaseMainFragment() , View.OnClickListener {

    var isFrom = ""
    private var userID: String? = null
    var banners = ArrayList<Refered>()
    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var bannersAdapter: ReferFriendsAdapter

    companion object {
        fun getInstance(isFrom: String = ""): FriendsOnSmarketFragment {
            val fragment = FriendsOnSmarketFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM , isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_friends_of_smarket , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        userID = SMarket.appDB.userDao().getData() !!.id.toString()

        getBundle()

        setHeader()

        callReferralAPI()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
        }
    }

    private fun setHeader() {

        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.friends_smarket)
    }

    private fun callReferralAPI() {
        activity !!.runOnUiThread {
            ProgressDialogUtil.showProgressDialog(mActivity)
        }

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callRefferalApi = WebApiClient.webApi().referList(userID !!)
        callRefferalApi?.enqueue(object : Callback<ReferredList?> {
            override fun onFailure(call: Call<ReferredList?> , t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<ReferredList?> , response: Response<ReferredList?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null)
                        if (response.body() !!.getData()?.getRefered()?.size !! > 0) {
                            refer_friends_tv.visibility = View.GONE

                            banners.clear()

                            response.body() !!.getData() !!.getRefered() !!.forEach {
                                banners.add(it!!)
                            }
                            linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
                            refer_list_rv.addItemDecoration(VerticalDividerDecoration(mActivity.convertDpToPixel(16F).toInt() , true))
                            refer_list_rv.layoutManager = linearLayoutManager
                            bannersAdapter = ReferFriendsAdapter(banners)
                            refer_list_rv.adapter = bannersAdapter
                        } else {
                            refer_friends_tv.visibility = View.VISIBLE
                        }
                }
            }
        })
    }
}