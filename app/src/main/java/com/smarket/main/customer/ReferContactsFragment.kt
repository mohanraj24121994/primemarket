package com.smarket.main.customer


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SectionIndexer
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatCheckBox
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.main.merchant.RateAndReferMerchantFragment
import com.smarket.model.MerchantDetails
import com.smarket.model.ReferDetails
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ContactDetails
import com.smarket.webservices.response.GenerateRedemptionCodeForStoreCreditResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_recommend_friends.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_contact.view.*
import kotlinx.android.synthetic.main.row_contact_header.view.*
import kotlinx.android.synthetic.main.search_contacts.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import utils.AnimationType


/**
 * A simple [Fragment] subclass.
 */
class ReferContactsFragment : BaseMainFragment() {

    companion object {
        fun getInstance(contacts: ArrayList<ContactDetails>? = null , referDetails: ReferDetails? = null , merchantDetails: MerchantDetails? = null): ReferContactsFragment {
            val fragment = ReferContactsFragment()
            val bundle = Bundle()
            bundle.putParcelableArrayList("contacts" , contacts)
            bundle.putParcelable("referDetails" , referDetails)
            bundle.putParcelable(MERCHANT_DETAILS , merchantDetails)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var contactsAdapter: ContactsAdapter
    var contacts = ArrayList<ContactDetails>()
    var contactsToDisplay = ArrayList<ContactDetails>()
    var referDetails = ReferDetails()
    var merchantDetails: MerchantDetails? = null

    lateinit var rateReferralMerchantObservable: Observable<Response<GenerateRedemptionCodeForStoreCreditResponse>>
    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recommend_friends , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)
        val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_medium) !!

        getBundle()

        setHeader()

        rv_contacts.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        contactsAdapter = ContactsAdapter()
        rv_contacts.adapter = contactsAdapter
        rv_contacts.addItemDecoration(StickyHeaderDecoration(contactsAdapter))
        rv_contacts.setIndexBarCornerRadius(0)
        rv_contacts.setIndexBarTransparentValue(0F)
        rv_contacts.setTypeface(typeface)
        rv_contacts.setIndexBarTextColor(R.color.black)
        rv_contacts.setIndexbarHighLateTextColor(R.color.purple)
        rv_contacts.setIndexBarHighLateTextVisibility(true)

        //To show or hide the IndexBar for rv_contacts , based on KeyBoard
        setKeyBoardListener()

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                contactsToDisplay.clear()
                contactsToDisplay.addAll(if (s.toString().isRequiredField()) {
                    contacts.filter {
                        it.name.contains(s.toString() , true)
                    } as ArrayList<ContactDetails>
                } else {
                    contacts
                })

                tv_no_data_found.visibility = if (contactsToDisplay.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                contactsAdapter.notifyDataSetChanged()
            }
        })

        btn_recommend_now.setOnClickListener(this)
        iv_clear.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("contacts")) {
                contacts = bundle.getParcelableArrayList("contacts") !!
                contactsToDisplay.addAll(contacts)
            }
            if (bundle.containsKey("referDetails")) {
                referDetails = bundle.getParcelable("referDetails") !!
            }
            if (bundle.containsKey(MERCHANT_DETAILS)) {
                merchantDetails = bundle.getParcelable(MERCHANT_DETAILS)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.select_contacts_refer)
//        fl_select_all.visibility = View.GONE
        ll_contacts.visibility = View.VISIBLE
        tv_skip.visibility = View.VISIBLE
        tv_skip.setOnClickListener(this)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    private fun setKeyBoardListener() {
        parent.viewTreeObserver.addOnGlobalLayoutListener {
            if (parent != null) {
                val heightDiff = parent.rootView.height - parent.height
                if (heightDiff > dpToPx(this , 200)) { // if more than 200 dp, it's probably a keyboard...
                    rv_contacts.setIndexBarVisibility(false)
                } else {
                    rv_contacts.setIndexBarVisibility(true)
                }
            }
        }
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_recommend_now -> {
                avoidDoubleClicks(btn_recommend_now)

                var receiversList = ""

                if (contacts.isNotEmpty()) {
                    if (contacts.filter { it.isSelected }.size >= 5) {
                        receiversList = contacts.filter { it.isSelected }.map { it.number + ";" }.toString()
                                .replace("[" , "")
                                .replace("," , "")
                                .replace("]" , "")
                    } else {
                        mActivity.showDialog(mActivity.resources.getString(R.string.select_at_least_contact))
                        return
                    }
                }

                if (receiversList.isRequiredField()) {
                    val alertDialog = AlertDialog.Builder(mActivity).create()
                    val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dailog_invite_friends , null)
                    alertDialog.setView(dialogView)
                    val btnInvite = dialogView.findViewById<AppCompatButton>(R.id.btnInvite)
                    val chkInvite = dialogView.findViewById<AppCompatCheckBox>(R.id.chkInvite)

                    btnInvite.setOnClickListener {
                        alertDialog.dismiss()

                        if (! mActivity.userDetails?.countryCode.equals("+91") && referDetails.referral_sms == "1") {
                            referDetails.refersms = "1"
                        } else {
                            referDetails.refersms = ""
                        }

                        if (merchantDetails?.offer?.size ?: 0 > 0) {
                            receiversList = receiversList.substring(0 , receiversList.length - 1)
                            callRateReferralMerchantAPI(receiversList)
                        } else {
                            val sendIntent = Intent(Intent.ACTION_VIEW)
                            sendIntent.data = Uri.parse("sms:" + receiversList)
                            sendIntent.putExtra("sms_body" ,
                                    mActivity.resources.getString(R.string.smarking_invite_flow_message , merchantDetails?.businessName , resources.getString(R.string.app_link)))
                            mActivity.startActivity(sendIntent)
                        }
                    }

                    chkInvite.setOnCheckedChangeListener { buttonView , isChecked ->
                        if (! isChecked) {
                            referDetails.refermail = ""
                        } else {
                            referDetails.refermail = "1"
                        }
                    }

                    alertDialog.show()
                } else {
                    mActivity.showDialog(mActivity.resources.getString(R.string.select_at_least_contact))
                }
            }

            R.id.iv_clear -> et_search.setText("")

            R.id.tv_skip -> {
                avoidDoubleClicks(tv_skip)
                callRateReferralMerchantAPI()
            }
        }
    }

    private fun callRateReferralMerchantAPI(receivers: String = "") {
        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }
        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.MERCHANT_ID] = referDetails.merchantId.toString()
        hashMap[ApiParam.RateAndRefer.RATING] = referDetails.rating.toString()
        hashMap[ApiParam.RateAndRefer.PRODUCT_NAME] = referDetails.productName
        hashMap[ApiParam.RateAndRefer.REVIEW] = referDetails.review
        hashMap[ApiParam.RateAndRefer.REFER_MAIL] = referDetails.refermail
        hashMap[ApiParam.RateAndRefer.REFER_SMS] = referDetails.refersms
        /* hashMap[ApiParam.RateAndRefer.SMARKET_CONTACT_LIST] = "${referDetails.smarketContactList.map {
             Regex("[^0-9 ]").replace(it.number , "")
         }}".replace("[" , "").replace("]" , "")*/
        hashMap[ApiParam.RateAndRefer.SMARKET_CONTACT_LIST] = receivers

        rateReferralMerchantObservable = if (referDetails.productImage != null) {
            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data") , referDetails.productImage !!)
            val body: MultipartBody.Part = MultipartBody.Part.createFormData(ApiParam.RateAndRefer.PRODUCT_IMAGE , referDetails.productImage !!.name , requestFile)
            WebApiClient.webApi().rateReferralMerchantAPI(hashMap , body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        } else {
            WebApiClient.webApi().rateReferralMerchantAPI(hashMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        }

        disposable = rateReferralMerchantObservable.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                mActivity.addFragment(OfferQRCodeFragment.getInstance(RateAndReferMerchantFragment::class.java.canonicalName , offerDetails = data , receiversList = receivers ,
                                                        merchantMessage = merchantDetails?.referralMessage
                                                                ?: "") , true , animationType = AnimationType.RightInZoomOut)
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                } ,
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
        )

    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }

    inner class ContactsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<ContactsAdapter.ViewHolder>() , StickyHeaderAdapter<ContactsAdapter.ViewHolderHeader> , SectionIndexer {

        private var mSectionPositions = ArrayList<Int>()
        var selectedContactList = 0

        override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ContactsAdapter.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(mActivity)

            view = inflater.inflate(R.layout.row_contact , parent , false)

            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return contactsToDisplay.size
        }

        override fun onBindViewHolder(holder: ContactsAdapter.ViewHolder , position: Int) {

            with(contactsToDisplay[position]) {
                holder.itemView.tv_prefix.text = name.substring(0 , 1).toUpperCase()
                holder.itemView.ctv_name.text = "$name\n$number"
                holder.itemView.ctv_name.isChecked = isSelected

                holder.itemView.isEnabled = ! isSmarketContact
                if (isSmarketContact) {
                    holder.itemView.iv_smark_icon.visibility = View.VISIBLE
                    holder.itemView.tv_already_on_smarket.visibility = View.VISIBLE
                } else {
                    holder.itemView.iv_smark_icon.visibility = View.GONE
                    holder.itemView.tv_already_on_smarket.visibility = View.GONE
                }

                holder.itemView.setOnClickListener {
                    isSelected = if (! isSelected) {
                        if (selectedContactList < 20) {
                            selectedContactList += 1
                            ! isSelected
                        } else {
                            mActivity.showDialog(mActivity.resources.getString(R.string.contacts_not_more_than))
                            isSelected
                        }
                    } else {
                        selectedContactList -= 1
                        ! isSelected
                    }
                    contactsAdapter.notifyDataSetChanged()
                }
            }
        }

        override fun getHeaderId(position: Int): Long {
            return contactsToDisplay[position].name[0].toUpperCase().toLong()
        }

        override fun onCreateHeaderViewHolder(parent: ViewGroup?): ViewHolderHeader {

            val view: View
            val inflater = LayoutInflater.from(mActivity)

            view = inflater.inflate(R.layout.row_contact_header , parent , false)

            return ViewHolderHeader(view)
        }

        override fun onBindHeaderViewHolder(viewholder: ViewHolderHeader? , position: Int) {

            viewholder?.itemView?.tv_header?.text = contactsToDisplay[position].name.substring(0 , 1).toUpperCase()
        }

        override fun getSections(): Array<String> {

            val sections = ArrayList<String>(26)

            for (i in contactsToDisplay.indices) {
                val section = contactsToDisplay[i].name.substring(0 , 1).toUpperCase()
                if (! sections.contains(section)) {
                    sections.add(section)
                    mSectionPositions.add(i)
                }
            }

            return sections.toTypedArray()
        }

        override fun getSectionForPosition(position: Int): Int {
            return 0
        }

        override fun getPositionForSection(sectionIndex: Int): Int {
            return mSectionPositions[sectionIndex]
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

        inner class ViewHolderHeader(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }
}
