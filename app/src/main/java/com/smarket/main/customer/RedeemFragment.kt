package com.smarket.main.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.smarket.R
import com.smarket.adapter.GiftAdapter
import com.smarket.interfaces.UpdateRefCash
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.itemdecoration.VerticalDividerDecoration
import com.smarket.main.BaseMainFragment
import com.smarket.model.giftcard.Datum
import com.smarket.model.giftcard.GiftList
import com.smarket.model.refcashredeemrequest.GiftCard
import com.smarket.model.refcashredeemrequest.RefCashRedeemRequest
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_redeem.*
import kotlinx.android.synthetic.main.header.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RedeemFragment : BaseMainFragment() , View.OnClickListener , UpdateRefCash {

    var isFrom = ""

    var giftList = ArrayList<Datum>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var giftAdapter: GiftAdapter
    private var disposable: Disposable? = null

    var giftRequestList: ArrayList<GiftCard> = ArrayList()
    var totalAmount: Int = 0

    companion object {
        fun getInstance(isFrom: String = ""): RedeemFragment {
            val fragment = RedeemFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM , isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_redeem , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        mActivity.updateUserDetailsObject()

        callGiftListAPI()

        val btn = view.findViewById<Button>(R.id.btn_redeem)
        btn.setOnClickListener {
            callRedeemRefCashAPI()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        UpdateRefCashManager.addCallBack(this)

        var balance = 0.0
        if (mActivity.userDetails != null) {
            balance = mActivity.userDetails !!.refcash
        }

        setAvailableBalance(balance)
    }

    private fun setAvailableBalance(refCash: Double) {
        val tv_available_balance = view?.findViewById<TextView>(R.id.tvTotalAmount)
        if (mActivity.userDetails?.countryCode != "+91") {
            tv_available_balance?.text = "$$refCash"
        } else {
            tv_available_balance?.text = "₹$refCash"
        }
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
        }
    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.gift_card)
    }

    private fun callGiftListAPI() {
        activity !!.runOnUiThread {
            ProgressDialogUtil.showProgressDialog(mActivity)
        }

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callRefferalApi = WebApiClient.webApi().giftList()
        callRefferalApi?.enqueue(object : Callback<GiftList?> {
            override fun onFailure(call: Call<GiftList?> , t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<GiftList?> , response: Response<GiftList?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null)
                        if (response.body() !!.getData()?.size !! > 0) {
                            tvGift.visibility = View.GONE

                            giftList.clear()
                            giftRequestList.clear()

                            response.body() !!.getData() !!.forEach {
                                giftList.add(it !!)
                            }
                            linearLayoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.VERTICAL , false)
                            rv_gift.addItemDecoration(VerticalDividerDecoration(mActivity.convertDpToPixel(14F).toInt() , true))
                            rv_gift.layoutManager = linearLayoutManager

                            giftAdapter = GiftAdapter(giftList , activity !! , mActivity.userDetails !!.countryCode , object : OnEditTextChanged {
                                override fun onTextChanged(position: Int , amount: Int , count: String?) {
                                    giftRequestList[position].gift_quantity = count
                                    totalAmount = amount
                                }
                            })

                            giftList.forEachIndexed { index , _ ->
                                val addValues = GiftCard(
                                        gift_amount = giftList[index].amount.toString() ,
                                        merchant_name = giftList[index].name ,
                                        gift_quantity = "0" ,
                                        gift_validity = giftList[index].expiryDate)
                                giftRequestList.add(addValues)
                            }


                            rv_gift.adapter = giftAdapter
                        } else {
                            tvGift.visibility = View.VISIBLE
                        }
                }
            }
        })
    }

    private fun callRedeemRefCashAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val refCashRequest = RefCashRedeemRequest(totalAmount.toString() , giftRequestList)

        disposable = WebApiClient.webApi().redeemRefCash(refCashRequest).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        mActivity.showDialog(message = message !!)

                                                        val refcash = meta !!.refcash !!.toDouble()
                                                        UpdateRefCashManager.executeCallBacks(mActivity , refcash)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message = message !!)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }

    override fun onUpdateRefCash(refCash: Double) {
        setAvailableBalance(refCash)
    }
}