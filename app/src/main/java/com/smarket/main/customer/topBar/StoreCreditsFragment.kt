package com.smarket.main.customer.topBar

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smarket.utils.*
import com.smarket.utils.ProgressDialogUtil.hideProgressDialog
import com.smarket.utils.ProgressDialogUtil.showProgressDialog
import com.smarket.R
import com.smarket.location.LocationUpdates
import com.smarket.main.BaseMainFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.model.OfferDetails
import com.smarket.model.StoreCredit
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.StoreCreditResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_store_credits.view.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_store_credits.*
import kotlinx.android.synthetic.main.fragment_store_credits.swipe_refresh
import kotlinx.android.synthetic.main.fragment_store_credits.tv_no_data_found
import kotlinx.android.synthetic.main.header_search.et_search
import kotlinx.android.synthetic.main.row_search.view.*
import kotlinx.android.synthetic.main.row_store_credits.view.*
import kotlinx.android.synthetic.main.row_store_credits.view.tv_address
import kotlinx.android.synthetic.main.row_store_credits.view.tv_business_name
import kotlinx.android.synthetic.main.row_store_credits.view.tv_store_credit
import kotlinx.android.synthetic.main.row_store_credits.view.tv_tag_line
import kotlinx.android.synthetic.main.search_contacts.*
import kotlinx.android.synthetic.main.topbar_search_header.*
import kotlinx.android.synthetic.main.view_merchant_contact_details.view.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class StoreCreditsFragment : BaseMainFragment(), View.OnClickListener,
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(isFrom: String = ""): StoreCreditsFragment {
            val fragment = StoreCreditsFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var storeCreditsAdapter: StoreCreditsAdapter

    var storeCreditList = ArrayList<StoreCredit>()
    var offerDetails: OfferDetails? = null

    var availble_storeCredit = 0.0
    var enter_storeCredit = ""
    var merchant_id = ""

    var page = 1
    var lastPage = 1
    var isLoading = false

    var mLatitude = ""
    var mLongitude = ""

    var isFrom = ""

    var merchants = java.util.ArrayList<StoreCredit>()


    var searchText = ""
    var zipCode = ""
    var radius = 25
    private var disposable: Disposable? = null
    var observable: Observable<Response<StoreCreditResponse>>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_store_credits, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            mActivity,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        rv_store_credits.layoutManager = linearLayoutManager
        storeCreditsAdapter = StoreCreditsAdapter()
        rv_store_credits.adapter = storeCreditsAdapter



        SearchData()
    }

    private fun SearchData() {
        /*searchData.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                searchText = charSequence.toString().trim()
                page = 1
                callSearchStoreCreditAPI()

            }
            override fun afterTextChanged(editable: Editable) {
                //after the change calling the method and passing the search input
                Log.d("Search",editable.toString())
                            }
        })*/
        et_search_top.requestFocus()
        if (et_search_top.requestFocus()) {
            mActivity.showKeyboard(et_search_top)
        }

        et_search_top.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchText = s.toString().trim()
                page = 1
                callSearchMerchantAPI()
            }
        })

    }

    private fun callSearchMerchantAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (storeCreditList.find { it.id == 0L } == null) {
                storeCreditList.add(StoreCredit())
                storeCreditsAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.SearchMerchant.SEARCH_TEXT] = searchText
        hashMap[ApiParam.SearchMerchant.POST_CODE] = zipCode
        hashMap[ApiParam.SearchMerchant.DISTANCE] = "$radius"
        /*hashMap[ApiParam.SearchMerchant.SHOW_TOP_MERCHANT] = if (showTopMerchants) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }*/
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable =
            WebApiClient.webApi().searchStoreCreditAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body()!!) {
                                if (meta != null) {
                                    with(meta!!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    storeCreditList.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    storeCreditList.addAll(data)
                                                }

                                                if (links != null && links!!.next != null
                                                    && links!!.next!!.isRequiredField()
                                                ) {
                                                    this@StoreCreditsFragment.lastPage++
                                                } else {
                                                    this@StoreCreditsFragment.lastPage = page
                                                }

                                                page++
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it, mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it, mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (storeCreditList.isNotEmpty()) {
                            storeCreditList.removeAll(storeCreditList.filter { it.id == 0L })
                        }
                    }

                    storeCreditsAdapter.notifyDataSetChanged()

                    if (storeCreditList.isEmpty()) {
                        rv_store_credits.visibility = View.GONE
                        btn_view_merchants.visibility = View.GONE
                        tv_no_data_found.visibility = View.VISIBLE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                        rv_store_credits.visibility = View.VISIBLE
                    }

                    isLoading = false
                },
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (storeCreditList.isNotEmpty()) {
                            storeCreditList.removeAll(storeCreditList.filter { it.id == 0L })
                            storeCreditsAdapter.notifyDataSetChanged()
                        }
                    }

                    if (storeCreditList.isEmpty()) {
                        tv_no_data_found.visibility = View.VISIBLE
//                        btn_view_merchants.visibility = View.GONE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                    }

                    isLoading = false
//                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM)!!
            }
        }
    }

    private fun setHeader() {

        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        iv_search.visibility = View.VISIBLE
        iv_search.setOnClickListener(this)
        cancelText.setOnClickListener(this)
        iv_clearHeader.setOnClickListener(this)
        tv_title.text = resources.getString(R.string.store_credits)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        rv_store_credits.addOnScrollListener(object :
            androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: androidx.recyclerview.widget.RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == storeCreditsAdapter.itemCount - 1 &&
                    page <= lastPage && !isLoading
                ) {
                    callStoreCreditAPI()
                }
            }
        })

        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.cancelText -> {
                et_search_top.visibility = View.GONE
                iv_clearHeader.visibility = View.GONE
                cancelText.visibility = View.GONE
                iv_search.visibility = View.VISIBLE
                tv_title.visibility = View.VISIBLE
                et_search_top.setText("")
            }
            R.id.iv_clearHeader -> et_search_top.setText("")
           R.id.iv_search -> {
               et_search_top.visibility = View.VISIBLE
               iv_clearHeader.visibility = View.VISIBLE
               cancelText.visibility = View.VISIBLE
               tv_title.visibility = View.GONE
               iv_search.visibility = View.GONE
           }
//                mActivity.addFragment(SearchStoreCreditFragment.getInstance(), true , animationType = AnimationType.RightInZoomOut)
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    inner class StoreCreditsAdapter :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_store_credits, parent, false)
            }

            return ViewHolder(view)

        }

        override fun getItemViewType(position: Int): Int {
            return if (storeCreditList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return storeCreditList.size
        }

        override fun onBindViewHolder(
            holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
            position: Int
        ) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(storeCreditList[holder.adapterPosition]) {
                    holder.itemView.sdv_logo.loadFrescoImage(businessLogo)
                    if (distance.isRequiredField()) {
                        holder.itemView.tv_distance.text =
                            "($distance ${resources.getString(R.string.mi)})"
                    }
                    holder.itemView.tv_business_name.text = businessName
                    if (tagLine.isRequiredField()) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = tagLine
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }
                    holder.itemView.tv_address.text = address

                    if (mActivity.userDetails?.countryCode != "+91") {
                        holder.itemView.tv_store_credit.setCustomText(
                            "$$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.poppins_regular
                        )
                    } else {
                        holder.itemView.tv_store_credit.setCustomText(
                            "₹$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.poppins_regular
                        )
                    }

                    if (website.isRequiredField()) {
                        holder.itemView.iv_website.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_website.visibility = View.GONE
                    }

                    if (mobile.isRequiredField()) {
                        holder.itemView.iv_call.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_call.visibility = View.GONE
                    }

                    if (address.isRequiredField()) {
                        holder.itemView.iv_direction.visibility = View.VISIBLE
                    } else {
                        holder.itemView.iv_direction.visibility = View.GONE
                    }

                    holder.itemView.iv_call.setOnClickListener {
                        mActivity.makeCall(mobile)
                    }

                    holder.itemView.iv_direction.setOnClickListener {
                        navigateToGoogleMap(mActivity, latitude, longitude, address)
                    }
                    holder.itemView.iv_website.setOnClickListener {
                        mActivity.showWeb(website)
                    }
                }
                holder.itemView.setOnClickListener {
                    showStoreCreditsDetailsDialog(storeCreditList[position])
                }
            }
        }

        fun filterList(filteredNames: ArrayList<StoreCredit>) {
            Log.e("FilteredNamelist", filteredNames.toString())
            Log.e("listSize", filteredNames.size.toString())
            // this.dataList.clear()
            storeCreditList = filteredNames
            notifyDataSetChanged()
        }

        inner class ViewHolder(itemView: View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    fun showStoreCreditsDetailsDialog(storeCredits: StoreCredit) {

        val alertDialog = AlertDialog.Builder(mActivity).create()

        val dialogView =
            LayoutInflater.from(mActivity).inflate(R.layout.dialog_store_credits, null, false)

        with(storeCredits) {
            dialogView.sdv_business_logo.loadFrescoImage(businessLogo)
            dialogView.txt_business_name.text = businessName
            if (tagLine.isRequiredField()) {
                dialogView.txt_tag_line.visibility = View.VISIBLE
                dialogView.txt_tag_line.text = tagLine
            } else {
                dialogView.txt_tag_line.visibility = View.GONE
            }
            if (mActivity.userDetails?.countryCode != "+91") {
                dialogView.tvCurrencyType.text = "$"
                dialogView.tv_details.text =
                    "${resources.getString(R.string.you_have)} $ $storeCredit ${
                        resources.getString(R.string.store_credit_details)
                    }"
            } else {
                dialogView.tvCurrencyType.text = "₹"
                dialogView.tv_details.text =
                    "${resources.getString(R.string.you_have)} ₹ $storeCredit ${
                        resources.getString(R.string.store_credit_details)
                    }"
            }
            availble_storeCredit = storeCredit
            merchant_id = "$merchantId"
        }

        dialogView.iv_clsoe.setOnClickListener {
            alertDialog.dismiss()
        }

        dialogView.btn_generate_code.setOnClickListener {

            enter_storeCredit = dialogView.et_store_credit.text.toString()

            if (checkForValidations()) {

                alertDialog.dismiss()
                callGenerateRedemptionCodeForStoreCreditAPI()

            }
        }

        alertDialog.setView(dialogView)

        alertDialog.show()
    }

    private fun checkForValidations(): Boolean {

        if (!enter_storeCredit.isRequiredField() || enter_storeCredit.toDouble() > availble_storeCredit) {
            mActivity.showDialog(resources.getString(R.string.valid_store_credit))
            return false
        }

        return true
    }

    private fun callStoreCreditAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (storeCreditList.find { it.id == 0L } == null) {
                storeCreditList.add(StoreCredit())
                storeCreditsAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().storeCreditAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body()!!) {
                                if (meta != null) {
                                    kotlin.with(meta!!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    this@StoreCreditsFragment.storeCreditList.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    this@StoreCreditsFragment.storeCreditList.addAll(
                                                        data
                                                    )
                                                }

                                                page++

                                                this@StoreCreditsFragment.lastPage = lastPage
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it, mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it, mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (storeCreditList.isNotEmpty()) {
                            storeCreditList.removeAll(storeCreditList.filter { it.id == 0L })
                        }
                    }

                    storeCreditsAdapter.notifyDataSetChanged()

                    tv_no_data_found.visibility = if (storeCreditList.isEmpty()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    isLoading = false
                },
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (storeCreditList.isNotEmpty()) {
                            storeCreditList.removeAll(storeCreditList.filter { it.id == 0L })
                            storeCreditsAdapter.notifyDataSetChanged()
                        }
                    }

                    tv_no_data_found.visibility = if (storeCreditList.isEmpty()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    private fun callGenerateRedemptionCodeForStoreCreditAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = merchant_id
        hashMap[ApiParam.AMOUNT] = enter_storeCredit
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude

        disposable = WebApiClient.webApi().generateRedemptionCodeForStoreCreditAPI(hashMap)
            .subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    hideProgressDialog()

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body()!!) {
                                if (meta != null) {
                                    kotlin.with(meta!!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                offerDetails = data
                                                offerDetails?.enter_storeCredit = enter_storeCredit
                                                mActivity.addFragment(
                                                    OfferQRCodeFragment.getInstance(
                                                        StoreCreditsFragment::class.java.canonicalName,
                                                        offerDetails = offerDetails,
                                                        merchantMessage = ""
                                                    ),
                                                    true,
                                                    animationType = AnimationType.RightInZoomOut
                                                )
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it, mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it, mActivity)
                        }
                    }
                },
                {
                    if (disposable == null) return@subscribe

                    hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        locationUpdates = null
        super.onDestroy()
    }

    private var locationUpdates: LocationUpdates? = LocationUpdates { isLatest, latLng ->

        if (isAdded && !isHidden) {

            if (swipe_refresh.isRefreshing) {
                swipe_refresh.isRefreshing = false
            }

            if (latLng != null) {

                mLatitude = latLng.latitude.toString()
                mLongitude = latLng.longitude.toString()

                if (isLatest) {
                    callStoreCreditAPI()

                } else {
                    mActivity.showDialogWithAction(
                        resources.getString(R.string.location_not_found_ask_for_next_action),
                        positiveButtonLabel = resources.getString(R.string.Continue),
                        showNegativeButton = true
                    ) {
                        callStoreCreditAPI()
                    }
                }

            } else {
                mActivity.showDialog(resources.getString(R.string.location_not_found))
            }
        }
    }
}