package com.smarket.main.customer

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.SearchProductAdapter
import com.smarket.database.User
import com.smarket.main.BaseMainFragment
import com.smarket.main.FAQsFragment
import com.smarket.main.customer.topBar.AwaitingRewardsFragment
import com.smarket.main.customer.topBar.ReferralAlertsFragment
import com.smarket.main.customer.topBar.StoreCreditsFragment
import com.smarket.model.searchproduct.OfferProduct
import com.smarket.model.searchproduct.Searchproductresponse
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.UserActivityResponse
import com.smarket.webservices.response.refferedlist.ReferredList
import com.tap.app.utils.CustomTypefaceSpan
import kotlinx.android.synthetic.main.fragment_cust_home.*
import kotlinx.android.synthetic.main.fragment_friends_of_smarket.*
import kotlinx.android.synthetic.main.header_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import utils.AnimationType
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SearchProductFragment : BaseMainFragment(), View.OnClickListener {

    var searchText = ""
    val offerProduct = ArrayList<OfferProduct>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var bannersAdapter: SearchProductAdapter


    var cartList: ArrayList<OfferProduct> = ArrayList()
    var cartcount: Int = 1

    val tv_available_cart = view?.findViewById<TextView>(R.id.ivcartcount)

    companion object {
        fun getInstance(searchText: String = ""): SearchProductFragment {
            val fragment = SearchProductFragment()
            val bundle = Bundle()
            bundle.putString("searchText", searchText)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()


    }

  /*  private fun setAvailablecartcount() {
        val tv_available_balance = view?.findViewById<TextView>(R.id.ivcartcount)
        if (mActivity.userDetails?.countryCode != "+91") {
            tv_available_balance?.text
        }
        tv_available_balance.setOnClickListener {
            val intent = Intent(this@SearchProductFragment, CartProductFragment::class.java)
            startActivity(intent)
        }

    }*/


   // val tv_available_balance = view?.findViewById<TextView>(R.id.ivcartcount)


    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("searchText")) {
                searchText = bundle.getString("searchText")!!
                if (searchText.isRequiredField()) {
                    et_search.setText(searchText)
                }
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.setOnClickListener(this)
        iv_clear.setOnClickListener(this)
        iv_search.setOnClickListener(this)
         ivcartcount.setOnClickListener(this)
        iv_search.visibility = View.VISIBLE
        iv_filter.visibility = View.GONE
        // iv_filter.setBackgroundResource(R.drawable.search)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        /* et_search.requestFocus()
         if (et_search.requestFocus()) {
             mActivity.showKeyboard(et_search)
         }
 */
        et_search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchText = s.toString().trim()
                //callSearchProductAPI()
            }
        })

        callSearchProductAPI()

        setUserDetails(mActivity.userDetails!!)

        callReferralAPI()

        ll_refcash.setOnClickListener(this)
        ll_referral_alerts.setOnClickListener(this)
        ll_awaiting_rewards.setOnClickListener(this)
        ll_store_credit.setOnClickListener(this)
        ll_rfriends.setOnClickListener(this)


    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_back -> mActivity.onBackPressed()
            R.id.iv_clear -> et_search.setText("")
            R.id.iv_search -> callSearchProductAPI()
            R.id.ll_refcash -> mActivity.addFragment(RedeemRefcashFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            R.id.ll_referral_alerts -> mActivity.addFragment(ReferralAlertsFragment.getInstance(IS_FROM_HOME), true, animationType = AnimationType.RightInZoomOut)
            R.id.ll_awaiting_rewards -> mActivity.addFragment(AwaitingRewardsFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            R.id.ll_store_credit -> mActivity.addFragment(StoreCreditsFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            R.id.ll_rfriends -> mActivity.addFragment(FriendsOnSmarketFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            R.id.ivcartcount -> mActivity.addFragment(CartProductFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)

        }
    }


    private fun callSearchProductAPI() {

        activity!!.runOnUiThread {
            ProgressDialogUtil.showProgressDialog(mActivity)
        }

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val callSearchAPI = WebApiClient.webApi().searchProductList(searchText)
        callSearchAPI?.enqueue(object : Callback<Searchproductresponse?> {
            override fun onFailure(call: Call<Searchproductresponse?>, t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<Searchproductresponse?>, response: Response<Searchproductresponse?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null) {

                        if (response.body()?.offerProduct != null) {
                            offerProduct.clear()
                            response.body()?.offerProduct?.forEach {
                                offerProduct.add(it!!)
                            }
                        }

                        if (offerProduct.size > 0) {
                                refer_friends_tv.visibility = View.GONE
                                // refer_list_rv.addItemDecoration(VerticalDividerDecoration(mActivity.convertDpToPixel(16F).toInt() , true))
                                refer_list_rv.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
                                bannersAdapter = SearchProductAdapter(offerProduct, mActivity, mActivity.userDetails!!.countryCode, object : OnCartTextChanged {
                                    override fun onTextChanged(position: Int, count: String?) {
                                        ivcartcount.setText(count)
                                        /*cartList[position].cart = count
                                        cartcount = position*/
                                    }
                                }) { itemList, i, view ->


                                when (view.id) {
                                    R.id.cvProductSearch -> {

                                        val sdf = SimpleDateFormat("yyyy-MM-dd:hh:mm:ss")
                                        val currentDate = sdf.format(Date())

                                        if (SMarket.appDB.userDao().getData()!!.countryCode != "+91") {
                                            if (itemList.get(i).storeName.equals("Amazon.in", true) || itemList.get(i).storeName.equals("Amazon.com", true)) {
                                                sharePopUpOutside(itemList[i].productName!!, itemList[i].productPrice!!, itemList.get(i).productUrl!!, currentDate, mActivity.userDetails!!.id.toString(), itemList.get(i).earnValue!!, itemList.get(i).storeName!!, itemList.get(i).productImage!!)
                                            } else {
                                                sharePopUpOutside(itemList[i].productName!!, itemList[i].productPrice!!, itemList.get(i).productUrl!!, currentDate, mActivity.userDetails!!.id.toString(), itemList.get(i).earnValue!!, itemList.get(i).storeName!!, itemList.get(i).productImage!!)
                                            }
                                        } else {
                                            if (itemList.get(i).storeName.equals("Amazon.in", true) || itemList.get(i).storeName.equals("Amazon.com", true)) {
                                                sharePopUp(itemList[i].productName!!, itemList.get(i).productPrice!!, itemList.get(i).productUrl!!, currentDate, mActivity.userDetails!!.id.toString(), itemList.get(i).earnValue!!, itemList.get(i).storeName!!, itemList.get(i).productImage!!)
                                            } else {
                                                sharePopUp(itemList[i].productName!!, itemList.get(i).productPrice!!, itemList.get(i).productUrl!!, currentDate, mActivity.userDetails!!.id.toString(), itemList.get(i).earnValue!!, itemList.get(i).storeName!!, itemList.get(i).productImage!!)
                                            }
                                        }
                                    }
                                }

                            }
                            refer_list_rv.adapter = bannersAdapter

                        } else {
                            refer_friends_tv.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })
    }

    private fun sharePopUpOutside(name: String, price: String, productUrl: String,
                                  clicked: String, userID: String, earn: String, merchantName: String, imageURL: String) {

        /*  val alertDialog = AlertDialog.Builder(mActivity).create()
          val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share , null)
          alertDialog.setView(dialogView)
          alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

          val text = dialogView.findViewById<TextView>(R.id.tv_text)
          val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
          val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
          val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
          val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)

          val typeface = ResourcesCompat.getFont(mActivity , R.font.stencil_std_bold) !!
          val adobe = ResourcesCompat.getFont(mActivity , R.font.adobe_devanagari) !!

          val spannable = CustomTypefaceSpan("" , typeface)
          val adobeFont = CustomTypefaceSpan("" , adobe)

          val spannableString = SpannableString(resources.getString(R.string.each_time))
          val rewardString = SpannableString(resources.getString(R.string.refer_friends))

          spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
          spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
          spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
          spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

          rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
          rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

          tv_life.text = spannableString
          reward.text = rewardString

          text.setOnClickListener {
              try {
                  mActivity.addFragment(RecommendFriendsFragment.getInstance()
                          , true , animationType = AnimationType.RightInZoomOut)
              } catch (e: Exception) {
                  Log.e("" , "" + e.message)
              }
              alertDialog.dismiss()
          }
          messanger.setOnClickListener {
              val intent = Intent()
              intent.action = Intent.ACTION_SEND
              intent.type = "text/plain"
              intent.putExtra(Intent.EXTRA_TEXT ,
                      mActivity.resources.getString(R.string.general_invite_flow_message ,
                              "https://rb.gy/0otkjs" , mActivity.userDetails?.name ,
                              mActivity.userDetails?.countryCode , mActivity.userDetails?.mobile , mActivity.userDetails?.referral_code))

              val shareMessage = mActivity.userDetails?.referral_message !!.replace("XXXXXX" , mActivity.userDetails?.referral_code !!)
              intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
              startActivity(Intent.createChooser(intent , "Share"));
          }

          btnViewSite.setOnClickListener {
              alertDialog.dismiss()
              callUserActivityAPI(name , price , productUrl , clicked , userID , earn , merchantName , imageURL)
          }

          alertDialog.show()*/

        mActivity.runOnUiThread {
            val alertDialog = AlertDialog.Builder(mActivity).create()
            val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share, null)
            alertDialog.setView(dialogView)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val messanger = dialogView.findViewById<Button>(R.id.btnShare)
            val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)

            val adobe = ResourcesCompat.getFont(mActivity, R.font.adobe_devanagari_bold)!!
            val adobeFont = CustomTypefaceSpan("", adobe)

            val typeface = ResourcesCompat.getFont(mActivity, R.font.poppins_italic)!!
            val spannable = CustomTypefaceSpan("", typeface)

            val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
            val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
            val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

            val second = SpannableString("₹20\nBonus Cash")
            second.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(spannable, 3, 14, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            second.setSpan(ForegroundColorSpan(Color.YELLOW), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSecond.text = second

            val third = SpannableString("30%\nCash Rewards")
            third.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(2.2f), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(RelativeSizeSpan(1.7f), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            third.setSpan(ForegroundColorSpan(Color.YELLOW), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvThird.text = third

            messanger.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                val shareMessage = mActivity.userDetails?.referral_message!!.replace("XXXXXX", mActivity.userDetails?.referral_code!!)
                intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(intent, "Share"))
            }

            tvFAQ.setOnClickListener {
                alertDialog.dismiss()
                mActivity.addFragment(FAQsFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
            }

            btnViewSite.setOnClickListener {
                alertDialog.dismiss()
                callUserActivityAPI(name, price, productUrl, clicked, userID, earn, merchantName, imageURL)
            }
            alertDialog.show()
        }
    }

    private fun sharePopUp(name: String, price: String, productUrl: String,
                           clicked: String, userID: String, earn: String, merchantName: String, imageURL: String) {

        val alertDialog = AlertDialog.Builder(mActivity).create()
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_share_new, null)
        alertDialog.setView(dialogView)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val messanger = dialogView.findViewById<Button>(R.id.btnShare)
        val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)

        val adobe = ResourcesCompat.getFont(mActivity, R.font.adobe_devanagari_bold)!!
        val adobeFont = CustomTypefaceSpan("", adobe)

        val typeface = ResourcesCompat.getFont(mActivity, R.font.poppins_italic)!!
        val spannable = CustomTypefaceSpan("", typeface)

        val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
        val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
        val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
        val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

        val first = SpannableString("₹50\nCash Rewards")
        first.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(ForegroundColorSpan(Color.YELLOW), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvFirst.text = first

        val second = SpannableString("₹20\nBonus Cash")
        second.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(spannable, 3, 14, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(RelativeSizeSpan(2.2f), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(ForegroundColorSpan(Color.YELLOW), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvSecond.text = second

        val third = SpannableString("30%\nCash Rewards")
        third.setSpan(adobeFont, 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(spannable, 3, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(2.2f), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(1.7f), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(ForegroundColorSpan(Color.YELLOW), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvThird.text = third

        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"

            val shareMessage = mActivity.userDetails?.referral_message!!.replace("XXXXXX", mActivity.userDetails?.referral_code!!)
            intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(intent, "Share"))

        }

        tvFAQ.setOnClickListener {
            alertDialog.dismiss()
            mActivity.addFragment(FAQsFragment.getInstance(), true, animationType = AnimationType.RightInZoomOut)
        }

        btnViewSite.setOnClickListener {
            alertDialog.dismiss()
            callUserActivityAPI(name, price, productUrl, clicked, userID, earn, merchantName, imageURL)
        }

        alertDialog.show()
    }

    private fun callUserActivityAPI(productName: String, price: String, productUrl: String,
                                    clicked: String, userID: String, earn: String, merchantName: String, imageURL: String) {
        activity!!.runOnUiThread {
            ProgressDialogUtil.showProgressDialog(mActivity)
        }

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            ProgressDialogUtil.hideProgressDialog()
            return
        }

        val hashMap = ApiParam.getHashMap()

        if (mActivity.userDetails != null) {
            with(mActivity.userDetails!!) {
                hashMap[ApiParam.PHONE_NUMBER] = mobile
                hashMap[ApiParam.MERCHANT_NAME] = merchantName
            }
        }
        hashMap[ApiParam.PRODUCT_NAME] = productName
        hashMap[ApiParam.PRODUCT_PRICE] = price
        hashMap[ApiParam.REWARD_VALUE] = earn
        hashMap[ApiParam.STATUS] = "1"
        hashMap[ApiParam.DEVICE_TYPE] = "Android"
        hashMap[ApiParam.PRODUCT_URL] = productUrl
        hashMap[ApiParam.PRODUCT_IMAGE_URL] = imageURL
        hashMap[ApiParam.CLICKED_AT] = clicked
        hashMap[ApiParam.USER_ID] = userID

        val callUserActivity = WebApiClient.webApi().userActivity(hashMap)

        callUserActivity?.enqueue(object : Callback<UserActivityResponse?> {
            override fun onFailure(call: Call<UserActivityResponse?>, t: Throwable) {
                ProgressDialogUtil.hideProgressDialog()
            }

            override fun onResponse(call: Call<UserActivityResponse?>, response: Response<UserActivityResponse?>) {
                ProgressDialogUtil.hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (!merchantName.contains("eBay", true)) {
                            if (merchantName == "Amazon India") {
                                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$productUrl/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarketindia-21"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon.in") {
                                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$productUrl&linkCode=ll1&tag=smarketindia-21"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon.com") {
                                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$productUrl/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=smarket0f-20"))
                                startActivity(browserIntent)
                            } else if (merchantName == "Amazon") {
                                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$productUrl&linkCode=ll1&tag=smarket0f-20"))
                                startActivity(browserIntent)
                            } else {
                                val browserIntent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("http://redirect.viglink.com?u=$productUrl&key=f5e38af288c412320cf9ff35e2299c98"))
                                startActivity(browserIntent)
                            }
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://rover.ebay.com/rover/1/711-53200-19255-0/1?campid=5338693049&customid=" + mActivity.userDetails!!.mobile + "!&toolid=20006&mpre=" + productUrl))
                            startActivity(browserIntent)
                        }

                    } else {
                        Toast.makeText(activity, "No product found!", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(activity, "No product found!", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setUserDetails(user: User) {
        with(user) {
            tv_referral.text = refferalAlert.toString()
            tv_awaiting_rewards.text = awaitingRewards.toString()
            if (mActivity.userDetails?.countryCode != "+91") {
                tv_store_credit.text = "$$storeCredit"
                tv_refcash.text = "$$refcash"
            } else {
                tv_store_credit.text = "₹$storeCredit"
                tv_refcash.text = "₹$refcash"
            }
        }
    }

    private fun callReferralAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        val callRefferalApi = WebApiClient.webApi().referList(SMarket.appDB.userDao().getData()!!.id.toString())
        callRefferalApi?.enqueue(object : Callback<ReferredList?> {
            override fun onFailure(call: Call<ReferredList?>, t: Throwable) {
                Log.e("referFriends", "referFriends" + t.message)
            }

            override fun onResponse(call: Call<ReferredList?>, response: Response<ReferredList?>) {
                if (response.isSuccessful) {
                    if (response.body() != null)
                        if (response.body()!!.getData()?.getRefered()?.size!! > 0) {
                            tv_rfriends.text = response.body()!!.getData()?.getRefered()?.size.toString()
                        } else {
                            try {
                                tv_rfriends.text = "0"
                            } catch (e: Exception) {
                                Log.e("referFriends", "referFriends" + e.message)
                            }
                        }
                }
            }
        })
    }
}