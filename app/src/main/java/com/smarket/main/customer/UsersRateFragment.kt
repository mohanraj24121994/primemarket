package com.smarket.main.customer

import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.main.BaseMainFragment
import com.smarket.model.Referrals
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.divider.view.*
import kotlinx.android.synthetic.main.fragment_users_rate.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_users_rate.view.*
import java.util.ArrayList

class UsersRateFragment : BaseMainFragment(), View.OnClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(merchantId: Long): UsersRateFragment {
            val fragment = UsersRateFragment()
            val bundle = Bundle()
            bundle.putLong(MERCHANT_ID, merchantId)
            fragment.arguments = bundle
            return fragment
        }
    }

    var merchantId = 0L
    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var usersRateAdapter: UsersRateAdapter
    var page = 1
    var lastPage = 1
    var isLoading = false

    private var disposable: Disposable? = null

    var referrals = ArrayList<Referrals>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_users_rate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_ratings.layoutManager = linearLayoutManager
        usersRateAdapter = UsersRateAdapter()
        rv_ratings.adapter = usersRateAdapter
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(MERCHANT_ID)) {
                merchantId = bundle.getLong(MERCHANT_ID)
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.visibility = View.VISIBLE
        iv_back.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.users_rate)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        rv_ratings.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == usersRateAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callMerchantRatereviewAPI()
                }
            }
        })

        callMerchantRatereviewAPI()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        callMerchantRatereviewAPI()
    }


    inner class UsersRateAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_users_rate, parent, false)
            }
            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (referrals[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return referrals.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(referrals[position]) {
                    holder.itemView.sdv_profile_pic.loadFrescoImage(profilePic)
                    holder.itemView.tv_name.text = mActivity.resources.getString(R.string.verified_user)
                    holder.itemView.rating_bar.rating = rating
                    holder.itemView.tv_average_ratings.text = "$rating"
                    holder.itemView.tv_rated_date.text = "${mActivity.resources.getString(R.string.rated_on)} ${getChangedDateFormat(rateOn, DATE_FORMAT, APP_DATE_FORMAT)}"

                    if (image.isRequiredField()) {
                        holder.itemView.sdv_offer.visibility = View.VISIBLE
                        holder.itemView.sdv_offer.loadFrescoImage(image)
                    } else {
                        holder.itemView.sdv_offer.visibility = View.GONE
                    }
                    if (review.isRequiredField()) {
                        holder.itemView.tv_review_label.visibility = View.VISIBLE
                        holder.itemView.tv_review.text = review
                    } else {
                        holder.itemView.tv_review_label.visibility = View.GONE
                        holder.itemView.tv_review.text = ""
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }


    private fun callMerchantRatereviewAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (referrals.find { it.id == 0L } == null) {
                referrals.add(Referrals())
                usersRateAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = "$merchantId"
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable = WebApiClient.webApi().merchantRateReviewAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            referrals.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            referrals.addAll(data)
                                                        }

                                                        page++

                                                        this@UsersRateFragment.lastPage = lastPage

                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }

                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (referrals.isNotEmpty()) {
                                    referrals.removeAll(referrals.filter { it.id == 0L })
                                }
                            }
                            usersRateAdapter.notifyDataSetChanged()
                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (referrals.isNotEmpty()) {
                                    referrals.removeAll(referrals.filter { it.id == 0L })
                                    usersRateAdapter.notifyDataSetChanged()
                                }
                            }
                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}