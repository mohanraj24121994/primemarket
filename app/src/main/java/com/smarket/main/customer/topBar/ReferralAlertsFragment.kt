package com.smarket.main.customer.topBar

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.NetworkUtil
import com.smarket.utils.addFragment
import com.smarket.utils.getPrimaryColor
import com.smarket.utils.showToast
import com.smarket.R
import com.smarket.location.LocationUpdates
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.ReferralAlertDetailsFragment
import com.smarket.model.ReferralAlertAndAwaitingReward
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ReferralAlertsAndAwaitingRewardsResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_awaiting_rewards.*
import kotlinx.android.synthetic.main.fragment_referral_alerts.*
import kotlinx.android.synthetic.main.fragment_referral_alerts.swipe_refresh
import kotlinx.android.synthetic.main.fragment_referral_alerts.tv_no_data_found
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.row_referral_alerts_and_awaiting_rewards.view.*
import kotlinx.android.synthetic.main.topbar_search_header.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.view.*
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class ReferralAlertsFragment : BaseMainFragment(), View.OnClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(isFrom: String = "", merchantId: Long = 0L): ReferralAlertsFragment {
            val fragment = ReferralAlertsFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            bundle.putLong(MERCHANT_ID, merchantId)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var referralAlertsAdapter: ReferralAlertsAdapter

    var referralAlertsList = ArrayList<ReferralAlertAndAwaitingReward>()

    var page = 1
    var lastPage = 1
    var isLoading = false
    var totalNoOfReferralAlerts = 0L
    var searchText = ""
    var zipCode = ""
    var radius = 25

    var mLatitude = ""
    var mLongitude = ""

    private var disposable: Disposable? = null
    var observable: Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>? = null

    var isFrom = ""
    var merchantId = 0L

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_referral_alerts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.VERTICAL , false)
        rv_referral_alerts.layoutManager = linearLayoutManager
        referralAlertsAdapter = ReferralAlertsAdapter()
        rv_referral_alerts.adapter = referralAlertsAdapter

        SearchData()
    }
    private fun SearchData() {
        et_search_top.requestFocus()
        if (et_search_top.requestFocus()) {
            mActivity.showKeyboard(et_search_top)
        }

        et_search_top.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                searchText = s.toString().trim()
                page = 1
                callSearchReferralAlertsAPI()
            }
        })

    }
    private fun callSearchReferralAlertsAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (referralAlertsList.find { it.id == 0L } == null) {
                referralAlertsList.add(ReferralAlertAndAwaitingReward())
                referralAlertsAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.SearchMerchant.SEARCH_TEXT] = searchText
        hashMap[ApiParam.SearchMerchant.POST_CODE] = zipCode
        hashMap[ApiParam.SearchMerchant.DISTANCE] = "$radius"
        /*hashMap[ApiParam.SearchMerchant.SHOW_TOP_MERCHANT] = if (showTopMerchants) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }*/
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().searchReferalListAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    referralAlertsList.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    referralAlertsList.addAll(data)
                                                }

                                                if (links != null && links !!.next != null
                                                    && links !!.next !!.isRequiredField()) {
                                                    this@ReferralAlertsFragment.lastPage ++
                                                } else {
                                                    this@ReferralAlertsFragment.lastPage = page
                                                }

                                                page ++
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (referralAlertsList.isNotEmpty()) {
                            referralAlertsList.removeAll(referralAlertsList.filter { it.id == 0L })
                        }
                    }

                    referralAlertsAdapter.notifyDataSetChanged()

                    if (referralAlertsList.isEmpty()) {
                        rv_referral_alerts.visibility = View.GONE
//                        btn_view_merchants.visibility = View.GONE
                        tv_no_data_found.visibility = View.VISIBLE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                        rv_referral_alerts.visibility = View.VISIBLE
                    }

                    isLoading = false
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (referralAlertsList.isNotEmpty()) {
                            referralAlertsList.removeAll(referralAlertsList.filter { it.id == 0L })
                            referralAlertsAdapter.notifyDataSetChanged()
                        }
                    }

                    if (referralAlertsList.isEmpty()) {
                        tv_no_data_found.visibility = View.VISIBLE
//                        btn_view_merchants.visibility = View.GONE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }


    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
            if (bundle.containsKey(MERCHANT_ID)) {
                merchantId = bundle.getLong(MERCHANT_ID)
            }
        }
    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
            iv_search.visibility = View.VISIBLE
            iv_search.setOnClickListener(this)
        } else if (isFrom == IS_FROM_HOME) {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
            iv_search.visibility = View.VISIBLE
            iv_search.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
            iv_search.visibility = View.GONE
        }
        iv_search.setOnClickListener(this)
        cancelText.setOnClickListener(this)
        iv_clearHeader.setOnClickListener(this)

        tv_title.text = resources.getString(R.string.referral_alerts)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        rv_referral_alerts.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == referralAlertsAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callReferralAlertsAPI()
                }
            }
        })

        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.cancelText -> {
                et_search_top.visibility = View.GONE
                iv_clearHeader.visibility = View.GONE
                cancelText.visibility = View.GONE
                iv_search.visibility = View.VISIBLE
                tv_title.visibility = View.VISIBLE
                et_search_top.setText("")
            }
            R.id.iv_clearHeader -> et_search_top.setText("")
            R.id.iv_search -> {
                et_search_top.visibility = View.VISIBLE
                iv_clearHeader.visibility = View.VISIBLE
                cancelText.visibility = View.VISIBLE
                tv_title.visibility = View.GONE
                iv_search.visibility = View.GONE
            }
               /* mActivity.addFragment(
                SearchReferralAlertsFragment.getInstance()
                    , true, animationType = AnimationType.RightInZoomOut)
            */else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    inner class ReferralAlertsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_referral_alerts_and_awaiting_rewards, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (referralAlertsList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return referralAlertsList.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(referralAlertsList[holder.adapterPosition]) {
                    holder.itemView.sdv_logo.loadFrescoImage(businessLogo)
                    if (distance.isRequiredField()) {
                        holder.itemView.tv_distance.text = "($distance ${resources.getString(R.string.mi)})"
                    }
                    holder.itemView.tv_business_name.text = businessName
                    if (tagLine.isRequiredField()) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = tagLine
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }
                    holder.itemView.rating_bar.rating = averageRating
                    holder.itemView.tv_average_ratings.text = "$averageRating"
                    holder.itemView.tv_ratings.text = "($noOfRating)"

                    if (mActivity.userDetails?.countryCode != "+91") {
                        holder.itemView.tv_store_credit.setCustomText(
                            "$$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.roboto_regular
                        )
                    }
                    else {
                        holder.itemView.tv_store_credit.setCustomText(
                            "₹$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.roboto_regular
                        )
                    }
                    holder.itemView.tv_referrals.setCustomText("$referrals"
                            , mActivity.resources.getString(R.string.referrals)
                            , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt)
                            , fontFamilyId = R.font.poppins_regular)

                    holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(this!!.expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                    if (status == 3) {
                        holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                        holder.itemView.tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    if(status == 2){
                        holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.redeemed_on)} ${getChangedDateFormat(this!!.redeemedDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                        holder.itemView.tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    when (subOfferCategory) {
                        ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                            holder.itemView.tv_offer_value.text = ""
                            holder.itemView.tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                            holder.itemView.tv_additional_details.text = title
                        }
                        ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {

                            if (mActivity.userDetails?.countryCode != "+91") {
                                holder.itemView.tv_offer_value.text = "$$amount"
                            }
                            else {
                                holder.itemView.tv_offer_value.text = "₹$amount"
                            }
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.store_credit)
                            if (purchasedBy == ApiParam.NO_BONUS) {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.no_bonus)
                            } else {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.welcome_bonus)
                            }
                        }
                        ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {

                            if (mActivity.userDetails?.countryCode != "+91") {
                                holder.itemView.tv_offer_value.text = "$$amount"
                            }
                            else {
                                holder.itemView.tv_offer_value.text = "₹$amount"
                            }
                                holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.gift_card)
                            if (purchasedBy == ApiParam.NO_BONUS) {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.no_bonus)
                            } else {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.welcome_bonus)
                            }
                        }
                    }

                    holder.itemView.setOnClickListener {
                        mActivity.addFragment(
                            ReferralAlertDetailsFragment.getInstance(referralAlertsList[position])
                                , true, animationType = AnimationType.RightInZoomOut)
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            if (isFrom == IS_FROM_DRAWER) {
                mActivity.setDrawerEnable(true)
            }
        }
        super.onHiddenChanged(hidden)
    }

    private fun callReferralAlertsAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (referralAlertsList.find { it.id == 0L } == null) {
                referralAlertsList.add(ReferralAlertAndAwaitingReward())
                referralAlertsAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude

        if (isFrom != IS_FROM_DRAWER && merchantId != 0L) {
            hashMap[ApiParam.MERCHANT_ID] = "$merchantId"
        }

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().referralAlertsAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            referralAlertsList.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            referralAlertsList.addAll(data)
                                                        }

                                                        page++

                                                        this@ReferralAlertsFragment.lastPage = lastPage

                                                        totalNoOfReferralAlerts = total
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (referralAlertsList.isNotEmpty()) {
                                    referralAlertsList.removeAll(referralAlertsList.filter { it.id == 0L })
                                }
                            }

                            referralAlertsAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (referralAlertsList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            setAvailableReferralAlerts()

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (referralAlertsList.isNotEmpty()) {
                                    referralAlertsList.removeAll(referralAlertsList.filter { it.id == 0L })
                                    referralAlertsAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (referralAlertsList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            setAvailableReferralAlerts()

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun setAvailableReferralAlerts() {
        if (referralAlertsList.isEmpty()) {
            tv_referral_alerts.visibility = View.GONE
        } else {
            tv_referral_alerts.visibility = View.VISIBLE
            tv_referral_alerts.setCustomText("$totalNoOfReferralAlerts"
                    , mActivity.resources.getString(R.string.referral_alerts_available)
                    , fontColor = mActivity.resources.getColor(R.color.black))
        }
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        locationUpdates = null
        super.onDestroy()
    }

    private var locationUpdates: LocationUpdates? = LocationUpdates { isLatest, latLng ->
        if (isAdded && !isHidden) {

            if (swipe_refresh.isRefreshing) {
                swipe_refresh.isRefreshing = false
            }

            if (latLng != null) {

                mLatitude = latLng.latitude.toString()
                mLongitude = latLng.longitude.toString()

                if (isLatest) {
                    callReferralAlertsAPI()
                } else {
                    mActivity.showDialogWithAction(resources.getString(R.string.location_not_found_ask_for_next_action),
                            positiveButtonLabel = resources.getString(R.string.Continue), showNegativeButton = true) {
                        callReferralAlertsAPI()
                    }
                }

            } else {
                mActivity.showDialog(resources.getString(R.string.location_not_found))
            }
        }
    }
}