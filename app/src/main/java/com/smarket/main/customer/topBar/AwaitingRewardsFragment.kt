package com.smarket.main.customer.topBar

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.location.LocationUpdates
import com.smarket.main.BaseMainFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.model.ReferralAlertAndAwaitingReward
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ReferralAlertsAndAwaitingRewardsResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_awaiting_rewards.*
import kotlinx.android.synthetic.main.fragment_awaiting_rewards.swipe_refresh
import kotlinx.android.synthetic.main.fragment_awaiting_rewards.tv_no_data_found
import kotlinx.android.synthetic.main.fragment_referral_alerts.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_store_credits.*
import kotlinx.android.synthetic.main.row_referral_alerts_and_awaiting_rewards.view.*
import kotlinx.android.synthetic.main.topbar_search_header.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.view.*
import retrofit2.Response
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class AwaitingRewardsFragment : BaseMainFragment(), View.OnClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(isFrom: String = ""): AwaitingRewardsFragment {
            val fragment = AwaitingRewardsFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var awaitingRewardsAdapter: AwaitingRewardsAdapter


    var awaitingRewardsList = ArrayList<ReferralAlertAndAwaitingReward>()
    var searchText = ""

    var page = 1
    var lastPage = 1
    var isLoading = false
    var totalNoOfRewards = 0L

    var zipCode = ""
    var radius = 25
    var mLatitude = ""
    var mLongitude = ""

    var isFrom = ""
    private var disposable: Disposable? = null
    var observable: Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_awaiting_rewards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_awaiting_rewards.layoutManager = linearLayoutManager
        awaitingRewardsAdapter = AwaitingRewardsAdapter()
        rv_awaiting_rewards.adapter = awaitingRewardsAdapter
        SearchData()
    }

    private fun SearchData() {
        et_search_top.requestFocus()
        if (et_search_top.requestFocus()) {
            mActivity.showKeyboard(et_search_top)
        }

        et_search_top.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                searchText = s.toString().trim()
                page = 1
                callSearchAwaitingRewardsAPI()
            }
        })

    }

    private fun callSearchAwaitingRewardsAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (awaitingRewardsList.find { it.id == 0L } == null) {
                awaitingRewardsList.add(ReferralAlertAndAwaitingReward())
                awaitingRewardsAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.SearchMerchant.SEARCH_TEXT] = searchText
        hashMap[ApiParam.SearchMerchant.POST_CODE] = zipCode
        hashMap[ApiParam.SearchMerchant.DISTANCE] = "$radius"
        /*hashMap[ApiParam.SearchMerchant.SHOW_TOP_MERCHANT] = if (showTopMerchants) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }*/
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().searchAwaitingRewardsAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    awaitingRewardsList.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    awaitingRewardsList.addAll(data)
                                                }

                                                if (links != null && links !!.next != null
                                                    && links !!.next !!.isRequiredField()) {
                                                    this@AwaitingRewardsFragment.lastPage ++
                                                } else {
                                                    this@AwaitingRewardsFragment.lastPage = page
                                                }

                                                page ++
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (awaitingRewardsList.isNotEmpty()) {
                            awaitingRewardsList.removeAll(awaitingRewardsList.filter { it.id == 0L })
                        }
                    }

                    awaitingRewardsAdapter.notifyDataSetChanged()

                    if (awaitingRewardsList.isEmpty()) {
                        rv_referral_alerts.visibility = View.GONE
//                        btn_view_merchants.visibility = View.GONE
                        tv_no_data_found.visibility = View.VISIBLE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                        rv_referral_alerts.visibility = View.VISIBLE
                    }

                    isLoading = false
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (awaitingRewardsList.isNotEmpty()) {
                            awaitingRewardsList.removeAll(awaitingRewardsList.filter { it.id == 0L })
                            awaitingRewardsAdapter.notifyDataSetChanged()
                        }
                    }

                    if (awaitingRewardsList.isEmpty()) {
                        tv_no_data_found.visibility = View.VISIBLE
//                        btn_view_merchants.visibility = View.GONE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }


    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
        }
    }

    private fun setHeader() {

        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)

        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        iv_search.visibility = View.VISIBLE
        iv_search.setOnClickListener(this)
        cancelText.setOnClickListener(this)
        iv_clearHeader.setOnClickListener(this)

        tv_title.text = resources.getString(R.string.awaiting_rewards)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        rv_awaiting_rewards.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == awaitingRewardsAdapter.itemCount - 1 &&
                        page <= lastPage && !isLoading) {
                    callAwaitingRewardsAPI()
                }
            }
        })

        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.cancelText -> {
                et_search_top.visibility = View.GONE
                iv_clearHeader.visibility = View.GONE
                cancelText.visibility = View.GONE
                iv_search.visibility = View.VISIBLE
                tv_title.visibility = View.VISIBLE
                et_search_top.setText("")
            }
            R.id.iv_clearHeader -> et_search_top.setText("")
            R.id.iv_search -> {
                et_search_top.visibility = View.VISIBLE
                iv_clearHeader.visibility = View.VISIBLE
                cancelText.visibility = View.VISIBLE
                tv_title.visibility = View.GONE
                iv_search.visibility = View.GONE
            } else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        mActivity.locationChecker!!.findLocation(locationUpdates)
    }

    inner class AwaitingRewardsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_referral_alerts_and_awaiting_rewards, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (awaitingRewardsList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return awaitingRewardsList.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(awaitingRewardsList[holder.adapterPosition]) {
                    holder.itemView.sdv_logo.loadFrescoImage(businessLogo)
                    if (distance.isRequiredField()) {
                        holder.itemView.tv_distance.text = "($distance ${resources.getString(R.string.mi)})"
                    }
                    holder.itemView.tv_business_name.text = businessName
                    if (tagLine.isRequiredField()) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = tagLine
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }
                    holder.itemView.rating_bar.rating = averageRating
                    holder.itemView.tv_average_ratings.text = "$averageRating"
                    holder.itemView.tv_ratings.text = "($noOfRating)"

                    if (mActivity.userDetails?.countryCode != "+91") {
                        holder.itemView.tv_store_credit.setCustomText(
                            "$$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.roboto_regular
                        )
                    }else {
                        holder.itemView.tv_store_credit.setCustomText(
                            "₹$storeCredit",
                            mActivity.resources.getString(R.string.store_credit),
                            fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                            fontFamilyId = R.font.roboto_regular
                        )
                    }
                    holder.itemView.tv_referrals.setCustomText("$referrals"
                            , mActivity.resources.getString(R.string.referrals)
                            , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt)
                            , fontFamilyId = R.font.poppins_regular)

                    holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(this!!.expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                    if (status == 3) {
                        holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                        holder.itemView.tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    when (subOfferCategory) {
                        ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                            holder.itemView.tv_offer_value.text = ""
                            holder.itemView.tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                        }
                        ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                            holder.itemView.tv_offer_value.text = "$$amount"
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.store_credit)
                        }
                        ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                            holder.itemView.tv_offer_value.text = "$$amount"
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.gift_card)
                        }
                    }

                    holder.itemView.tv_additional_details.text = "${mActivity.resources.getString(R.string.for_the_purchase_by)} $purchasedBy"

                    holder.itemView.setOnClickListener {

                        callOfferDetailAPI(id)
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    private fun callAwaitingRewardsAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (awaitingRewardsList.find { it.id == 0L } == null) {
                awaitingRewardsList.add(ReferralAlertAndAwaitingReward())
                awaitingRewardsAdapter.notifyDataSetChanged()
            }
        }
        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().awaitingRewardsAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            this@AwaitingRewardsFragment.awaitingRewardsList.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            this@AwaitingRewardsFragment.awaitingRewardsList.addAll(data)
                                                        }

                                                        page++

                                                        this@AwaitingRewardsFragment.lastPage = lastPage

                                                        totalNoOfRewards = total
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (awaitingRewardsList.isNotEmpty()) {
                                    awaitingRewardsList.removeAll(awaitingRewardsList.filter { it.id == 0L })
                                }
                            }

                            awaitingRewardsAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (awaitingRewardsList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            setAvailableRewards()

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (awaitingRewardsList.isNotEmpty()) {
                                    awaitingRewardsList.removeAll(awaitingRewardsList.filter { it.id == 0L })
                                    awaitingRewardsAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (awaitingRewardsList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            setAvailableRewards()

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun callOfferDetailAPI(id: Long) {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ID] = "$id"

        disposable = WebApiClient.webApi().offerDetailAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        mActivity.addFragment(OfferQRCodeFragment.getInstance(isFrom = AwaitingRewardsFragment::class.java.canonicalName, offerDetails = data,
                                                        merchantMessage = "")
                                                                , true, animationType = AnimationType.RightInZoomOut)
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )

    }

    private fun setAvailableRewards() {
        if (awaitingRewardsList.isEmpty()) {
            tv_awaiting_rewards.visibility = View.GONE
        } else {
            tv_awaiting_rewards.visibility = View.VISIBLE
            tv_awaiting_rewards.setCustomText("$totalNoOfRewards"
                    , mActivity.resources.getString(R.string.rewards_available)
                    , fontColor = mActivity.resources.getColor(R.color.black))
        }
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        locationUpdates = null
        super.onDestroy()
    }

    private var locationUpdates: LocationUpdates? = LocationUpdates { isLatest, latLng ->
        if (isAdded && !isHidden) {

            if (swipe_refresh.isRefreshing) {
                swipe_refresh.isRefreshing = false
            }

            if (latLng != null) {

                mLatitude = latLng.latitude.toString()
                mLongitude = latLng.longitude.toString()

                if (isLatest) {
                    callAwaitingRewardsAPI()
                } else {
                    mActivity.showDialogWithAction(resources.getString(R.string.location_not_found_ask_for_next_action),
                            positiveButtonLabel = resources.getString(R.string.Continue), showNegativeButton = true) {
                        callAwaitingRewardsAPI()
                    }
                }

            } else {
                mActivity.showDialog(resources.getString(R.string.location_not_found))
            }
        }

    }
}