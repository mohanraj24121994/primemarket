package com.smarket.main.customer

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.User
import com.smarket.interfaces.UpdateRefCash
import com.smarket.interfaces.UpdateRefCashManager
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_redeem_refcash.*
import kotlinx.android.synthetic.main.fragment_refferal.*
import kotlinx.android.synthetic.main.header.*
import utils.AnimationType

/**
 * Created by MI-062 on 11/4/18.
 */
class RedeemRefcashFragment : BaseMainFragment(), View.OnClickListener, UpdateRefCash {

    companion object {
        fun getInstance(isFrom: String = ""): RedeemRefcashFragment {
            val fragment = RedeemRefcashFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM, isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var tabTitleArray: Array<String>

    var amount = ""
    private var disposable: Disposable? = null

    val userDetails: User? = SMarket.appDB.userDao().getData()

    var isFrom = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_redeem_refcash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        tabTitleArray = arrayOf(mActivity.resources.getString(R.string.credit), mActivity.resources.getString(R.string.debit))

        btn_redeem.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM).toString()
            }
        }
    }

    private fun setHeader() {

        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.redeem_refcash)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        UpdateRefCashManager.addCallBack(this)

        var balance = 0.0
        if (userDetails != null) {
            balance = userDetails.refcash
            if (mActivity.userDetails?.countryCode != "+91") {
                tv_instruction.text = mActivity.resources.getString(R.string.minimum_redemption_amount)+" $${userDetails.min_withdraw_amount_outside}"
            }else{
                tv_instruction.text = mActivity.resources.getString(R.string.minimum_redemption_amount)+" ₹${userDetails.minWithdrawAmount}"
            }
        }

        setAvailableBalance(balance)


        if (mActivity.userDetails?.countryCode != "+91") {
            tv_refcash_rate.setCustomText(mActivity.resources.getString(R.string._1_rewards)
                    , "$1"
                    , fontColor = mActivity.resources.getColor(R.color.purple))
        } else {
            tv_refcash_rate.setCustomText(mActivity.resources.getString(R.string._1_rewards)
                    , "₹1"
                    , fontColor = mActivity.resources.getColor(R.color.purple))
        }


        setUpTabLayoutAndViewPager()
    }

    override fun onUpdateRefCash(refCash: Double) {
        setAvailableBalance(refCash)
    }

    private fun setUpTabLayoutAndViewPager() {

        vp_refcash_history.adapter = ViewPagerAdapter(childFragmentManager, tabTitleArray.size)
        tab_layout.setupWithViewPager(vp_refcash_history)
        vp_refcash_history.offscreenPageLimit = 2

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                vp_refcash_history.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_redeem -> {
                /*if (checkForValidations()) {
                    callRedeemRefCashAPI()
                }*/
                mActivity.addFragment(RedeemFragment.getInstance()
                        , true , animationType = AnimationType.RightInZoomOut)
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        amount = et_amount.text.toString()

        if (!amount.isRequiredField()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.amount_req))
            return false
        } else if (amount.toDouble() < userDetails!!.minWithdrawAmount) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_amount_req))
            return false
        } else if (amount.toDouble() > userDetails!!.refcash) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_refcash_req))
            return false
        }
        return true
    }

    private fun setAvailableBalance(refCash: Double) {
        val tv_available_balance = view?.findViewById<TextView>(R.id.tv_available_balance)

        if (mActivity.userDetails?.countryCode != "+91") {
            tv_available_balance?.setCustomText(mActivity.resources.getString(R.string.rewards)
                    , "$${refCash}"
                    , fontColor = mActivity.resources.getColor(R.color.purple))
        }else {
            tv_available_balance?.setCustomText(mActivity.resources.getString(R.string.rewards)
                    , "₹${refCash}"
                    , fontColor = mActivity.resources.getColor(R.color.purple))
        }
    }

   /* private fun callRedeemRefCashAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.AMOUNT] = amount

        disposable = WebApiClient.webApi().redeemRefCashAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe
                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        mActivity.showDialog(message)
                                                        val refcash = meta!!.refcash
                                                        UpdateRefCashManager.executeCallBacks(mActivity, refcash)

                                                        et_amount.setText("")
                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }
                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }*/

    private inner class ViewPagerAdapter(fragmentManager: FragmentManager , internal var tabCount: Int) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> RedeemRefcashCreditHistoryFragment.getInstance()
                1 -> RedeemRefcashDebitHistoryFragment.getInstance()
                else -> throw IllegalStateException("position $position is invalid for this viewpager")
            }
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitleArray[position]
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}