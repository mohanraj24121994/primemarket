package com.smarket.main.customer.topBar

import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.smarket.R
import com.smarket.location.LocationUpdates
import com.smarket.main.BaseMainFragment
import com.smarket.main.customer.ReferralAlertDetailsFragment
import com.smarket.main.merchant.MerchantDetailsFragment
import com.smarket.main.merchant.ReferMerchantFragment
import com.smarket.model.ReferralAlertAndAwaitingReward
import com.smarket.model.StoreCredit
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ReferralAlertsAndAwaitingRewardsResponse
import com.smarket.webservices.response.StoreCreditResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.header_filter.*
import kotlinx.android.synthetic.main.header_search.*
import kotlinx.android.synthetic.main.row_referral_alerts_and_awaiting_rewards.view.*
import kotlinx.android.synthetic.main.row_search.view.*
import kotlinx.android.synthetic.main.row_search.view.rating_bar
import kotlinx.android.synthetic.main.row_search.view.tv_average_ratings
import kotlinx.android.synthetic.main.row_search.view.tv_business_name
import kotlinx.android.synthetic.main.row_search.view.tv_ratings
import kotlinx.android.synthetic.main.row_search.view.tv_referrals
import kotlinx.android.synthetic.main.row_search.view.tv_store_credit
import kotlinx.android.synthetic.main.row_search.view.tv_tag_line
import kotlinx.android.synthetic.main.view_merchant_contact_details.view.*
import kotlinx.android.synthetic.main.view_merchant_logo_with_distance.view.*
import kotlinx.android.synthetic.main.view_offer_type_and_details.view.*
import retrofit2.Response
import utils.AnimationType
import java.util.*


/**
 * Created by MI-062 on 11/4/18.
 */
class SearchReferralAlertsFragment : BaseMainFragment() , View.OnClickListener , androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(searchText: String = ""): SearchReferralAlertsFragment {
            val fragment = SearchReferralAlertsFragment()
            val bundle = Bundle()
            bundle.putString("searchText" , searchText)
            fragment.arguments = bundle
            return fragment
        }
    }

    var sheetBehavior: BottomSheetBehavior<*>? = null

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var searchAdapter: SearchAdapter

    var merchants = ArrayList<ReferralAlertAndAwaitingReward>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    var mLatitude = ""
    var mLongitude = ""
    var searchText = ""
    var zipCode = ""
    var radius = 25
    var showTopMerchants = false

    private var disposable: Disposable? = null
    var observable: Observable<Response<ReferralAlertsAndAwaitingRewardsResponse>>? = null

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_store_credit , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

        getBundle()

        setHeader()

        sheetBehavior = BottomSheetBehavior.from(cl_filter)
        sheetBehavior?.peekHeight = 0

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_search.layoutManager = linearLayoutManager
        searchAdapter = SearchAdapter()
        rv_search.adapter = searchAdapter

        swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
        swipe_refresh.setOnRefreshListener(this)

        tv_refer_merchant.setOnClickListener(this)
        iv_reset.setOnClickListener(this)
        iv_apply.setOnClickListener(this)
        iv_cancel.setOnClickListener(this)
        btn_view_merchants.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("searchText")) {
                searchText = bundle.getString("searchText") !!
                if (searchText.isRequiredField()) {
                    et_search.setText(searchText)
                }
            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(false)
        iv_back.setOnClickListener(this)
        iv_clear.setOnClickListener(this)
        iv_filter.setOnClickListener(this)
        ivAddCart.visibility = View.GONE
        iv_filter.visibility = View.GONE
        ivcartcount.visibility = View.GONE
        iv_search.visibility = View.INVISIBLE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        et_search.requestFocus()
        if (et_search.requestFocus()) {
            mActivity.showKeyboard(et_search)
        }

        et_search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                searchText = s.toString().trim()
                page = 1
                callSearchMerchantAPI()
            }
        })

        rv_search.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView , newState)
                if (linearLayoutManager.findLastVisibleItemPosition() == searchAdapter.itemCount - 1 &&
                    page <= lastPage && ! isLoading) {
                    callSearchMerchantAPI()
                }
            }
        })

        mActivity.locationChecker !!.findLocation(locationUpdates)

        setFilterData()
    }

    private fun setFilterData() {
        if (zipCode.isRequiredField()) {
            tv_filtered_by.text = "${mActivity.resources.getString(R.string.search_region)}" + " $zipCode," + " $radius " + "${mActivity.resources.getString(R.string.mi)} " + "${mActivity.resources.getString(R.string.radius)}"
        } else {
            tv_filtered_by.text = "${mActivity.resources.getString(R.string.search_region)}" + " $radius " + "${mActivity.resources.getString(R.string.mi)} " + "${mActivity.resources.getString(R.string.radius)}"
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_back -> mActivity.onBackPressed()
            R.id.iv_clear -> et_search.setText("")
            R.id.iv_filter -> openFilter()
            R.id.tv_refer_merchant -> mActivity.addFragment(ReferMerchantFragment.getInstance()
                , true , animationType = AnimationType.RightInZoomOut)
            R.id.iv_reset -> {
                seek_bar_distance.setProgress(24)
                et_zip_code.setText("")
            }
            R.id.iv_apply -> {
                radius = seek_bar_distance.infoText.split(" ")[0].toInt()
                zipCode = et_zip_code.text.toString()
                setFilterData()
                closeFilter()
                page = 1
                callSearchMerchantAPI()
            }
            R.id.iv_cancel -> {
                closeFilter()
            }
            R.id.btn_view_merchants -> {
                showTopMerchants = ! showTopMerchants
                if (showTopMerchants) {
                    btn_view_merchants.text = mActivity.resources.getString(R.string.view_all_merchants)
                } else {
                    btn_view_merchants.text = mActivity.resources.getString(R.string.view_all_top_merchants)
                }
                page = 1
                callSearchMerchantAPI()
            }
            else -> {
            }
        }
    }

    override fun onRefresh() {
        page = 1
        mActivity.locationChecker !!.findLocation(locationUpdates)
    }

    private fun openFilter() {
        sheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        seek_bar_distance.setProgress(radius - 1)
        et_zip_code.setText(zipCode)
    }

    private fun closeFilter() {
        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    inner class SearchAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress , parent , false)
            } else {
                inflater.inflate(R.layout.row_referral_alerts_and_awaiting_rewards , parent , false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (merchants[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return merchants.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(merchants[holder.adapterPosition]) {
                    holder.itemView.sdv_logo.loadFrescoImage(businessLogo)
                    if (distance.isRequiredField()) {
                        holder.itemView.tv_distance.text = "($distance ${mActivity.resources.getString(R.string.mi)})"
                    }
                    holder.itemView.tv_business_name.text = businessName
                    if (tagLine.isRequiredField()) {
                        holder.itemView.tv_tag_line.visibility = View.VISIBLE
                        holder.itemView.tv_tag_line.text = tagLine
                    } else {
                        holder.itemView.tv_tag_line.visibility = View.GONE
                    }
//                    holder.itemView.tv_address.text = address
                    holder.itemView.rating_bar.rating = averageRating
                      holder.itemView.tv_average_ratings.text = "$averageRating"
                      holder.itemView.tv_ratings.text = "($noOfRating)"
                     if (mActivity.userDetails?.countryCode != "+91") {
                    holder.itemView.tv_store_credit.setCustomText(
                        "$$storeCredit",
                        mActivity.resources.getString(R.string.store_credit),
                        fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                        fontFamilyId = R.font.poppins_regular
                    )
                }
                else
                {
                    holder.itemView.tv_store_credit.setCustomText(
                        "₹$storeCredit",
                        mActivity.resources.getString(R.string.store_credit),
                        fontColor = mActivity.resources.getColor(R.color.dim_gray_txt),
                        fontFamilyId = R.font.roboto_regular
                    )
                }
                    holder.itemView.tv_referrals.setCustomText("$referrals"
                        , mActivity.resources.getString(R.string.referrals)
                        , fontColor = mActivity.resources.getColor(R.color.dim_gray_txt)
                        , fontFamilyId = R.font.poppins_regular)

                    holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expires_on)} ${getChangedDateFormat(this!!.expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"

                    if (status == 3) {
                        holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.expired_on)} ${getChangedDateFormat(expiryDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                        holder.itemView.tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    if(status == 2){
                        holder.itemView.tv_expiry_date.text = "${mActivity.resources.getString(R.string.redeemed_on)} ${getChangedDateFormat(this!!.redeemedDate, DATE_FORMAT, APP_DATE_FORMAT)}"
                        holder.itemView.tv_expiry_date.setTextColor(mActivity.resources.getColor(R.color.red))
                    }

                    when (subOfferCategory) {
                        ApiParam.SUB_OFFER_CATEGORY_IN_STORE -> {
                            holder.itemView.tv_offer_value.text = ""
                            holder.itemView.tv_offer_value.setBackgroundResource(R.drawable.exclusive)
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.exclusive)
                            holder.itemView.tv_additional_details.text = title
                        }
                        ApiParam.SUB_OFFER_CATEGORY_STORE_CREDIT -> {
                            holder.itemView.tv_offer_value.text = "$$amount"
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.store_credit)
                            if (purchasedBy == ApiParam.NO_BONUS) {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.no_bonus)
                            } else {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.welcome_bonus)
                            }
                        }
                        ApiParam.SUB_OFFER_CATEGORY_GIFT_CARD -> {
                            holder.itemView.tv_offer_value.text = "$$amount"
                            holder.itemView.tv_sub_offer_type.text = mActivity.resources.getString(R.string.gift_card)
                            if (purchasedBy == ApiParam.NO_BONUS) {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.no_bonus)
                            } else {
                                holder.itemView.tv_additional_details.text = mActivity.resources.getString(R.string.welcome_bonus)
                            }
                        }
                    }

                    holder.itemView.setOnClickListener {
                        mActivity.addFragment(
                            ReferralAlertDetailsFragment.getInstance(merchants[position])
                            , true, animationType = AnimationType.RightInZoomOut)
                    }
                   /* holder.itemView.iv_website.visibility = if (website.isRequiredField()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                holder.itemView.iv_call.visibility = if (mobile.isRequiredField()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }

                    holder.itemView.iv_direction.visibility = if (address.isRequiredField()) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }*/
              /*      holder.itemView.iv_call.setOnClickListener {
                        mActivity.makeCall(mobile)
                    }*/

                /*    holder.itemView.iv_direction.setOnClickListener {
                        navigateToGoogleMap(mActivity , latitude , longitude , address)
                    }
                    holder.itemView.iv_website.setOnClickListener {
                        mActivity.showWeb(website)
                    }*/

                    holder.itemView.setOnClickListener {
                        mActivity.addFragment(MerchantDetailsFragment.getInstance(id)
                            , true , animationType = AnimationType.RightInZoomOut)
                    }
                }
            }
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }

    private fun callSearchMerchantAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (merchants.find { it.id == 0L } == null) {
                merchants.add(ReferralAlertAndAwaitingReward())
                searchAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.LATITUDE] = mLatitude
        hashMap[ApiParam.LONGITUDE] = mLongitude
        hashMap[ApiParam.SearchMerchant.SEARCH_TEXT] = searchText
        hashMap[ApiParam.SearchMerchant.POST_CODE] = zipCode
        hashMap[ApiParam.SearchMerchant.DISTANCE] = "$radius"
        /*hashMap[ApiParam.SearchMerchant.SHOW_TOP_MERCHANT] = if (showTopMerchants) {
            ApiParam.TRUE
        } else {
            ApiParam.FALSE
        }*/
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE

        disposable?.let {
            if (! disposable?.isDisposed !!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().searchReferalListAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            with(body() !!) {
                                if (meta != null) {
                                    with(meta !!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {

                                                if (page == 1) {
                                                    merchants.clear()
                                                }

                                                if (data.isNotEmpty()) {
                                                    merchants.addAll(data)
                                                }

                                                if (links != null && links !!.next != null
                                                    && links !!.next !!.isRequiredField()) {
                                                    this@SearchReferralAlertsFragment.lastPage ++
                                                } else {
                                                    this@SearchReferralAlertsFragment.lastPage = page
                                                }

                                                page ++
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it , mActivity)
                        }
                    }

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (merchants.isNotEmpty()) {
                            merchants.removeAll(merchants.filter { it.id == 0L })
                        }
                    }

                    searchAdapter.notifyDataSetChanged()

                    if (merchants.isEmpty()) {
                        rv_search.visibility = View.GONE
                        btn_view_merchants.visibility = View.GONE
                        tv_no_data_found.visibility = View.VISIBLE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                        rv_search.visibility = View.VISIBLE
                    }

                    isLoading = false
                } ,
                {
                    if (disposable == null) return@subscribe

                    if (swipe_refresh.isRefreshing) {
                        swipe_refresh.isRefreshing = false
                    } else {
                        if (merchants.isNotEmpty()) {
                            merchants.removeAll(merchants.filter { it.id == 0L })
                            searchAdapter.notifyDataSetChanged()
                        }
                    }

                    if (merchants.isEmpty()) {
                        tv_no_data_found.visibility = View.VISIBLE
                        btn_view_merchants.visibility = View.GONE
                    } else {
                        tv_no_data_found.visibility = View.GONE
                    }

                    isLoading = false
                    ErrorUtil.setExceptionMessage(it)
                }
            )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        locationUpdates = null
        super.onDestroy()
    }

    private var locationUpdates: LocationUpdates? = LocationUpdates { isLatest , latLng ->
        if (isAdded && ! isHidden) {

            if (swipe_refresh.isRefreshing) {
                swipe_refresh.isRefreshing = false
            }

            if (latLng != null) {

                mLatitude = latLng.latitude.toString()
                mLongitude = latLng.longitude.toString()

                if (! zipCode.isRequiredField() && mLatitude.isRequiredField() && mLongitude.isRequiredField()) {
                    val geocoder = Geocoder(mActivity , Locale.getDefault())
                    val addresses = geocoder.getFromLocation(mLatitude.toDouble() , mLongitude.toDouble() , 1)
                    if (addresses != null && addresses.isNotEmpty()) {
                        zipCode = addresses[0].postalCode
                        setFilterData()
                    }
                }

                if (isLatest) {
                    callSearchMerchantAPI()
                } else {
                    mActivity.showDialogWithAction(mActivity.resources.getString(R.string.location_not_found_ask_for_next_action) ,
                        positiveButtonLabel = mActivity.resources.getString(R.string.Continue) , showNegativeButton = true) {
                        callSearchMerchantAPI()
                    }
                }

            } else {
                mActivity.showDialog(mActivity.resources.getString(R.string.location_not_found))
            }
        }
    }
}