package com.smarket.main.customer

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.SectionIndexer
import bolts.Task
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration
import com.mi.mycontacts.Contact
import com.mi.mycontacts.Contacts
import com.smarket.R
import com.smarket.SMarket
import com.smarket.main.BaseMainFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.ContactDetails
import com.smarket.webservices.response.MyContact
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_recommend_friends.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_contact.view.*
import kotlinx.android.synthetic.main.row_contact_header.view.*
import kotlinx.android.synthetic.main.search_contacts.*

/**
 * Created by MI-062 on 11/4/18.
 */
class RecommendFriendsFragment : BaseMainFragment() , View.OnClickListener {

    companion object {
        fun getInstance(isFrom: String = ""): RecommendFriendsFragment {
            val fragment = RecommendFriendsFragment()
            val bundle = Bundle()
            bundle.putString(IS_FROM , isFrom)
            fragment.arguments = bundle
            return fragment
        }
    }

    var READ_CONTACTS_PERMISSION = Manifest.permission.READ_CONTACTS
    val READ_CONTACTS_PERMISSION_REQUEST_CODE = 76
    val READ_CONTACTS_PERMISSION_GRANTED = 1
    val READ_CONTACTS_PERMISSION_DENIED = 2
    val READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS = 3

    var mDataArray = ArrayList<String>()
    var myContacts = ArrayList<Contact>()
    val contacts = ArrayList<ContactDetails>()
    val contactsToDisplay = ArrayList<ContactDetails>()
    lateinit var contactsAdapter: ContactsAdapter
    var selectedContactList = 0

    var isFrom = ""
    private var disposable: Disposable? = null

    var isWhatsappContacts = false

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recommend_friends , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {
        super.onViewCreated(view , savedInstanceState)

//        mActivity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        getBundle()

        setHeader()

        val typeface = ResourcesCompat.getFont(mActivity , R.font.poppins_medium) !!
//        cb_select_all.typeface = typeface

        rv_contacts.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        contactsAdapter = ContactsAdapter()
        rv_contacts.adapter = contactsAdapter
        rv_contacts.addItemDecoration(StickyHeaderDecoration(contactsAdapter))
        rv_contacts.setIndexBarCornerRadius(0)
        rv_contacts.setIndexBarTransparentValue(0F)
        rv_contacts.setTypeface(typeface)
        rv_contacts.setIndexBarTextColor(R.color.black)
        rv_contacts.setIndexbarHighLateTextColor(R.color.purple)
        rv_contacts.setIndexBarHighLateTextVisibility(true)
        rv_contacts.setIndexBarVisibility(true)

        //To show or hide the IndexBar for rv_contacts , based on KeyBoard
        setKeyBoardListener()

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                contactsToDisplay.clear()
                contactsToDisplay.addAll(if (s.toString().isRequiredField()) {
                    contacts.filter {
                        it.name.contains(s.toString() , true)
                    } as ArrayList<ContactDetails>
                } else {
                    contacts
                })

                tv_no_data_found.visibility = if (contactsToDisplay.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                contactsAdapter.notifyDataSetChanged()
            }
        })

        btn_recommend_now.setOnClickListener(this)
        iv_clearHeader.setOnClickListener(this)

    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(IS_FROM)) {
                isFrom = bundle.getString(IS_FROM) !!
            }
        }
    }

    private fun setHeader() {
        if (isFrom == IS_FROM_DRAWER) {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.recommend_to_friends)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        checkForReadContactsPermission()

    }

    private fun setKeyBoardListener() {
        parent.viewTreeObserver.addOnGlobalLayoutListener {
            if (parent != null) {
                val heightDiff = parent.rootView.height - parent.height
                if (heightDiff > dpToPx(this , 200)) { // if more than 200 dp, it's probably a keyboard...
                    rv_contacts.setIndexBarVisibility(false)
                } else {
                    rv_contacts.setIndexBarVisibility(true)
                }
            }
        }
    }

    private fun checkForReadContactsPermission() {
        if (isReadContactsPermissionGranted()) {
            // Android version is below Marshmallow or read myContactsList permission is already allowed.
            getContacts()
        } else {
            // Android version is above Marshmallow and read myContactsList permission is not allowed, ask user to allow.
            // Check the result in onActivityResult().
            //Create one popup

            AlertDialog.Builder(mActivity)
                    .setTitle("Info")
                    .setMessage(mActivity.getString(R.string.contact_disclaimer))
                    .setPositiveButton("ok") { v , _ ->
                        requestPermissions(arrayOf(READ_CONTACTS_PERMISSION) , READ_CONTACTS_PERMISSION_REQUEST_CODE)
                        v.dismiss()
                    }.setNegativeButton("cancel") { v , _ ->
                        v.dismiss()
                    }
                    .show()
        }
    }

    private fun isReadContactsPermissionGranted(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        when (requestCode) {
            READ_CONTACTS_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Read myContactsList permission is allowed.
                getContacts()
            } else {
                if (! shouldShowRequestPermissionRationale(READ_CONTACTS_PERMISSION)) {
                    // Read myContactsList permission is denied for always, ask user to turn it on from settings.
                    setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS)
                } else {
                    // Read myContactsList permission is denied for now.
                    setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_DENIED)
                }
            }
            else -> {
            }
        }

        if (requestCode == READ_CONTACTS_PERMISSION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getContacts()
        }
    }

    private fun setUpReadContactsPermissionStatusMessage(permissionStatus: Int) {
        when (permissionStatus) {
            READ_CONTACTS_PERMISSION_DENIED_FOR_ALWAYS -> tv_read_contacts_permission_status.text = resources.getString(R.string.contacts_permission_denied)
            READ_CONTACTS_PERMISSION_DENIED -> {

                val prefixMessage = mActivity.resources.getString(R.string.contacts_permission_pending_prefix_message) + " "
                val clickableMessage = mActivity.resources.getString(R.string.click_here)
                val postfixMessage = " " + mActivity.resources.getString(R.string.camera_permission_pending_postfix_message)
                val contactsPermissionStatusMessage = prefixMessage + clickableMessage + postfixMessage

                val ss = SpannableString(contactsPermissionStatusMessage)

                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(textView: View) {
                        checkForReadContactsPermission()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = true
                        ds.color = mActivity.resources.getColor(R.color.purple)
                    }
                }

                ss.setSpan(clickableSpan , prefixMessage.length ,
                        contactsPermissionStatusMessage.length - postfixMessage.length ,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                tv_read_contacts_permission_status.movementMethod = LinkMovementMethod.getInstance()
                tv_read_contacts_permission_status.text = ss
            }
            READ_CONTACTS_PERMISSION_GRANTED -> tv_read_contacts_permission_status.text = ""
            else -> {
            }
        }
    }

    private fun getContacts() {

        setUpReadContactsPermissionStatusMessage(READ_CONTACTS_PERMISSION_GRANTED)

        Task.callInBackground {
            myContacts = Contacts.getQuery(true).find() as ArrayList<Contact>
            isWhatsappContacts = appInstalledOrNot("com.whatsapp" , mActivity)
            contacts.clear()
            contactsToDisplay.clear()
            setUpContacts()
        }.continueWith { task ->
            null
        }
    }

    private fun setUpContacts() {

        if (myContacts.isNotEmpty()) {
            var myContactToPass: ContactDetails
            for (contact in myContacts) {
                myContactToPass = ContactDetails()
                myContactToPass.id = contact.id
                myContactToPass.name = contact.displayName
                if (contact.phoneNumbers.isNotEmpty()) {
                    if (contact.phoneNumbers[0].normalizedNumber != null &&
                            contact.phoneNumbers[0].normalizedNumber.isRequiredField()) {
                        myContactToPass.number = contact.phoneNumbers[0].normalizedNumber
                    } else if (contact.phoneNumbers[0].number != null &&
                            contact.phoneNumbers[0].number.isRequiredField()) {
                        myContactToPass.number = contact.phoneNumbers[0].number
                    }
                }
                contacts.add(myContactToPass)
                contactsToDisplay.add(myContactToPass)
            }
        }

        for (i in myContacts.indices) {
            mDataArray.add(myContacts[i].displayName)
        }

        mActivity.runOnUiThread {

            val countryCode = SMarket.appDB.userDao().getData() !!.countryCode
            /* if (isFrom == IS_FROM_DRAWER) {
                 ll_contacts.visibility = View.VISIBLE
                 if (isWhatsappContacts) {
                     var myAllContacts = ArrayList<ContactDetails>()
                     myAllContacts = contacts.filter { it.number.startsWith("$countryCode") } as ArrayList<ContactDetails>
                     contacts.clear()
                     contactsToDisplay.clear()
                     contacts.addAll(myAllContacts)
                     contactsToDisplay.addAll(myAllContacts)
                 }
                 contactsAdapter.notifyDataSetChanged()

             } else {*/
            if (isWhatsappContacts) {
                callContactListAPI(contacts.filter { it.number.startsWith("$countryCode") } as ArrayList<ContactDetails>)
            } else {
                callContactListAPI(contacts)
            }
        }
    }

    @SuppressLint("StringFormatMatches")
    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {

            R.id.iv_clearHeader -> et_search.setText("")

            R.id.btn_recommend_now -> {

                var receiversList = ""
                if (contacts.isNotEmpty()) {
                    receiversList = contacts.filter { it.isSelected }.map { it.number + ";" }.toString()
                            .replace("[" , "")
                            .replace("," , "")
                            .replace("]" , "")
                }

                if (receiversList.isRequiredField()) {
                    receiversList = receiversList.substring(0 , receiversList.length - 1)
                    val sendIntent = Intent(Intent.ACTION_VIEW)
                    sendIntent.data = Uri.parse("sms:" + receiversList)
                    if (mActivity.userDetails != null) {
                        /*sendIntent.putExtra("sms_body" , mActivity.resources.getString(R.string.general_invite_flow_message ,
                                "https://play.google.com/store/apps/details?id=com.smarket.app&hl=en_IN" , mActivity.userDetails?.name
                                , mActivity.userDetails?.countryCode , mActivity.userDetails?.mobile, mActivity.userDetails?.referral_code))*/
                        val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + mActivity.userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                                "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"
                        sendIntent.putExtra("sms_body" , shareMessage)
                    }
                    mActivity.startActivity(sendIntent)
                } else {
                    mActivity.showDialog(mActivity.resources.getString(R.string.select_at_least_contact))
                }
            }
            else -> {
            }
        }
    }

    private fun callContactListAPI(myContactsListToPass: ArrayList<ContactDetails>) {

        try {

            if (! NetworkUtil.isNetworkAvailable()) {
                showToast(ErrorUtil.NO_INTERNET)
                return
            }

            ProgressDialogUtil.showProgressDialog(mActivity)

            val myContact = MyContact()
            myContact.contactList.addAll(myContactsListToPass)

            disposable = WebApiClient.webApi().contactListAPI(myContact).subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(
                            {
                                if (disposable == null) return@subscribe

                                with(it) {
                                    if (isSuccessful && body() != null) {
                                        kotlin.with(body() !!) {
                                            if (meta != null) {
                                                with(meta !!) {
                                                    when (status) {
                                                        ApiParam.META_SUCCESS -> {
                                                            if (data != null) {
                                                                contacts.clear()
                                                                contactsToDisplay.clear()
                                                                with(myContact) {
                                                                    //To combine both smarketContactList & otherContactList and for to show s-mark icon

                                                                    data.smarketContactList.forEach { it.isSmarketContact = true }

                                                                    contacts.addAll(data.smarketContactList)
                                                                    contactsToDisplay.addAll(data.smarketContactList)

                                                                    contacts.addAll(data.otherContactList)
                                                                    contactsToDisplay.addAll(data.otherContactList)

                                                                    contacts.sortBy { it.name }
                                                                    contactsToDisplay.sortBy { it.name }

                                                                    ll_contacts.visibility = View.VISIBLE

                                                                    tv_no_data_found.visibility = if (contactsToDisplay.isEmpty()) {
                                                                        View.VISIBLE
                                                                    } else {
                                                                        View.GONE
                                                                    }

                                                                    contactsAdapter.notifyDataSetChanged()
                                                                }
                                                            }
                                                        }
                                                        else -> {
                                                            mActivity.showDialog(message)
                                                        }
                                                    }
                                                }
                                            } else {
                                                ErrorUtil.showError(it , mActivity)
                                            }
                                        }
                                    } else {
                                        ErrorUtil.showError(it , mActivity)
                                    }
                                }

                                ProgressDialogUtil.hideProgressDialog()
                            } ,
                            {
                                if (disposable == null) return@subscribe

                                tv_no_data_found.visibility = if (contactsToDisplay.isEmpty()) {
                                    View.VISIBLE
                                } else {
                                    View.GONE
                                }

                                ProgressDialogUtil.hideProgressDialog()
                                ErrorUtil.setExceptionMessage(it)
                            }
                    )

        } catch (e: Exception) {
            Log.e("exception" , "contacts==>" + e.localizedMessage)
        }
    }


    inner class ContactsAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<ContactsAdapter.ViewHolder>()
            , StickyHeaderAdapter<ContactsAdapter.ViewHolderHeader> , SectionIndexer {

        private var mSectionPositions = ArrayList<Int>()

        override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ContactsAdapter.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(mActivity)

            view = inflater.inflate(R.layout.row_contact , parent , false)

            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return contactsToDisplay.size
        }

        override fun onBindViewHolder(holder: ContactsAdapter.ViewHolder , position: Int) {

            with(contactsToDisplay[position]) {
                holder.itemView.tv_prefix.text = name.substring(0 , 1).toUpperCase()
                holder.itemView.ctv_name.text = "$name\n$number"
                holder.itemView.ctv_name.isChecked = isSelected

                holder.itemView.isEnabled = ! isSmarketContact
                if (isSmarketContact) {
                    holder.itemView.iv_smark_icon.visibility = View.VISIBLE
                    holder.itemView.tv_already_on_smarket.visibility = View.VISIBLE
                } else {
                    holder.itemView.iv_smark_icon.visibility = View.GONE
                    holder.itemView.tv_already_on_smarket.visibility = View.GONE
                }

                holder.itemView.setOnClickListener {

                    isSelected = if (! isSelected) {
                        if (selectedContactList < 20) {
                            selectedContactList += 1
                            ! isSelected
                        } else {
                            mActivity.showDialog(mActivity.resources.getString(R.string.contacts_not_more_than))
                            isSelected
                        }
                    } else {
                        selectedContactList -= 1
                        ! isSelected
                    }
                    contactsAdapter.notifyDataSetChanged()
                }
            }
        }

        override fun getHeaderId(position: Int): Long {
            return contactsToDisplay[position].name[0].toUpperCase().toLong()
        }

        override fun onCreateHeaderViewHolder(parent: ViewGroup?): ViewHolderHeader {

            val view: View
            val inflater = LayoutInflater.from(mActivity)

            view = inflater.inflate(R.layout.row_contact_header , parent , false)

            return ViewHolderHeader(view)
        }

        override fun onBindHeaderViewHolder(viewholder: ViewHolderHeader? , position: Int) {

            viewholder?.itemView?.tv_header?.text = contactsToDisplay[position].name.substring(0 , 1).toUpperCase()
        }

        override fun getSections(): Array<String> {

            val sections = ArrayList<String>(26)

            for (i in contactsToDisplay.indices) {
                val section = contactsToDisplay[i].name.substring(0 , 1).toUpperCase()
                if (! sections.contains(section)) {
                    sections.add(section)
                    mSectionPositions.add(i)
                }
            }

            return sections.toTypedArray()
        }

        override fun getSectionForPosition(position: Int): Int {
            return 0
        }

        override fun getPositionForSection(sectionIndex: Int): Int {
            return mSectionPositions[sectionIndex]
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

        inner class ViewHolderHeader(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && ! hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}