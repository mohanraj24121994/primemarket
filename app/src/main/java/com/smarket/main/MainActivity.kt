package com.smarket.main

import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import com.smarket.R
import com.smarket.SMarket
import com.smarket.adapter.CountryCodeAdapter
import com.smarket.database.User
import com.smarket.location.LocationChecker
import com.smarket.lrf.LRFActivity
import com.smarket.main.customer.*
import com.smarket.main.customer.topBar.AwaitingRewardsFragment
import com.smarket.main.customer.topBar.ReferralAlertsFragment
import com.smarket.main.customer.topBar.StoreCreditsFragment
import com.smarket.main.merchant.MerHomeFragment
import com.smarket.main.merchant.MerchantDetailsFragment
import com.smarket.main.merchant.OfferQRCodeFragment
import com.smarket.main.merchant.RateAndReferMerchantFragment
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ApiUtil.callAddRemoveDeviceTokenAPI
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.tap.app.utils.CustomTypefaceSpan
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer_cust.*
import kotlinx.android.synthetic.main.drawer_mer.*
import kotlinx.android.synthetic.main.view_drawer.*
import kotlinx.android.synthetic.main.view_drawer_header.*
import utils.AnimationType


class MainActivity : AppCompatActivity() , View.OnClickListener , androidx.drawerlayout.widget.DrawerLayout.DrawerListener {

    var isUserTypeCustomer = false

    lateinit var countryCodeList: List<String>

    var userDetails: User? = null
    var userToken = ""

    var locationChecker: LocationChecker? = null
    private var disposable: Disposable? = null

    var MyPREFERANCE = "Pref"
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isUserTypeCustomer = PreferenceUtil.getIntPref(USER_TYPE) == USER_TYPE_CUSTOMER
        if (isUserTypeCustomer) {
            setTheme(R.style.AppThemeCustomer)
        } else {
            setTheme(R.style.AppThemeMerchant)
        }

        setContentView(R.layout.activity_main)

        drawer_layout.addDrawerListener(this)
        countryCodeList = this.getCountryCodeList()

        updateUserDetailsObject()

        setUpNavigationDrawer()

        if (isUserTypeCustomer) {

            locationChecker = LocationChecker(this@MainActivity)
            locationChecker !!.onActivityCreated(savedInstanceState)
            val shareURL = PreferenceUtil.getStringPref("shareURL")
            Log.e("shareURL" , "shareURL$shareURL")

            if (shareURL == "") {
                replaceFragment(CustHomeFragment.getInstance())
            } else {
                replaceFragment(ProductComparisonFragment.getInstance())
            }

            registerBroadcast()

            //handling the notification onclick Listeners depends on "type" of Notification
            handleNotificationClicks()

        } else {
            replaceFragment(MerHomeFragment.getInstance())
        }
    }

    private fun handleNotificationClicks() {
        //// APP -> 1, Admin => 2
        val intent = intent
        if (intent != null && intent.action != null && intent.action == "MainActivity") {
            val type = intent.getStringExtra("type")

            if (type == "2") { // is it notification of admin
                replaceFragment(NotificationsFragment.getInstance())
            } else {
                replaceFragment(CustHomeFragment.getInstance())
            }
        }
    }

    fun updateUserDetailsObject() {
        userDetails = SMarket.appDB.userDao().getData()
        if (userDetails != null) {
            userToken = userDetails !!.token

            sharedPreferences = application.getSharedPreferences(MyPREFERANCE , Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("countryCode" , userDetails !!.countryCode)
            editor.apply()
        }
    }

    override fun onBackPressed() {
        hideKeyboard()

        val fragment = getCurrentFragment()
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else if (fragment is OfferQRCodeFragment && getAllFragments(this).find { it is RateAndReferMerchantFragment } != null && getAllFragments(this).find { it is MerchantDetailsFragment } != null) {
            Handler().postDelayed({
                popBackStack(MerchantDetailsFragment::class.java.canonicalName)
            } , 300)
        } else {
            super.onBackPressed()
        }
    }

    /*====================================== methods to handle NAVIGATION DRAWER start ======================================*/
    private fun setUpNavigationDrawer() {

        val navigationMenuView: View

        if (isUserTypeCustomer) {
            navigationMenuView = LayoutInflater.from(this).inflate(R.layout.drawer_cust , null)
            ll_navigation_menu.addView(navigationMenuView)
            iv_edit.setImageResource(R.drawable.edit_profile_cust)
            fl_refcash.setOnClickListener(this)
            tv_home_cust.setOnClickListener(this)
            tv_referral.setOnClickListener(this)
            fl_referral_alerts.setOnClickListener(this)
            fl_awaiting_rewards.setOnClickListener(this)
            tv_store_credits.setOnClickListener(this)
            tv_redemption_history.setOnClickListener(this)
            fl_notifications_cust.setOnClickListener(this)
            tv_recommend_to_your_friends.setOnClickListener(this)
            tv_settings.setOnClickListener(this)
            tv_login_as_merchant.setOnClickListener(this)
            tv_logout_cust.setOnClickListener(this)
            fl_friends_on_smarket.setOnClickListener(this)
        } else {
            navigationMenuView = LayoutInflater.from(this).inflate(R.layout.drawer_mer , null)
            ll_navigation_menu.addView(navigationMenuView)
            iv_edit.setImageResource(R.drawable.edit_profile_mer)
            tv_home_mer.setOnClickListener(this)
            tv_change_password.setOnClickListener(this)
            fl_notifications_mer.setOnClickListener(this)
            tv_faqs.setOnClickListener(this)
            tv_how_to_use_this_app.setOnClickListener(this)
            tv_terms_and_conditions.setOnClickListener(this)
            tv_privacy_policy.setOnClickListener(this)
            tv_about_us.setOnClickListener(this)
            tv_contact_us.setOnClickListener(this)
            tv_login_as_customer.setOnClickListener(this)
            tv_logout_mer.setOnClickListener(this)
        }

        iv_edit.setOnClickListener(this)
        sdv_profile_pic.setOnClickListener(this)

        setUserDetails()
    }

    fun setUserDetails() {
        if (userDetails != null) {
            with(userDetails !!) {
                if (isUserTypeCustomer) {
                    sdv_profile_pic.loadFrescoImage(profilePic)
                    tv_user_name.text = name
                } else {
                    sdv_profile_pic.loadFrescoImage(businessLogo)
                    tv_user_name.text = businessName
                }
            }
        }
    }

    fun setDrawerEnable(isEnable: Boolean) {
        if (isEnable) {
            drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED)
        } else {
            drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }

    fun openDrawer() {
        hideKeyboard()
        drawer_layout.openDrawer(Gravity.START)
    }

    fun closeDrawer() {
        drawer_layout.closeDrawer(Gravity.START)
    }

    override fun onDrawerStateChanged(newState: Int) {
    }

    override fun onDrawerSlide(drawerView: View , slideOffset: Float) {
    }

    override fun onDrawerClosed(drawerView: View) {
    }

    override fun onDrawerOpened(drawerView: View) {
        if (isUserTypeCustomer) {
            callUserDetailAPI()
        }
    }

    /*====================================== methods to handle NAVIGATION DRAWER end ======================================*/

    override fun onClick(v: View?) {

        closeDrawer()

        val fragmentToNavigate = when (v?.id) {
            R.id.iv_edit -> EditProfileFragment.getInstance()
            R.id.fl_refcash -> RedeemRefcashFragment.getInstance(IS_FROM_DRAWER)
            R.id.tv_home_cust -> CustHomeFragment.getInstance()
            R.id.fl_referral_alerts -> ReferralAlertsFragment.getInstance(IS_FROM_DRAWER)
            R.id.fl_awaiting_rewards -> AwaitingRewardsFragment.getInstance(IS_FROM_DRAWER)
            R.id.tv_store_credits -> StoreCreditsFragment.getInstance(IS_FROM_DRAWER)
            R.id.tv_redemption_history -> RedemptionHistoryFragment.getInstance(IS_FROM_DRAWER)
            R.id.tv_home_mer -> MerHomeFragment.getInstance()
            R.id.tv_change_password -> ChangePasswordFragment.getInstance()
            R.id.fl_notifications_cust ,
            R.id.fl_notifications_mer -> NotificationsFragment.getInstance()
            R.id.tv_how_to_use_this_app -> CMSFragment.getInstance(CMSFragment.HOW_TO_USE)
            R.id.tv_faqs -> FAQsFragment.getInstance()
            R.id.tv_terms_and_conditions -> CMSFragment.getInstance(CMSFragment.TERMS_CONDITIONS)
            R.id.tv_privacy_policy -> CMSFragment.getInstance(CMSFragment.PRIVACY_POLICY)
            R.id.tv_about_us -> CMSFragment.getInstance(CMSFragment.ABOUT_US)
            R.id.tv_settings -> SettingsFragment.getInstance()
            R.id.tv_referral -> ProductComparisonFragment.getInstance()
            R.id.fl_friends_on_smarket -> FriendsOnSmarketFragment.getInstance(IS_FROM_DRAWER)
            else -> {
                null
            }
        }
        when (v?.id) {
            R.id.tv_recommend_to_your_friends -> {

                if (userDetails !!.countryCode.equals("+91")) {
                    sharePopUpWithoutHeader()
                } else {
                    //sharePopUpWithoutHeaderOutsideIndia()
                    try {
                        this.addFragment(RecommendFriendsFragment.getInstance(IS_FROM_DRAWER)
                                , true , animationType = AnimationType.RightInZoomOut)
                    } catch (e: Exception) {
                        Log.e("" , "" + e.message)
                    }
                }
            }
        }

        if (fragmentToNavigate != null) {
            Handler().postDelayed({ replaceFragment(fragmentToNavigate , fragmentToNavigate !is CMSFragment) } , 300)
        }

        when (v?.id) {
            R.id.tv_contact_us -> {
                val cms = SMarket.appDB.cmsDao().getData(CMSFragment.CONTACT_US)
                if (cms != null) {
                    sendMail(resources.getString(R.string.app_name) , emailId = Html.fromHtml(cms.content).toString())
                }
            }
            R.id.tv_login_as_customer ,
            R.id.tv_login_as_merchant -> showConfirmationDialog(true)
            R.id.tv_logout_cust ,
            R.id.tv_logout_mer -> showConfirmationDialog(false)
            R.id.sdv_profile_pic -> {
                if (userDetails != null) {
                    var image = ""
                    if (isUserTypeCustomer) {
                        image = userDetails !!.profilePic
                    } else {
                        image = userDetails !!.businessLogo
                    }
                    if (image.isRequiredField()) {
                        ZoomDialog(this , image).show()
                    }
                }
            }
            else -> {
            }
        }
    }

    fun setCountryCodeSpinner(sp_country_code: AppCompatSpinner , isForEdit: Boolean = false) {

        val countryCodeAdapter = CountryCodeAdapter(this , R.layout.row_spinner_country_code_with_name
                , countryCodeList , isForEdit)
        sp_country_code.adapter = countryCodeAdapter

        setCurrentCountryCodeAsDefault(sp_country_code)
    }

    fun setCurrentCountryCodeAsDefault(sp_country_code: AppCompatSpinner) {

        val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val currentCountryCode = tm.networkCountryIso

        if (currentCountryCode.isRequiredField()) {
            val currentCountryDetails = countryCodeList.find { it.equals(currentCountryCode , true) }
            if (currentCountryDetails != null) {
                sp_country_code.setSelection(countryCodeList.indexOf(currentCountryDetails))
            }
        }
    }

    fun setSelectedCountyCode(sp_country_code: AppCompatSpinner , countryCode: String) {

        sp_country_code.setSelection(countryCodeList.indexOf(countryCodeList.find { it == countryCode }))
    }

    private fun showConfirmationDialog(loginAsOtherUserType: Boolean) {

        AlertDialog.Builder(this)
                .setMessage(resources.getString(R.string.logout_confirmation))
                .setPositiveButton(resources.getString(R.string.logout)) { dialog , which ->
                    dialog.dismiss()

                    val prefs = SMarket.appContext.getSharedPreferences(PreferenceUtil.prefID , Context.MODE_PRIVATE)
                    val editor = prefs.edit()
                    editor.remove("shareURL")
                    editor.apply()
                    editor.commit()

                    if (loginAsOtherUserType) {
                        if (PreferenceUtil.getIntPref(USER_TYPE) == USER_TYPE_CUSTOMER) {
                            PreferenceUtil.setPref(USER_TYPE , USER_TYPE_MERCHANT)
                        } else {
                            PreferenceUtil.setPref(USER_TYPE , USER_TYPE_CUSTOMER)
                        }
                    }
                    logoutAndNavigateToLRFActivity()
                }
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog , which ->
                    dialog.dismiss()
                }
                .show()
    }


    private fun callUserDetailAPI() {

        if (! NetworkUtil.isNetworkAvailable()) {
            return
        }

        disposable = WebApiClient.webApi().userDetailAPI().subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body() !!) {
                                        if (meta != null) {
                                            with(meta !!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {
                                                        if (data != null) {
                                                            val userDetails = data

                                                            SMarket.appDB.userDao().updateRefCash(userDetails !!.refcash)

                                                            if (userDetails !!.refcash > 0) {
                                                                tv_refcash_count.visibility = View.VISIBLE
                                                                tv_refcash_count.text = userDetails.refcash.toString()
                                                            } else {
                                                                tv_refcash_count.visibility = View.GONE
                                                            }

                                                            if (userDetails.refferalAlert > 0) {
                                                                tv_referral_alerts_count.visibility = View.VISIBLE
                                                                tv_referral_alerts_count.text = userDetails.refferalAlert.toString()
                                                            } else {
                                                                tv_referral_alerts_count.visibility = View.GONE
                                                            }

                                                            if (userDetails.awaitingRewards > 0) {
                                                                tv_awaiting_rewards_count.visibility = View.VISIBLE
                                                                tv_awaiting_rewards_count.text = userDetails.awaitingRewards.toString()
                                                            } else {
                                                                tv_awaiting_rewards_count.visibility = View.GONE
                                                            }
                                                            val store_credit = if (userDetails.storeCredit > 0) userDetails.storeCredit.toString() else "0"

                                                            tv_store_credits_count.text = "$$store_credit ${resources.getString(R.string.credit_among_all_stores)}"

                                                            if (userDetails.notification > 0) {
                                                                tv_notifications_count_cust.visibility = View.VISIBLE
                                                                tv_notifications_count_cust.text = userDetails.notification.toString()
                                                            } else {
                                                                tv_notifications_count_cust.visibility = View.GONE
                                                            }
                                                        }
                                                    }
                                                    ApiParam.META_UNVERIFIED_USER -> {
                                                    }
                                                    else -> {
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it , this@MainActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it , this@MainActivity)
                                }
                            }

                        } ,
                        {
                            if (disposable == null) return@subscribe

                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    private fun logoutAndNavigateToLRFActivity() {

        if (isUserTypeCustomer) {
            //removing token to stop Notifications , when he logged out as a customer
            callAddRemoveDeviceTokenAPI(User.getLoginUserId() , PreferenceUtil.getStringPref(DEVICE_TOKEN) , "remove")
            clearNotifications(this)//To clear the status bar notifications when he logged out as customer.
            PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_CUSTOMER , "${userDetails?.countryCode},${userDetails?.mobile}")
        } else {
            PreferenceUtil.setPref(PreferenceUtil.PREF_KEY_LAST_LOGGED_IN_MERCHANT , userDetails?.email)
        }

        Handler().postDelayed({
            SMarket.appDB.userDao().deleteData()
        } , 300)

        val intent = Intent(this , LRFActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        overridePendingTransition(R.anim.zoom_in , R.anim.zoom_out)
        finish()
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        if (isUserTypeCustomer) {
            locationChecker?.onActivityResult(requestCode , resultCode , data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<out String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
        if (isUserTypeCustomer) {
            locationChecker?.onRequestPermissionsResult(requestCode , permissions , grantResults)
        }
    }


    private var notificationBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context? , intent: Intent) {

            if (intent.action == ApiParam.PUSH_NOTIFICATION && isUserTypeCustomer) {
                // new push notification is received
                val message = intent.getStringExtra("message")
                val title = intent.getStringExtra("title")
                val type = intent.getStringExtra("type")

                val alertDialog = AlertDialog.Builder(this@MainActivity , R.style.NotificationDialog).create()
                alertDialog?.setCancelable(false)
                alertDialog?.setTitle(title)
                alertDialog?.setMessage(message)
                alertDialog?.setButton(AlertDialog.BUTTON_POSITIVE , resources.getString(R.string.ok)) { dialog , which ->
                    dialog.dismiss()
                    val fragment = getCurrentFragment()
                    if (type == "2") {      // for Admin Notifications we are listing in NotificationsFragment
                        if (fragment is NotificationsFragment) {
                            //do Nothing
                        } else {
                            replaceFragment(NotificationsFragment.getInstance() , animationType = AnimationType.LeftInZoomOut)
                        }
                    } else {   //For App Notifications We need to replace CustHomeFragment
                        if (fragment is CustHomeFragment) {
                            //Do nothing
                        } else {
                            replaceFragment(CustHomeFragment.getInstance() , animationType = AnimationType.LeftInZoomOut)
                        }
                    }
                }
                alertDialog?.show()
            }
        }
    }


    fun registerBroadcast() {
        val intentFilter = IntentFilter()
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this).registerReceiver(notificationBroadcastReceiver , intentFilter)
    }

    override fun onResume() {
        super.onResume()
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this).registerReceiver(notificationBroadcastReceiver ,
                IntentFilter(ApiParam.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)

    }

    override fun onPause() {
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationBroadcastReceiver)
        super.onPause()
    }

    override fun onDestroy() {
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationBroadcastReceiver)
        disposable = null
        super.onDestroy()
    }

    private fun sharePopUpWithoutHeader() {
        val alertDialog = AlertDialog.Builder(this).create()
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_share_new , null)
        alertDialog.setView(dialogView)

        alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val messanger = dialogView.findViewById<Button>(R.id.btnShare)
        val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view_site)
        btnViewSite.visibility = View.GONE

        val adobe = ResourcesCompat.getFont(this , R.font.adobe_devanagari_bold) !!
        val adobeFont = CustomTypefaceSpan("" , adobe)

        val typeface = ResourcesCompat.getFont(this , R.font.poppins_italic) !!
        val spannable = CustomTypefaceSpan("" , typeface)

        val tvFirst = dialogView.findViewById<TextView>(R.id.tvFirst)
        val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
        val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
        val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

        val first = SpannableString("₹50\nCash Rewards")
        first.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        first.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvFirst.text = first

        val second = SpannableString("₹20\nBonus Cash")
        second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvSecond.text = second

        val third = SpannableString("30%\nCash Rewards")
        third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvThird.text = third

        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"

            /*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                    "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*/
            val shareMessage = userDetails?.referral_message !!.replace("XXXXXX" , userDetails?.referral_code !!)
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            startActivity(Intent.createChooser(intent , "Share"))

        }

        tvFAQ.setOnClickListener {
            alertDialog.dismiss()
            addFragment(FAQsFragment.getInstance()
                    , true , animationType = AnimationType.RightInZoomOut)
        }

        alertDialog.show()
    }

    private fun sharePopUpWithoutHeaderOutsideIndia() {
        /*val alertDialog = AlertDialog.Builder(this).create()
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_share , null)
        alertDialog.setView(dialogView)
        val text = dialogView.findViewById<TextView>(R.id.tv_text)
        val tv_life = dialogView.findViewById<TextView>(R.id.tv_lifetime)
        val reward = dialogView.findViewById<TextView>(R.id.tv_reward)
        val messanger = dialogView.findViewById<LinearLayout>(R.id.ll_social)
        val btnViewSite = dialogView.findViewById<Button>(R.id.btn_view_site)
        btnViewSite.visibility = View.GONE

        val typeface = ResourcesCompat.getFont(this , R.font.stencil_std_bold) !!
        val adobe = ResourcesCompat.getFont(this , R.font.adobe_devanagari) !!

        val spannable = CustomTypefaceSpan("" , typeface)
        val adobeFont = CustomTypefaceSpan("" , adobe)

        val spannableString = SpannableString(resources.getString(R.string.each_time))
        val rewardString = SpannableString(resources.getString(R.string.refer_friends))

        spannableString.setSpan(adobeFont , 0 , 83 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(RelativeSizeSpan(1.4f) , 52 , 55 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(spannable , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(ForegroundColorSpan(Color.RED) , 84 , 93 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        rewardString.setSpan(RelativeSizeSpan(1.3f) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        rewardString.setSpan(ForegroundColorSpan(Color.YELLOW) , 22 , 26 , Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        tv_life.text = spannableString
        reward.text = rewardString

        text.setOnClickListener {
            try {
                this.addFragment(RecommendFriendsFragment.getInstance(IS_FROM_DRAWER)
                        , true , animationType = AnimationType.RightInZoomOut)
            } catch (e: Exception) {
                Log.e("" , "" + e.message)
            }
            alertDialog.dismiss()
        }
        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"

            *//*val shareMessage = "Hi, join SMARKET app ( from www.smarketworld.net ) to Compare, Save and Earn rewards while shopping online. Use the referral code " + userDetails?.referral_code + " at sign-up to receive extra 5% cash rewards on every purchase." + "\n" +
                    "Android: " + "https://rb.gy/0otkjs, " + "IOS:" + "https://apple.co/2u6Jm7k"*//*
            val shareMessage = userDetails?.referral_message!!.replace("XXXXXX",userDetails?.referral_code!!)
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            startActivity(Intent.createChooser(intent , "Share"))

        }
        alertDialog.show()
    }*/

        val alertDialog = AlertDialog.Builder(this).create()
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_share , null)
        alertDialog.setView(dialogView)
        alertDialog.window !!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val messanger = dialogView.findViewById<Button>(R.id.btnShare)
        val btnViewSite = dialogView.findViewById<TextView>(R.id.btn_view)
        btnViewSite.visibility = View.GONE

        val adobe = ResourcesCompat.getFont(this , R.font.adobe_devanagari_bold) !!
        val adobeFont = CustomTypefaceSpan("" , adobe)

        val typeface = ResourcesCompat.getFont(this , R.font.poppins_italic) !!
        val spannable = CustomTypefaceSpan("" , typeface)

        val tvSecond = dialogView.findViewById<TextView>(R.id.tvSecond)
        val tvThird = dialogView.findViewById<TextView>(R.id.tvThird)
        val tvFAQ = dialogView.findViewById<TextView>(R.id.tvFAQ)

        val second = SpannableString("₹20\nBonus Cash")
        second.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(spannable , 3 , 14 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(RelativeSizeSpan(2.2f) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        second.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvSecond.text = second

        val third = SpannableString("30%\nCash Rewards")
        third.setSpan(adobeFont , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(spannable , 3 , 16 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(2.2f) , 0 , 2 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(RelativeSizeSpan(1.7f) , 2 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        third.setSpan(ForegroundColorSpan(Color.YELLOW) , 0 , 3 , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvThird.text = third

        messanger.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "text/plain"
            val shareMessage = userDetails?.referral_message !!.replace("XXXXXX" , userDetails?.referral_code !!)
            intent.putExtra(Intent.EXTRA_TEXT , shareMessage)
            startActivity(Intent.createChooser(intent , "Share"))
        }

        tvFAQ.setOnClickListener {
            alertDialog.dismiss()
            addFragment(FAQsFragment.getInstance() , true , animationType = AnimationType.RightInZoomOut)
        }

        alertDialog.show()
    }
}
