package com.smarket.main

import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.model.Notifications
import com.smarket.utils.*
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.NotificationListResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.row_notifications.view.*
import retrofit2.Response
import java.util.*


/**
 * Created by MI-062 on 11/4/18.
 */
class NotificationsFragment : BaseMainFragment(), View.OnClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun getInstance(): NotificationsFragment {
            val fragment = NotificationsFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    lateinit var notificationListAdapter: NotificationListAdapter

    var notificationList = ArrayList<Notifications>()

    var page = 1
    var lastPage = 1
    var isLoading = false

    private var disposable: Disposable? = null
    var observable: Observable<Response<NotificationListResponse>>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(mActivity , androidx.recyclerview.widget.LinearLayoutManager.VERTICAL , false)
        rv_notifications.layoutManager = linearLayoutManager
        notificationListAdapter = NotificationListAdapter()
        rv_notifications.adapter = notificationListAdapter
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        tv_title.text = mActivity.resources.getString(R.string.notifications)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (mActivity.isUserTypeCustomer) {

            swipe_refresh.setColorSchemeColors(mActivity.getPrimaryColor())
            swipe_refresh.setOnRefreshListener(this)

            rv_notifications.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView , newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (linearLayoutManager.findLastVisibleItemPosition() == notificationListAdapter.itemCount - 1 &&
                            page <= lastPage && !isLoading) {
                        callNotificationListApi()
                    }
                }
            })

            callNotificationListApi()
        }
    }

    override fun onRefresh() {
        page = 1
        callNotificationListApi()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            else -> {
            }
        }
    }

    inner class NotificationListAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

            val view: View
            val inflater = LayoutInflater.from(parent.context)

            view = if (viewType == LOAD_MORE) {
                inflater.inflate(R.layout.row_progress, parent, false)
            } else {
                inflater.inflate(R.layout.row_notifications, parent, false)
            }

            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return if (notificationList[position].id == 0L) {
                LOAD_MORE
            } else {
                OTHER
            }
        }

        override fun getItemCount(): Int {
            return notificationList.size
        }

        override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

            if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
                return
            } else {
                with(notificationList[position]) {
                    holder.itemView.tv_message.text = message
                    holder.itemView.tv_date.text = getTimePeriod(date.toLong())
                }
            }
        }

        inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }


    private fun callNotificationListApi() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)

            swipe_refresh.isRefreshing = false
            return
        }

        if (page == 1) {
            swipe_refresh.isRefreshing = true
        } else {
            if (notificationList.find { it.id == 0L } == null) {
                notificationList.add(Notifications())
                notificationListAdapter.notifyDataSetChanged()
            }
        }

        isLoading = true

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.PAGE] = "$page"
        hashMap[ApiParam.PER_PAGE] = ApiParam.PER_PAGE_VALUE
        hashMap[ApiParam.TYPE] = ApiParam.CUSTOMER //for the Customer side Notifications

        disposable?.let {
            if (!disposable?.isDisposed!!) {
                disposable?.dispose()
            }
        }

        observable?.let {
            observable = null
        }

        observable = WebApiClient.webApi().notificationListAPI(hashMap).subscribeOn(Schedulers.io())
        disposable = observable
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    with(body()!!) {
                                        if (meta != null) {
                                            with(meta!!) {
                                                when (status) {
                                                    ApiParam.META_SUCCESS -> {

                                                        if (page == 1) {
                                                            notificationList.clear()
                                                        }

                                                        if (data.isNotEmpty()) {
                                                            notificationList.addAll(data)
                                                        }

                                                        page++

                                                        this@NotificationsFragment.lastPage = lastPage

                                                    }
                                                    else -> {
                                                        mActivity.showDialog(message)
                                                    }
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (notificationList.isNotEmpty()) {
                                    notificationList.removeAll(notificationList.filter { it.id == 0L })
                                }
                            }

                            notificationListAdapter.notifyDataSetChanged()

                            tv_no_data_found.visibility = if (notificationList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                        },
                        {
                            if (disposable == null) return@subscribe

                            if (swipe_refresh.isRefreshing) {
                                swipe_refresh.isRefreshing = false
                            } else {
                                if (notificationList.isNotEmpty()) {
                                    notificationList.removeAll(notificationList.filter { it.id == 0L })
                                    notificationListAdapter.notifyDataSetChanged()
                                }
                            }

                            tv_no_data_found.visibility = if (notificationList.isEmpty()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }

                            isLoading = false
                            ErrorUtil.setExceptionMessage(it)
                        }
                )


    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }

}