package com.smarket.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.*
import com.smarket.utils.showDialog
import com.smarket.utils.showDialogWithBackNavigation
import com.smarket.R
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 11/4/18.
 */
class ChangePasswordFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): ChangePasswordFragment {
            val fragment = ChangePasswordFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var oldPassword = ""
    var newPassword = ""

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        btn_update_password.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        if (mActivity.isUserTypeCustomer) {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        }
        tv_title.text = mActivity.resources.getString(R.string.change_password)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_update_password -> {
                if (checkForValidations()) {
                    callChangePasswordAPI()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        oldPassword = et_old_password.text.toString()
        newPassword = et_new_password.text.toString()
        val confirmPassword = et_confirm_password.text.toString()

        if (oldPassword.isEmpty()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.old_password_req))
            return false
        } else if (newPassword.isEmpty()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.new_password_req))
            return false
        } else if (!newPassword.isValidPassword()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.valid_new_password_req))
            return false
        } else if (confirmPassword.isEmpty()) {
            mActivity.showDialog(mActivity.resources.getString(R.string.confirm_password_req))
            return false
        } else if (newPassword != confirmPassword) {
            mActivity.showDialog(mActivity.resources.getString(R.string.reset_passwords_mismatch))
            return false
        }

        return true
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            if (!mActivity.isUserTypeCustomer) {
                mActivity.setDrawerEnable(true)
            }
        }
        super.onHiddenChanged(hidden)
    }

    private fun callChangePasswordAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val hashMap = ApiParam.getHashMap()
        hashMap[ApiParam.ChangePassword.OLD_PASSWORD] = oldPassword
        hashMap[ApiParam.ChangePassword.NEW_PASSWORD] = newPassword

        disposable = WebApiClient.webApi().changePasswordAPI(hashMap).subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            if (disposable == null) return@subscribe

                            with(it) {
                                if (isSuccessful && body() != null) {
                                    kotlin.with(body()!!) {
                                        if (meta != null) {
                                            kotlin.with(meta!!) {
                                                if (status == ApiParam.META_SUCCESS) {

                                                    et_old_password.setText("")
                                                    et_new_password.setText("")
                                                    et_confirm_password.setText("")

                                                    if (mActivity.isUserTypeCustomer) {
                                                        mActivity.showDialogWithBackNavigation(mActivity, message)
                                                    } else {
                                                        mActivity.showDialog(message)
                                                    }

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                        } else {
                                            ErrorUtil.showError(it,mActivity)
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }

                            ProgressDialogUtil.hideProgressDialog()
                        },
                        {
                            if (disposable == null) return@subscribe

                            ProgressDialogUtil.hideProgressDialog()
                            ErrorUtil.setExceptionMessage(it)
                        }
                )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}