package com.smarket.main

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.SMarket
import com.smarket.database.CMS
import kotlinx.android.synthetic.main.fragment_cms.*
import kotlinx.android.synthetic.main.header.*

/**
 * Created by MI-062 on 11/4/18.
 */
class CMSFragment : BaseMainFragment() {

    companion object {

        val ABOUT_US = 1L
        val TERMS_CONDITIONS = 2L
        val PRIVACY_POLICY = 3L
        val HOW_TO_USE = 4L
        val CONTACT_US = 5L

        fun getInstance(whatToShow: Long): CMSFragment {
            val fragment = CMSFragment()
            val bundle = Bundle()
            bundle.putLong("whatToShow", whatToShow)
            fragment.arguments = bundle
            return fragment
        }
    }

    var whatToShow = 0L
    var cms_content = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("whatToShow")) {
                whatToShow = bundle.getLong("whatToShow")
            }
        }
    }

    private fun setHeader() {
        if (mActivity.isUserTypeCustomer) {
            mActivity.setDrawerEnable(false)
            iv_back.visibility = View.VISIBLE
            iv_back.setOnClickListener(this)
        } else {
            mActivity.setDrawerEnable(true)
            iv_drawer.visibility = View.VISIBLE
            iv_drawer.setOnClickListener(this)
        }
        tv_title.text = if (whatToShow == ABOUT_US) {
            mActivity.resources.getString(R.string.about_us)
        } else if (whatToShow == TERMS_CONDITIONS) {
            mActivity.resources.getString(R.string.terms_and_conditions)
        } else if (whatToShow == PRIVACY_POLICY) {
            mActivity.resources.getString(R.string.privacy_policy)
        } else {
            mActivity.resources.getString(R.string.how_to_use_this_app)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setCMSData()
    }

    private fun setCMSData() {

        val cmsData: CMS? = SMarket.appDB.cmsDao().getData(whatToShow)

        if (cmsData != null) {
            cms_content = cmsData.content
        }

        val text = "<html>" +
                "<style type='text/css'>" +
                "@font-face {font-family: MyFont;src: url('font/poppins_regular.ttf');}" +
                "body {color: #363737;font-family: MyFont;font-size: small;text-align: justify;padding: 8;}" +
                "</style>" +
                "<body>" +
                cms_content +
                "</body>" +
                "</html>"

        wv_cms.setBackgroundColor(Color.WHITE)
        wv_cms.loadDataWithBaseURL("file:///android_res/", text, "text/html", "UTF-8", null)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            if (!mActivity.isUserTypeCustomer) {
                mActivity.setDrawerEnable(true)
            }
        }
        super.onHiddenChanged(hidden)
    }
}