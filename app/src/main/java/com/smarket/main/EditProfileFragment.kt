package com.smarket.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.EditText
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlacePicker
import com.smarket.utils.*
import com.smarket.R
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.view_business_category.*
import kotlinx.android.synthetic.main.view_mobile_number.*
import java.io.File
import android.widget.ArrayAdapter
import com.smarket.SMarket
import com.smarket.webservices.ApiParam
import com.smarket.webservices.ErrorUtil
import com.smarket.webservices.WebApiClient
import com.smarket.webservices.response.LRFResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

/**
 * Created by MI-062 on 20/6/18.
 */
class EditProfileFragment : BaseMainFragment(), View.OnClickListener {

    companion object {
        fun getInstance(): EditProfileFragment {
            val fragment = EditProfileFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    var businessCategoriesList = ArrayList<String>()

    var picture: File? = null
    var needToUploadPicture = false
    var name = ""
    var businessName = ""
    var tagLine = ""
    var email = ""
    var countryCode = ""
    var mobile = ""
    var website = ""
    var businessCategory = ""
    var lattitude = 0.0
    var longitude = 0.0
    var postCode = ""
    var address = ""
    var description = ""
    var productAndServices = ""

    lateinit var editProfileObservable: Observable<Response<LRFResponse>>
    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundle()

        setHeader()

        if (mActivity.isUserTypeCustomer) {

            sdv_profile_pic.setBackgroundResource(R.drawable.upload_profile_pic_customer)
            et_business_name.visibility = View.GONE
            et_tag_line.visibility = View.GONE
            et_website.visibility = View.GONE
            cl_business_category.visibility = View.GONE
            et_address.visibility = View.GONE
            et_description.visibility = View.GONE
            et_product_and_services.visibility = View.GONE

            sp_country_code.isEnabled = false
            et_mobile.isEnabled = false
            et_mobile.imeOptions = EditorInfo.IME_ACTION_DONE

            divider.setBackgroundColor(mActivity.resources.getColor(R.color.dim_gray_txt))
            et_mobile.setTextColor(mActivity.resources.getColor(R.color.dim_gray_txt))

        } else {

            sdv_profile_pic.setBackgroundResource(R.drawable.upload_profile_pic_merchant)
            et_name.visibility = View.GONE

            businessCategoriesList = SMarket.appDB.businessCategoryDao().getNames() as ArrayList<String>

            val businessCategoryAdapter = ArrayAdapter<String>(mActivity
                    , R.layout.row_spinner_business_category
                    , businessCategoriesList)
            businessCategoryAdapter.setDropDownViewResource(R.layout.row_dropdown_business_category)
            sp_business_category.adapter = businessCategoryAdapter

            sp_business_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    businessCategory = businessCategoriesList[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }

            et_tag_line.nextFocusDownId = R.id.et_mobile
            et_email.isEnabled = false
            et_email.setTextColor(mActivity.resources.getColor(R.color.dim_gray_txt))
        }

        mActivity.setCountryCodeSpinner(sp_country_code, mActivity.isUserTypeCustomer)

        sp_country_code.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                countryCode = mActivity.countryCodeList[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        sdv_profile_pic.setOnClickListener(this)
        tv_change_profile_pic.setOnClickListener(this)
        et_address.setOnClickListener(this)
        btn_update.setOnClickListener(this)
    }

    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey("")) {

            }
        }
    }

    private fun setHeader() {
        mActivity.setDrawerEnable(true)
        iv_drawer.visibility = View.VISIBLE
        iv_drawer.setOnClickListener(this)
        tv_title.setText(R.string.edit_profile)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setUserDetails()
    }

    private fun setUserDetails() {

        if (mActivity.userDetails != null) {
            with(mActivity.userDetails!!) {
                if (email.isRequiredField()) {
                    et_email.setText(email)
                }
                if (mobile.isRequiredField()) {
                    mActivity.setSelectedCountyCode(sp_country_code, countryCode)
                    et_mobile.setText(mobile)
                }
                if (mActivity.isUserTypeCustomer) {
                    sdv_profile_pic.loadFrescoImage(profilePic)
                    et_name.setText(name)
                } else {
                    picture = File(businessLogo)
                    sdv_profile_pic.loadFrescoImage(businessLogo)
                    et_business_name.setText(businessName)
                    et_tag_line.setText(tagLine)
                    et_website.setText(website)
                    sp_business_category.setSelection(businessCategoriesList.indexOf(SMarket.appDB.businessCategoryDao().getNameById(businessCategoryId)))
                    et_address.setText(address)
                    this@EditProfileFragment.lattitude = latitude
                    this@EditProfileFragment.longitude = longitude
                    this@EditProfileFragment.postCode = postCode
                    et_description.setText(description)
                    et_product_and_services.setText(productAndServices)
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.sdv_profile_pic,
            R.id.tv_change_profile_pic -> {
                val intent = Intent(mActivity, ImageChooserActivity::class.java)
                startActivityForResult(intent, ImageChooserActivity.REQUEST_CODE)
            }
            R.id.et_address -> {
                avoidDoubleClicks(et_address)
                val builder = PlacePicker.IntentBuilder()
                try {
                    startActivityForResult(builder.build(mActivity), REQUEST_CODE_GOOGLE_PLACE_API)
                } catch (e: GooglePlayServicesRepairableException) {
                    e.printStackTrace()
                } catch (e: GooglePlayServicesNotAvailableException) {
                    e.printStackTrace()
                }
            }
            R.id.btn_update -> {
                if (checkForValidations()) {
                    callEditProfileAPI()
                }
            }
            else -> {
            }
        }
    }

    private fun checkForValidations(): Boolean {

        mActivity.hideKeyboard()

        email = et_email.text.toString()
        mobile = et_mobile.text.toString()

        if (mActivity.isUserTypeCustomer) {

            name = et_name.text.toString()

            if (!name.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.name_req))
                return false
            } else if (email.isRequiredField() && !email.isValidEmail()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_email_req))
                return false
            } else if (!mobile.isRequiredField()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.mobile_req))
                return false
            } else if (!mobile.isValidMobile()) {
                mActivity.showDialog(mActivity.resources.getString(R.string.valid_mobile_req))
                return false
            }

        } else {

            businessName = et_business_name.text.toString()
            tagLine = et_tag_line.text.toString()
            website = et_website.text.toString()
            address = et_address.text.toString()
            description = et_description.text.toString()
            productAndServices = et_product_and_services.text.toString()

            if (needToUploadPicture && picture == null) {
                mActivity.showDialog(resources.getString(R.string.business_logo_req))
                return false
            } else if (!businessName.isRequiredField()) {
                mActivity.showDialog(resources.getString(R.string.business_name_req))
                return false
            } else if (!email.isRequiredField()) {
                mActivity.showDialog(resources.getString(R.string.email_req))
                return false
            } else if (!email.isValidEmail()) {
                mActivity.showDialog(resources.getString(R.string.valid_email_req))
                return false
            } else if (mobile.isRequiredField() && !mobile.isValidMobile()) {
                mActivity.showDialog(resources.getString(R.string.valid_mobile_req))
                return false
            } else if (!businessCategory.isRequiredField()) {
                mActivity.showDialog(resources.getString(R.string.business_category_req))
                return false
            } else if (!address.isRequiredField()) {
                mActivity.showDialog(resources.getString(R.string.address_req))
                return false
            } else if (!productAndServices.isRequiredField()) {
                mActivity.showDialog(resources.getString(R.string.product_and_services_req))
                return false
            }
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                ImageChooserActivity.REQUEST_CODE -> {
                    val picturePath = data.getStringExtra(ImageChooserActivity.KEY_OF_URI)
                    picture = File(picturePath)
                    sdv_profile_pic.loadFrescoImageFromFile(picture)
                    needToUploadPicture = true
                }
                REQUEST_CODE_GOOGLE_PLACE_API -> {
                    val place = PlacePicker.getPlace(mActivity, data)
                    val latLng = place.latLng
                    lattitude = latLng.latitude
                    longitude = latLng.longitude
                    showAddressDialog(place.address!!.toString())
                }
                else -> {
                }
            }
        }
    }

    private fun showAddressDialog(address: String) {
        val dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_address, null)
        val et_selected_address = dialogView.findViewById<EditText>(R.id.et_selected_address)
        et_selected_address.setText(address)
        val alertDialog = AlertDialog.Builder(mActivity).create()
        alertDialog.setView(dialogView)
        alertDialog.setCancelable(false)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, resources.getString(R.string.ok)) { dialogInterface, i ->
            et_address.setText(et_selected_address.text.toString())
            dialogInterface.dismiss()
        }
        alertDialog.show()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (isAdded && !hidden) {
            mActivity.setDrawerEnable(true)
        }
        super.onHiddenChanged(hidden)
    }

    private fun callEditProfileAPI() {

        if (!NetworkUtil.isNetworkAvailable()) {
            showToast(ErrorUtil.NO_INTERNET)
            return
        }

        ProgressDialogUtil.showProgressDialog(mActivity)

        val tag: String
        val hashMap = ApiParam.getHashMap()

        if (email.isRequiredField()) {
            hashMap[ApiParam.EMAIL] = email
        }

        if (mobile.isRequiredField()) {
            hashMap[ApiParam.COUNTRY_CODE] = countryCode
            hashMap[ApiParam.MOBILE] = mobile
        }

        if (mActivity.isUserTypeCustomer) {
            tag = ApiParam.Customer.TAG_EDIT_PROFILE
            hashMap[ApiParam.NAME] = name
        } else {
            tag = ApiParam.Merchant.TAG_EDIT_PROFILE
            hashMap[ApiParam.BUSINESS_NAME] = businessName
            if (tagLine.isRequiredField()) {
                hashMap[ApiParam.TAG_LINE] = tagLine
            }
            hashMap[ApiParam.WEBSITE] = website
            hashMap[ApiParam.BUSINESS_CATEGORY_ID] = "${SMarket.appDB.businessCategoryDao().getIdByName(businessCategory)}"
            hashMap[ApiParam.ADDRESS] = address
            hashMap[ApiParam.LATITUDE] = "$lattitude"
            hashMap[ApiParam.LONGITUDE] = "$longitude"
            hashMap[ApiParam.POST_CODE] = postCode
            hashMap[ApiParam.DESCRIPTION] = description
            hashMap[ApiParam.PRODUCT_AND_SERVICES] = productAndServices
        }

        editProfileObservable = if (needToUploadPicture) {
            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), picture!!)
            val body: MultipartBody.Part = MultipartBody.Part.createFormData(if (mActivity.isUserTypeCustomer) {
                ApiParam.PROFILE_PIC
            } else {
                ApiParam.BUSINESS_LOGO
            }, picture!!.name, requestFile)
            WebApiClient.webApi().editProfileAPI(tag, hashMap, body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        } else {
            WebApiClient.webApi().editProfileAPI(tag, hashMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        }

        disposable = editProfileObservable.subscribe(
                {
                    if (disposable == null) return@subscribe

                    with(it) {
                        if (isSuccessful && body() != null) {
                            kotlin.with(body()!!) {
                                if (meta != null) {
                                    kotlin.with(meta!!) {
                                        when (status) {
                                            ApiParam.META_SUCCESS -> {
                                                if (data != null) {

                                                    val userDetails = data!!
                                                    userDetails.token = mActivity.userToken

                                                    SMarket.appDB.userDao().updateData(userDetails)

                                                    mActivity.updateUserDetailsObject()

                                                    mActivity.setUserDetails()

                                                    mActivity.showDialog(message)

                                                } else {
                                                    mActivity.showDialog(message)
                                                }
                                            }
                                            else -> {
                                                mActivity.showDialog(message)
                                            }
                                        }
                                    }
                                } else {
                                    ErrorUtil.showError(it,mActivity)
                                }
                            }
                        } else {
                            ErrorUtil.showError(it,mActivity)
                        }
                    }

                    ProgressDialogUtil.hideProgressDialog()
                },
                {
                    if (disposable == null) return@subscribe

                    ProgressDialogUtil.hideProgressDialog()
                    ErrorUtil.setExceptionMessage(it)
                }
        )
    }

    override fun onDestroyView() {
        disposable = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }
}