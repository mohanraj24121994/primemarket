package com.smarket.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.smarket.R
import com.smarket.model.giftcard.Datum
import com.smarket.model.refcashredeemrequest.GiftCard
import com.smarket.utils.OnEditTextChanged
import kotlinx.android.synthetic.main.fragment_refferal.*
import kotlinx.android.synthetic.main.row_gift_card.view.*


class GiftAdapter(var giftList: ArrayList<Datum> , var activity: Activity , var countryCode: String
                  , var onEditTextChanged: OnEditTextChanged) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    var totalRefcashAmount: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        view = inflater.inflate(R.layout.row_gift_card , parent , false)
        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return giftList.size
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

        if (countryCode != "+91") {
            holder.itemView.tvAmount.text = "$" + giftList[position].amount.toString()
        } else {
            holder.itemView.tvAmount.text = "₹" + giftList[position].amount.toString()
        }

        holder.itemView.tvValidity.text = "Validity " + giftList[position].expiryDate

        Glide.with(activity).load(giftList[position].logo).into(holder.itemView.ivLogo)

        if (countryCode != "+91") {
            holder.itemView.txtTotal.text = "$" + "0"
        } else {
            holder.itemView.txtTotal.text = "₹" + "0"
        }

        onEditTextChanged.onTextChanged(position ,totalRefcashAmount , holder.itemView.tvCount.text.toString())

        holder.itemView.iv_plus.setOnClickListener {

            val dimensionList: ArrayList<GiftCard>? = ArrayList()
            val addValues = GiftCard()
            addValues.gift_quantity = holder.itemView.tvCount.text.toString()
            dimensionList !!.add(addValues)

            holder.itemView.tvCount.text = (holder.itemView.tvCount.text.toString().toInt() + 1).toString()
            val getCount = holder.itemView.txtTotal.text.toString().replace("₹" , "").replace("$" , "")
            if (countryCode != "+91") {
                holder.itemView.txtTotal.text = "$" + (getCount.toInt() + giftList[position].amount !!).toString()
            } else {
                holder.itemView.txtTotal.text = "₹" + (getCount.toInt() + giftList[position].amount !!).toString()
            }

            totalRefcashAmount += holder.itemView.tvAmount.text.toString().replace("₹" , "").replace("$" , "").toInt()

            onEditTextChanged.onTextChanged(position ,totalRefcashAmount, holder.itemView.tvCount.text.toString())
        }

        holder.itemView.iv_minus.setOnClickListener {

            val dimensionList: ArrayList<GiftCard>? = ArrayList()
            val addValues = GiftCard()
            addValues.gift_quantity = holder.itemView.tvCount.text.toString()
            dimensionList !!.add(addValues)


            if (holder.itemView.tvCount.text.toString().toInt() > 0) {

                holder.itemView.tvCount.text = (holder.itemView.tvCount.text.toString().toInt() - 1).toString()

                val getCount = holder.itemView.txtTotal.text.toString().replace("₹" , "").replace("$" , "")

                if (countryCode != "+91") {
                    holder.itemView.txtTotal.text = "$" + (getCount.toInt() - giftList[position].amount !!).toString()
                } else {
                    holder.itemView.txtTotal.text = "₹" + (getCount.toInt() - giftList[position].amount !!).toString()
                }

            } else {
                if (countryCode != "+91") {
                    holder.itemView.txtTotal.text = "$" + "0"
                } else {
                    holder.itemView.txtTotal.text = "₹" + "0"
                }
            }

            totalRefcashAmount -= holder.itemView.tvAmount.text.toString().replace("₹" , "").replace("$" , "").toInt()

            onEditTextChanged.onTextChanged(position , totalRefcashAmount, holder.itemView.tvCount.text.toString())
        }
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
}