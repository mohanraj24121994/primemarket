package com.smarket.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.smarket.R
import com.smarket.model.referrallist.ProductCompareList
import kotlinx.android.synthetic.main.product_compare_list.view.*


class ProductComparisonAdapter(private var mProductCompareList: ArrayList<ProductCompareList> , var mActivity: Activity ,
                               private val listener: (List<ProductCompareList> , Int , View) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): RecyclerView.ViewHolder {
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        view = inflater.inflate(R.layout.product_compare_list , parent , false)
        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return mProductCompareList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder , position: Int) {
        holder.itemView.product_name.text = mProductCompareList[position].mProductName
        holder.itemView.product_name.setShowingLine(3)
        holder.itemView.product_name.addShowMoreText("more")
        holder.itemView.product_name.addShowLessText("Less")
        holder.itemView.product_name.setShowMoreColor(ContextCompat.getColor(mActivity , R.color.customer_app_color))
        holder.itemView.product_name.setShowLessTextColor(ContextCompat.getColor(mActivity , R.color.customer_app_color))

        holder.itemView.price_txt.text = mProductCompareList[position].mPrice
        holder.itemView.earn_txt.text = mProductCompareList[position].mEarn

        Glide.with(holder.itemView.context).load(mProductCompareList[position].mImage).into(holder.itemView.ivProductImg)
        Glide.with(holder.itemView.context).load(mProductCompareList[position].logo).into(holder.itemView.logo)

        holder.itemView.cvProductRoot.setOnClickListener {
            listener(mProductCompareList , position , it)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}