package com.smarket.adapter

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.R
import com.smarket.webservices.response.refferedlist.Refered
import kotlinx.android.synthetic.main.refer_friends_list.view.*


class ReferFriendsAdapter(var banners: ArrayList<Refered>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        view = inflater.inflate(R.layout.refer_friends_list , parent , false)
        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return banners.size
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {
        holder.itemView.refer_name.text = banners[position].getName()
        holder.itemView.refer_phone.text = banners[position].getMobileNumber()

        val text = "<font size=8>earns you</font>" + " <font color=#FF001A>" + banners[position].refer_percentage + "%" + "</font>"
        holder.itemView.tv_reward.text = Html.fromHtml(text)
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
}