package com.smarket.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.smarket.R
import com.smarket.utils.loadFrescoImage
import kotlinx.android.synthetic.main.row_cart_product.view.*


class CartProductAdapter(private var offerProduct1: List<CartItemModel>, private var activity: Activity) : RecyclerView.Adapter<CartProductAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_cart_product, parent, false)
        return ViewHolder(v, activity)
    }

    override fun getItemCount(): Int {
        return offerProduct1.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(offerProduct1, position)




    }

    inner class ViewHolder(itemView: View, private var mContext: Activity) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(offerProduct1: List<CartItemModel>, position: Int) {


            itemView.tvcartName.text = offerProduct1[position].productName
            itemView.tvcartName.setShowingLine(3)
            itemView.tvcartName.addShowMoreText("more")
            itemView.tvcartName.addShowLessText("Less")
            itemView.tvcartName.setShowMoreColor(ContextCompat.getColor(mContext, R.color.customer_app_color))
            itemView.tvcartName.setShowLessTextColor(ContextCompat.getColor(mContext, R.color.customer_app_color))
            itemView.ivProduct.loadFrescoImage(offerProduct1[position].productImage)
            itemView.tvPrice.text = offerProduct1[position].productPrice
            itemView.tvcartcout.text = offerProduct1[position].count




        }

    }
    //override fun getItemId(position: Int) = position.toLong()
    //override fun getItemViewType(position: Int) = position
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


}