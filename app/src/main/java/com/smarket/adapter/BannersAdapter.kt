package com.smarket.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarket.utils.loadFrescoImage
import com.smarket.R
import com.smarket.model.Banner
import com.smarket.utils.LOAD_MORE
import com.smarket.utils.OTHER
import kotlinx.android.synthetic.main.row_banners.view.*

/**
 * Created by MI-062 on 17/8/18.
 */
class BannersAdapter(var banners: ArrayList<Banner>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

        val view: View
        val inflater = LayoutInflater.from(parent.context)

        view = if (viewType == LOAD_MORE) {
            inflater.inflate(R.layout.row_progress, parent, false)
        } else {
            inflater.inflate(R.layout.row_banners, parent, false)
        }

        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return if (banners[position].id == 0L) {
            LOAD_MORE
        } else {
            OTHER
        }
    }

    override fun getItemCount(): Int {
        return banners.size
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder , position: Int) {

        if (getItemViewType(holder.adapterPosition) == LOAD_MORE) {
            return
        } else {
            holder.itemView.sdv_banner.loadFrescoImage(banners[holder.adapterPosition].banner)
        }
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
}