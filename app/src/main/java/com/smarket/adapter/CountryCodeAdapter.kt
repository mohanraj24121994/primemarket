package com.smarket.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.smarket.R
import com.smarket.model.Country
import com.smarket.model.CountryCode

/**
 * Created by MI-062 on 11/7/18.
 */
class CountryCodeAdapter(var mActivity: AppCompatActivity, var layoutResourceId: Int
                         , var countryCodeList: List<String>, var isForEdit: Boolean = false) : ArrayAdapter<Country>(mActivity, layoutResourceId) {

    private val dropDownResourceId = R.layout.row_dropdown_country_code

    override fun getCount(): Int {
        return countryCodeList.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {

        return getCustomView(position, convertView, parent, true)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        return getCustomView(position, convertView, parent, false)
    }

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup, isDropDownView: Boolean): View {

        var view = convertView

        val countryCode = countryCodeList[position]

        var viewHolder: ViewHolder? = null

        if (viewHolder == null) {
            val inflater = mActivity.layoutInflater
            if (isDropDownView) {
                view = inflater.inflate(layoutResourceId, parent, false)
                viewHolder = ViewHolder(view)
                viewHolder.tv_code = view!!.findViewById(R.id.tv_code) as TextView
                viewHolder.tv_code!!.text = countryCode
            } else {
                view = inflater.inflate(dropDownResourceId, parent, false)
                viewHolder = ViewHolder(view)
                viewHolder.tv_name = view!!.findViewById(R.id.tv_code) as TextView
                viewHolder.tv_name!!.text = countryCode
                if (isForEdit) {
                    viewHolder.tv_name!!.setTextColor(mActivity.resources.getColor(R.color.dim_gray_txt))
                }
            }
            view.tag = viewHolder
        }

        return view!!
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        internal var tv_name: TextView? = null
        internal var tv_code: TextView? = null
    }
}