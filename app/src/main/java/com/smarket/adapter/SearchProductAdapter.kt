package com.smarket.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.smarket.R
import com.smarket.model.searchproduct.OfferProduct
import com.smarket.utils.OnCartTextChanged
import com.smarket.utils.Singleton
import com.smarket.utils.loadFrescoImage
import com.smarket.utils.showToast
import kotlinx.android.synthetic.main.product_search_list.view.*
import kotlinx.android.synthetic.main.product_search_list.view.ivProduct


class SearchProductAdapter(private var offerProduct: ArrayList<OfferProduct>, private var activity: Activity, private var countryCode: String, var onCartTextChanged: OnCartTextChanged, private val listener: (List<OfferProduct>, Int, View) -> Unit) : RecyclerView.Adapter<SearchProductAdapter.ViewHolder>() {
    var offer = CartItemModel()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.product_search_list, parent, false)
        return ViewHolder(v, activity)
    }

    override fun getItemCount(): Int {
        return offerProduct.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(offerProduct, listener, position)
    }

    inner class ViewHolder(itemView: View, private var mContext: Activity) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(offerProduct: List<OfferProduct>, listener: (List<OfferProduct>, Int, View) -> Unit, position: Int) {

            itemView.tvName.text = offerProduct[position].productName
            itemView.tvName.setShowingLine(3)
            itemView.tvName.addShowMoreText("more")
            itemView.tvName.addShowLessText("Less")
            itemView.tvName.setShowMoreColor(ContextCompat.getColor(mContext, R.color.customer_app_color))
            itemView.tvName.setShowLessTextColor(ContextCompat.getColor(mContext, R.color.customer_app_color))
            if (offerProduct[position].storeName.equals("Amazon.in", true) || offerProduct[position].storeName.equals("Amazon.com", true)) {
                offerProduct[position].earnValue = itemView.tvEarn.text.toString()
            } else {
                offerProduct[position].earnValue = "0"
            }

            if (countryCode != "+91") {
                itemView.tvPrice.text = "$".plus(offerProduct[position].productPrice)
            } else {
                itemView.tvPrice.text = "₹".plus(offerProduct[position].productPrice)
            }

            if (offerProduct[position].storeName.equals("Amazon.in", true) || offerProduct[position].storeName.equals("Amazon.com", true)) {
                val firstPercentage = percentageCalculator(offerProduct[position].productPrice!!.toDouble())
                val finalPercentage = percentageCalculatorAdmin(firstPercentage)

                offerProduct[position].earnValue = finalPercentage.toString()
            } else {
                offerProduct[position].earnValue = "0"
            }

            if (countryCode == "+91") {
                itemView.tvEarn.text = "₹".plus(String.format("%.1f", offerProduct[position].earnValue!!.toDouble()))
            } else {
                itemView.tvEarn.text = "$".plus(String.format("%.1f", offerProduct[position].earnValue!!.toDouble()))
            }

            itemView.ivProduct.loadFrescoImage(offerProduct[position].productImage!!)

            if (offerProduct[position].storeName.equals("Amazon.in", true) || offerProduct[position].storeName.equals("Amazon.com", true)) {
                Glide.with(mContext).load(ContextCompat.getDrawable(mContext, R.drawable.amazon)).into(itemView.ivLogo)
            } else if (offerProduct[position].storeName.equals("Flipkart", true)) {
                Glide.with(mContext).load(ContextCompat.getDrawable(mContext, R.drawable.flip)).into(itemView.ivLogo)
            } else if (offerProduct[position].storeName.equals("Ebay", true)) {
                Glide.with(mContext).load(ContextCompat.getDrawable(mContext, R.drawable.ebay)).into(itemView.ivLogo)
            } else if (offerProduct[position].storeName.equals("Smarket.in", true)) {
                Glide.with(mContext).load(ContextCompat.getDrawable(mContext, R.drawable.smarket_logo)).into(itemView.ivLogo)
                itemView.tvCount.visibility = View.VISIBLE
                itemView.iv_minus.visibility = View.VISIBLE
                itemView.iv_plus.visibility = View.VISIBLE
            } else {
                itemView.ivLogo.visibility = View.GONE
            }

            itemView.cvProductSearch.setOnClickListener {
                listener(offerProduct, position, it)
            }
            itemView.iv_plus.setOnClickListener {
                itemView.tvCount.text = (itemView.tvCount.text.toString().toInt() + 1).toString()
                offer.productImage = offerProduct[position].productImage.toString()
                offer.productPrice = offerProduct[position].productPrice.toString()
                offer.productName = offerProduct[position].productName.toString()
                offer.count = itemView.tvCount.text.toString()
                if (Singleton.addOfferProduct.size == 0) {
                    Singleton.addOfferProduct.add(offer)
                } else
                    Singleton.addOfferProduct.clear()
                showToast("Added item to cart")
                onCartTextChanged.onTextChanged(position, itemView.tvCount.text.toString())
            }

            itemView.iv_minus.setOnClickListener {
                if (itemView.tvCount.text.toString().toInt() > 0) {
                    itemView.tvCount.text = (itemView.tvCount.text.toString().toInt() - 1).toString()
                    showToast("Removed item to cart")
                }
                onCartTextChanged.onTextChanged(position, itemView.tvCount.text.toString())
            }
        }
    }

    private fun percentageCalculator(price: Double): Double {
        val amount: Double = price
        return amount / 100.0f * 10
    }

    private fun percentageCalculatorAdmin(price: Double): Double {
        val amount: Double = price
        return amount / 100.0f * 70
    }


}


