package com.smarket.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.smarket.utils.showToast

/**
 * Created by MI-062 on 4/9/18.
 */
object mGpsSwitchStateReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent!!.action!!.matches("android.location.PROVIDERS_CHANGED".toRegex())) {
            showToast("changes in gps status")
            println(" = ${intent!!.action}")
        }
    }
}