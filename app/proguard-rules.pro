# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#Fresco Proguard
#-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
#-keep @com.facebook.common.internal.DoNotStrip class *
#-keepclassmembers class * {
#    @com.facebook.common.internal.DoNotStrip *;
#}

# Keep native methods
-keepclassmembers class * {
    native <methods>;
}

#-dontwarn com.android.volley.toolbox.**
#-dontwarn com.facebook.infer.**

# Retrofit
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
-dontwarn com.squareup.okhttp.**
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn org.apache.commons.**
-dontwarn org.apache.http.**

#Parcelable
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

#This is put to get line number for error as all names are obfuscated
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

#Retain class variable names and inner class members
-keepattributes InnerClasses
-keepclassmembers class * { *; }

-keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

#-keepnames class com.facebook.FacebookActivity
#-keepnames class com.facebook.CustomTabActivity
#
#-keep class com.facebook.all.All
#
#-keep public class com.android.vending.billing.IInAppBillingService {
#    public static com.android.vending.billing.IInAppBillingService asInterface(android.os.IBinder);
#    public android.os.Bundle getSkuDetails(int, java.lang.String, java.lang.String, android.os.Bundle);
#}